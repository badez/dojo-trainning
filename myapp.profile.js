var profile = (function() {
    return {
        basePath: "./src",
        releaseDir: "../release",
        releaseName: "",
        action: "release",
        mini: true,
        stripConsole: "warn",
        selectorEngine: "lite",
        defaultConfig: {
            async: 1
        },
        dojoConfig: "./assets/js/config.js",
        staticHasFeatures: {
            'dojo-trace-api': 0,
            'dojo-log-api': 0,
            'dojo-publish-privates': 0,
            'dojo-test-sniff': 0
        },
        packages: [{
            name: "dojo",
            location: "dojo"
        }, {
            name: "dijit",
            location: "dijit"
        }, {
            name: "dojox",
            location: "dojox"
        }, {
            name: "app",
            location: "../release/app"
        }],
        layers: {
            "dojo/dojo": {
                include: [
                    "dojo/dojo",
                    "dojo/selector/acme",
                    "dojo/selector/lite",
                    "dojo/_firebug/firebug",
                    "app/main",
                    "app/topic-center",
                ],
                customBase: true,
                boot: true
            },
            "app/keypairs/common/commonKeypairs": {
                include: [
                    "app/keypairs/common/commonKeypairs"
                ]
            },
            "app/keypairs/test/extraPageKeypairs": {
                include: [
                    "app/keypairs/test/extraPageKeypairs"
                ]
            },
            "app/keypairs/test/sampleModalKeypairs": {
                include: [
                    "app/keypairs/test/sampleModalKeypairs"
                ]
            },
            "app/keypairs/test/sampleJumpKeypairs": {
                include: [
                    "app/keypairs/test/sampleJumpKeypairs"
                ]
            },
            "app/keypairs/test/samplePageKeypairs": {
                include: [
                    "app/keypairs/test/samplePageKeypairs"
                ]
            }
        }
    };
})();
