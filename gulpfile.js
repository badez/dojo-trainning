'use strict';
var gulp = require('gulp');
var concat = require('gulp-concat');
var cleanCSS = require('gulp-clean-css');
var uglify = require('gulp-uglify');
var clean = require('del');
var htmlmin = require('gulp-htmlmin');
var pump = require('pump');
var spawn = require('child_process').spawn;
var sass = require('gulp-sass');
var replace = require('gulp-regex-replace');
var randomstring = require('randomstring');
var str2hex = require('gulp-str2hex');

gulp.task('compile-sass', function() {
    return gulp.src('./src/assets/scss/base.scss')
        .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
        .pipe(gulp.dest('./src/assets/css'));

});

gulp.task('install-clean', function() {
    return clean([
        './src/dojo',
        './src/dijit',
        './src/dojox',
        './src/util',
        './src/assets/img/green*.png',
        './src/assets/css/*',
        './src/assets/fonts/*',
        '!./src/assets/fonts/icomoon*',
        './src/assets/js/*',
        '!./src/assets/js/config.js',
        '!./src/assets/js/ga.js'
    ]);
});

gulp.task('sass', ['install-clean'], function() {
    return gulp.src('./src/assets/scss/base.scss')
        .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
        .pipe(gulp.dest('./src/assets/css'));

});

gulp.task('concat-css', ['install-clean'], function() {
    return gulp.src([
            './node_modules/animate.css/animate.css',
            './node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.css',
            './node_modules/datatables.net-bs/css/dataTables.bootstrap.css',
            './node_modules/datatables.net-responsive-bs/css/responsive.bootstrap.css',
            './node_modules/icheck/skins/square/green.css',
            './node_modules/ion-rangeslider/css/ion.rangeSlider.css',
            './node_modules/ion-rangeslider/css/ion.rangeSlider.skinFlat.css',
            './node_modules/gridstack/dist/gridstack.css',
            './node_modules/owl.carousel/dist/assets/owl.carousel.css',
            './node_modules/owl.carousel/dist/assets//owl.theme.default.css',
            './node_modules/switchery/dist/switchery.css'
        ])
        .pipe(concat('all.css'))
        .pipe(cleanCSS({ compatibility: 'ie8' }))
        .pipe(gulp.dest('./src/assets/css/'));
});

gulp.task('copy-css', ['install-clean'], function() {
    return gulp.src([
            './node_modules/bootstrap/dist/css/bootstrap.css',
            './node_modules/bootstrap/dist/css/bootstrap.css.map',
            './node_modules/font-awesome/css/font-awesome.css',
            './node_modules/font-awesome/css/font-awesome.css.map'
        ])
        .pipe(gulp.dest('./src/assets/css/'));
});

gulp.task('copy-dojo', ['install-clean'], function() {
    return gulp.src([
            './node_modules/dojo/**/*',
            './node_modules/dijit/**/*',
            './node_modules/dojox/**/*'
        ], {
            "base": "node_modules"
        })
        .pipe(gulp.dest('./src'));
});

gulp.task('copy-dojo-util', ['install-clean'], function() {
    return gulp.src([
            './node_modules/dojo-util/**/*',
        ])
        .pipe(gulp.dest('./src/util'));
});

gulp.task('copy-js', ['install-clean'], function() {
    return gulp.src([])
        .pipe(uglify())
        .pipe(gulp.dest('./src/assets/js/'));
});

gulp.task('concat-js', ['install-clean'], function() {
    return gulp.src([
            './node_modules/jquery/dist/jquery.js',
            './node_modules/jquery-mask-plugin/dist/jquery.mask.js',
            './node_modules/jquery-placeholder/jquery.placeholder.js',
            './node_modules/jquery-slimscroll/jquery.slimscroll.js',
            './node_modules/jquery-ui-dist/jquery-ui.js',
            './node_modules/bootstrap/dist/js/bootstrap.js',
            './node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.js',
            './node_modules/autonumeric/autoNumeric.js',
            './node_modules/chart.js/dist/Chart.js',
            './node_modules/datatables.net/js/jquery.dataTables.js',
            './node_modules/datatables.net-bs/js/dataTables.bootstrap.js',
            './node_modules/datatables.net-responsive/js/dataTables.responsive.js',
            './node_modules/datatables.net-responsive-bs/js/responsive.bootstrap.js',
            './node_modules/dropzone/dist/dropzone.js',
            './node_modules/icheck/icheck.js',
            './node_modules/ion-rangeslider/js/ion.rangeSlider.js',
            './node_modules/jsencrypt/bin/jsencrypt.js',
            './node_modules/jstorage/jstorage.js',
            './node_modules/numeral/min/numeral.min.js',
            './node_modules/underscore/underscore.js',
            './node_modules/gridstack/dist/gridstack.min.js',
            './node_modules/metismenu/dist/metisMenu.js',
            './node_modules/owl.carousel/dist/owl.carousel.js',
            './node_modules/pace-progress/pace.js',
            './node_modules/pdfjs-dist/build/pdf.js',
            './node_modules/pdfjs-dist/web/compatibility.js'
        ])
        .pipe(concat('all.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./src/assets/js/'));
});

gulp.task('copy-fonts', ['install-clean'], function() {
    return gulp.src([
            './node_modules/bootstrap/fonts/*',
            './node_modules/font-awesome/fonts/*',
            './node_modules/open-sans-fontface/fonts/**/*',
            '!./node_modules/open-sans-fontface/fonts/BoldItalic',
            '!./node_modules/open-sans-fontface/fonts/BoldItalic/*',
            '!./node_modules/open-sans-fontface/fonts/ExtraBold',
            '!./node_modules/open-sans-fontface/fonts/ExtraBold/*',
            '!./node_modules/open-sans-fontface/fonts/ExtraBoldItalic',
            '!./node_modules/open-sans-fontface/fonts/ExtraBoldItalic/*',
            '!./node_modules/open-sans-fontface/fonts/Italic',
            '!./node_modules/open-sans-fontface/fonts/Italic/*',
            '!./node_modules/open-sans-fontface/fonts/LightItalic',
            '!./node_modules/open-sans-fontface/fonts/LightItalic/*',
            '!./node_modules/open-sans-fontface/fonts/SemiboldItalic',
            '!./node_modules/open-sans-fontface/fonts/SemiboldItalic/*'
        ])
        .pipe(gulp.dest('./src/assets/fonts/'));
});

gulp.task('copy-images', ['install-clean'], function() {
    return gulp.src([
            './node_modules/icheck/skins/square/green*.png'
        ])
        .pipe(gulp.dest('./src/assets/img/'));
});

gulp.task('copy-owl-assets', ['install-clean'], function() {
    return gulp.src([
            './node_modules/owl.carousel/dist/assets/ajax-loader.gif'
        ])
        .pipe(gulp.dest('./src/assets/css/'));
});

gulp.task('release-clean', function() {
    return clean([
        './release'
    ]);
});

gulp.task('release-copy', ['release-clean'], function() {
    return gulp.src([
            './src/assets/**/*',
            '!./src/assets/js/all.js',
            '!./src/assets/js/config.js',
            '!./src/assets/scss',
            '!./src/assets/scss/*.scss',
            '!./src/assets/scss/*.scssc'
        ], {
            "base": "src"
        })
        .pipe(gulp.dest('./release/'));
});

gulp.task('release-uglify', ['release-clean'], function() {
    return gulp.src([
            './src/assets/js/*.js'
        ])
        .pipe(uglify({
            mangle: {
                toplevel: true,
                eval: true
            },
            compress: {
                properties: false
            },
            output: {
                quote_keys: true,
                ascii_only: true
            }
        }))
        .pipe(gulp.dest('./release/assets/js/'));
});

gulp.task('release-htmlmin', ['release-clean'], function() {
    return gulp.src('./src/index.html')
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest('./release/'));
});

gulp.task('release-copy-app', ['release-clean'], function(f) {
    pump([
            gulp.src([
                'src/app/**/*.html'
            ], {
                "base": "src"
            }),
            htmlmin(htmlmin({ collapseWhitespace: true })),
            gulp.dest("./release")
        ],
        f
    );
});

gulp.task('release-app-uglify', ['release-clean'], function(f) {
    var cache = {};
    var stringCache = {};
    pump([
            gulp.src([
                'src/app/**/*.js'
            ], {
                "base": "src"
            }),
            // Uncomment for obfuscation
            uglify({
                mangle: false,
                compress: {
                    properties: false
                },
                output: {
                    quote_keys: true
                }
            }),
            // uncomment for obfuscation
            // replace({
            //     regex: '[fpsv]{1}_[a-zA-Z0-9]+',
            //     replace: function (str) {
            //         if (!cache[str]) {
            //             cache[str] = '$' + randomstring.generate(9);
            //         }
            //         return cache[str];
            //     }
            // }),
            // replace({
            //     regex: '\"[a-zA-Z0-9_$]{3,}[.]{1}[a-zA-Z0-9_$.]{3,}\"',
            //     replace: function (str) {
            //         if (!stringCache[str]) {
            //             stringCache[str] = '"$' + randomstring.generate(9) + '"';
            //         }
            //         return stringCache[str];
            //     }
            // }),
            gulp.dest("./release")
        ],
        f
    );
});

gulp.task('web-install', ['install-clean', 'sass', 'copy-fonts', 'copy-images', 'concat-css', 'concat-js', 'copy-owl-assets', 'copy-css', 'copy-js', 'copy-dojo', 'copy-dojo-util']);
gulp.task('release', ['release-clean', 'release-htmlmin', 'release-copy', 'release-uglify', 'release-copy-app', 'release-app-uglify']);

gulp.task('dojo-build', ['release'], function(f) {
    var cmd = spawn('node', [
        './src/dojo/dojo.js',
        'load=build',
        '--profile',
        './profile/myapp.profile.js'
    ], { stdio: 'inherit' });

    return cmd.on('close', function(code) {
        console.log('Dojo build completed ' + (code === 0 ? 'successfully!' : 'with issues.'));
        f(code);
    });
});

gulp.task('clean-uncompressed', ['dojo-build'], function() {
    return clean([
        './release/**/*.uncompressed.js'
    ]);
});

gulp.task('hex-string', ['clean-uncompressed'], function() {
    // uncomment for obfuscation
    // return gulp.src(['release/dojo/dojo.js', 'release/dojo/nls/*.js', 'release/app/keypairs/**/*.js'], { 'base': 'release'})
    //     .pipe(str2hex({
    //         hexall: true
    //     })).pipe(gulp.dest('./release'));
});

gulp.task('web-release', ['hex-string']);
