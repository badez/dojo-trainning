var profile = (function() {
    return {
        basePath: "../src",
        releaseDir: "../release",
        releaseName: "",
        action: "release",
        mini: true,
        stripConsole: "warn",
        selectorEngine: "lite",
        // Uncomment for obfuscation
        // optimize: "closure",
        // layerOptimize: "closure",
        // optimizeOptions: {
        //     languageIn: Packages.com.google.javascript.jscomp.CompilerOptions.LanguageMode.ECMASCRIPT5
        // },
        defaultConfig: {
            async: 1
        },
        dojoConfig: "./assets/js/config.js",
        staticHasFeatures: {
            'dojo-trace-api': 0,
            'dojo-log-api': 0,
            'dojo-publish-privates': 0,
            'dojo-test-sniff': 0
        },
        packages: [{
            name: "dojo",
            location: "dojo"
        }, {
            name: "dijit",
            location: "dijit"
        }, {
            name: "dojox",
            location: "dojox"
        }, {
            name: "app",
            location: "../release/app"
        }, {
            name: "proj",
            location: "../release/projSample/app",
            destLocation: "projSample/app"
        }],
        layers: {
            "dojo/dojo": {
                include: [
                    "dojo/dojo",
                    "dojo/selector/acme",
                    "dojo/selector/lite",
                    "dojo/_firebug/firebug",
                    "app/main",
                    "app/topic-center",
                    "app/inspinia"
                ],
                customBase: true,
                boot: true
            },
            "app/keypairs/test/sample": {
                include: [
                    "app/keypairs/test/sample"
                ]
            },
            "app/keypairs/generic": {
                include: [
                    "app/keypairs/generic"
                ]
            },
            "app/keypairs/inboxnotification/inboxnotification": {
                include: [
                    "app/keypairs/inboxnotification/inboxnotification"
                ]
            }
        }
    };
})();
