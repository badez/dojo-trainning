var profile = (function() {
    return {
        basePath: "../src",
        releaseDir: "../release",
        releaseName: "",
        action: "release",
        mini: true,
        stripConsole: "warn",
        selectorEngine: "lite",
        // Uncomment for obfuscation
        // optimize: "closure",
        // layerOptimize: "closure",
        // optimizeOptions: {
        //     languageIn: Packages.com.google.javascript.jscomp.CompilerOptions.LanguageMode.ECMASCRIPT5
        // },
        dojoConfig: {
            async: 1
        },
        dojoConfig: "./assets/js/config.js",
        staticHasFeatures: {
            'dojo-trace-api': 0,
            'dojo-log-api': 0,
            'dojo-publish-privates': 0,
            'dojo-test-sniff': 0
        },
        packages: [{
            name: "dojo",
            location: "dojo"
        }, {
            name: "dijit",
            location: "dijit"
        }, {
            name: "dojox",
            location: "dojox"
        }, {
            name: "app",
            location: "../release/app"
        }, {
            name: "proj",
            location: "../release/dlos/app",
            destLocation: "dlos/app"
        }],
        layers: {
            "dojo/dojo": {
                include: [
                    "dojo/dojo",
                    "dojo/selector/acme",
                    "dojo/selector/lite",
                    "dojo/_firebug/firebug",
                    "proj/main",
                    "app/topic-center",
                    "app/inspinia"
                ],
                customBase: true,
                boot: true
            },
            "app/keypairs/test/sample": {
                include: [
                    "app/keypairs/test/sample"
                ]
            },
            "app/keypairs/inboxnotification/inboxnotification": {
                include: [
                    "app/keypairs/inboxnotification/inboxnotification"
                ]
            },
            "app/keypairs/generic": {
                include: [
                    "app/keypairs/generic"
                ]
            },
            "proj/keypairs/test/sample": {
                include: [
                    "proj/keypairs/test/sample"
                ]
            },
            "proj/keypairs/test/loanenquiry": {
                include: [
                    "proj/keypairs/test/loanenquiry"
                ]
            },
            "proj/keypairs/test/cadashboard": {
                include: [
                    "proj/keypairs/test/cadashboard"
                ]
            },
            "proj/keypairs/test/verifylof": {
                include: [
                    "proj/keypairs/test/verifylof"
                ]
            },
            "proj/keypairs/test/caapproval": {
                include: [
                    "proj/keypairs/test/caapproval"
                ]
            },
            "proj/keypairs/test/upload": {
                include: [
                    "proj/keypairs/test/upload"
                ]
            },
            "proj/keypairs/test/quicksearch": {
                include: [
                    "proj/keypairs/test/quicksearch"
                ]
            },
            "proj/keypairs/test/quickca": {
                include: [
                    "proj/keypairs/test/quickca"
                ]
            },
            "proj/keypairs/dlosmodal": {
                include: [
                    "proj/keypairs/dlosmodal"
                ]
            }
        }
    };
})();
