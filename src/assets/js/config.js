var _locale = "en_US";

if ($.jStorage.get("app.data.locale") !== null) {
    _locale = $.jStorage.get("app.data.locale");
} else {
    $.jStorage.set("app.data.locale", _locale);
}

dojoConfig = {
    baseUrl: "./",
    has: {
        "dojo-firebug": true,
        "dojo-debug-messages": true
    },
    parseOnLoad: false,
    async: true,
    packages: [{
        name: "dojo",
        location: "dojo"
    }, {
        name: "dijit",
        location: "dijit"
    }, {
        name: "dojox",
        location: "dojox"
    }, {
        name: "app",
        location: "app"
    }, {
        name: "assets",
        location: "assets"
    }],
    cibaseurl: "http://10.10.0.70:9080/dloswsapi/api/",
    isExperimental: true,
    analyticsUniqueId: "UA-87172220-1",
    locale: _locale,
    isIE8: false,
    sessionTimeout: 100, //in Minutes
    cacheBust: false,//true = nocache, false =cached | disable cache for testing purpose only

    // menu (static/dynamic)
    menu: "static",

    //Mask format
    caNumber : '00000/0000/00/00000000/00',
    amountMask : '00',
    accountNumberMask: '00000/00/000000/00',
    NRIC : '000000-00-0000',
    mobileNo : '000-0000000',

    // decimal
    percentageDecimal: 2,
    interestRateDecimal: 5,
    amountDecimal: 2,
    exchangeRateDecimal: 2,

    //date format
    dateSeparator: "/",
    dayFormat: "dd",
    monthFormat: "MM",
    yearFormat: "yyyy",

    landingPage: "COMPONENTS"
};
