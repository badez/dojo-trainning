require([
    "dojo/_base/config",
    "dojo/topic"
], function (
    v_config,
    v_topic
) {

    f_init();

    function f_init() {
        //============================================================ START: Google Analytics!
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o), m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', {
            trackingId: v_config.analyticsUniqueId, //pass tracking id to analytics.js 
            cookieDomain: 'auto' //define how cookies is stored. "auto" will tell analytics.js to recommend 
        });

        ga('send', {
            hitType: 'pageview'
        });

        // START: define disabled/enabled Analytics
        window['ga-disable-' + v_config.analyticsUniqueId] = v_config.analyticsUniqueId ? true : false;
        // END: define disabled/enabled Analytics

        //============================================================ END: Google Analytics!

        f_startListening();
    }

    function f_startListening() {

        //START: get page loaded, and store in google analytic
        v_topic.subscribe("analytics.page", function (v_page, v_title) {
            if (v_config.proj) {
                query("title").forEach(function (v_node) {
                    v_title = v_node.innerHTML;
                });
            }
            if (v_config.analyticsUniqueId ? true : false) {
                ga('set', {
                    hitType: 'pageview',
                    page: v_page,
                    title: v_title,
                    hitcallback: function () {
                        console.log("Page track", v_page + " : " + v_title);
                    }
                });
                ga('send', 'pageview');
            }
        });
        //END: get page loaded, and store in google analytic

        //START: get label based on click event
        v_topic.subscribe("analytics.event", function (v_event, v_value) {
            v_value = v_value ? v_value : 0;
            if (v_config.analyticsUniqueId ? true : false) {
                // Sends the event to Google Analytics and
                ga('send', {
                    hitType: 'event', // Required [pageview, screenview, event, transaction, item, social, exception, timing].
                    eventCategory: 'button', // Required.
                    eventAction: v_event.type, // Required.
                    eventLabel: v_event.target.innerHTML,
                    eventValue: v_value,
                    hitcallback: function () {
                        //executed when the data is successfully submitted
                        console.log("Event track", v_event);
                    }
                });

            }
        });
        //END: get label based on click event

        //START: get the loaded page
        v_topic.subscribe("navigation.block", function (v_page) {
            v_topic.publish("analytics.page", v_page, "");
        });
        //END: get the loaded page

        console.log("[GA Running]");
    }
});
