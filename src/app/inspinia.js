define([
    "dojo/_base/window",
    "dojo/dom",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/on",
    "dojo/query"
], function (
    v_win,
    v_dom,
    v_domClass,
    v_domStyle,
    v_on,
    v_query
) {
    // Full height of sidebar
    function f_fix_height() {
        var heightWithoutNavbar = $("body > #wrapper").height() - 61;
        $(".sidebard-panel").css("min-height", heightWithoutNavbar + "px");

        var navbarHeigh = $('nav.navbar-default').height();
        var wrapperHeigh = $('#page-wrapper').height();

        if (navbarHeigh > wrapperHeigh) {
            $('#page-wrapper').css("min-height", navbarHeigh + "px");
        }

        if (navbarHeigh < wrapperHeigh) {
            $('#page-wrapper').css("min-height", $(window).height() + "px");
        }

        if ($('body').hasClass('fixed-nav')) {
            if (navbarHeigh > wrapperHeigh) {
                $('#page-wrapper').css("min-height", navbarHeigh + "px");
            } else {
                $('#page-wrapper').css("min-height", $(window).height() - 60 + "px");
            }
        }
    }

    function f_SmoothlyMenu() {
        var v_sideMenu = v_dom.byId('side-menu');
        if (!v_sideMenu) {
            return;
        }
        
        v_domStyle.set(v_sideMenu, "display", "none");
        v_domClass.remove(v_sideMenu, "animated fadeIn");

        if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
            setTimeout(
                function () {
                    v_domClass.add(v_sideMenu, "animated fadeIn");
                    v_domStyle.set(v_sideMenu, "display", "");
                }, 200);
        } else if ($('body').hasClass('fixed-sidebar')) {
            setTimeout(
                function () {
                    v_domClass.add(v_sideMenu, "animated fadeIn");
                    v_domStyle.set(v_sideMenu, "display", "");
                }, 100);
        } else {
            // Remove all inline style from jquery fadeIn function to reset menu state
            $('#side-menu').removeAttr('style');
        }
    }

    return {
        p_init: function () {
            if ($('body').width() < 769) {
                $('body').addClass('body-small');
            } else {
                $('body').removeClass('body-small');
            }

            f_fix_height();

            v_on(window, "resize, load, scroll", function () {
                if ($('body').width() > 768) {
                    if (v_domClass.contains(v_win.body(), "body-small")) {
                        v_domClass.remove(v_win.body(), "body-small");
                        f_SmoothlyMenu();
                    }
                } else {
                    if (!v_domClass.contains(v_win.body(), "body-small")) {
                        v_domClass.add(v_win.body(), "body-small");
                        f_SmoothlyMenu();
                    }
                }

                if (!$("body").hasClass('body-small')) {
                    f_fix_height();
                }
            });

            // v_on(window, "load", function () {
            //     if ($("body").hasClass('fixed-sidebar')) {
            //         $('.sidebar-collapse').slimScroll({
            //             height: '100%',
            //             railOpacity: 0.9
            //         });
            //     }
            // });
        },
        p_iboxInit: function (v_domNode) {
            v_query(".collapse-link", v_domNode).on("click", function (v_single) {
                var ibox = $(v_single.currentTarget).closest('div.ibox');
                var button = $(v_single.currentTarget).find('i');
                var content = ibox.find('div.ibox-content');
                content.slideToggle(200);
                button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                ibox.toggleClass('').toggleClass('border-bottom');
                setTimeout(function () {
                    ibox.resize();
                    ibox.find('[id^=map-]').resize();
                }, 50);
            });
            v_query(".close-link", v_domNode).on("click", function (v_single) {
                var content = $(v_single.currentTarget).closest('div.ibox');
                content.remove();
            });
            v_query(".fullscreen-link", v_domNode).on("click", function (v_single) {
                var ibox = $(v_single.currentTarget).closest('div.ibox');
                var button = $(v_single.currentTarget).find('i');
                $('body').toggleClass('fullscreen-ibox-mode');
                button.toggleClass('fa-expand').toggleClass('fa-compress');
                ibox.toggleClass('fullscreen');
                setTimeout(function () {
                    $(window).trigger('resize');
                }, 100);
            });
        },
        p_sideMenuInit: function () {
            v_query(".navbar-minimalize").on("click", function () {
                $("body").toggleClass("mini-navbar");
                f_SmoothlyMenu();
            });
            f_fix_height();
        }
    };
});
