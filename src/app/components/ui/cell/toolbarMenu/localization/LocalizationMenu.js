define([
    "dojo/_base/declare",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/html",
    "dojo/text!./html/LocalizationMenu.html",
    "../../BaseCell",
    "../../../element/localization/Localization"
], function (
    v_declare,
    v_domClass,
    v_domStyle,
    v_html,
    v_template,
    v_BaseCell,
    v_Localization
) {
    return v_declare("LocalizationMenu", [v_BaseCell], {
        templateString: v_template,
        p_construct: function (v_setting) {
            this.inherited(arguments);
            var v_context = this;

            v_context.p_setupContent(v_setting);
            if (!v_setting.p_iconOnly) {
                v_context.p_i18n(v_setting);
            }

            if (v_setting.p_noIcon) {
                v_domStyle.set(v_context.localizationIconNode, "display", "none");
            }
        },
        p_setupContent: function (v_setting) {
            var v_context = this;
            var v_localization = new v_Localization();
            v_localization.placeAt(v_context.localizationNode);
            v_localization.p_construct();
        },
        p_i18n: function (v_setting) {
            var v_context = this;
            v_html.set(v_context.localizationLabelNode, "Localisation");
        }
    });
});
