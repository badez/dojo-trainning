define([
    "dojo/_base/declare",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/html",
    "dojo/text!./html/InboxNotificationMenu.html",
    "../../BaseCell",
    "../../../element/inboxnotification/InboxNotification"
], function (
    v_declare,
    v_domClass,
    v_domStyle,
    v_html,
    v_template,
    v_BaseCell,
    v_InboxNotification
) {
    return v_declare("InboxNotificationMenu", [v_BaseCell], {
        templateString: v_template,
        p_construct: function (v_setting) {
            this.inherited(arguments);
            var v_context = this;

            v_context.p_setupContent(v_setting);
            if (!v_setting.p_iconOnly) {
                v_context.p_i18n(v_setting);
            }

            if (v_setting.p_noIcon) {
                v_domStyle.set(v_context.inboxIconNode, "display", "none");
            }
        },
        p_setupContent: function (v_setting) {
            var v_context = this;
            var v_inbox = new v_InboxNotification();
            v_inbox.placeAt(v_context.inboxMenuNode);
            v_inbox.p_construct();
        },
        p_i18n: function (v_setting) {
            var v_context = this;
            v_html.set(v_context.inboxMenuLabelNode, "Inbox");
        }
    });
});
