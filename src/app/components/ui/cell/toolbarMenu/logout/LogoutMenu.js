define([
    "dojo/_base/declare",
    "dojo/dom-style",
    "dojo/html",
    "dojo/on",
    "dojo/text!./html/LogoutMenu.html",
    "dojo/topic",
    "dojo/i18n!app/locale/nls/login-bundle",
    "../../BaseCell"
], function (
    v_declare,
    v_domStyle,
    v_html,
    v_on,
    v_template,
    v_topic,
    v_i18n,
    v_BaseCell
    
) {
    return v_declare("LogoutMenu", [v_BaseCell], {
        templateString: v_template,
        p_construct: function (v_setting) {
            this.inherited(arguments);
            var v_context = this;

            v_context.p_initInteraction(v_setting);
            if (!v_setting.p_iconOnly) {
                v_context.p_i18n(v_setting);
            }

            if (v_setting.p_noIcon) {
                v_domStyle.set(v_context.logoutIconNode, "display", "none");
            }
        },
        p_initInteraction: function (v_setting) {
            var v_context = this;

            v_on(v_context.logoutNode, "click", function () {
                v_topic.publish("navigation.logout");
            });
        },
        p_i18n: function (v_setting) {
            var v_context = this;
            v_html.set(v_context.logoutLabelNode, v_i18n.logout);
        }
    });
});
