define([
    "dojo/_base/declare",
    "dojo/dom-class",
    "dojo/html",
    "dojo/on",
    "dojo/text!./html/QuickNavigationMenuCell.html",
    "app/components/ui/cell/BaseCell",
    "app/components/utils/General",
    "app/components/utils/Rest"
], function(
    v_declare,
    v_domClass,
    v_html,
    v_on,
    v_template,
    v_BaseCell,
    v_GeneralUtils,
    v_Rest
) {
    return v_declare("app.components.ui.cell.quicknavigationmenu.QuickNavigationMenuCell", [v_BaseCell], {
        templateString: v_template,
        construct: function(v_setting) {
            var v_context = this;

            v_context.initInteraction(v_setting);
            v_context.setupContent(v_setting);
        },
        initInteraction: function(v_setting) {
            var v_context = this;

            v_on(v_context.quickNavigationMenuItemNode, "click", function() {
                $('html, body').animate({
                    scrollTop: $("#" + v_setting.id).offset().top - 100
                }, 1000);
            });
        },
        setupContent: function(v_setting) {
            var v_context = this;
            
            v_html.set(v_context.quickNavigationMenuItemLabelNode, v_setting.label);
            v_domClass.add(v_context.quickNavigationMenuItemIconNode, v_setting.icon);
        }
    });
})
