define([
    "dojo/_base/declare",
    "dojo/_base/array",
    "dojo/dom-construct",
    "dojo/dom-attr",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/on",
    "dojo/topic",
    "dojo/query",
    "app/components/ui/cell/BaseCell",
    "dijit/_TemplatedMixin"
], function(
    declare,
    array,
    domConstruct,
    domAttr,
    domClass,
    domStyle,
    on,
    topic,
    query,
    BaseCell,
    _TemplatedMixin) {
    return declare("app.components.ui.cell.widget.BaseWidget", [BaseCell, _TemplatedMixin], {
        minHeight: null,
        outerDiv: null,
        iboxNodes: null,
        resizeTimer: null,
        setupWidget: function(_setting) {
            var context = this;
            var domNode = context.domNode;
            var domNodeId = domAttr.get(domNode, "id");

            // domStyle.set(domNode, "height", "100%");

            context.outerDiv = domConstruct.create("div", { "class": "grid-widget-item" });
            var div = domConstruct.create("div", { "class": "grid-stack-item-content" }, context.outerDiv);
            context.placeAt(div);
            context.grid = _setting.grid;

            if (!context.minHeight) {
                context.minHeight = 1;
            }

            if (context.iboxNodes === undefined || !context.iboxNodes) {
                context.iboxNodes = [];
                context.iboxNodes.push(domNode);
            }

            context.resetupIboxNodes();

            // $(window).bind('resize orientationchange', function() {
            //     console.log("triggered");
            //     context.cellFromHeight();
            // });

            // on(window, "resize", function(event) {
            //     context.resizeWidgetHeight();
            // });

            var resizeWidgetHandler = topic.subscribe("resize.widget", function() {
                context.resizeWidgetHeight();
            });
            topic.publish("topic.handlers.add", resizeWidgetHandler, domNodeId);
        },
        resetupIboxNodes: function() {
            var context = this;
            var domNode = this.domNode;
            var domNodeId = domAttr.get(domNode, "id");
            array.forEach(context.iboxNodes, function(iboxNode, i) {
                var iboxNodeId = domAttr.get(iboxNode, "id");

                // var titleHeight = (context.minHeight * (context.grid.cellHeight() + 15)) - 15;

                // domStyle.set(iboxNode, "width", "100%");
                // if (query(".ibox", iboxNode)[0]) {
                //     domStyle.set(query(".ibox", iboxNode)[0], "height", "100%");
                //     domStyle.set(query(".ibox", iboxNode)[0], "margin-bottom", "0px");
                // }
                //  if (query(".ibox-content", iboxNode)) {
                //     domStyle.set(query(".ibox-content", iboxNode)[0], "height", "calc(100% - " + titleHeight + "px)");
                // }
                // if (query(".ibox-title", iboxNode)) {

                //     domStyle.set(query(".ibox-title", iboxNode)[0], "height", titleHeight + "px");
                // }

                // topic.publish("cell.loaded", iboxNode);

                // var toggleCollapseHandler = topic.subscribe("toggle.collapse-link" + iboxNodeId, function() {
                    // if (domAttr.get(context.outerDiv, "data-gs-height") == context.minHeight) {
                    //     context.grid.resize(context.outerDiv, context.widgetWidth, context.widgetHeight);
                    // } else {
                    //     setTimeout(function() {
                    //         context.grid.resize(context.outerDiv, context.widgetWidth, context.minHeight);
                    //     }, 500);
                    // }
                    // context.resizeWidgetHeight();
                // });
                // topic.publish("topic.handlers.add", toggleCollapseHandler, domNodeId);
                query(".collapse-link", domNode).on("click", function(v_single) {
                    context.resizeWidgetHeight();
                });
            });
        },
        cellFromHeight: function(domNodeHeight) {
            var context = this;
            var cell = domNodeHeight / (context.grid.cellHeight() + context.verticalMargin);
            return Math.ceil(cell);
        },
        resizeWidgetHeight: function() {
            var context = this;
            var domNode = context.domNode;

            clearTimeout(context.resizeTimer);

            context.resizeTimer = setTimeout(function() {
                domClass.remove(domNode, "fullHeight");
                if (query(".ibox-content", domNode)[0]) {
                    domStyle.set(query(".ibox-content", domNode)[0], "height", "auto");
                }

                var outerDivHeight = $(context.outerDiv).innerHeight();
                var domNodeHeight = $(domNode).outerHeight(true);

                var cell = context.cellFromHeight(domNodeHeight);
                context.grid.resize(context.outerDiv, context.widgetWidth, cell);
                setTimeout(function() {
                    outerDivHeight = $(context.outerDiv).innerHeight();
                    domNodeHeight = $(domNode).outerHeight(true);

                    if (outerDivHeight < domNodeHeight) {
                        context.grid.resize(context.outerDiv, context.widgetWidth, cell + 1);
                    }
                    domClass.add(domNode, "fullHeight");
                    array.forEach(query(".ibox-title", domNode), function(iboxTitle, i) {
                        var titleHeight = $(iboxTitle).outerHeight();
                        domStyle.set(query(".ibox-content", domNode)[i], "height", "calc(100% - " + titleHeight + "px)");
                    });
                }, 500);
                // }
            }, 500);
        }
    });
});
