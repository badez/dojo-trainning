define([
    "dojo/_base/declare",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/html",
    "dojo/on",
    "dojo/text!./html/BreadcrumbsCell.html",
    "dojo/topic",
    "../BaseCell"
], function (
    v_declare,
    v_domClass,
    v_domConstruct,
    v_html,
    v_on,
    v_template,
    v_topic,
    v_BaseCell
) {
    return v_declare([v_BaseCell], {
        templateString: v_template,
        p_construct: function (v_setting) {
            var v_context = this;

            v_context.p_setupContent(v_setting);
            v_context.p_initInteraction(v_setting);
        },
        p_setupContent: function (v_setting) {
            var v_context = this;

            if (v_setting.p_isLast) {
                v_domClass.add(v_context.titleNode, "active font-bold");
            }
            v_html.set(v_context.titleNode, v_setting.p_title);
        },
        p_initInteraction: function (v_setting) {
            var v_context = this;

            v_on(v_context.titleNode, "click", function () {
                for (var v_i = 0; v_i < v_setting.p_counter; v_i++) {
                    v_topic.publish("navigation.block.previous");
                }
            });
        }
    });
});
