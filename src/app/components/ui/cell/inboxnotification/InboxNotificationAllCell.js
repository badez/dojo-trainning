define([
    "dojo/_base/declare",
    "dojo/dom-attr",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/dom-style",
    "dojo/html",
    "dojo/on",
    "dojo/text!./html/InboxNotificationAllCell.html",
    "dojo/topic",
    "dojo/query",
    "dijit/_TemplatedMixin",
    "app/components/ui/cell/BaseCell",
    "app/components/utils/General",
    "app/components/utils/Rest"
], function(
    declare,
    domAttr,
    domClass,
    domConstruct,
    domStyle,
    html,
    on,
    template,
    topic,
    query,
    TemplatedMixin,
    BaseCell,
    GeneralUtils,
    Rest
) {
    return declare("app.components.ui.cell.inboxnotification.InboxNotificationAllCell", [BaseCell, TemplatedMixin], {
        templateString: template,
        p_construct: function(setting) {
            var context = this;
            var domNode = context.domNode;

            context.startListening(setting);
            // context.initInteraction(setting);
            context.setupContent(setting);
        },
        startListening: function(_setting) {
            var context = this;
            var domNode = context.domNode;
            var domNodeId = domAttr.get(domNode, "id");

            var messageDeletedHandler = topic.subscribe("delete.checked.messages." + _setting.parentid, function(returned) {
                if (context.getCheckboxValue("status")[0] == "true") {
                    returned(_setting);
                }
            });
            topic.publish("topic.handlers.add", messageDeletedHandler, domNodeId);
        },
        setupContent: function(_setting) {
            var context = this;
            var domNode = context.domNode;

            query("td[class='clickable']", context.notificationListItemNode).forEach(function(single) {
                on(single, "click", function() {

                    console.log(context.getCheckboxValue("status")[0]);
                    //call api to change the read/unread status
                    domStyle.set(context.notificationListItemNode, "font-weight", "normal");
                    domStyle.set(context.notificationListItemNode, "background-color", "#fff");
                    topic.publish("navigation.modal.open", "INBOX_NOTIFICATION_DETAIL", _setting);
                });
            });

            query("ins[class='iCheck-helper']", domAttr.get(domNode, "id")).forEach(function(single) {
                on(single, "click", function() {
                    console.log("enable.disabled." + _setting.parentid);
                    topic.publish("enable.disabled." + _setting.parentid, {
                        cellId: domAttr.get(domNode, "id"),
                        status: context.getCheckboxValue("status")[0]
                    });
                });
            });

            html.set(context.senderNode, _setting.sender);
            html.set(context.subjectNode, _setting.subject);
            var date = new GeneralUtils().formatDate(_setting.date, { display: "datetime" });
            var newDate = date.slice(0, 16) + date.slice(19);
            var index = newDate.charAt(11);
            if (index == 0) {
                newDate = newDate.slice(0, 11) + newDate.slice(12);
            }
            html.set(context.dateNode, newDate);

            if (_setting.status == "n") {
                domStyle.set(context.notificationListItemNode, "font-weight", "bold");
                domStyle.set(context.notificationListItemNode, "background-color", "#eeeeee");
            } else if (_setting.status == "y") {
                domStyle.set(context.notificationListItemNode, "font-weight", "normal");
                domStyle.set(context.notificationListItemNode, "background-color", "#fff");
            } else if (_setting.status == 0) {
                domConstruct.empty(context.notificationListItemNode);
                var td = domConstruct.create("td", { className: "text-center" });
                domStyle.set(td, "padding-top", "15px");
                domStyle.set(td, "padding-bottom", "15px");
                domStyle.set(td, "border-bottom", "1 px solid# D1D3D5");
                domConstruct.place(td, context.notificationListItemNode);
                html.set(td, _setting.subject);
            }
        }
    });
})
