define([
    "dojo/_base/declare",
    "dojo/dom",
    "dojo/dom-attr",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/dom-style",
    "dojo/html",
    "dojo/i18n!app/locale/nls/common",
    "dojo/on",
    "dojo/text!./html/InboxNotificationCell.html",
    "dojo/topic",
    "dijit/_TemplatedMixin",
    "app/components/ui/cell/BaseCell",
    "app/components/utils/General",
    "app/components/utils/Rest"
], function(
    declare,
    dom,
    domAttr,
    domClass,
    domConstruct,
    domStyle,
    html,
    i18n,
    on,
    template,
    topic,
    TemplatedMixin,
    BaseCell,
    GeneralUtils,
    Rest
) {
    return declare("app.components.ui.cell.inboxnotification.InboxNotificationCell", [BaseCell, TemplatedMixin], {
        templateString: template,
        p_construct: function(setting) {
            var domNode = this.domNode;
            var context = this;

            context.setupContent(setting);
            context.initInteraction(setting);
        },
        initInteraction: function(_setting) {
            var context = this;
            var domNode = this.domNode;

            on(context.inboxMessagesItemNode, "click", function() {
                console.log("ENTERING HEREE");
                topic.publish("navigation.modal.open", "INBOX_NOTIFICATION_DETAIL", _setting);
            });

        },
        setupContent: function(_setting) {
            var context = this;
            var domNode = this.domNode;

            var oldDate = new Date(_setting.date);
            var currentDate = new Date().getTime();

            var oneMinute = 60 * 1000;
            var oneHour = 60 * oneMinute;
            var oneDay = 24 * oneHour;
            var oneWeek = 7 * oneDay;

            var differentMinutes = Math.round(Math.abs((oldDate.getTime() - currentDate) / (oneMinute)));
            var differentHours = Math.round(Math.abs((oldDate.getTime() - currentDate) / (oneHour)));
            var differentDays = Math.round(Math.abs((oldDate.getTime() - currentDate) / (oneDay)));
            var differentWeek = Math.round(Math.abs((oldDate.getTime() - currentDate) / (oneWeek)));

            var setTime;
            var details;

            var differentYears = new Date().getFullYear() - new Date(_setting.date).getFullYear();
            var differentMonth = new Date().getMonth() - new Date(_setting.date).getMonth();

            if (differentMinutes) {
                setTime = differentMinutes + " m ago";
                if (differentMinutes < 60) {
                    details = differentMinutes + " minutes ago";
                }
                if (differentHours) {
                    setTime = differentHours + " h ago";
                    if (differentHours < 24) {
                        details = differentHours + " hours ago";
                    }
                    if (differentDays) {
                        setTime = differentDays + " d ago";
                        if (differentDays < 7) {
                            details = differentDays + " days ago";
                        }
                        if (differentWeek) {
                            setTime = differentWeek + " w ago";
                            if (differentWeek > 1) {
                                details = differentWeek + " weeks ago";
                            }
                            if (differentYears) {
                                setTime = differentYears + " y ago";
                                details = differentYears + " years ago";

                            } else {
                                if (differentMonth) {
                                    setTime = differentMonth + " m ago";
                                    details = differentMonth + " months ago";

                                }
                            }
                        }
                    }
                }
            }

            var date = new GeneralUtils().formatDate(_setting.date, { display: "datetime" });
            var newDate = date.slice(0, 16) + date.slice(19);
            var index = newDate.charAt(11);
            if (index == 0) {
                newDate = newDate.slice(0, 11) + newDate.slice(12);
            }
            newDate = details + " " + newDate;
            html.set(context.dateNode, newDate);

            html.set(context.passedTimeNode, setTime);

            var sender = _setting.sender;
            if (_setting.sender.length > 20) {
                sender = _setting.sender.substring(0, 20) + " . . .";
            }
            html.set(context.senderNode, sender);

            var subject = _setting.subject;
            if (_setting.subject.length > 30) {
                subject = _setting.subject.substring(0, 30) + " . . .";
            }
            html.set(context.subjectNode, subject);
        }

    });
})
