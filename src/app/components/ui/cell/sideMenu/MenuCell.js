define([
    "dojo/_base/array",
    "dojo/_base/declare",
    "dojo/dom-attr",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/html",
    "dojo/on",
    "dojo/query",
    "dojo/text!./html/MenuCell.html",
    "dojo/topic",
    "../BaseCell"
], function (
    v_array,
    v_declare,
    v_domAttr,
    v_domClass,
    v_domConstruct,
    v_html,
    v_on,
    v_query,
    v_template,
    v_topic,
    v_BaseCell
) {
    return v_declare([v_BaseCell], {
        templateString: v_template,
        p_construct: function (v_setting) {
            var v_context = this;
            v_context.p_i18n(v_setting);
            v_context.p_startListening(v_setting);
            v_context.p_setupContent(v_setting);
        },
        p_startListening: function (v_setting) {
            var v_context = this;
            var v_domNode = v_context.domNode;
            var v_domNodeId = v_domAttr.get(v_domNode, "id");

            var v_menuActive = v_topic.subscribe("block.active", function (v_function) {
                if (v_setting.functionCode === v_function.getSetting().p_baseFunction) {
                    v_context.p_setActive();
                }
            });
            v_topic.publish("topic.handlers.add", v_menuActive, v_domNodeId);
        },
        p_setupContent: function (v_setting) {
            var v_context = this;

            if (v_setting.icon) {
                v_domClass.add(v_context.iconNode, v_setting.icon);
            } else {
                v_domConstruct.destroy(v_context.iconNode);
            }

            if (v_setting.childMenus === undefined) {
                v_on(v_context.menuNode, "click", function () {
                    v_topic.publish("navigation.block.open", v_setting.functionCode, v_setting);
                });
            } else {
                var v_span = v_domConstruct.create("span", { "class": "fa arrow" });
                v_domConstruct.place(v_span, v_context.menuNode);
            }
        },
        p_setActive: function () {
            var v_context = this;
            var v_domNode = v_context.domNode;

            // collapse and remove active from active node
            var v_activeNodes = $(v_domNode).siblings().toArray();
            var v_parents = $(v_domNode).parents("li");
            v_activeNodes.push.apply(v_activeNodes, v_parents.siblings().toArray());
            if (v_activeNodes.length > 0) {
                v_array.forEach(v_activeNodes, function (v_activeNode) {
                    v_domClass.remove(v_activeNode, "active");
                    v_query(".active", v_activeNode).forEach(function (v_node) {
                        v_domClass.remove(v_node, "active");
                    });
                    v_query(".collapse", v_activeNode).forEach(function (v_node) {
                        v_domClass.remove(v_node, "in");
                    });
                    v_query("[aria-expanded]", v_activeNode).forEach(function (v_node) {
                        v_domAttr.set(v_node, "aria-expanded", "false");
                    });
                });
            }

            // expand parent expand and active to clicked node
            v_array.forEach($(v_domNode).parents("[aria-expanded]"), function (v_node) {
                v_domAttr.set(v_node, "aria-expanded", "true");
                v_domAttr.set($(v_node).siblings()[0], "aria-expanded", "true");
            });
            v_array.forEach($(v_domNode).parents(".collapse"), function (v_node) {
                v_domClass.add(v_node, "in");
            });
            v_array.forEach(v_parents, function (v_parent) {
                v_domClass.add(v_parent, "active");
            });
            v_domClass.add(v_domNode, "active");
        },
        p_i18n: function (v_setting) {
            var v_context = this;
            var v_label = v_setting.functionName ? v_setting.functionName : v_setting.menuName;
            v_html.set(v_context.labelNode, v_label);
        }
    });

});
