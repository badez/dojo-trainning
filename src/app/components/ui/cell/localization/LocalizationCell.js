define([
    "dojo/dom-class",
    "dojo/on",
    "dojo/html",
    "dojo/topic",
    "dojo/_base/config",
    "dojo/_base/declare",
    "dojo/text!./html/LocalizationCell.html",
    "dojo/i18n!app/locale/nls/localization-bundle",
    "app/components/ui/cell/BaseCell"
], function (
    v_domClass,
    v_on,
    v_html,
    v_topic,
    v_config,
    v_declare,
    v_template,
    v_i18n,
    v_BaseCell
) {
    return v_declare([v_BaseCell], {
        templateString: v_template,
        p_construct: function (v_setting) {
            var v_context = this;
            v_context.p_setupContent(v_setting);
            v_context.p_initInteraction(v_setting);
        },
        p_initInteraction: function (v_setting) {
            var v_context = this;

            v_on(v_context.localeNode, "click", function () {
                v_topic.publish("navigation.logout", v_setting.keyname);
            });
        },
        p_setupContent: function (v_setting) {
            var v_context = this;
            var v_domNode = v_context.domNode;
            v_html.set(v_context.languageDescriptionNode, v_i18n[v_setting.keyname]);
            v_html.set(v_context.languageCodeNode, v_setting.keyname);

            if ($.jStorage.get("app.data.locale") === v_setting.keyname) {
                v_domClass.add(v_domNode, "active");
            }
        }
    });
})
