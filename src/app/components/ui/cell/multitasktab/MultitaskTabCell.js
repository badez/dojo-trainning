define([
    "dojo/_base/config",
    "dojo/_base/declare",
    "dojo/dom-attr",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/html",
    "dojo/on",
    "dojo/topic",
    "app/components/ui/cell/BaseCell",
    "dojo/text!./html/MultitaskTabCell.html"
], function(
    v_config,
    v_declare,
    v_domAttr,
    v_domClass,
    v_domStyle,
    v_html,
    v_on,
    v_topic,
    v_BaseCell,
    v_template
) {
    return v_declare("app.components.ui.cell.multitasktab.MultitaskTabCell", [v_BaseCell], {
        templateString: v_template,
        p_construct: function(v_setting) {
            var v_context = this;
            v_context.p_setupContent(v_setting);
        },
        p_setupContent: function(v_setting) {
            var v_context = this;
            var v_domNode = this.v_domNode;

            v_html.set(v_context.multitaskTabNode, v_setting.p_functionName);

            if (v_setting.p_function === v_config.landingPage) {
                v_domStyle.set(v_context.multitaskItemCloseIconNode, "display", "none");
            }
        },
    });
})
