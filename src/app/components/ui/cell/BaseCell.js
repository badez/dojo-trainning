define([
    "dojo/_base/declare",
    "dojo/topic",
    "app/components/utils/BaseUtils"
], function (
    v_declare,
    v_topic,
    v_BaseUtils
) {
    return v_declare("app.components.ui.cell.BaseCell", [v_BaseUtils], {
        p_construct: function (v_setting) {
            var v_context = this;

            v_context.getSetting = function () {
                return v_setting ? v_setting : {};
            };

            v_topic.publish("cell.loaded", v_context);
        }
    });
});
