define([
    "dojo/_base/declare",
    "dojo/html",
    "dojo/dom-class",
    "dojo/on",
    "dojo/topic",
    "dojo/text!./html/IcomoonCell.html",
    "app/components/ui/cell/BaseCell"
], function(
    v_declare,
    v_html,
    v_domClass,
    v_on,
    v_topic,
    v_template,
    v_BaseCell
) {
    return v_declare([v_BaseCell], {
        templateString: v_template,
        p_construct: function(v_setting) {
            var v_context = this;

            v_context.v_setupContent(v_setting);
            v_context.v_initInteraction(v_setting);
        },
        v_initInteraction: function(v_setting) {
            var v_context = this;

            v_on(v_context.clickableNode, "click", function() {
                // v_topic.publish("navigation.modal.open", "ICOMOON",{ inputValue: "",title: v_setting.data });
            });
        },
        v_setupContent: function(v_setting) {
            var v_context = this;

            v_domClass.add(v_context.icomoonClassNode, v_setting.p_data);
            v_html.set(v_context.icomoonLabelNode, v_setting.p_data);

        }
    });
})
