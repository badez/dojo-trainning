define([
    "dojo/topic",
    "dojo/_base/declare",
    "dojo/_base/array",
    "dojo/dom-attr",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/html",
    "dojo/i18n!app/locale/nls/common",
    "dojo/text!./html/DynamicCell.html",
    "dijit/_TemplatedMixin",
    "app/components/ui/cell/BaseCell",
    "app/components/ui/element/datepicker/Datepicker",
    "app/components/utils/General"
], function(
    topic,
    declare,
    Array,
    domAttr,
    domClass,
    domConstruct,
    html,
    i18n_common,
    template,
    _TemplatedMixin,
    BaseCell,
    Datepicker,
    GeneralUtils
) {
    return declare("app.components.ui.cell.dynamic.DynamicCell", [BaseCell,
        _TemplatedMixin
    ], {
        templateString: template,
        p_construct: function(setting) {
            var domNode = this.domNode;
            var context = this;

            context.setupContent(setting);
        },
        createUUID: function(node) {
            var context = this;
            var uuid = new GeneralUtils().createUUID();
            domAttr.set(node, "id", uuid);
            return uuid;
        },
        setupContent: function(_setting) {
            var context = this;
            var domNode = this.domNode;

            var inputType = _setting.inputType;
            var column = _setting.inputColumn;
            var bootstrapCol = "";

            switch (column) {
                case 4:
                    bootstrapCol = bootstrapCol + " col-lg-3";
                case 3:
                    bootstrapCol = bootstrapCol + " col-md-4";
                case 2:
                    bootstrapCol = bootstrapCol + " col-sm-6";
                    break;
            }

            var inputValidation = "";

            if (_setting.inputMandatory) {
                inputValidation = "required ";
            }

            if (_setting.inputLength) {
                inputValidation += "maxlength|" + _setting.inputLength + " ";
            }

            var type;
            var inputClass;

            domClass.add(domNode, bootstrapCol);

            var isDecimal;
            switch (inputType) {
                case "D":
                    var div = domConstruct.create("div");
                    domClass.add(div, "form-group has-feedback-left");
                    var label = domConstruct.create("label");
                    html.set(label, _setting.inputLabel);
                    domConstruct.place(label, div);
                    var uniqueIdDatepicker = context.createUUID(div);
                    var datepicker = new Datepicker();

                    if (typeof _setting.inputDefault === "string") {
                        _setting.inputDefault = parseInt(_setting.inputDefault);
                    }

                    datepicker.p_construct({
                        parentid: domAttr.get(domNode, "id"),
                        datepickerid: uniqueIdDatepicker,
                        required: _setting.inputMandatory,
                        options: _setting.inputDefault !== undefined ? { defaultDates: { date1: new Date(_setting.inputDefault) } } : {},
                        savedData: function(object) {
                            _setting.savedData(object.timestamp);
                        }
                    });
                    datepicker.placeAt(div);
                    domConstruct.destroy(domNode.firstChild);
                    domConstruct.place(div, domNode);

                    if (_setting.isReadOnly) {
                        topic.publish("datepicker.enable." + uniqueIdDatepicker, false);
                    }

                    break;
                case "N":
                    domAttr.set(domNode.firstChild, "step", "0.00001");
                    isDecimal = true;
                case "I":
                    context.createNumberInput(domNode.firstChild, isDecimal ? isDecimal : false);
                    inputValidation += "decimal|" + isDecimal ? "-1 " : "0 ";
                case "C":
                    type = "text";
                default:
                    domClass.add(domNode.firstChild, inputClass);
                    domAttr.set(domNode.firstChild, "id", _setting.inputId);
                    domAttr.set(domNode.firstChild, "type", type ? type : "text");
                    domAttr.set(domNode.firstChild, "label", _setting.inputLabel);
                    domAttr.set(domNode.firstChild, "disabled", _setting.isReadOnly);
                    if (_setting.inputDefault) {
                        domAttr.set(domNode.firstChild, "value", _setting.inputDefault);
                    }
                    if (inputValidation !== "") {
                        domAttr.set(domNode.firstChild, "data-validation", inputValidation);
                    }
                    break;
            }
        }
    });
})
