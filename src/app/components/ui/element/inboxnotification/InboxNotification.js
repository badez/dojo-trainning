define([
    "dojo/_base/array",
    "dojo/_base/declare",
    "dojo/dom",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/dom-style",
    "dojo/html",
    "dojo/i18n!app/locale/nls/common",
    "dojo/on",
    "dojo/text!./html/InboxNotification.html",
    "dojo/topic",
    "app/components/ui/cell/inboxnotification/InboxNotificationCell",
    "app/components/ui/element/BaseElement",
    "app/components/utils/Rest"
], function(
    array,
    declare,
    dom,
    domClass,
    domConstruct,
    domStyle,
    html,
    i18n_common,
    on,
    template,
    topic,
    InboxNotificationCell,
    BaseElement,
    Rest
) {
    return declare("InboxNotification", [BaseElement], {
        templateString: template,
        p_construct: function(setting) {
            var context = this;
            context.i18n(setting);
            context.setupContent(setting);
            context.initInteraction(setting);
        },
        setupContent: function(_setting) {
            var context = this;
            var domNode = this.domNode;

            var prototypeUrl = "assets/data/inboxnotification_data.json";
            var datapackage = {};

            var rest = new Rest().p_get("assets/data/inboxnotification_data.json");
            rest.then(function(response) {
                if (response.webInboxNotification instanceof Array && response.webInboxNotification.length > 0) {
                    domConstruct.empty(context.notificationNode);

                    var tempArray = [];
                    array.forEach(response.webInboxNotification, function(single, index) {
                        if (index < 3) {
                            var inboxNotificationCell = new InboxNotificationCell();
                            inboxNotificationCell.placeAt(context.notificationNode);
                            inboxNotificationCell.p_construct(single);
                        } else {
                            tempArray.push(single);
                        }
                    });
                } else {
                    domStyle.set(context.notificationNode, "display", "none");
                }
            });
        },
        initInteraction: function(_setting) {
            var context = this;
            var domNode = this.domNode;
        
            //onclick event to open all messages page 
            // on(context.readAllMessageBtnNode, "click", function() {
            //     topic.publish("navigation.block", "INBOX_NOTIFICATION_ALL", {
            //         formId: "INBOX_NOTIFICATION_ALL",
            //         functionCode: "INBOX_NOTIFICATION_ALL",
            //         functionName: "Inbox Messages",
            //         icbaApplicationDate: 1395072000000,
            //         moduleCode: "DLOS",
            //         permissions: [{
            //             actionCode: "FUNCACCESS"
            //         }, {
            //             actionCode: "SELECT"
            //         }]
            //     });
            // });
        },
        i18n: function(_setting) {
            var context = this;
            var domNode = this.domNode;

            // html.set(context.readAllMessagesLabelNode, i18n_common.readAllMessages);
        }
    });
});
