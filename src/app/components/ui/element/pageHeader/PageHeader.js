define([
    "dojo/_base/array",
    "dojo/_base/declare",
    "dojo/dom-construct",
    "dojo/on",
    "dojo/text!./html/PageHeader.html",
    "dojo/topic",
    "../BaseElement",
    "../toolbarMenu/ToolbarMenu"
], function (
    v_array,
    v_declare,
    v_domConstruct,
    v_on,
    v_template,
    v_topic,
    v_BaseElement,
    v_ToolbarMenu
) {
    return v_declare([v_BaseElement], {
        templateString: v_template,
        p_construct: function (v_setting) {
            this.inherited(arguments);
            var v_context = this;

            v_context.p_setupContent(v_setting);
        },
        p_setupContent: function (v_setting) {
            var v_context = this;

            var v_toolbarMenu = new v_ToolbarMenu("list");
            v_toolbarMenu.p_construct({
                p_iconOnly: true,
                p_otherItems: v_setting.p_headeritems,
                p_localeMenu: false,
            });
            v_toolbarMenu.placeAt(v_context.welcomeBarNode);
        }
    });
});
