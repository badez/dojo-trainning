define([
    "dojo/_base/declare",
    "dojo/_base/array",
    "dojo/dom",
    "dojo/dom-attr",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/dom-style",
    "dojo/html",
    "dojo/on",
    "dojo/query",
    "dojo/text!./html/Carousel.html",
    "dojo/topic",
    "dijit/_TemplatedMixin",
    "app/components/ui/element/BaseElement",
    "app/components/utils/General"
], function(
    declare,
    array,
    dom,
    domAttr,
    domClass,
    domConstruct,
    domStyle,
    html,
    on,
    query,
    template,
    topic,
    _TemplatedMixin,
    BaseElement,
    GeneralUtils
) {
    return declare("dbrn.app.components.ui.element.bootstrapcarousel.Carousel", [BaseElement, _TemplatedMixin], {
        templateString: template,
        p_construct: function(setting) {
            this.inherited(arguments);
            var context = this;

            context.setupContent(setting);
            context.initInteraction(setting);
        },
        initInteraction: function(_setting) {
            var context = this;
        },
        setupContent: function(_setting) {
            var context = this;
            var domNode = this.domNode;
            var domNodeId = domAttr.get(domNode, "id");

            var carouselId = new GeneralUtils().createUUID();
            domAttr.set(context.carouselNode, "id", carouselId);
            var carouselId = "#" + carouselId;

            var singleContent = false;

            if (_setting.data.length === 1) {
                singleContent = true;
            }

            if (singleContent) {
                domClass.add(context.prevPanelNode, "hide");
                domClass.add(context.nextPanelNode, "hide");

            } else if (_setting.buttons) {
                if (_setting.buttons.removePanels) {
                    domStyle.set(context.prevPanelNode, "background-image", "none");
                    domStyle.set(context.nextPanelNode, "background-image", "none");
                }

                switch (_setting.buttons.option) {
                    case 1:
                        domClass.remove(context.prevBtnNode, "icon-prev");
                        domClass.remove(context.nextBtnNode, "icon-next");
                        domClass.add(context.prevBtnNode, "ico-back carousel-btn-styles");
                        domClass.add(context.nextBtnNode, "ico-next carousel-btn-styles");
                        break;
                    default:
                        break;
                }

                if (_setting.buttons.position) {
                    if (_setting.buttons.position.top) {
                        domStyle.set(context.prevBtnNode, "top", _setting.buttons.position.top);
                        domStyle.set(context.nextBtnNode, "top", _setting.buttons.position.top);
                    }

                    if (_setting.buttons.position.left) {
                        domStyle.set(context.prevBtnNode, "left", _setting.buttons.position.left);
                    }

                    if (_setting.buttons.position.right) {
                        domStyle.set(context.nextBtnNode, "right", _setting.buttons.position.right);
                    }
                }
            }

            if (_setting.isDashboard) {
                domStyle.set(context.carouselContainerNode, "height", "100%");
                domStyle.set(context.carouselNode, "height", "100%");
                domStyle.set(context.carouselInnerNode, "height", "100%");
            }

            array.forEach(_setting.data, function(single, i) {
                var uniqueIdCarouselCell = new GeneralUtils().createUUID();

                var row = domConstruct.create("div");
                domClass.add(row, "item");
                domAttr.set(row, "id", uniqueIdCarouselCell);

                if (_setting.isDashboard) {
                    domStyle.set(row, "height", "100%");
                }

                domConstruct.place(row, context.carouselInnerNode);
                if (i == 0) {
                    domClass.add(row, "active");
                    var row1 = domConstruct.toDom('<li data-slide-to="' + i + '" data-target="' + carouselId + '" class="active"></li>');
                } else {
                    var row1 = domConstruct.toDom('<li data-slide-to="' + i + '" data-target="' + carouselId + '"></li>');
                }

                /*if (_setting.dotsEnable) {  
                    domConstruct.place(row1, context.carouselDotsNode);
                }*/

                single.placeAt(row);
            });

            domAttr.set(context.prevPanelNode, "href", carouselId);
            domAttr.set(context.nextPanelNode, "href", carouselId);

            if (_setting.disableAutoplay) {
                $(carouselId).carousel({
                    interval: false
                });
            }

            $(carouselId).carousel();
        }
    });
});
