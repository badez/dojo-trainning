define([
    "dojo/_base/declare",
    "dojo/html",
    "dojo/on",
    "dojo/text!./html/ToolbarMenu.html",
    "dojo/text!./html/DropdownMenu.html",
    "dojo/topic",
    "../BaseElement",
    "../../cell/toolbarMenu/localization/LocalizationMenu",
    "../../cell/toolbarMenu/logout/LogoutMenu"
], function(
    v_declare,
    v_html,
    v_on,
    v_templateList,
    v_templateDropdown,
    v_topic,
    v_BaseElement,
    v_LocalizationMenu,
    v_LogoutMenu
) {
    return v_declare("ToolbarMenu", [v_BaseElement], {
        templateString: v_templateList,
        constructor: function(v_setting) {
            var v_context = this;
            switch (v_setting) {
                case "list":
                    v_context.templateString = v_templateList;
                    break;
                case "dropdown":
                    v_context.templateString = v_templateDropdown;
                    break;
            }
        },
        p_construct: function(v_setting) {
            this.inherited(arguments);
            var v_context = this;

            v_context.p_setupContent(v_setting);
        },
        p_setupContent: function(v_setting) {
            var v_context = this;

            if (v_setting.p_localeMenu === undefined || v_setting.p_localeMenu !== false) {
                var v_localizationMenu = new v_LocalizationMenu();
                v_localizationMenu.placeAt(v_context.toolbarMenuNode);
                v_localizationMenu.p_construct(v_setting);
            }

            if (v_setting.p_otherItems) {
                v_array.forEach(v_setting.p_otherItems, function(single) {
                    single.placeAt(v_context.toolbarMenuNode);
                });
            }

            var v_logoutMenu = new v_LogoutMenu();
            v_logoutMenu.placeAt(v_context.toolbarMenuNode);
            v_logoutMenu.p_construct(v_setting);
        }
    });
});
