define([
    "dojo/_base/declare",
    "dojo/text!./html/PageFooter.html",
    "../BaseElement"
], function (
    v_declare,
    v_template,
    v_BaseElement
) {
    return v_declare([v_BaseElement], {
        templateString: v_template
    });
});
