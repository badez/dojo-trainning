define([
    "dojo/_base/declare",
    "dojo/text!./html/Slider.html",
    "dijit/_TemplatedMixin",
    "app/components/ui/element/BaseElement"
], function(
    declare,
    template,
    TemplatedMixin,
    BaseElement
) {

    return declare("app.components.ui.element.slider.Slider", [BaseElement, TemplatedMixin], {
        templateString: template,
        slider: null,
        p_construct: function(setting) {
            var domNode = this.domNode;
            var context = this;

            context.setupContent(setting);
        },
        startup: function() {
            this.inherited(arguments);

        },
        destroy: function() {
            this.inherited(arguments);
            var context = this;
        },
        setupContent: function(_setting) {
            var context = this;

            $(context.sliderNode).ionRangeSlider(_setting);
        },
        reset: function() {
            var context = this;

            $(context.sliderNode).data("ionRangeSlider").reset();
        },
        update: function(data) {
            var context = this;

            $(context.sliderNode).data("ionRangeSlider").update({
                from: data.from,
                to: data.to
            })
        }
    });
});
