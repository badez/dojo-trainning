define([
    "dojo/on",
    "dojo/topic",
    "dojo/dom-attr",
    "dojo/dom-style",
    "dojo/_base/array",
    "dojo/_base/declare",
    "dojo/dom-construct",
    "dojo/text!./html/Localization.html",
    "app/components/ui/cell/localization/LocalizationCell",
    "app/components/ui/element/BaseElement",
    "app/components/utils/Rest"
], function(
    v_on,
    v_topic,
    v_domAttr,
    v_domStyle,
    v_array,
    v_declare,
    v_domConstruct,
    v_template,
    v_LocalizationCell,
    v_BaseElement,
    v_Rest
) {
    return v_declare([v_BaseElement], {
        templateString: v_template,
        p_construct: function(setting) {
            var v_context = this;
            v_context.p_setupContent(setting);
            v_context.p_startListening(setting);
        },
        p_setupContent: function(v_setting) {
            var v_context = this;

            if ($.jStorage.get("app.login")) {
                v_context.p_loadLanguage();
            }
        },
        p_startListening: function(v_setting) {
            var v_context = this;
            var v_domNode = this.domNode;

            var v_loadLanguage = v_topic.subscribe("load.language", function() {
                v_context.loadLanguage();
            });
            v_topic.publish("topic.handlers.add", v_loadLanguage, v_domAttr.get(v_domNode, "id"));
        },
        p_loadLanguage: function() {
            var v_context = this;

            var v_prototypeUrl = "assets/data/localization_data.json";
            var v_dataPackage = {};
            v_dataPackage.baseFunctionCode = "KERN_SWITCH_LOCALE";
            v_dataPackage.baseModuleCode = "KERN";

            // var v_rest = new v_Rest().p_postapi({ p_api: "systemparam/locale" }, v_prototypeUrl, v_dataPackage);
            // v_rest.then(function(v_response) {

            var v_rest = new v_Rest().p_get(v_prototypeUrl);
            v_rest.then(function(v_response) {
                if (v_response.locale.length > 0) {
                    v_domConstruct.empty(v_context.localizationNode);

                    var v_localizationCell;
                    v_array.forEach(v_response.locale, function(single, index) {
                        v_localizationCell = new v_LocalizationCell();
                        v_localizationCell.placeAt(v_context.localizationNode);
                        v_localizationCell.p_construct(single);
                    });
                } else {
                    v_domStyle.set(v_context.localizationNode, "display", "none");
                }
            });
        }
    });
});
