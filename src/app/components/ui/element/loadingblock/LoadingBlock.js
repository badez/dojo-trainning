define([
    "dojo/_base/declare",
    "dojo/dom-attr",
    "dojo/text!./html/LoadingBlock.html",
    "app/components/ui/element/BaseElement"
], function(
    declare,
    domAttr,
    template,
    BaseElement
) {

    return declare("app.components.ui.element.loadingblock.LoadingBlock", [BaseElement], {
        templateString: template,
        p_construct: function(setting) {
            var domNode = this.domNode;
            var context = this;

            context.setupContent(setting);
        },
        setupContent: function(_setting) {
            var context = this;

            // $(context.loaderNode).attr("src", "assets/img/light_blue_material_design_loading.gif");
            domAttr.set(context.loaderNode, "src", "assets/img/light_blue_material_design_loading.gif");
        }
    });
});
