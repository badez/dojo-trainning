define([
    "dojo/_base/array",
    "dojo/_base/config",
    "dojo/_base/declare",
    "dojo/_base/window",
    "dojo/dom",
    "dojo/dom-attr",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/dom-style",
    "dojo/html",
    "dojo/i18n!app/locale/nls/functionName-bundle",
    "dojo/query",
    "dojo/text!./html/SideMenu.html",
    "dojo/topic",
    "../BaseElement",
    "../multitasktab/MultitaskTab",
    "../toolbarMenu/ToolbarMenu",
    "../../cell/sideMenu/MenuCell",
    "../../../utils/Rest"
], function (
    v_array,
    v_config,
    v_declare,
    v_win,
    v_dom,
    v_domAttr,
    v_domClass,
    v_domConstruct,
    v_domStyle,
    v_html,
    v_menu_i18n,
    v_query,
    v_template,
    v_topic,
    v_BaseElement,
    v_MultitaskTab,
    v_ToolbarMenu,
    v_MenuCell,
    v_Rest
) {
    return v_declare([v_BaseElement], {
        templateString: v_template,
        v_multiTaskTab: null,
        p_construct: function (v_setting) {
            this.inherited(arguments);
            var v_context = this;
            v_context.p_i18n();
            v_context.p_setupContent(v_setting);
        },
        destroy: function () {
            this.inherited(arguments);
            var v_context = this;
            if (v_context.v_multiTaskTab) {
                v_context.v_multiTaskTab.destroy();
            }
        },
        p_setupContent: function () {
            var v_context = this;
            if (!$.jStorage.get("app.login")) {
                v_context.destroy();
                return;
            }

            v_context.v_multiTaskTab = new v_MultitaskTab().placeAt(v_dom.byId("multitabContainer"), 0);
            v_context.v_multiTaskTab.p_construct({});

            var v_toolbarMenu = new v_ToolbarMenu("dropdown");
            v_toolbarMenu.p_construct({
                p_noIcon: true,
                p_localeMenu: false
            });
            v_toolbarMenu.placeAt(v_context.dropdownNode);

            var v_project = v_config.projLocation ? v_config.projLocation + "/" : "";
            var v_sideMenuJson = v_project + "assets/data/sidemenu.json";
            var v_rest;
            var v_isStatic = v_config.isExperimental ? true : v_config.menu === "static" ? true : false;
            if (v_isStatic) {
                v_rest = new v_Rest().p_get(v_sideMenuJson);
            } else {
                v_rest = new v_Rest().p_postapi({ p_api: "menu/getmenu" }, v_sideMenuJson);
            }

            v_rest.then(function (v_response) {
                var v_landingPageSetting = f_sideBarChild(v_response.childMenus, v_context.sideMenuNode, 1);

                // need to removed the metis menu before reinit. currently metismenu can only be load once.
                $(v_context.sideMenuNode).removeData("metisMenu");
                $(v_context.sideMenuNode).metisMenu();

                if (v_landingPageSetting) {
                    v_landingPageSetting.p_mainPage = true;
                    v_topic.publish("navigation.block.open", v_landingPageSetting.functionCode, v_landingPageSetting);
                }

                // Fixed Sidebar
                if (v_domClass.contains(v_win.body(), "fixed-sidebar")) {
                    $('.sidebar-collapse', v_context.domNode).slimScroll({
                        height: '100%',
                        railOpacity: 0.9
                    });
                }
            });

            function f_sideBarChild(v_children, v_domNode, v_level) {
                var v_levelArr = ["first", "second", "third"];

                  if (!v_config.isExperimental) {
                    v_children = v_array.filter(v_children, function(v_single) {
                        return (v_single.isOnlineMenu === undefined || v_single.isOnlineMenu === true);
                    });
                }

                var v_landingPageSetting;
                v_array.forEach(v_children, function (v_single) {
                    if (v_single.functionCode) {
                        v_single.functionName = v_menu_i18n[v_single.functionCode] ? v_menu_i18n[v_single.functionCode] : v_single.functionName;
                    }
                    if (v_single.menuCode) {
                        v_single.menuName = v_menu_i18n[v_single.menuCode] ? v_menu_i18n[v_single.menuCode] : v_single.menuName;
                    }

                    if (v_config.landingPage && v_config.landingPage === v_single.functionCode) {
                        v_landingPageSetting = v_single;
                    }

                    var v_menuCell = new v_MenuCell();
                    v_menuCell.p_construct(v_single);
                    var v_landingPageSettingTemp;

                    if (v_single.childMenus) {
                        var v_unorderedList = v_domConstruct.create("ul", { "class": "nav nav-" + v_levelArr[v_level] + "-level collapse" });
                        v_domConstruct.place(v_unorderedList, v_menuCell.domNode);
                        v_landingPageSettingTemp = f_sideBarChild(v_single.childMenus, v_unorderedList, v_level + 1);
                    }

                    v_menuCell.placeAt(v_domNode);

                    if (v_landingPageSettingTemp) {
                        v_landingPageSetting = v_landingPageSettingTemp;
                    }
                });

                return v_landingPageSetting;
            }
        },
        p_i18n: function () {
            var v_context = this;

            if ($.jStorage.get("app.login.username") != undefined) {
                v_html.set(v_context.userIdNode, $.jStorage.get("app.login.username") + " ");
            }
            if ($.jStorage.get("app.login.userid") != undefined) {
                v_html.set(v_context.usernameNode, $.jStorage.get("app.login.userid") + " ");
            }
        }
    });
});
