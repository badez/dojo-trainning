define([
    "dojo/_base/declare",
    "dojo/text!./html/DateRangePicker.html",
    "dojo/text!./html/DateRangePickerSide.html",
    "dojo/dom-attr",
    "dojo/topic",
    "dojo/dom-class",
    "dojo/on",
    "dojo/_base/config",
    "dojo/html",
    "dojo/dom-style",
    "dijit/_TemplatedMixin",
    "app/components/ui/element/BaseElement",
    "dojo/i18n!app/locale/nls/common"
], function(
    declare,
    template,
    template_side,
    domAttr,
    topic,
    domClass,
    on,
    config,
    html,
    domStyle,
    TemplatedMixin,
    BaseElement,
    i18n_common
) {
    return declare("app.components.ui.element.daterangepicker.DateRangePicker", [BaseElement, TemplatedMixin], {
        templateString: template,
        fromDate: null,
        toDate: null,
        p_construct: function(setting) {
            var domNode = this.domNode;
            var context = this;

            context.startListening(setting);
            context.setupContent(setting);
        },
        constructor: function(setting) {
            var context = this;
            switch (setting) {
                case "side":
                    context.templateString = template_side;
                    break;

                default:
                    context.templateString = template;
                    break;
            }
        },
        setupContent: function(_setting) {
            var context = this;
            var domNode = this.domNode;
            var isDefaultSet = false;

            var dateFormat = config.dayFormat + config.dateSeparator + config.monthFormat + config.dateSeparator +
                config.yearFormat;

            domAttr.set(context.fromDateNode, "placeholder", dateFormat);
            domAttr.set(context.toDateNode, "placeholder", dateFormat);

            if (_setting.fromDateValidation) {
                domClass.add(context.labelFromNode, "required");
                domAttr.set(context.inputFromNode, "data-validation", _setting.fromDateValidation);
                domAttr.set(context.inputFromNode, "data-target", "datepicker|" + context.id);
            }

            if (_setting.toDateValidation) {
                domClass.add(context.labelToNode, "required");
                domAttr.set(context.inputToNode, "data-validation", _setting.toDateValidation);
                domAttr.set(context.inputToNode, "data-target", "datepicker|" + context.id);
            }

            _setting.fromDateVisible = _setting.fromDateVisible == undefined ? true : _setting.fromDateVisible;
            _setting.toDateVisible = _setting.toDateVisible == undefined ? true : _setting.toDateVisible;

            _setting.fromDateLabel ? html.set(context.labelFromNode, _setting.fromDateLabel) : "";
            _setting.toDateLabel ? html.set(context.labelToNode, _setting.toDateLabel) : "";

            // FORMAT
            // d, dd: Numeric date, no leading zero and leading zero, respectively. Eg, 5, 05.
            // D, DD: Abbreviated and full weekday names, respectively. Eg, Mon, Monday.
            // m, mm: Numeric month, no leading zero and leading zero, respectively. Eg, 7, 07.
            // M, MM: Abbreviated and full month names, respectively. Eg, Jan, January
            // yy, yyyy: 2- and 4-digit years, respectively. Eg, 12, 2012.

            // refer here for extra options 
            // http://eternicode.github.io/bootstrap-datepicker/

            if (_setting.options == undefined) {
                _setting.options = {};
            }

            var monthFormat = config.monthFormat === "M" ? "m" : config.monthFormat === "MM" ? "mm" : config.monthFormat ===
                "MMM" ? "M" : "MM";
            var datepickerFormat = config.dayFormat + config.dateSeparator + monthFormat + config.dateSeparator +
                config.yearFormat;

            _setting.options.format = datepickerFormat;
            _setting.options.clearBtn = true;
            _setting.options.autoclose = true;
            _setting.options.todayHighlight = true;
            _setting.options.orientation = "auto center";

            function changeDate(evt, inputNode, dateNode, setter) {
                if (evt.date == undefined) {
                    domAttr.set(inputNode, "value", "");
                } else {
                    domAttr.set(inputNode, "value", evt.date.getTime());
                }

                var date = domAttr.get(inputNode, "value");

                if (date !== "") {
                    $(dateNode).datepicker(setter, new Date(Number(date)));
                } else {
                    $(dateNode).datepicker(setter, false);
                }

                $(inputNode).trigger("change");

                topic.publish("daterangepicker.selected.retrieve." + _setting.dateRangePickerId, {
                    fromDate: domAttr.get(context.inputFromNode, "value"),
                    toDate: domAttr.get(context.inputToNode, "value")
                });
                context.fromDate = domAttr.get(context.inputFromNode, "value");
                context.toDate = domAttr.get(context.inputToNode, "value");
            }

            $(context.fromNode).datepicker(_setting.options).on("hide", function(evt) {}).on("changeDate", function(evt) {
                changeDate(evt, context.inputFromNode, context.toNode, "setStartDate");
            });

            $(context.toNode).datepicker(_setting.options).on("hide", function(evt) {}).on("changeDate", function(evt) {
                changeDate(evt, context.inputToNode, context.fromNode, "setEndDate");
            });
            if (_setting.defaultFromDate) {
                $(context.fromNode).datepicker("setDate", _setting.defaultFromDate);
                $(context.toNode).datepicker("setStartDate", _setting.defaultFromDate);
            }
            if (_setting.defaultToDate) {
                $(context.toNode).datepicker("setDate", _setting.defaultToDate);
                $(context.fromNode).datepicker("setEndDate", _setting.defaultToDate);
            }

            !_setting.fromDateVisible ? domStyle.set(context.fromContainerNode, "display", "none") : "";
            !_setting.toDateVisible ? domStyle.set(context.toContainerNode, "display", "none") : "";

            context.fromDate = domAttr.get(context.inputFromNode, "value");
            context.toDate = domAttr.get(context.inputToNode, "value");
        },
        startListening: function(_setting) {
            var context = this;
            var domNode = this.domNode;

            var getDateHandler = topic.subscribe("daterangepicker.retrieve.date." + _setting.dateRangePickerId,
                function(data) {
                    var date = {};
                    date.fromDate = context.fromDate;
                    date.toDate = context.toDate;
                    data.callBack(date);
                });
            topic.publish("topic.handlers.add", getDateHandler, domAttr.get(domNode, "id"));

            var setDatesHandler = topic.subscribe("daterangepicker.setdates", function(_id, setting) {
                if (_setting.dateRangePickerId === _id) {
                    $(context.fromNode).datepicker("setDate", setting.fromDate);
                    $(context.toNode).datepicker("setStartDate", setting.fromDate);
                    $(context.toNode).datepicker("setDate", setting.toDate);
                    $(context.fromNode).datepicker("setEndDate", setting.toDate);
                }
                context.fromDate = domAttr.get(context.inputFromNode, "value");
                context.toDate = domAttr.get(context.inputToNode, "value");
            });
            topic.publish("topic.handlers.add", setDatesHandler, domAttr.get(domNode, "id"));

            var resetHandler = topic.subscribe("reset.field", function(_id) {
                if (_setting.dateRangePickerId === _id) {
                    domClass.remove(domNode, "input-error");
                    domClass.remove(domNode, "input-valid");

                    domAttr.set(context.inputFromNode, "value", "");
                    domAttr.set(context.inputToNode, "value", "");

                    if (_setting.defaultFromDate) {
                        $(context.fromNode).datepicker("setDate", _setting.defaultFromDate);
                        $(context.toNode).datepicker("setStartDate", _setting.defaultFromDate);
                    } else {
                        $(context.fromNode).datepicker("clearDate");
                    }
                    if (_setting.defaultToDate) {
                        $(context.toNode).datepicker("setDate", _setting.defaultToDate);
                        $(context.fromNode).datepicker("setEndDate", _setting.defaultToDate);
                    } else {
                        $(context.toNode).datepicker("clearDate");
                    }

                    topic.publish("daterangepicker.selected.retrieve." + _setting.dateRangePickerId, {
                        fromDate: domAttr.get(context.inputFromNode, "value"),
                        toDate: domAttr.get(context.inputToNode, "value")
                    });
                    context.fromDate = domAttr.get(context.inputFromNode, "value");
                    context.toDate = domAttr.get(context.inputToNode, "value");
                }
            });
            topic.publish("topic.handlers.add", resetHandler, domAttr.get(domNode, "id"));
        }
    });
});
