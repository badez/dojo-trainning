define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/text!./html/Datepicker.html",
    "dojo/text!./html/DatepickerRange.html",
    "dojo/dom-attr",
    "dojo/topic",
    "dojo/dom-class",
    "dojo/on",
    "dojo/query",
    "dojo/dom-style",
    "dojo/_base/config",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "app/components/ui/element/BaseElement"
], function(
    declare,
    lang,
    template,
    template_range,
    domAttr,
    topic,
    domClass,
    on,
    query,
    domStyle,
    config,
    _Widget,
    TemplatedMixin,
    BaseElement
) {
    return declare("app.components.ui.element.datepicker.Datepicker", [BaseElement, TemplatedMixin], {
        templateString: template,
        p_construct: function(setting) {
            var domNode = this.domNode;
            var context = this;
            var duplicate = lang.clone(setting);

            context.startListening(duplicate);
            context.initInteraction(duplicate);
            context.setupContent(duplicate);
        },
        constructor: function(setting) {
            var context = this;

            switch (setting) {
                case "range":
                    context.templateString = template_range;
                    break;

                default:
                    context.templateString = template;
                    break;
            }
        },
        startup: function() {
            this.inherited(arguments);

        },
        destroy: function() {
            this.inherited(arguments);
        },
        setupContent: function(_setting) {
            var context = this;
            var domNode = this.domNode;
            var isDefaultSet = false;

            query("button", domNode).forEach(function(input) {
                domStyle.set(input, "border", "transparent");
                domStyle.set(input, "background-color", "transparent");
                domStyle.set(input, "padding", "8px 12px");
            });

            var dateFormat = config.dayFormat + config.dateSeparator + config.monthFormat + config.dateSeparator + config.yearFormat;
            var placeholder = _setting.placeholder? _setting.placeholder : dateFormat;
            switch (_setting.type) {
                case "range":
                    domAttr.set(context.toDateNode, "placeholder", placeholder);
                    domAttr.set(context.fromDateNode, "placeholder", placeholder);
                    break;

                default:
                    domAttr.set(context.dateNode, "placeholder", placeholder);
                    break;
            }

            var monthFormat = config.monthFormat === "M" ? "m" : config.monthFormat === "MM" ? "mm" : config.monthFormat ===
                "MMM" ? "M" : "MM";
            var datepickerFormat = config.dayFormat + config.dateSeparator + monthFormat + config.dateSeparator + config.yearFormat;

            //                    FORMAT
            //                    d, dd: Numeric date, no leading zero and leading zero, respectively. Eg, 5, 05.
            //                    D, DD: Abbreviated and full weekday names, respectively. Eg, Mon, Monday.
            //                    m, mm: Numeric month, no leading zero and leading zero, respectively. Eg, 7, 07.
            //                    M, MM: Abbreviated and full month names, respectively. Eg, Jan, January
            //                    yy, yyyy: 2- and 4-digit years, respectively. Eg, 12, 2012.

            //                    refer here for extra options 
            //                    http://eternicode.github.io/bootstrap-datepicker/

            if (_setting.options == undefined)
                _setting.options = {};

            if (_setting.required != undefined && _setting.required) {
                domAttr.set(context.inputNode, "data-validation", "required");
                domAttr.set(context.inputNode, "data-target", "datepicker|" + context.id);
            } else {
                domAttr.set(context.inputNode, "data-validation", "optional");
                domAttr.set(context.inputNode, "data-target", "datepicker|" + context.id);
            }

            _setting.options.format = datepickerFormat;
            _setting.options.clearBtn = true;
            _setting.options.autoclose = true;
            _setting.options.todayHighlight = true;
            _setting.options.orientation = "auto left";

            //                    var datepicker = $.fn.datepicker.noConflict(); // return $.fn.datepicker to previously assigned value
            //                    $.fn.bootstrapDP = datepicker;

            $(context.datePickerNode).datepicker(_setting.options)
                .on("hide", function(e) {
                    if (e.target.name == "to" || e.target.name == "from")
                        _setting.target = e.target.name;

                    if (e.date != undefined)
                        _setting.timestamp = e.date.getTime();
                    else
                        _setting.timestamp = null;

                    if (_setting.required != undefined && _setting.required && e.date == undefined)
                        domAttr.set(context.inputNode, "value", "");

                    topic.publish("datepicker.selected.retrieve." + _setting.parentid, lang.clone(_setting));
                })
                .on("changeDate", function(e) {
                    switch (_setting.type) {
                        case "range":
                            if (e.target.name != "to") {
                                $(context.toDateNode).datepicker({
                                    autoclose: true
                                }).datepicker('setStartDate', e.date);
                            } else {
                                $(context.fromDateNode).datepicker({
                                    autoclose: true
                                }).datepicker('setEndDate', e.date);
                            }
                            break;
                        default:
                            break;
                    }

                    if (isDefaultSet && e.date == undefined) {
                        switch (_setting.type) {
                            case "range":
                                if (e.target.name == "to") {
                                    $(context.toDateNode).datepicker('update', _setting.options.defaultDates.date2);
                                    e.date = _setting.options.defaultDates.date2;
                                } else {
                                    $(context.fromDateNode).datepicker('update', _setting.options.defaultDates.date1);
                                    e.date = _setting.options.defaultDates.date1;
                                }
                                break;

                            default:
                                $(context.datePickerNode).datepicker('update', _setting.options.defaultDates.date1);
                                e.date = _setting.options.defaultDates.date1;
                                break;
                        }
                    }

                    if (e.target.name == "to" || e.target.name == "from")
                        _setting.target = e.target.name;

                    if (e.date == undefined)
                        _setting.timestamp = null;
                    else
                        _setting.timestamp = e.date.getTime();

                    if (_setting.required != undefined && _setting.required)
                        domAttr.set(context.inputNode, "value", "required");

                    topic.publish("datepicker.selected.retrieve." + _setting.parentid, lang.clone(_setting));
                })
                .on("hide", function(e) {
                    domAttr.set(context.inputNode, "value", domAttr.get(context.inputNode, "value"));
                    $(context.inputNode).trigger("change");
                });

            if (_setting.options.defaultDates != undefined) {
                isDefaultSet = true;

                //set default values
                switch (_setting.type) {
                    case "range":
                        $(context.toDateNode).datepicker('setDate', _setting.options.defaultDates.date2);
                        $(context.fromDateNode).datepicker('setDate', _setting.options.defaultDates.date1);
                        break;

                    default:
                        $(context.datePickerNode).datepicker('setDate', _setting.options.defaultDates.date1);
                        break;
                }
            }
        },
        initInteraction: function(_setting) {
            var context = this;
            var domNode = this.domNode;
        },
        startListening: function(_setting) {
            var context = this;
            var domNode = this.domNode;

            var resetHandler = topic.subscribe("reset.field", function(_id) {
                if (domAttr.get(context.inputNode, "id") == _id) {
                    domClass.remove(domNode, "input-error");
                    domClass.remove(domNode, "input-valid");
                    switch (_setting.type) {
                        case "range":
                            domAttr.set(context.toDateNode, "value", "");
                            domAttr.set(context.fromDateNode, "value", "");
                            break;

                        default:
                            $(context.datePickerNode).datepicker('clearDates');
                            break;
                    }
                    //                            topic.publish("datepicker.selected.retrieve." + _setting.parentid, _setting);
                }
            });
            topic.publish("topic.handlers.add", resetHandler, domAttr.get(domNode, "id"));

            var resetWithIdHandler = topic.subscribe("reset.field." + _setting.datepickerid, function() {
                domClass.remove(domNode, "input-error");
                domClass.remove(domNode, "input-valid");
                switch (_setting.type) {
                    case "range":
                        domAttr.set(context.toDateNode, "value", "");
                        domAttr.set(context.fromDateNode, "value", "");
                        break;

                    default:
                        $(context.datePickerNode).datepicker('clearDates');
                        break;
                        //                            topic.publish("datepicker.selected.retrieve." + _setting.parentid, _setting);
                }
            });
            topic.publish("topic.handlers.add", resetWithIdHandler, domAttr.get(domNode, "id"));

            var enableHandler = topic.subscribe("datepicker.enable." + _setting.datepickerid,
                function(_flag) {
                    context.enable(_flag);
                });
            topic.publish("topic.handlers.add", enableHandler, domAttr.get(domNode, "id"));

            var datepickerSetValueHandler = topic.subscribe("datepicker.select.value." + _setting.datepickerid,
                function(value) {
                    switch (_setting.type) {
                        case "range":
                            $(context.toDateNode).datepicker('setDate', new Date(Number(value.date2)));
                            $(context.fromDateNode).datepicker('setDate', new Date(Number(value.date1)));
                            break;

                        default:
                            $(context.datePickerNode).datepicker('setDate', new Date(Number(value.date1)));
                            break;
                    }
                    $(context.inputNode).trigger("change");
                });
            topic.publish("topic.handlers.add", datepickerSetValueHandler, domAttr.get(domNode, "id"));
        },
        enable: function(_flag) {
            var context = this;
            var domNode = this.domNode;

            domClass.remove(domNode, "input-error");
            domClass.remove(domNode, "input-valid");

            context.state = _flag;

            if (_flag) {
                query("input[disabled]", domNode).forEach(function(input) {
                    domAttr.remove(input, "disabled");
                });
                query("button[disabled]", domNode).forEach(function(input) {
                    domAttr.remove(input, "disabled");
                    domStyle.set(input, "padding", "14px 15px");
                    domStyle.set(input.parentNode, "border", "");
                });
            } else {
                query("input", domNode).forEach(function(input) {
                    domAttr.set(input, "disabled", "");
                });
                query("button", domNode).forEach(function(input) {
                    domAttr.set(input, "disabled", "");
                    domStyle.set(input, "padding", "8px 12px");
                    domStyle.set(input.parentNode);
                });
            }
        }
    });

});
