define([
    "dojo/_base/declare",
    "dojo/text!./html/Steps.html",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "app/components/ui/blocks/BaseBlock",
    "app/components/ui/element/steps/StepsCell",
    "app/components/utils/General"
], function(
    declare,
    template,
    _Widget,
    TemplatedMixin,
    BaseBlock,
    StepsCell,
    GeneralUtils
) {

    return declare("app.components.ui.element.steps.Steps", [BaseBlock, TemplatedMixin], {
        templateString: template,
        p_construct: function(setting) {
            var context = this;
            context.setupContent(setting);
        },
        postCreate: function postCreate() {
            this.inherited(arguments);
        },
        startup: function() {
            this.inherited(arguments);

            var domNode = this.domNode;
            var context = this;
        },
        destroy: function() {
            this.inherited(arguments);
        },
        setupContent: function(_setting) {
            var context = this;
            var count = 1;

            html.set(context.stepLabelNode, i18n_common.step + _setting.current_step + ":");

            _setting.current_step = _setting.current_step - 1;
            html.set(context.descLabelNode, _setting.data[_setting.current_step].content);

            for (var i = 0; i < _setting.data.length; i++) {
                _setting.data[i].status = "";
                _setting.data[i].length = _setting.data.length;

                var stepsCell = new StepsCell().placeAt(context.stepsNode);

                if (_setting.current_step == i)
                    stepsCell.p_construct("active");
                else if (i < _setting.current_step)
                    stepsCell.p_construct("done");
            }
        }
    });

});
