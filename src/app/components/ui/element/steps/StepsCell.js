define([
    "dojo/_base/declare",
    "dojo/text!./html/StepsCell.html",
    "dojo/dom-class",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "app/components/utils/General"
], function(
    declare,
    template,
    domClass,
    _Widget,
    TemplatedMixin,
    GeneralUtils
) {

    return declare("app.components.ui.element.steps.StepsCell", [_Widget, TemplatedMixin], {
        templateString: template,
        p_construct: function(setting) {
            var context = this;

            context.setupContent(setting);
        },
        postMixInProperties: function() {
            this.inherited(arguments);
        },
        postCreate: function postCreate() {
            this.inherited(arguments);
        },
        startup: function() {
            this.inherited(arguments);

            var domNode = this.domNode;
            var context = this;
        },
        destroy: function() {
            this.inherited(arguments);
        },
        setupContent: function(_setting) {
            var domNode = this.domNode;
            var context = this;

            domClass.add(context.circleNode, _setting);
        }
    });

});
