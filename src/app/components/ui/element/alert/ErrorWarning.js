define([
    "dojo/_base/declare",
    "dojo/dom-class",
    "dojo/text!./html/ErrorWarning.html",
    "dojo/html",
    "app/components/utils/BaseUtils"
], function (
    v_declare,
    v_domClass,
    v_template,
    v_html,
    v_BaseElement
) {
    return v_declare("ErrorWarning", [v_BaseElement], {
        templateString: v_template,
        p_construct: function (v_setting) {
            var v_context = this;
            v_context.p_setupContent(v_setting);
        },
        p_setupContent: function (v_setting) {
            var v_context = this;
            var v_domNode = v_context.domNode;

            //only show one error
            v_html.set(v_context.errorContainerNode, v_setting.errorArray);

            v_domClass.add(v_domNode, "input-error-message " + v_setting.unique_identifier);
        }
    });

});
