define([
    "dojo/dom-attr",
    "dojo/_base/array",
    "dojo/dom-class",
    "dojo/_base/declare",
    "dojo/_base/array",
    "dojo/text!./html/TimeFrame.html",
    "dijit/_TemplatedMixin",
    "app/components/ui/element/BaseElement",
    "app/components/ui/element/timeframe/TimeFrameCell",
    "app/components/utils/General"
], function(
    domAttr,
    array,
    domClass,
    declare,
    array,
    template,
    TemplatedMixin,
    BaseElement,
    TimeFrameCell,
    GeneralUtils
) {
    return declare("app.components.ui.element.timeframe.TimeFrame", [BaseElement, TemplatedMixin], {
        templateString: template,
        p_construct: function(setting) {
            var domNode = this.domNode;
            var context = this;

            context.setupContent(setting);
        },
        startup: function() {
            this.inherited(arguments);

            var domNode = this.domNode;
            var context = this;
        },
        destroy: function() {
            this.inherited(arguments);
        },
        setupContent: function(_setting) {
            var context = this;
            var domNode = this.domNode;

            if (_setting.timeFrameClass) {
                domClass.add(context.cellNode, _setting.timeFrameClass);
            }

            if (_setting.timeFrameAttr) {
                array.forEach(_setting.timeFrameAttr, function(single) {
                    domAttr.set(context.cellNode, single.attrName, single.attrData);
                });
            }

            if (_setting.cellArr) {
                array.forEach(_setting.cellArr, function(single) {
                    single.placeAt(context.cellNode);
                });
            } else {
                array.forEach(_setting.data, function(single) {
                    var timeFrameCell = new TimeFrameCell().placeAt(context.cellNode);
                    timeFrameCell.p_construct(single);
                });
            }
        }
    });
});
