define([
    "dojo/_base/declare",
    "dojo/_base/array",
    "dojo/dom-attr",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/html",
    "dojo/i18n!app/locale/nls/timeframe",
    "dojo/on",
    "dojo/dom-style",
    "dojo/text!./html/TimeFrameCellV3.html",
    "dijit/_TemplatedMixin",
    "app/components/ui/cell/BaseCell",
    "app/components/utils/General"
], function(
    declare,
    array,
    domAttr,
    domClass,
    domConstruct,
    html,
    i18n,
    on,
    domStyle,
    template,
    TemplatedMixin,
    BaseCell,
    GeneralUtils
) {

    return declare("app.components.ui.element.timeframe.TimeFrameCellV3", [BaseCell, TemplatedMixin], {
        templateString: template,
        p_construct: function(setting) {
            var domNode = this.domNode;
            var context = this;

            context.i18n(setting);
            context.setupContent(setting);
        },
        setupContent: function(_setting) {
            var context = this;
            var domNode = this.domNode;

            if (_setting.titleUnderlined) {
                var domLine = domConstruct.create("hr");
                domConstruct.place(domLine, context.titleDivNode);

                domClass.add(context.contentNode, "add-line");
            }

            if (_setting.title) {
                html.set(context.titleNode, _setting.title);
            }

            if (_setting.description) {
                html.set(context.descrNode, _setting.description);
            }

            if (_setting.icon) {
                if (_setting.icon.img) {
                    domClass.remove(context.iconNode, "fa fa-briefcase");
                    domClass.add(context.iconNode, _setting.icon.img);
                }

                if (_setting.icon.bgColor) {
                    domStyle.set(context.iconDivNode, "background-color", _setting.icon.bgColor);
                }
            }

            if (_setting.date) {
                var date = new GeneralUtils().formatDate(_setting.date, { display: "date" });
                var time = new GeneralUtils().formatDate(_setting.date, { display: "time" });

                html.set(context.dateNode, date);
                html.set(context.timeNode, time);
            }

            if (_setting.info) {
                var domTable = domConstruct.create("table", { style: "width: 100%;" });
                array.forEach(_setting.info, function(detail) {
                    var domRow = domConstruct.create("tr");
                    domConstruct.place(domRow, domTable);

                    var domCellLabel = domConstruct.create("td", { "class": "text-left", style: "width: 50%;" });
                    domCellLabel.innerHTML = detail.label;
                    domConstruct.place(domCellLabel, domRow);

                    var domCellData = domConstruct.create("td", { "class": "text-left font-bold", style: "width: 50%;" });
                    domCellData.innerHTML = detail.data;
                    domConstruct.place(domCellData, domRow);
                });
                domConstruct.place(domTable, context.extraNode);
            }

            if (_setting.buttons) {
                if (_setting.buttons.label) {
                    html.set(context.btnNode, _setting.buttons.label);
                }

                if (_setting.buttons.position) {
                    switch (_setting.buttons.position) {
                        case "right":
                            domClass.add(context.btnNode, "btn-right");
                            break;
                    }
                }

                on(context.btnNode, "click", function() {
                    _setting.buttons.action(_setting);
                });
            }
        },
        i18n: function(_setting) {
            var context = this;

            html.set(context.titleNode, i18n.defaulttitle);
            html.set(context.btnNode, i18n.defaultinfo);
            html.set(context.dateNode, i18n.defaultday);
            html.set(context.timeNode, i18n.defaultdate);

        }
    });

});
