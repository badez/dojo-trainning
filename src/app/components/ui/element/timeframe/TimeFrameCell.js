define([
    "dojo/_base/declare",
    "dojo/text!./html/TimeFrameCell.html",
    "dojo/dom-attr",
    "dojo/html",
    "dijit/_TemplatedMixin",
    "app/components/ui/cell/BaseCell",
    "app/components/utils/General"
], function(
    declare,
    template,
    domAttr,
    html,
    TemplatedMixin,
    BaseCell,
    GeneralUtils
) {

    return declare("app.components.ui.navigation.timeframe.TimeFrameCell", [BaseCell, TemplatedMixin], {
        templateString: template,
        p_construct: function(setting) {
            var domNode = this.domNode;
            var context = this;

            context.initInteraction(setting);
            context.setupContent(setting);
            // context.i18n(setting);
        },
        startup: function() {
            this.inherited(arguments);

            var domNode = this.domNode;
            var context = this;
        },
        destroy: function() {
            this.inherited(arguments);
        },
        initInteraction: function(_setting) {
            var context = this;
            var domNode = this.domNode;
            var domNodeId = domAttr.get(domNode, "id");

        },
        setupContent: function(_setting) {
            var context = this;
            var domNode = this.domNode;

            var optionDate = {
                display: "date"
            };
            var date = new GeneralUtils().formatDate(_setting.date, optionDate);

            var optionTime = {
                display: "time"
            };
            var time = new GeneralUtils().formatDate(_setting.date, optionTime);


            html.set(context.dateNode, date);
            html.set(context.timeNode, time);
            html.set(context.labelNode, _setting.label);
            html.set(context.titleNode, _setting.title);


        },
        i18n: function(_setting) {
            var context = this;
        }
    });

});
