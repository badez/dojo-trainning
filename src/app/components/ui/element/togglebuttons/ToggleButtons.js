define([
    "app/components/ui/element/BaseElement",
    "dijit/_TemplatedMixin",
    "dojo/_base/array",
    "dojo/_base/declare",
    "dojo/dom-attr",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/on",
    "dojo/i18n!app/locale/nls/common",
    "dojo/html",
    "dojo/text!./html/ToggleButtons.html"
], function(
    BaseElement,
    TemplatedMixin,
    array,
    declare,
    domAttr,
    domClass,
    domConstruct,
    on,
    i18n,
    html,
    template
) {
    return declare("app.components.ui.element.togglebutton.ToggleButtons", [BaseElement, TemplatedMixin], {
        templateString: template,
        selectedKey: null,
        keyArray: null,
        keyMap: null,
        p_construct: function(setting) {
            var context = this;
            var domNode = context.domNode;

            context.setupContent(setting);
        },
        setupContent: function(_setting) {
            var context = this;
            context.keyMap = {};
            context.selectedKey = [];
            context.keyArray = [];
            array.forEach(_setting.args, function(arg) {
                context.addToggleButton(_setting, arg);
            });

            if(_setting.args && _setting.args.length > 0){
                domAttr.set(context.wrapperNode, "class", "btn-group");
            }
        },
        addToggleButton: function(_setting, param) {
            var context = this;
            var button = domConstruct.create("button", { type: "button", "class": "btn btn-default" }, context.wrapperNode);
            if (param.text) {
                html.set(button, param.text);
            }
            if (param.createDisplay) {
                param.createDisplay(button);
            }
            on(button, "click", function(event) {
                context.select(param.key);
                if (param.onClick) {
                    param.onClick(event);
                }
            });
            if(param.enable === false){
                domAttr.set(button, "disabled", null);
            }
            context.keyArray.push(param.key);
            context.keyMap[param.key] = {
                button: button,
                enable: function(enable) {
                    if (!enable) {
                        domAttr.set(button, "disabled", null);
                    } else {
                        domAttr.remove(button, "disabled");
                    }
                },
                select: function() {
                    context.addSelection(_setting, param.key);
                    if(param.onSelect){
                        param.onSelect(param.key);
                    }
                }
            }
        },
        //============================= to be call by use case (start)=============================
        select: function(key) {
            var context = this;
            context.keyMap[key].select();
        },
        enable: function(key, enable) {
            var context = this;
            if(key !== null){
                context.keyMap[key].enable(enable);
            } else {
                array.forEach(context.keyArray, function(key){
                    context.keyMap[key].enable(enable);
                });
            }
        },
        reset: function() {
            var context = this;
            context.selectedKey = [];
            array.forEach(context.keyArray, function(key){
                domClass.remove(context.keyMap[key].button, "btn-primary");
                domClass.add(context.keyMap[key].button, "btn-default");
            });
        },
        //============================= to be call by use case (end)=============================
        addSelection: function(_setting, key) {
            var context = this;
            var hasBeenSelected = false;
            array.some(context.selectedKey, function(selectedKey, i) {
                hasBeenSelected = (selectedKey == key);
                return (selectedKey == key);
            });
            if (!hasBeenSelected) {
                context.selectedKey.push(key);
                if (_setting.selectedPerTime && context.selectedKey.length > _setting.selectedPerTime) {
                    context.removeSelection(null);
                }
                domClass.add(context.keyMap[key].button, "btn-primary");
                domClass.remove(context.keyMap[key].button, "btn-default");
            }else{
                if(_setting.toggleOnSelect === undefined || _setting.toggleOnSelect){
                    context.removeSelection(key);
                }
            }
        },
        removeSelection: function(key) {
            var context = this;
            var removeIndex;
            if (key != null) {
                array.some(context.selectedKey, function(selectedKey, i) {
                    removeIndex = i;
                    return (selectedKey == key);
                });
            } else {
                removeIndex = 0;
            }
            domClass.remove(context.keyMap[context.selectedKey[removeIndex]].button, "btn-primary");
            domClass.add(context.keyMap[context.selectedKey[removeIndex]].button, "btn-default");
            context.selectedKey.splice(removeIndex, 1);
        }
    });
});
