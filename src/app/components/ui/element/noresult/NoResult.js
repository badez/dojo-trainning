/*note: NoResult component can be used by usecase on other common component*/
define([
    "app/components/ui/element/BaseElement",
    "dijit/_TemplatedMixin",
    "dojo/_base/declare",
    "dojo/i18n!app/locale/nls/common",
    "dojo/html",
    "dojo/text!./html/NoResult.html"
], function(
    BaseElement,
    TemplatedMixin,
    declare,
    i18n,
    html,
    template
) {
    return declare("app.components.ui.element.noresult.NoResult", [BaseElement, TemplatedMixin], {
        templateString: template,
        p_construct: function(setting) {
            var domNode = this.domNode;
            var context = this;

            context.i18n(setting);
        },
        i18n: function(_setting) {
            var context = this;

            _setting.noResultTitle = _setting.noResultTitle ? _setting.noResultTitle : i18n.noResultTitle;
            _setting.noResultDesc = _setting.noResultDesc ? _setting.noResultDesc : i18n.noResultDesc;

            html.set(context.noresultTitleNode, _setting.noResultTitle);
            html.set(context.noresultLabelNode, _setting.noResultDesc);
        }
    });
});
