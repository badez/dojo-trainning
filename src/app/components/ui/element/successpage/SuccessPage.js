define([
    "dojo/_base/declare",
    "dojo/text!./html/SuccessPage.html",
    "dojo/_base/array",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/dom-style",
    "dojo/on",
    "dojo/html",
    "dijit/_TemplatedMixin",
    "app/components/ui/element/BaseElement"
], function(
    declare,
    template,
    array,
    domClass,
    domConstruct,
    domStyle,
    on,
    html,
    TemplatedMixin,
    BaseElement
) {

    return declare("app.components.ui.element.successpage.SuccessPage", [BaseElement, TemplatedMixin], {
        templateString: template,
        p_construct: function(setting) {
            this.inherited(arguments);

            var domNode = this.domNode;
            var context = this;

            context.setupContent(setting);
            context.initInteraction(setting);
        },
        initInteraction: function(_setting) {
            var context = this;
            var domNode = this.domNode;
        },
        setupContent: function(_setting) {
            var context = this;
            var domNode = this.domNode;

            var isFail = _setting.isFail ? _setting.isFail : false;

            // set header
            if (isFail) {
                domClass.replace(context.headerNode, "red-bg", "navy-bg");
                domClass.replace(context.iconNode, "fa-close", "fa-check");



                _setting.title = _setting.title ? _setting.title : "Unsuccessful";
            } else {


                _setting.title = _setting.title ? _setting.title : "Success";
            }

            // set content
            html.set(context.titleNode, _setting.title);
            html.set(context.descriptionNode, _setting.description ? _setting.description : "");
            if (_setting.extraDom) {
                domConstruct.place(_setting.extraDom, context.extraNode);
            } else if (_setting.details) {
                var domTable = domConstruct.create("table", { style: "width: 100%" });
                array.forEach(_setting.details, function(detail) {
                    var domRow = domConstruct.create("tr");
                    domConstruct.place(domRow, domTable);

                    var domCellTitle = domConstruct.create("td", { style: "text-align: left" });
                    domCellTitle.innerHTML = detail.title;
                    domConstruct.place(domCellTitle, domRow);

                    var domCellValue = domConstruct.create("td", { style: "text-align: right" });
                    domCellValue.innerHTML = detail.data;
                    domConstruct.place(domCellValue, domRow);
                });

                domConstruct.place(domTable, context.extraNode);
            } else {
                domStyle.set(context.extraNode, "display", "none");
            }

            // create button
            var arrButtons = [];

            if (_setting.buttons) {
                array.forEach(_setting.buttons, function(button) {
                    var buttonDom = domConstruct.create("button", {
                        className: "btn rippler " +
                            (button.className ? button.className : "") +
                            (button.type === "primary" ? " btn-primary" : (button.type === "secondary" ? " btn-default rippler-inverse" : "")),
                        innerHTML: button.text,
                        style: {
                            margin: "0px 1.5px"
                        },
                        type: "button"
                    }, context.buttonNode);
                    on(buttonDom, "click", function(event) { button.action() });
                    arrButtons.push(buttonDom);
                });

                array.forEach(arrButtons, function(button) {
                    domStyle.set(button, "display", "block");
                    domStyle.set(button, "width", "100%");
                    domStyle.set(button, "margin", "10px 0px 0px 0px");
                });
            } else {
                domStyle.set(context.lineNode, "display", "none");
            }
        }
    });
});
