define([
    "dojo/_base/declare",
    "dojo/dom-attr",
    "dojo/_base/array",
    "dojo/dom-style",
    "dojo/topic",
    "dojo/html",
    "dojo/on",
    "dojo/text!./html/AlertBar.html",
    "dijit/_TemplatedMixin",
    "app/components/ui/element/BaseElement",
    "app/components/utils/General"
], function(
    declare,
    domAttr,
    array,
    domStyle,
    topic,
    html,
    on,
    template,
    TemplatedMixin,
    BaseElement,
    GeneralUtils
) {
    return declare("app.components.ui.element.alertbar.AlertBar", [BaseElement, TemplatedMixin], {
        templateString: template,

        p_construct: function(setting) {
            var context = this;


            context.startListening(setting);
            context.setupContent(setting);
        },
        startListening: function(_setting) {
            var context = this;
            var domNode = this.domNode;
            var domNodeId = domAttr.get(domNode, "id");

            var showAlertHandler = topic.subscribe("alertbar.show." + _setting.node + "." + _setting.callerNodeId, function(_type) {

                domStyle.set(context.alertSuccessBarNode, "display", "none");
                domStyle.set(context.alertInfoBarNode, "display", "none");
                domStyle.set(context.alertWarningBarNode, "display", "none");
                domStyle.set(context.alertDangerBarNode, "display", "none");

                switch (_type) {
                    case "success":
                        domStyle.set(context.alertSuccessBarNode, "display", "block");
                        break;
                    case "info":
                        domStyle.set(context.alertInfoBarNode, "display", "block");
                        break;
                    case "warning":
                        domStyle.set(context.alertWarningBarNode, "display", "block");
                        break;
                    case "danger":
                        domStyle.set(context.alertDangerBarNode, "display", "block");
                        break;

                }
            });
            topic.publish("topic.handlers.add", showAlertHandler, domNodeId);



        },
        setupContent: function(_setting) {
            var context = this;
            var domNode = this.domNode;

            var type = _setting.type;
            var label = _setting.label

            array.forEach(_setting.data, function(single, i) {


                switch (single.type) {
                    case "success":
                        html.set(context.alertSuccessBarLabelNode, single.label);
                        break;
                    case "info":
                        html.set(context.alertInfoBarLabelNode, single.label);
                        break;
                    case "warning":
                        html.set(context.alertWarningBarLabelNode, single.label);
                        break;
                    case "danger":
                        html.set(context.alertDangerBarLabelNode, single.label);
                        break;

                }

            });

            domStyle.set(context.alertSuccessBarNode, "display", "none");
            domStyle.set(context.alertInfoBarNode, "display", "none");
            domStyle.set(context.alertWarningBarNode, "display", "none");
            domStyle.set(context.alertDangerBarNode, "display", "none");

        }
    });

});
