define([
    "dojo/_base/declare",
    "dojo/_base/array",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/on",
    "dojo/i18n!app/locale/nls/common",
    "dojo/text!./html/DynamicPanel.html",
    "dijit/_TemplatedMixin",
    "app/components/ui/cell/BaseCell",
    "app/components/ui/cell/dynamic/DynamicCell"
], function(
    declare,
    Array,
    domClass,
    domConstruct,
    on,
    i18n_common,
    template,
    _TemplatedMixin,
    BaseCell,
    DynamicCell
) {
    return declare("app.components.ui.element.dynamicpanel.DynamicPanel", [BaseCell,
        _TemplatedMixin
    ], {
        templateString: template,
        savedData: null,
        p_construct: function(setting) {
            var domNode = this.domNode;
            var context = this;

            context.setupContent(setting);
        },
        startup: function () {
            this.inherited(arguments);

            var context = this;

            context.savedData = {};
        },
        setupContent: function(_setting) {
            var context = this;
            var domNode = this.domNode;

            context.savedData = _setting.data;
            var idAccessor = _setting.idAccessor;
            var typeAccessor = _setting.typeAccessor;
            var descriptionAccessor = _setting.descriptionAccessor;
            var valueAccessor = _setting.valueAccessor;
            var mandatoryAccessor = _setting.mandatoryAccessor;
            var lengthAccessor = _setting.lengthAccessor;
            var column = _setting.column;
            var isReadOnly = _setting.isReadOnly;
            var cellPerRow = _setting.cellPerRow;
            var placementDom;

            Array.forEach(context.savedData, function(single) {

                if (cellPerRow && cellPerRow !== null) {
                    if (i === 0 || i % cellPerRow === 0) {
                        placementDom = domConstruct.create("div");
                        domClass.add(placementDom, "row no-margins");
                        domConstruct.place(placementDom, domNode);
                    }
                } else {
                    placementDom = domNode;
                }

                var dynamicCell = new DynamicCell();
                dynamicCell.p_construct({
                    inputColumn: _setting.column,
                    inputId: single[idAccessor],
                    inputType: single[typeAccessor],
                    inputLabel: single[descriptionAccessor],
                    inputMandatory: single[mandatoryAccessor],
                    inputLength: single[lengthAccessor],
                    inputDefault: single[valueAccessor],
                    isReadOnly: isReadOnly,
                    savedData: function (value) {
                        single[valueAccessor] = value;
                    }
                });
                dynamicCell.placeAt(domNode);

                on($("input", dynamicCell.domNode)[0], "blur", function (event) {
                    single[valueAccessor] = event.target.value;
                    _setting.savedData(context.savedData);
                });
            });
        }
    });
})
