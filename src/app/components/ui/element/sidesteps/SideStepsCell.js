define([
    "dojo/_base/declare",
    "dojo/text!./html/SideStepsCell.html",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/dom-attr",
    "dojo/on",
    "dojo/topic",
    "dojo/html",
    "dijit/_TemplatedMixin",
    "app/components/ui/cell/BaseCell",
    "app/components/utils/General"
], function(
    declare,
    template,
    domClass,
    domStyle,
    domAttr,
    on,
    topic,
    html,
    TemplatedMixin,
    BaseCell,
    GeneralUtils
) {

    return declare("app.components.ui.navigation.sidesteps.SideStepsCell", [BaseCell, TemplatedMixin], {
        templateString: template,
        checkValue: null,
        p_construct: function(setting) {
            var domNode = this.domNode;
            var context = this;

            context.startListening(setting);
            context.initInteraction(setting);
            context.setupContent(setting);
        },
        startup: function() {
            this.inherited(arguments);

            var domNode = this.domNode;
            var context = this;

            context.checkValue = false;
        },
        destroy: function() {
            var context = this;
            var domNode = this.domNode;

            topic.publish("topic.handlers.destroy", domAttr.get(domNode, "id"));

            this.inherited(arguments);
        },
        startListening: function(_setting) {
            var context = this;
            var domNode = this.domNode;
            var domNodeId = domAttr.get(domNode, "id");
            var childChecked = 0;

            var returnCheckValueHandler = topic.subscribe(_setting.allName + ".getCheckValue", function(callback) {
                callback(context.checkValue);
            });
            topic.publish("topic.handlers.add", returnCheckValueHandler, domNodeId);

            var activeClassHandler = topic.subscribe("sidesteps.changeactive." + _setting.parentNodeId, function(nodeId) {
                if (nodeId === _setting.singleNodeId) {
                    // set active
                    domClass.add(context.stepNameNode, "active");
                } else {
                    // buang active
                    domClass.remove(context.stepNameNode, "active");
                }
            });
            topic.publish("topic.handlers.add", activeClassHandler, domNodeId);

            var nextStepHandler = topic.subscribe("sidesteps.nextstep", function(_nodeId, data) {
                if (_nodeId === _setting.singleNodeId) {
                    // set pass

                    domClass.add(context.stepNameNode, "pass");
                    // domClass.add(context.btnNode, "button-pass");
                    domStyle.set(context.completeIconNode, "display", "block");
                    // domClass.remove(context.stepNameNode, "active");
                    _setting.pass = true;

                    topic.publish("sidesteps.block.nextpage." + _setting.parentNodeId, _nodeId, data);
                    // topic.publish("sidesteps.changeactive", domAttr.get(domNode, "id"));
                }
            });
            topic.publish("topic.handlers.add", nextStepHandler, domNodeId);


            var activeClassHandler = topic.subscribe("sidesteps.incomplete." + _setting.singleNodeId, function(incomplete) {
                if (incomplete) {
                    // set incomplete icon
                    domStyle.set(context.incompleteIconNode, "display", "block");
                } else {
                    // hide incomplete icon
                    domStyle.set(context.incompleteIconNode, "display", "none");
                }
            });
            topic.publish("topic.handlers.add", activeClassHandler, domNodeId);

            // var checkboxChildHandler = topic.subscribe(_setting.parentName + ".checkbox.checkchild", function(isChecked) {
            //      if (isChecked) {
            //         childChecked = _setting.size;
            //     } else {
            //         childChecked = 0;
            //     }

            //     if (!isChecked === context.checkboxPanelNode.checked) {
            //         context.checkboxEventHandler(context.checkboxPanelNode, "toggle");
            //     }
            // });
            // topic.publish("topic.handlers.add", checkboxChildHandler, domNodeId);


            // var checkboxParentHandler = topic.subscribe(_setting.name + ".checkbox.checkParent", function(isChecked) {

            //     console.log("woi:::" + isChecked);
            //     if (isChecked) {
            //         childChecked++;
            //     } else {
            //         childChecked--;
            //     }

            //     if (childChecked === _setting.childSize) {
            //         context.checkboxToggle(context.checkboxPanelNode, "check");
            //     } else {
            //         context.checkboxToggle(context.checkboxPanelNode, "uncheck");

            //     }
            // });
            // topic.publish("topic.handlers.add", checkboxParentHandler, domNodeId);

            var checkboxParentHandler = topic.subscribe(_setting.allName + ".checkbox.allChild", function(isChecked) {
                if (isChecked) {
                    context.checkboxToggle(context.checkboxPanelNode, "check");
                } else {
                    context.checkboxToggle(context.checkboxPanelNode, "uncheck");
                }
                context.checkValue = isChecked;
                topic.publish(_setting.callerNodeId + ".checkbox.checkedItem", isChecked, _setting.data);
            });
            topic.publish("topic.handlers.add", checkboxParentHandler, domNodeId);

            var checkboxParentHandler = topic.subscribe(_setting.name + ".checkbox.parent", function(isChecked) {
                if (isChecked) {
                    context.checkboxToggle(context.checkboxPanelNode, "check");
                } else {
                    context.checkboxToggle(context.checkboxPanelNode, "uncheck");
                }
                context.checkValue = isChecked;
                topic.publish(_setting.callerNodeId + ".checkbox.checkedItem", isChecked, _setting.data);
            });
            topic.publish("topic.handlers.add", checkboxParentHandler, domNodeId);

            var checkboxChildHandler = topic.subscribe(_setting.parentName + ".checkbox.child", function(isChecked) {
                if (isChecked) {
                    context.checkboxToggle(context.checkboxPanelNode, "check");
                } else {
                    context.checkboxToggle(context.checkboxPanelNode, "uncheck");
                }
                context.checkValue = isChecked;
                topic.publish(_setting.callerNodeId + ".checkbox.checkedItem", isChecked, _setting.data);
            });
            topic.publish("topic.handlers.add", checkboxChildHandler, domNodeId);
        },
        initInteraction: function(_setting) {
            var context = this;
            var domNode = this.domNode;
            var domNodeId = domAttr.get(domNode, "id");

            on(context.stepNameNode, "click", function() {

                //commented out condition to ease devs
                if (_setting.pass) {
                    topic.publish("sidesteps.block." + _setting.parentNodeId, _setting.singleNodeId, {});
                    topic.publish("sidesteps.changeactive." + _setting.parentNodeId, _setting.singleNodeId);
                }
                topic.publish("sidesteps.additionalAction." + _setting.singleNodeId);
            });
            context.checkboxEventHandler(context.checkboxPanelNode, "click", function(isChecked) {
                context.checkValue = isChecked;
                // If child
                if (_setting.parentContainer) {
                    var length = context.getCheckboxValue(_setting.name, _setting.parentContainer).length;
                    if (length === 0) {
                        topic.publish(_setting.parentName + ".checkbox.parent", false);
                    } else {
                        topic.publish(_setting.parentName + ".checkbox.parent", true);
                    }
                } else {
                    topic.publish(_setting.name + ".checkbox.child", isChecked);
                }
                topic.publish(_setting.allName + ".checkbox.all", isChecked);
                // // If child
                // if (_setting.parentContainer) {
                //     var length = context.getCheckboxValue(_setting.name, _setting.parentContainer).length;
                //     if (length === _setting.childSize) {
                //         topic.publish(_setting.parentName + ".checkbox.parent", true);
                //     } else {
                //         topic.publish(_setting.parentName + ".checkbox.parent", false);
                //     }
                // } else {
                //     topic.publish(_setting.name + ".checkbox.child", isChecked);
                // }
                topic.publish(_setting.callerNodeId + ".checkbox.checkedItem", isChecked, _setting.data);
            });
        },
        setupContent: function(_setting) {
            var context = this;
            var domNode = this.domNode;

            html.set(context.stepNameNode, _setting.data.stepName);
            domStyle.set(context.completeIconNode, "display", "none");
            domStyle.set(context.incompleteIconNode, "display", "none");


            if (_setting.number === 0) {
                domClass.add(context.stepNameNode, "active");
            }

            if (_setting.checkBox) {
                domAttr.set(context.checkboxPanelNode, "name", _setting.name);
                domAttr.set(context.checkboxPanelNode, "parentName", _setting.parentName);
            } else {
                context.destroyCheckbox(context.checkboxPanelNode);
            }

            if (_setting.pass) {
                domClass.add(context.stepNameNode, "pass");
            }

        },
        i18n: function(_setting) {
            var context = this;
        },
        getChildContainer: function() {
            var context = this;

            return context.childMenuNode;
        }
    });
});
