define([
    "dojo/_base/declare",
    "dojo/text!./html/SideSteps.html",
    "dojo/_base/array",
    "dojo/dom-style",
    "dojo/dom-attr",
    "dojo/topic",
    "dojo/html",
    "dojo/i18n!app/locale/nls/common",
    "dijit/_TemplatedMixin",
    "app/components/ui/blocks/BaseBlock",
    "app/components/ui/element/sidesteps/SideStepsCell",
    "app/components/utils/General",
    "app/components/utils/Rest",
    "app/components/ui/block-keypairs"
], function (
    declare,
    template,
    array,
    domStyle,
    domAttr,
    topic,
    html,
    i18n_common,
    TemplatedMixin,
    BaseBlock,
    SideStepsCell,
    GeneralUtils,
    Rest,
    blockKeypairs
) {

    return declare("app.components.ui.element.sidesteps.SideSteps", [BaseBlock, TemplatedMixin], {
        templateString: template,
        masterArray: [],
        sortedMasterArray: [],
        childArray: [],
        sortedChildArray: [],

        counter: null,
        domListLength: null,
        uniqueCheckboxAllName: null,
        p_construct: function (setting) {
            var context = this;

            context.domListLength = setting.data.length;
            context.i18n(setting);
            context.startListening(setting);
            context.initInteraction(setting);
            context.setupContent(setting);
        },
        postCreate: function postCreate() {
            this.inherited(arguments);
        },
        startup: function () {
            this.inherited(arguments);

            var domNode = this.domNode;
            var context = this;

            context.masterArray = [];
            context.sortedMasterArray = [];
            context.childArray = [];
            context.sortedChildArray = [];
            context.counter = 0;
            context.uniqueCheckboxAllName = new GeneralUtils().createUUID();;
        },
        destroy: function () {
            this.inherited(arguments);
        },
        startListening: function (_setting) {
            var context = this;
            var domNode = this.domNode;
            var domNodeId = domAttr.get(domNode, "id");

            var checkboxAllChildHandler = topic.subscribe(context.uniqueCheckboxAllName +
                ".checkbox.all",
                function (isChecked) {
                    var countCheck = 0;
                    if (isChecked) {
                        topic.publish(context.uniqueCheckboxAllName + ".getCheckValue",
                            function (isCheck) {
                                if (isCheck) {
                                    countCheck++;
                                }
                                if (countCheck === context.counter) {
                                    context.checkboxToggle(context.checkboxAllNode, "check");
                                }
                            });
                    } else {
                        context.checkboxToggle(context.checkboxAllNode, "uncheck");
                    }
                });
            topic.publish("topic.handlers.add", checkboxAllChildHandler, domNodeId);

            var sideStepBlockHandler = topic.subscribe("sidesteps.block." + domNodeId, function (
                _nodeId, _param) {

                array.forEach(context.masterArray, function (single, i) {
                    var singleNodeId = domAttr.get(single.domNode, "id");

                    if (singleNodeId == _nodeId) {
                        domStyle.set(single.domNode, "display", "block");
                        //single.reconstruct(_param);
                        //console.log(_param);
                    } else {
                        domStyle.set(single.domNode, "display", "none");
                    }

                    if (single.childArray != undefined) {
                        array.forEach(single.childArray, function (child) {
                            var childNodeId = domAttr.get(child.domNode,
                                "id");

                            if (childNodeId == _nodeId) {
                                domStyle.set(child.domNode, "display",
                                    "block");
                                child.reconstruct(_param);
                            } else {
                                domStyle.set(child.domNode, "display",
                                    "none");
                            }
                        });
                    }
                });
            });
            topic.publish("topic.handlers.add", sideStepBlockHandler, domNodeId);


            var sideStepBlockHandler = topic.subscribe("sidesteps.block.nextpage." + domNodeId,
                function (_nodeId, _param) {
                    var nextNodeId;

                    array.forEach(context.sortedMasterArray, function (single, i) {
                        var singleNodeId = domAttr.get(single.domNode, "id");

                        if (i < context.sortedMasterArray.length - 1) {
                            if (singleNodeId == _nodeId) {
                                nextNodeId = domAttr.get(context.sortedMasterArray[i +
                                    1].domNode, "id");
                            }
                        }

                        if (single.childArray != undefined) {
                            if (singleNodeId == _nodeId) {
                                nextNodeId = domAttr.get(single.childArray[0].domNode,
                                    "id");
                            }
                            array.forEach(single.childArray, function (child, y) {
                                var childNodeId = domAttr.get(child.domNode,
                                    "id");

                                if (y < single.childArray.length - 1) {
                                    if (childNodeId == _nodeId) {
                                        nextNodeId = domAttr.get(single.childArray[
                                            y + 1].domNode, "id");

                                    }
                                } else {
                                    if (childNodeId == _nodeId) {
                                        nextNodeId = domAttr.get(context.sortedMasterArray[
                                            i + 1].domNode, "id");
                                    }
                                }
                            });
                        }
                    });
                    topic.publish("sidesteps.block." + domNodeId, nextNodeId, _param);
                    topic.publish("sidesteps.changeactive." + domNodeId, nextNodeId);

                });
            topic.publish("topic.handlers.add", sideStepBlockHandler, domNodeId);

            var sideStepConstructHandler = topic.subscribe("sidestep.block.p_constructed." +
                domNodeId,
                function () {
                    // sorting
                    var countObject = 0;
                    context.sortedMasterArray = _.sortBy(context.masterArray, 'order');
                    array.forEach(_setting.data, function (single, i) {
                        var uniqueCheckboxName = new GeneralUtils().createUUID();
                        var childSize = 0;

                        if (single.childList != undefined) {
                            childSize = single.childList.length
                            context.counter += (childSize + 1);
                            countObject += 1;
                        } else {
                            context.counter += 1;
                            countObject += 1;
                        }

                        var stepsCell = new SideStepsCell().placeAt(context.sideMenuNode);

                        stepsCell.p_construct({
                            data: single,
                            singleNodeId: domAttr.get(context.sortedMasterArray[
                                i].domNode, "id"),
                            number: i,
                            checkBox: _setting.checkBox,
                            name: uniqueCheckboxName,
                            allName: context.uniqueCheckboxAllName,
                            childSize: childSize,
                            callerNodeId: _setting.callerNodeId,
                            parentNodeId: domNodeId,
                            pass: single.clickable
                        });

                        var uniqueChildCheckboxName = new GeneralUtils().createUUID();
                        array.forEach(single.childList, function (child, y) {
                            var childStepsCell = new SideStepsCell().placeAt(
                                stepsCell.getChildContainer());

                            childStepsCell.p_construct({
                                data: child,
                                singleNodeId: domAttr.get(single.childArray[
                                    y].domNode, "id"),
                                checkBox: _setting.checkBox,
                                name: uniqueChildCheckboxName,
                                parentName: uniqueCheckboxName,
                                allName: context.uniqueCheckboxAllName,
                                childSize: childSize,
                                parentContainer: domNode,
                                callerNodeId: _setting.callerNodeId,
                                parentNodeId: domNodeId,
                                pass: child.clickable

                            });
                        });
                    });
                    if (_setting.checkBoxAll) {
                        if (countObject > 1) {
                            domStyle.set(context.checkboxAllBoxNode, "display", "block");
                        } else {
                            domStyle.set(context.checkboxAllBoxNode, "display", "none");
                        }
                    }
                });
            topic.publish("topic.handlers.add", sideStepConstructHandler, domNodeId);
        },
        initInteraction: function (_setting) {
            var context = this;
            var domNode = this.domNode;
            var domNodeId = domAttr.get(domNode, "id");

            context.checkboxEventHandler(context.checkboxAllNode, "click", function (isChecked) {
                topic.publish(context.uniqueCheckboxAllName + ".checkbox.allChild",
                    isChecked);
            });
        },
        setupContent: function (_setting) {
            var context = this;
            var domNode = this.domNode;

            domStyle.set(context.checkboxAllBoxNode, "display", "none");

            if (_setting.status == 1) {
                domStyle.set(context.stepsTitleNode, "font-weight", "bold");
            }

            html.set(context.stepsTitleNode, _setting.stepsTitle);

            var i = 0;
            mainLoop(_setting.data);

            function mainLoop(single) {
                // single.status = "";
                // single.length = _setting.data.length;

                var createCallback = function (obj) {
                    if (i === 0) {
                        domStyle.set(obj.domNode, "display", "block");
                    } else {
                        domStyle.set(obj.domNode, "display", "none");
                    }
                    i++;

                    if (i < _setting.data.length) {
                        mainLoop(single);
                    }
                };

                if (single[i].childList == undefined) {
                    create(single[i], context.formContainerNode, i, createCallback);
                } else {
                    context.childArray = [];
                    context.sortedChildArray = [];
                    var index = 0;
                    createChildLoop(single[i].childList);

                    function createChildLoop(arr) {
                        createChild(arr[index], context.formContainerNode, index,
                            function (block) {

                                context.childArray.push(block);
                                if (context.childArray.length === single[i].childList
                                    .length) {
                                    context.sortedChildArray = _.sortBy(context.childArray,
                                        'order');
                                    single[i].childArray = context.sortedChildArray;
                                    create(single[i], context.formContainerNode, i,
                                        createCallback);
                                }
                                index++;

                                if (index < arr.length) {
                                    createChildLoop(arr);
                                }
                            });
                    }
                }
            }

            function create(single, container, placement, callback) {
                var block = null;

                if (single.loadData == undefined) {
                    single.loadData = {};
                }

                blockKeypairs.getBlock(single.functionCode, function (Block) {
                    block = new Block();
                    block.placeAt(container);
                    block.startup();
                    block.p_construct(single.loadData);
                    block.order = placement;
                    block.childArray = single.childArray;
                    context.masterArray.push(block);
                    if (context.masterArray.length === context.domListLength) {
                        topic.publish("sidestep.block.p_constructed." + domAttr.get(
                            domNode, "id"));
                    }
                    if (callback !== undefined) {
                        callback(block);
                    }
                });
            }

            function createChild(single, container, placement, callback) {
                var block = null;

                if (single.loadData == undefined) {
                    single.loadData = {};
                }

                blockKeypairs.getBlock(single.functionCode, function (Block) {
                    block = new Block();
                    block.placeAt(container);
                    block.startup();
                    block.p_construct(single.loadData);
                    block.order = placement;
                    if (callback !== undefined) {
                        callback(block);
                        domStyle.set(block.domNode, "display", "none");
                    }
                });
            }
        },
        i18n: function (_setting) {
            var context = this;

            html.set(context.checkboxAllLabelNode, i18n_common.all);
        }
    });
});
