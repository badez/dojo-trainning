define([
    "dojo/_base/array",
    "dojo/_base/declare",
    "dojo/dom-attr",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/on",
    "dojo/topic",
    "dojo/text!./html/QuickNavigationMenu.html",
    "app/components/ui/cell/quicknavigationmenu/QuickNavigationMenuCell",
    "app/components/ui/element/BaseElement"
], function(
    v_array,
    v_declare,
    v_domAttr,
    v_domClass,
    v_domConstruct,
    v_on,
    v_topic,
    v_template,
    v_QuickNavigationMenuCell,
    v_BaseElement
) {

    return v_declare("app.components.ui.element.quicknavigationmenu.QuickNavigationMenu", [v_BaseElement], {
        templateString: v_template,
        construct: function(v_setting) {
            var v_context = this;

            v_context.p_startListening(v_setting);
            v_context.p_setupContent(v_setting);
            v_context.p_initInteraction(v_setting);
        },
        p_startListening: function(v_setting) {
            var v_context = this;
            var v_domNode = this.domNode;

            var v_resetHandler = v_topic.subscribe("reset.items.quicknavmenu." + v_setting.quickNavigationMenuId, function(v_itemLists) {
                v_domConstruct.empty(v_context.quickNavigationMenuListNode);
                v_context.p_setupItemList(v_itemLists);
            });
            v_topic.publish("topic.handlers.add", v_resetHandler, v_domAttr.get(v_domNode, "id"));
        },
        p_initInteraction: function(v_setting) {
            var v_context = this;

            v_on(v_context.quickNavigationMenuNode, "click", function() {
                if (v_setting.quickNavigationMenuRemoveBtn) {
                    v_domClass.toggle(v_context.quickNavigationMenuIconNode, v_setting.quickNavigationMenuBtn);
                    v_domClass.toggle(v_context.quickNavigationMenuIconNode, v_setting.quickNavigationMenuRemoveBtn);
                }
                v_domClass.toggle(v_context.quickNavigationMenuWrapperNode, "hide");
            });

            $(document).click(function(event) {
                if (v_context.quickNavigationMenuWrapperNode && !v_domClass.contains(v_context.quickNavigationMenuWrapperNode, "hide") && !$(event.target).closest(v_context.quickNavigationMenuNode).length) {
                    if (v_setting.quickNavigationMenuRemoveBtn) {
                        v_domClass.toggle(v_context.quickNavigationMenuIconNode, v_setting.quickNavigationMenuBtn);
                        v_domClass.toggle(v_context.quickNavigationMenuIconNode, v_setting.quickNavigationMenuRemoveBtn);
                    }
                    v_domClass.toggle(v_context.quickNavigationMenuWrapperNode, "hide");
                }
            });
        },
        p_setupContent: function(v_setting) {
            var v_context = this;

            v_domClass.add(v_context.quickNavigationMenuIconNode, v_setting.quickNavigationMenuBtn);

            v_context.p_setupItemList(v_setting.quickNavigationMenuData);
        },
        p_setupItemList: function(v_setting) {
            var v_context = this;

            if (v_setting.length > 9) {
                v_domClass.add(v_context.quickNavigationMenuWrapperNode, "quicknavigationmenu-list-overflow");
                if (v_domClass.contains(v_context.quickNavigationMenuWrapperNode, "quicknavigationmenu-list"))
                    v_domClass.remove(v_context.quickNavigationMenuWrapperNode, "quicknavigationmenu-list");
            } else {
                v_domClass.add(v_context.quickNavigationMenuWrapperNode, "quicknavigationmenu-list");
                if (v_domClass.contains(v_context.quickNavigationMenuWrapperNode, "quicknavigationmenu-list-overflow"))
                    v_domClass.remove(v_context.quickNavigationMenuWrapperNode, "quicknavigationmenu-list-overflow");
            }

            v_array.forEach(v_setting, function(v_single, v_i) {
                var v_id = v_context.createUUID(v_single.domNodeItem);
                var v_liNode = new v_QuickNavigationMenuCell().placeAt(v_context.quickNavigationMenuListNode);
                v_liNode.construct({
                    id: v_id,
                    label: v_single.label,
                    icon: v_single.icon
                });
            });
        }
    });
});
