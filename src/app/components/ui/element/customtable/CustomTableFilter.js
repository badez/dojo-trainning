define([
    "dojo/_base/declare",
    "dojo/_base/array",
    "dojo/dom-attr",
    "dojo/dom-construct",
    "dojo/html",
    "dojo/on",
    "dojo/query",
    "dojo/i18n!app/locale/nls/common",
    "dojo/text!./html/CustomTableFilter.html",
    "dijit/_TemplatedMixin",
    "app/components/ui/blocks/BaseBlock",
    "dojo/topic",
    "dojo/dom-class",
    "dojo/dom-style"
], function(
    declare,
    array,
    domAttr,
    domConstruct,
    html,
    on,
    query,
    i18n_common,
    template,
    _TemplatedMixin,
    BaseBlock,
    topic,
    domClass,
    domStyle
) {
    return declare("app.components.ui.element.customtable.CustomTableFilter", [BaseBlock, _TemplatedMixin], {
        templateString: template,
        filterData: null,
        inputData: null,
        checkboxData: null,
        simpanId: null,
        p_construct: function(setting) {
            var domNode = this.domNode;
            var context = this;

            context.i18n(setting);
            context.setupContent(setting);
            context.initInteraction(setting);
        },
        startup: function() {
            this.inherited(arguments);

            var context = this;
            context.filterData = {};
            context.checkboxData = [];
            context.inputData = [];
            context.simpanId = [];
        },
        setupContent: function(_setting) {
            var context = this;
            var domNode = this.domNode;
            var domNodeId = domAttr.get(domNode, "id");

            _setting.addClass(context.dynamicElementNode);

            var tempArr = [];

            array.forEach(_setting.data.columns, function(single, index) {
                if (single.filter) {
                    var tempObj = {};
                    tempObj = single;
                    tempArr.push(tempObj);
                }
            });

            array.forEach(tempArr, function(single, index) {
                if (single.filter) {
                    setupList(single, context.filterContainerNode, index + 1);
                }
            });

            function setupList(data, container, index) {
                if (data.filter.type == "checkbox") {

                    var grandParent = domConstruct.create("div", { "class": "col-lg-12" });

                    var parentList = domConstruct.create("div");
                    data.index = index;

                    var list = dojo.create("p", {
                        innerHTML: data.title + " - All"
                    }, parentList);

                    var checkbox = dojo.create("input", {
                        type: data.filter.type,
                        id: data.index,
                        customId: data.index,
                        value: data.key,
                        "class": data.key,
                        name: "data"
                    }, parentList);

                    var label = dojo.create("label", {
                        "for": data.index,
                        style: "float: right;"
                    }, parentList);

                    domClass.add(list, "ptag-label");
                    domClass.add(parentList, "list-filter");
                    domClass.add(checkbox, "input-state");

                    context.filterData[data.key] = [];

                    var obj = {
                        id: data.index,
                        key: data.key
                    };

                    context.simpanId.push(obj);

                    domConstruct.place(parentList, grandParent);
                    domConstruct.place(grandParent, container);
					domStyle.set(list, "color", "#ffffff");

                    var tempArray = [];
                    array.forEach(_setting.data.data, function(single, i) {
                        tempArray.push(single[data.key]);
                    });

                    var checkDuplicatedValue = eliminateDuplicates(tempArray);

                    function eliminateDuplicates(arr) {
                        var i,
                            len = arr.length,
                            out = [],
                            obj = {};

                        for (i = 0; i < len; i++) {
                            obj[arr[i]] = 0;
                        }
                        for (i in obj) {
                            out.push(i);
                        }
                        return out;
                    }

                    array.forEach(checkDuplicatedValue, function(single, i) {
                        var obj = {
                            key: data.key,
                            title: single,
                            filter: {
                                type: "checkbox",
                            },
                            index: i
                        };
                        setupCheckbox(obj, container, index + "." + (i + 1));
                    });

                    on(checkbox, "click", function(e) {
                        var isChecked = $(checkbox).context.checked;
                        var checkboxId = domAttr.get(checkbox, "customId");

                        topic.publish("customtable.filter.checkbox.checked", checkboxId, isChecked);
                    });


                    var checkboxHandler = topic.subscribe("customtable.filter.checkbox.checked", function(checkboxId, isChecked) {
                        var found = checkboxId.indexOf(".");
                        //if child, found > 0
                        //if parent, found < 0

                        var checkboxParentChildId = domAttr.get(checkbox, "customId");
                        //if child, checkboxParentChildId = .1, .2, .3
                        //if parent, checkboxParentChildId = 1, 2, 3

                        var charParent = checkboxParentChildId.split(".");
                        //convert all id into char and push into array

                        var foundParent = checkboxParentChildId.indexOf(".");
                        //if child, foundParent > 0
                        //if parent, foundParent < 0

                        if (found >= 0) {
                            var charParentClicked = checkboxId.split(".");
                            var parentIdClicked = charParentClicked[0];

                            if (foundParent <= 0) {
                                var tempArray = [];
                                var checkedChildArray = [];

                                query("input[customId^='" + parentIdClicked + ".']", domNode).forEach(function(single) {
                                    var a = domAttr.get(single, "customId");
                                    var b = $(single).context.checked
                                    var child = {};
                                    child[a] = b;
                                    tempArray.push(child);
                                });

                                array.forEach(tempArray, function(single, index) {
                                    var addOn = index + 1;
                                    var result = addOn.toString();
                                });

                                query("input[customId^='" + parentIdClicked + ".']", domNode).forEach(function(single) {
                                    var singleChecked = $(single).context.checked;
                                    checkedChildArray.push(singleChecked);
                                });
                                var checked = _.contains(checkedChildArray, true);

                                if (!checked && parentIdClicked == charParent[0]) {
                                    $(checkbox).context.checked = isChecked;

                                    query("input[customId^='" + parentIdClicked + "']", domNode).forEach(function(single) {
                                        $(single).context.checked = isChecked;
                                    });
                                }

                            } else {
                                if (isChecked) {

                                    var tempArray = [];
                                    var checkedChildArray = [];
                                    var status;
                                    var boolArray = [];

                                    query("input[customId^='" + parentIdClicked + ".']", domNode).forEach(function(single) {
                                        var a = domAttr.get(single, "customId");
                                        var b = $(single).context.checked
                                        var child = {};
                                        child[a] = b;
                                        tempArray.push(child);
                                    });

                                    array.forEach(tempArray, function(single, index) {
                                        var addOn = index + 1;
                                        var result = addOn.toString();
                                        console.log(single[charParent[0] + "." + result]);
                                        boolArray.push(single[charParent[0] + "." + result]);
                                    });

                                    function checkStatus(boolArray) {
                                        return boolArray;
                                    }

                                    var status = boolArray.every(checkStatus);

                                    if (status) {
                                        query("input[customId='" + parentIdClicked + "']", domNode).forEach(function(single) {
                                            $(single).context.checked = true;
                                        });
                                    }

                                } else {
                                    query("input[customId='" + parentIdClicked + "']", domNode).forEach(function(single) {
                                        $(single).context.checked = false;
                                    });
                                }
                            }
                        } else {
                            if (foundParent > 0) {
                                if (charParent[0] === checkboxId) {
                                    $(checkbox).context.checked = isChecked;
                                }
                            }
                        }
                    });
                    topic.publish("topic.handlers.add", checkboxHandler, domNodeId);

                } else {

                    var grandParent = domConstruct.create("div", { "class": "col-lg-12" });
                    var parentList = domConstruct.create("div", { "class": "col-lg-12" });
                    data.index = index;

                    var checkbox = dojo.create("input", {
                        type: data.filter.type,
                        id: data.index,
                        customId: data.index,
                        placeholder: "Enter " + data.title,
                        "class": data.key
                    }, parentList);

                    var label = dojo.create("label", {
                        "for": data.index,
                        style: "float: right;"
                    }, parentList);

                    domClass.add(parentList, "list-filter");
                    domClass.add(checkbox, "input-state");
                    domStyle.set(parentList, "color", "#999999");

                    var obj = {
                        id: data.index,
                        key: data.key
                    };

                    context.inputData.push(obj);
                    domConstruct.place(parentList, grandParent);
                    domConstruct.place(grandParent, container);
                }

            }

            function setupCheckbox(data, container, index) {
                var grandParent = domConstruct.create("div", { "class": "col-lg-12" });

                var parentList = domConstruct.create("div");
                data.index = index;

                var closeIcon = domConstruct.create("i");
                domClass.add(closeIcon, "ico-relationship");
                domClass.add(closeIcon, "icon-child");
                domConstruct.place(closeIcon, parentList);
                domStyle.set(closeIcon, "font-size", "1.4em");

                var list = dojo.create("p", {
                    innerHTML: data.title
                }, parentList);

                var checkbox = dojo.create("input", {
                    type: data.filter.type,
                    id: data.index,
                    customId: data.index,
                    value: data.title,
                    "class": data.key,
                    name: "data"
                }, parentList);

                var label = dojo.create("label", {
                    "for": data.index,
                    style: "float: right;"
                }, parentList);

				domStyle.set(list, "color", "#ffffff");
                domClass.add(parentList, "list-filter");
                domClass.add(checkbox, "input-state");
                domClass.add(list, "ptag-label");
                domConstruct.place(parentList, grandParent);
                domConstruct.place(grandParent, container);

                on(checkbox, "click", function(e) {
                    var isChecked = $(checkbox).context.checked;
                    var checkboxId = domAttr.get(checkbox, "customId");

                    topic.publish("customtable.filter.checkbox.checked", checkboxId, isChecked);
                });

                var checkboxHandler = topic.subscribe("customtable.filter.checkbox.checked", function(checkboxId, isChecked) {
                    var found = checkboxId.indexOf(".");
                    //if child, found > 0
                    //if parent, found < 0

                    var checkboxParentChildId = domAttr.get(checkbox, "customId");
                    //if child, checkboxParentChildId = .1, .2, .3
                    //if parent, checkboxParentChildId = 1, 2, 3

                    var charParent = checkboxParentChildId.split(".");
                    //convert all id into char and push into array

                    var foundParent = checkboxParentChildId.indexOf(".");
                    //if child, foundParent > 0
                    //if parent, foundParent < 0

                    if (found >= 0) {
                        var charParentClicked = checkboxId.split(".");
                        var parentIdClicked = charParentClicked[0];

                        if (foundParent <= 0) {
                            var tempArray = [];
                            var checkedChildArray = [];

                            query("input[customId^='" + parentIdClicked + ".']", domNode).forEach(function(single) {
                                var a = domAttr.get(single, "customId");
                                var b = $(single).context.checked
                                var child = {};
                                child[a] = b;
                                tempArray.push(child);
                            });

                            array.forEach(tempArray, function(single, index) {
                                var addOn = index + 1;
                                var result = addOn.toString();
                            });

                            query("input[customId^='" + parentIdClicked + ".']", domNode).forEach(function(single) {
                                var singleChecked = $(single).context.checked;
                                checkedChildArray.push(singleChecked);
                            });
                            var checked = _.contains(checkedChildArray, true);

                            if (!checked && parentIdClicked == charParent[0]) {
                                $(checkbox).context.checked = isChecked;

                                query("input[customId^='" + parentIdClicked + "']", domNode).forEach(function(single) {
                                    $(single).context.checked = isChecked;
                                });
                            }

                        } else {
                            if (isChecked) {

                                var tempArray = [];
                                var checkedChildArray = [];
                                var status;
                                var boolArray = [];

                                query("input[customId^='" + parentIdClicked + ".']", domNode).forEach(function(single) {
                                    var a = domAttr.get(single, "customId");
                                    var b = $(single).context.checked
                                    var child = {};
                                    child[a] = b;
                                    tempArray.push(child);
                                });

                                array.forEach(tempArray, function(single, index) {
                                    var addOn = index + 1;
                                    var result = addOn.toString();
                                    // console.log(single[charParent[0] + "." + result]);
                                    boolArray.push(single[charParent[0] + "." + result]);
                                });

                                function checkStatus(boolArray) {
                                    return boolArray;
                                }

                                var status = boolArray.every(checkStatus);

                                if (status) {
                                    query("input[customId='" + parentIdClicked + "']", domNode).forEach(function(single) {
                                        $(single).context.checked = true;
                                    });
                                }

                            } else {
                                query("input[customId='" + parentIdClicked + "']", domNode).forEach(function(single) {
                                    $(single).context.checked = false;
                                });
                            }
                        }
                    } else {
                        if (foundParent > 0) {
                            if (charParent[0] === checkboxId) {
                                $(checkbox).context.checked = isChecked;
                            }
                        }
                    }
                });
                topic.publish("topic.handlers.add", checkboxHandler, domNodeId);
            }

        },
        initInteraction: function(_setting) {
            var context = this;
            var domNode = this.domNode;

            on(context.resetBtnNode, "click", function() {
                _setting.callBack(false);
            });

            on(context.applyBtnNode, "click", function(event) {
                var finalizedArray = [];
                var finalizedData = {};
                array.forEach(context.inputData, function(singleInput) {
                    query("input[customId='" + singleInput.id + "']", domNode).forEach(function(single) {
                        context.filterData[singleInput.key] = domAttr.get(single, "value");
                    });
                });

                var checked = context.getCheckboxValue("data");
                var arrayStoreIdAndKeyOfCheckbox = [];
                array.forEach(checked, function(getId) {
                    query("input[value='" + getId + "']", domNode).forEach(function(single) {

                        var idChild = domAttr.get(single, "id").charAt(0);

                        array.forEach(context.simpanId, function(getParentId) {
                            if (getParentId.id == idChild) {

                                var ObjToStoreKeyAndIdOfCheckbox = {
                                    key: getParentId.key,
                                    checked: getId
                                };
                                arrayStoreIdAndKeyOfCheckbox.push(ObjToStoreKeyAndIdOfCheckbox);
                                context.filterData[ObjToStoreKeyAndIdOfCheckbox.key] = [];
                            }
                        });
                        array.forEach(arrayStoreIdAndKeyOfCheckbox, function(finalChecked) {
                            if (finalChecked.key != finalChecked.checked) {
                                context.filterData[finalChecked.key].push(finalChecked.checked);
                            }
                            var checkDuplicatedValue = eliminateDuplicates(context.filterData[finalChecked.key]);

                            //method to check the dupicate value in array
                            function eliminateDuplicates(arr) {
                                var i,
                                    len = arr.length,
                                    out = [],
                                    obj = {};

                                for (i = 0; i < len; i++) {
                                    obj[arr[i]] = 0;
                                }
                                for (i in obj) {
                                    out.push(i);
                                }
                                return out;
                            }

                            context.filterData[finalChecked.key] = checkDuplicatedValue;
                        });
                    });
                });

                //get the list of the property(s) in objects
                var listOfPropertyInObj = Object.keys(context.filterData);

                var objPassed = {};
                array.forEach(listOfPropertyInObj, function(propertyInObj) {
                    if (context.filterData[propertyInObj].length > 0) {
                        objPassed[propertyInObj] = context.filterData[propertyInObj];
                    }
                });
                _setting.callBack(objPassed);
            });
        },
        i18n: function(_setting) {
            var context = this;
            html.set(context.filterLabelNode, i18n_common.filterBy);
            html.set(context.resetBtnNode, i18n_common.reset);
            html.set(context.applyBtnNode, i18n_common.apply);
        }
    });
})
