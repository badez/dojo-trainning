// This class will act as util for use case to construct table.
// With this, use will able to customize the renderer as in edit or display mode.
define([
    "dojo/_base/declare",
    "dojo/dom",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/dom-attr",
    "dojo/_base/array",
    "dojo/i18n!app/locale/nls/common",
    "dojo/dom-style",
    "dojo/NodeList-traverse",
    "dojo/query",
    "dojo/topic",
    "dojo/html",
    "dojo/on",
    "dojo/text!./html/CustomTable.html",
    "dijit/_TemplatedMixin",
    "app/components/ui/element/BaseElement",
    "app/components/utils/General",
    "app/components/ui/element/customtable/CustomTableFilter"
], function(
    declare,
    dom,
    domClass,
    domConstruct,
    domAttr,
    array,
    i18n_common,
    domStyle,
    nodeList,
    query,
    topic,
    html,
    on,
    template,
    TemplatedMixin,
    BaseElement,
    GeneralUtils,
    CustomTableFilter
) {
    return declare("app.components.ui.element.general.customtable.CustomTable", [BaseElement, TemplatedMixin], {
        templateString: template,
        data: null,
        tableObj: null,
        columnKeyColMap: null,
        editCreateFunctions: null,
        clickHandler: null,
        columnOnClickFunctions: null,
        postTableDrawColumnCellFunctions: null,
        p_construct: function(setting) {
            var context = this;
            context.startListening(setting);
            context.setupContent(setting);
        },
        startListening: function(_setting) {
            var context = this;
            var domNode = this.domNode;
            var domNodeId = domAttr.get(domNode, "id");

            var updateTableData = topic.subscribe("customtable.data.update." + _setting.tableId,
                function(_data, alwaysEditable) {
                    context.data[_data.row][_data.columnKey] = _data.value;
                    if (!alwaysEditable) {
                        context.tableObj.cell(_data.row, context.columnKeyColMap[_data.columnKey]).data(_data.value);
                    }
                    context.attachOnClickListener(_setting);
                    if (_data.getReturnValue) {
                        _data.getReturnValue(context.data[_data.row]);
                    }
                });
            topic.publish("topic.handlers.add", updateTableData, domNodeId);

            var getTableData = topic.subscribe("customtable.getdata." + _setting.tableId,
                function(callBack) {
                    callBack(context.tableObj.data());
                });
            topic.publish("topic.handlers.add", updateTableData, domNodeId);

            var refreshTable = topic.subscribe("customtable.refresh." + _setting.parentId,
                function() {
                    context.refreshTable();
                });
            topic.publish("topic.handlers.add", refreshTable, domNodeId);

            if (_setting.onRowClick) {
                on(context.container, "tr:click", function(event) {
                    _setting.onRowClick(this, context.tableObj.row(this).data());
                });
            }

            if (_setting.rowSelection) {
                on(context.container, "tr:not(.child):click", function(event) {
                    if (!domClass.contains(this, "selected")) {
                        query("tr.selected", context.container).forEach(function(selectedRow) {
                            domClass.remove(selectedRow, "selected");
                        });

                        domClass.add(this, "selected");
                        context.selectedData = context.tableObj.row(this).data();
                    }
                });
            }

            var refreshAllTableHandler = topic.subscribe("customtable.refresh",
                function() {
                    context.refreshTable();
                });
            topic.publish("topic.handlers.add", refreshAllTableHandler, domNodeId);

            var updateAllTableData = topic.subscribe("customtable.alldata.update." + _setting.tableId,
                function(_data) {
                    context.data = _data;
                    context.tableObj.clear().rows.add(_data);
                    context.tableObj.columns.adjust().draw();
                });
            topic.publish("topic.handlers.add", updateAllTableData, domNodeId);

            var removeRow = topic.subscribe("customtable.remove.row." + _setting.tableId,
                function(_data) {
                    var newDataAfterRemove = [];
                    context.data.splice(_data.row, 1);
                    context.tableObj.row(_data.rowElement).remove();
                    newDataAfterRemove = context.data;
                    context.tableObj.clear().rows.add(newDataAfterRemove);
                    context.tableObj.columns.adjust().draw();
                });
            topic.publish("topic.handlers.add", removeRow, domNodeId);

            var addRow = topic.subscribe("customtable.add.row." + _setting.tableId,
                function(rowData) {
                    context.data.push(rowData);
                    context.tableObj.row.add(rowData).draw();
                });
            topic.publish("topic.handlers.add", addRow, domNodeId);
        },
        setupContent: function(_setting) {
            var context = this;
            var domNode = this.domNode;

            context.data = _setting.data;
            context.columnKeyColMap = {};
            context.editCreateFunctions = [];
            context.clickHandler = [];
            context.columnOnClickFunctions = [];
            context.postTableDrawColumnCellFunctions = [];

            //set minimum table columns display
            var columeDisplay = 3;
            var drawCallback = function(settings) {};
            array.forEach(_setting.columns, function(column, i) {
                if (column.allwaysShow && columeDisplay > 0 && columeDisplay <= 3) {
                    columeDisplay--;
                }
                drawCallback = function(settings) {
                    if (column.custom && context.postTableDrawColumnCellFunctions.length > 0) {
                        context.postTableDrawColumnCellListener(_setting);
                    }
                    context.attachOnClickListener(_setting);
                }
            });

            //start define column
            var options = {
                responsive: (_setting.responsive === undefined) ? false : _setting.responsive,
                scrollX: (_setting.scrollX === undefined) ? false : _setting.scrollX,
                paging: (_setting.paging === undefined) ? true : _setting.paging,
                searching: (_setting.searching === undefined) ? true : _setting.searching,
                ordering: (_setting.ordering === undefined) ? true : _setting.ordering,
                info: (_setting.info === undefined) ? true : _setting.info,
                drawCallback: drawCallback,
                pagingType: 'full_numbers',
                language: {
                    search: "_INPUT_",
                    searchPlaceholder: "Find",
                    paginate: {
                        first: '<i class="ico-paging-previousmax fs21"></i>',
                        last: '<i class="ico-paging-nextmax fs21"></i>',
                        previous: '<i class="ico-paging-previous fs21"></i>',
                        next: '<i class="ico-paging-next fs21"></i>'
                    }
                },
            };

            if (_setting.dom !== undefined && options.dom !== undefined) {
                options.dom = options.dom + _setting.dom;
            } else if (_setting.dom !== undefined) {
                options.dom = _setting.dom;
            }

            if (_setting.disableFirstColumnSorting) {
                options.order = [];
            }

            if (_setting.filter) {

                options.searching = _setting.filter;

                var filterButtonId = new GeneralUtils().createUUID();
                var topDomDiv = "<'row'<'col-sm-6'><'col-sm-12 col-lg-6'<'col-xs-6 col-sm-6 col-lg-6 custom-filter-display-none'f><'col-xs-6 col-sm-6 col-lg-6 text-right'B>>><'row'<'col-sm-6 col-lg-12'<'col-xs-6 col-sm-6 col-lg-6'>><'col-sm-6 col-lg-12'<'#" + filterButtonId + ".col-xs-12 col-sm-12 col-lg-12'>>>";

                if (_setting.dom) {
                    if (_setting.dom.search('f') >= 0) {
                        topDomDiv = "<'row'<'col-sm-6'><'col-sm-12 col-lg-6'<'col-xs-6 col-sm-6 col-lg-6 text-right'B><'col-xs-6 col-sm-6 col-lg-6 pr0'f>>><'row'<'col-sm-6 col-lg-9'<'#" + filterButtonId + ".col-xs-12 col-sm-12 col-lg-12'><'col-xs-6 col-sm-6 col-lg-6'>><'col-sm-6'>>";
                        options.dom = options.dom.replace("f", "");
                    }
                } else {
                    options.dom = options.dom + "t";
                }
                options.dom = topDomDiv + options.dom;

                var pTagId = new GeneralUtils().createUUID();
                var iTagId = new GeneralUtils().createUUID();
                var spanTagId = new GeneralUtils().createUUID();
                var reserveSpaceForIcon = "fa fa-check custom-filter-display-none";

                options.buttons = {
                    dom: {
                        button: {
                            tag: "button",
                            className: "btn btn-primary",
                        }
                    },
                    buttons: [{
                        text: "<p id='" + pTagId + "' style='margin:0'><i class='" + reserveSpaceForIcon + "' id='" + iTagId + "'></i> " + i18n_common.filter + " " + "<span " + spanTagId + " class='fa fa-angle-down add-margin-left'></span></p>",
                        action: function(e, dt, node, config) {
                            domClass.replace(dom.byId(iTagId), "fa fa-check custom-filter-display-none");
                            // domStyle.set(dom.byId(spanTagId), "transform", "rotate(90)");
                            if (_setting.dom) {
                                if (_setting.dom.search('f') >= 0) {
                                    domStyle.set(dom.byId(filterButtonId), "padding-right", "0");
                                } else {
                                    domStyle.set(dom.byId(filterButtonId), "padding-right", "15px");
                                }
                            }

                            $(context.container).DataTable().settings()[0].filterredData = {};
                            $(context.container).DataTable().draw();

                            if ((dom.byId(filterButtonId).innerHTML)) {
                                domConstruct.empty(dom.byId(filterButtonId));
                            } else {
                                var filter = new CustomTableFilter();
                                filter.placeAt(dom.byId(filterButtonId));
                                filter.p_construct({
                                    data: _setting,
                                    addClass: function(node) {
                                        if (_setting.dom) {
                                            if (_setting.dom.search('f') >= 0) {
                                                domClass.replace(node, "col-sm-12 col-lg-6 custom-table-filter");
                                            } else {
                                                domClass.replace(node, "col-sm-12 col-lg-4 custom-table-filter");
                                            }
                                        }
                                    },
                                    callBack: function(data) {
                                        if (data) {

                                            domClass.replace(dom.byId(iTagId), "fa fa-check");
                                            domStyle.set(dom.byId(iTagId), "font-size", "20px");
                                            domStyle.set(dom.byId(filterButtonId), "bottom", "100%");

                                            domStyle.set(dom.byId(pTagId), "margin", "0");

                                            $(context.container).DataTable().settings()[0].filterredData = data;
                                            $(context.container).DataTable().draw();
                                        }
                                        domConstruct.empty(dom.byId(filterButtonId));
                                    }
                                });
                            }
                        }
                    }]
                };
            }

            if (_setting.noVerticalBorder) {
                domClass.add(context.container, "no-vertical-border");
            }

            options.columns = [];
            // for special case column
            options.columnDefs = [];

            var counter = 0;
            array.forEach(_setting.columns, function(column, i) {
                if (!column.isDetail && !column.isHidden) {
                    counter++;
                }

                var columnOption = {
                    title: column.title,
                    data: column.key,
                    name: column.key,
                    visible: (column.isHidden) ? false : true,
                    className: (column.isDetail) ? "none" : ((counter <= columeDisplay || column.allwaysShow) ? "all" : null),
                    defaultContent: ""
                };
                if (column.width) {
                    columnOption.width = column.width;
                }
                options.columns.push(columnOption);
                options.columnDefs.push({
                    render: function(data, type, full, meta) {
                        if (type === 'display') {
                            var div = domConstruct.create("div", { cellId: "cell_" + _setting.tableId + "_" + column.key + "_" + meta.row });
                            if (column.custom && column.custom.displayCreateFunction) {
                                domAttr.set(div, "customDetailTitleClass", column.customDetailTitleClass);
                                column.custom.displayCreateFunction(div, data, full, meta.row, meta.col);
                            } else {
                                div.innerHTML = data;
                            }
                            return div.outerHTML;
                        }
                        return data;
                    },
                    orderable: (column.sortable === undefined) ? true : column.sortable,
                    targets: i
                });
                context.columnKeyColMap[column.key] = i;
                if (column.custom && column.custom.columnOnClickFunction) {
                    context.columnOnClickFunctions.push({
                        columnKey: column.key,
                        col: i,
                        columnOnClickFunction: column.custom.columnOnClickFunction,
                    });
                } else if (column.custom && column.custom.editCreateFunction) {
                    context.editCreateFunctions.push({
                        columnKey: column.key,
                        col: i,
                        editCreateFunction: column.custom.editCreateFunction
                    });
                } else if (column.custom && column.custom.postTableDrawColumnCellFunction) {
                    context.postTableDrawColumnCellFunctions.push({
                        columnKey: column.key,
                        col: i,
                        postTableDrawColumnCellFunction: column.custom.postTableDrawColumnCellFunction
                    });
                }
            });
            //end define column

            // setup responsive
            if (options.responsive) {
                options.responsive = {
                    details: {
                        renderer: function(api, rowIdx, columns) {
                            var counter = 1;
                            var dataRow = domConstruct.create('tr');
                            domClass.add(dataRow, "child");

                            var dataRowTd = domConstruct.create('td');
                            domAttr.set(dataRowTd, "colspan", 99);

                            var dataRowDiv = domConstruct.create('div');
                            domStyle.set(dataRowDiv, "margin-left", "-25px");
                            domConstruct.place(dataRowDiv, dataRowTd);

                            var data = $.map(columns, function(col, i) {
                                if (col.hidden) {
                                    var title = api.column(col.columnIndex).header();
                                    var displayData = col.data;

                                    var customDetailTitleClass = domAttr.get(domConstruct.toDom(displayData), "customDetailTitleClass");
                                    var displayDataClasses = domAttr.get(domConstruct.toDom(displayData), "class");
                                    if (customDetailTitleClass !== undefined && displayDataClasses.indexOf("col-") !== -1) {
                                        var dataDivChildHeader = domConstruct.create('div');
                                        domClass.add(dataDivChildHeader, customDetailTitleClass);
                                        domStyle.set(dataDivChildHeader, "padding", "5px 15px 0px 15px");
                                        var dataDivChildData = domConstruct.create('div');
                                        domClass.add(dataDivChildData, title.className);
                                        dataDivChildHeader.innerHTML = col.title;
                                        dataDivChildData.innerHTML = displayData;
                                        domConstruct.place(dataDivChildHeader, dataRowDiv);
                                        domConstruct.place(dataDivChildData, dataRowDiv);
                                    } else {
                                        var dataRowDivChild = domConstruct.create('div');
                                        if (counter % 2 == 0) {
                                            domClass.add(dataRowDivChild, "col-xs-12 col-sm-5 col-sm-offset-2 pr0 pl0");
                                        } else {
                                            domClass.add(dataRowDivChild, "col-xs-12 col-sm-5 pr0 pl0");
                                        }
                                        var dataDivChildHeader = domConstruct.create('div');
                                        var dataDivChildData = domConstruct.create('div');
                                        domStyle.set(dataDivChildHeader, "padding", "10px 15px");
                                        domStyle.set(dataDivChildData, "padding", "10px 15px");

                                        domClass.add(dataDivChildHeader, "col-xs-6");
                                        domClass.add(dataDivChildData, "col-xs-6 text-right font-bold");
                                        domClass.add(dataDivChildData, title.className);

                                        dataDivChildHeader.innerHTML = col.title;
                                        dataDivChildData.innerHTML = displayData;

                                        domConstruct.place(dataDivChildHeader, dataRowDivChild);
                                        domConstruct.place(dataDivChildData, dataRowDivChild);
                                        domConstruct.place(dataRowDivChild, dataRowDiv);
                                        counter++;
                                    }
                                    return col.hidden ? dataRow : undefined;
                                }
                            });

                            domConstruct.place(dataRowTd, dataRow);
                            return data ? data : false;
                        }
                    }
                };
            }
            // append data to be display.
            options.data = context.data;
            // create table
            context.tableObj = $(context.container).DataTable(options);

            if (_setting.customSearchOuter) {
                var tableSearchId = domAttr.get(context.container, "id") + "_filter";
                var tableSearchNode = dom.byId(tableSearchId);

                domStyle.set(tableSearchNode, "display", "none");

                $(_setting.customSearchOuter).on('keyup', function() {
                    context.tableObj.search(this.value).draw();
                });
            }

            if (_setting.reaponsiveRowClick) {
                // Add event listener for opening and closing details
                $(context.container).on('click', 'td:not(:first-child)', function() {
                    var tdFirst = query(this).closest("tr").children(':first-child');
                    $(tdFirst).trigger('click');
                });
            }

            $.fn.dataTable.ext.search.push(function(settings, data, dataIndex) {
                var originalData = settings.aoData[dataIndex]._aData;
                var filterredData = settings.filterredData;

                if (!filterredData) {
                    return true;
                }
                var flag = true;
                for (var property in filterredData) {
                    if (typeof filterredData[property] === "string") {
                        if (originalData[property] !== undefined &&
                            originalData[property].toString().toLowerCase().indexOf(
                                filterredData[property].toLowerCase()) > -1) {
                            flag = true;
                        } else {
                            flag = false;
                        }
                    } else {
                        flag = array.some(filterredData[property], function(value) {
                            if (originalData[property].toString().toLowerCase() === value.toLowerCase()) {
                                return true;
                            }
                        });
                    }

                    if (!flag) {
                        return flag;
                    }
                }
                return flag;
            });
            try {
                $(context.tableObj).DataTable(options);
            } catch (err) { console.log(err) }

            context.refreshTable();
        },
        attachOnClickListener: function(_setting) {
            var context = this;
            //clean up previous onclick events
            context.clickHandler.forEach(function(handle) {
                handle.remove();
            });

            array.forEach(context.editCreateFunctions, function(editCreateFunctionMap) {
                context.clickHandler.push(query("div[cellId^='cell_" + _setting.tableId + "_" + editCreateFunctionMap.columnKey + "_']").on("click", function(e) {
                    var row = domAttr.get(this, "cellId").split("_").pop();
                    domConstruct.empty(this);
                    var cellData = context.data[row][editCreateFunctionMap.columnKey];
                    var rowData = context.data[row];

                    editCreateFunctionMap.editCreateFunction(this, cellData, rowData, row, editCreateFunctionMap.col, editCreateFunctionMap.columnKey);
                }));
            });

            array.forEach(context.columnOnClickFunctions, function(columnOnClickFunctionMap) {
                context.clickHandler.push(query("div[cellId^='cell_" + _setting.tableId + "_" + columnOnClickFunctionMap.columnKey + "_']").on("click", function(e) {
                    e.stopPropagation();
                    var row = domAttr.get(this, "cellId").split("_").pop();
                    var cellData = context.data[row][columnOnClickFunctionMap.columnKey];
                    var rowData = context.data[row];
                    columnOnClickFunctionMap.columnOnClickFunction(this, cellData, rowData, row, columnOnClickFunctionMap.col, columnOnClickFunctionMap.columnKey);
                }));
            });
        },
        postTableDrawColumnCellListener: function(_setting) {
            var context = this;

            array.forEach(context.postTableDrawColumnCellFunctions, function(postTableDrawColumnCellFunctionMap) {
                var div = query("div[cellId^='cell_" + _setting.tableId + "_" + postTableDrawColumnCellFunctionMap.columnKey + "_']");
                array.forEach(div, function(divMap) {
                    var row = domAttr.get(divMap, "cellId").split("_").pop();
                    domConstruct.empty(divMap);
                    var cellData = context.data[row][postTableDrawColumnCellFunctionMap.columnKey];
                    var rowData = context.data[row];
                    postTableDrawColumnCellFunctionMap.postTableDrawColumnCellFunction(divMap, cellData, rowData, row, postTableDrawColumnCellFunctionMap.col, postTableDrawColumnCellFunctionMap.columnKey);
                });
            });
        },
        refreshTable: function() {
            var context = this;

            setTimeout(function() {
                //to fix table header size when window resize. might cause performance issue.
                context.tableObj.columns.adjust();
                context.tableObj.columns.adjust();
                context.tableObj.columns.adjust();
            }, 1000);
        }
    });
});
