define([
    "dojo/_base/declare",
    "dojo/text!./html/AmountTextfield.html",
    "dojo/_base/array",
    "dojo/dom-style",
    "dojo/html",
    "dojo/on",
    "dojo/topic",
    "dojo/dom-attr",
    "dojo/dom-class",
    "dojo/_base/config",
    "dijit/_TemplatedMixin",
    "app/components/ui/element/BaseElement",
    "app/components/utils/General",
    "app/components/utils/Rest",
    "app/components/ui/element/dropdown/Dropdown",
    "app/components/ui/element/slider/Slider",
    "dojo/i18n!app/locale/nls/common"
], function(
    declare,
    template,
    array,
    domStyle,
    html,
    on,
    topic,
    domAttr,
    domClass,
    config,
    TemplatedMixin,
    BaseElement,
    GeneralUtils,
    Rest,
    Dropdown,
    Slider,
    i18n_common
) {
    return declare("app.components.ui.element.amounttextfield.AmountTextfield", [BaseElement,
        TemplatedMixin
    ], {
        templateString: template,
        valueObj: null,
        sliderAmt: null,
        p_construct: function(setting) {
            var context = this;

            context.startListening(setting);
            context.setupContent(setting);
        },
        startup: function() {
            this.inherited(arguments);

            var context = this;
            context.valueObj = {};
            context.sliderAmt;
        },
        startListening: function(_setting) {
            var context = this;
            var domNode = this.domNode;
            var domNodeId = domAttr.get(domNode, "id");

            var dropdownRetrievalHandler = topic.subscribe(
                "dropdown.selected.retrieve." + domNodeId,
                function(_selectedData) {
                    switch (_selectedData.dropdownid) {
                        case domAttr.get(context.amtTextfieldFccdNode, "id"):
                            context.valueObj.fccd = _selectedData.value;
                            context.getAmountValue(_setting);
                            break;
                        default:
                            break;
                    }
                });
            topic.publish("topic.handlers.add", dropdownRetrievalHandler,
                domAttr.get(domNode, "id"));

            on(context.amtTextfieldNode, "keyup", function(e) {
                context.validateField(_setting);
                context.setInputNodeValue(true);
                context.getAmountValue(_setting);
                if (context.sliderAmt != null) {
                    var data = {};
                    data.from = domAttr.get(context.inputNode, "value");
                    context.sliderAmt.update(data);
                }
            });

            on(context.amtTextfieldDecimalNode, "keyup", function(e) {
                context.validateField(_setting);
                context.setInputNodeValue(true);
                context.getAmountValue(_setting);
            });

            var amountTextfieldDisableHandler = topic.subscribe(
                "amounttextfield.disable.handler." + _setting.componentid,
                function(_setting) {
                    array.forEach(_setting.components, function(single) {
                        switch (single.type) {
                            case "fccd":
                                _setting.disabled ? topic.publish(
                                    "dropdown.enable." +
                                    domAttr.get(context.amtTextfieldFccdNode,
                                        "id"), false) : topic.publish(
                                    "dropdown.enable." +
                                    domAttr.get(context.amtTextfieldFccdNode,
                                        "id"), true);
                                break;
                            case "amt":
                                _setting.disabled ? domAttr.set(
                                    context.amtTextfieldNode,
                                    "disabled", "") : domAttr.remove(
                                    context.amtTextfieldNode,
                                    "disabled");
                                break;
                            case "decimal":
                                _setting.disabled ? domAttr.set(
                                    context.amtTextfieldDecimalNode,
                                    "disabled", "") : domAttr.remove(
                                    context.amtTextfieldDecimalNode,
                                    "disabled");
                                break;
                        }
                    });
                });
            topic.publish("topic.handlers.add", amountTextfieldDisableHandler,
                domAttr.get(domNode, "id"));

            var amountTextfieldMaxMinAmount = topic.subscribe(
                "amounttextfield.max.min.limit." + _setting.componentid,
                function(maxMinLimit) {
                    array.forEach(maxMinLimit.limiter, function(single) {
                        switch (single.type) {
                            case "max":
                                context.valueObj.maxAmount = single
                                    .amount;
                                break;
                            case "min":
                                context.valueObj.minAmount = single
                                    .amount;
                                break;
                        }
                    });

                    var listOfValidations = "";
                    if (undefined != _setting.isRequired && _setting.isRequired) {
                        listOfValidations = listOfValidations + "required";
                    }

                    if (undefined != context.valueObj.maxAmount) {
                        listOfValidations = listOfValidations + " maximum|" +
                            context.valueObj.maxAmount;
                    }

                    if (undefined != context.valueObj.minAmount) {
                        listOfValidations = listOfValidations + " minimum|" +
                            context.valueObj.minAmount;
                    }

                    if (listOfValidations.length > 0) {
                        domAttr.set(context.inputNode, "data-validation",
                            listOfValidations);
                    }
                });
            topic.publish("topic.handlers.add", amountTextfieldMaxMinAmount,
                domAttr.get(domNode, "id"));

            var amountTextfieldResetField = topic.subscribe(
                "amounttextfield.reset.field." + _setting.componentid,
                function(data) {
                    array.forEach(data.resetData, function(single) {
                        switch (single.type) {
                            case "fccd":
                                topic.publish(
                                    "dropdown.resetDropdownList." +
                                    context.valueObj.fccdId,
                                    single.fccdList);
                                context.valueObj.fccd = single.fccdList[
                                    0].value;
                                context.getAmountValue(_setting);
                                break;
                            case "amount":
                                $(context.amtTextfieldNode).autoNumeric(
                                    "set", single.amount);
                                context.setInputNodeValue(false);
                                context.getAmountValue(_setting);
                                if (context.sliderAmt != null) {
                                    var data = {};
                                    data.from = domAttr.get(context.inputNode, "value");
                                    context.sliderAmt.update(data);
                                }
                                break;
                            case "decimal":
                                context.amtTextfieldDecimalNode.value =
                                    single.amount;
                                context.setInputNodeValue(false);
                                context.getAmountValue(_setting);
                                break;
                        }
                    });
                });
            topic.publish("topic.handlers.add", amountTextfieldResetField,
                domAttr.get(domNode, "id"));

            var amountTextfieldValidate = topic.subscribe("amounttextfield.validate." + _setting.componentid, function(data) {
                context.validateField(data);
            });
            topic.publish("topic.handlers.add", amountTextfieldValidate, domAttr.get(domNode, "id"));
        },
        setupContent: function(_setting) {
            var context = this;
            var domNode = this.domNode;
            var domNodeId = domAttr.get(domNode, "id");

            $(context.amtTextfieldNode).autoNumeric("init");

            if (_setting.slider) {
                var saveAmtResult = function(data) {
                    $(context.amtTextfieldNode).autoNumeric("set", data.from);
                    context.validateField(_setting);
                    context.setInputNodeValue(true);
                    context.getAmountValue(_setting);
                };

                var min = (_setting.slider.min === undefined) ? 0 : _setting.slider.min;
                var max = (_setting.slider.max === undefined) ? parseInt(domAttr.get(context.amtTextfieldNode, "data-v-max")) : _setting.slider.max;

                html.set(context.minSliderLabelNode, min);
                var maxValue = context.convertToNumeral(max);
                html.set(context.maxSliderLabelNode, maxValue);

                // Start create slider
                context.sliderAmt = new Slider();
                context.sliderAmt.placeAt(context.sliderAmountContainerNode);
                context.sliderAmt.p_construct({
                    parentid: domNodeId,
                    type: (_setting.slider.type === undefined) ? "single" : _setting.slider.type,
                    grid: (_setting.slider.grid === undefined) ? false : _setting.slider.grid,
                    min: min,
                    max: max,
                    from: (_setting.defaultValue !== undefined) ? _setting.defaultValue : 0,
                    step: (_setting.slider.step === undefined) ? 1 : _setting.slider.step,
                    onChange: (_setting.slider.saveAmtResult === undefined) ? saveAmtResult : _setting.slider.saveAmtResult
                });
            } else {
                domClass.add(context.sliderWrapperNode, "hidden");
            }

            context.createMask(context.amtTextfieldDecimalNode, {
                type: "amountMask"
            });


            if (undefined == _setting.defaultValue || "" == _setting.defaultValue) {
                $(context.amtTextfieldNode).autoNumeric("set", 0);
            } else {
                $(context.amtTextfieldNode).autoNumeric("set", _setting.defaultValue);
            }

            if (undefined == _setting.decimalDefaultValue || "" == _setting.decimalDefaultValue) {
                context.amtTextfieldDecimalNode.value = "00";
            } else {
                context.amtTextfieldDecimalNode.value = _setting.decimalDefaultValue;
            }

            if (undefined == _setting.fccdList) {
                var prototypeUrl = "assets/data/fccd.json";
                var dataPackage = {};

                var rest = new Rest().p_postapi({ "api": "lov/getcurrency" },
                    prototypeUrl, dataPackage);
                rest.then(function(response) {
                    context.constructfccd(response.currencies);
                }, function(err) {
                    console.log("Error", err);
                });
            } else {
                context.constructfccd(_setting.fccdList);
            }

            if (undefined != _setting.disableFccd && _setting.disableFccd) {
                topic.publish("dropdown.enable." + domAttr.get(context.amtTextfieldFccdNode,
                    "id"), false);
            }

            if (undefined != _setting.disableAmount && _setting.disableAmount) {
                domAttr.set(context.amtTextfieldNode, "disabled", "");
            }

            if (undefined != _setting.disableDecimal && _setting.disableDecimal) {
                domAttr.set(context.amtTextfieldDecimalNode, "disabled", "");
            }

            if (undefined != _setting.disableAll && _setting.disableAll) {
                topic.publish("dropdown.enable." + domAttr.get(context.amtTextfieldFccdNode,
                    "id"), false);
                domAttr.set(context.amtTextfieldNode, "disabled", "");
                domAttr.set(context.amtTextfieldDecimalNode, "disabled", "");
            }


            var listOfValidations = "";
            if (undefined != _setting.isRequired && _setting.isRequired) {
                listOfValidations = listOfValidations + "required";
            }

            if (undefined != _setting.maxAmount) {
                context.valueObj.maxAmount = _setting.maxAmount;
                listOfValidations = listOfValidations + " maximum|" + _setting.maxAmount;
            }

            if (undefined != _setting.minAmount) {
                context.valueObj.minAmount = _setting.minAmount;
                listOfValidations = listOfValidations + " minimum|" + _setting.minAmount;
            }

            if (listOfValidations.length > 0) {
                domAttr.set(context.inputNode, "data-validation",
                    listOfValidations);
            }

            context.setInputNodeValue(false);
            context.getAmountValue(_setting);
        },
        convertToNumeral: function(maxNumber) {
            numeral.language('fr', {
                delimiters: {
                    thousands: ',',
                    decimal: '.'
                },
                abbreviations: {
                    thousand: 'k',
                    million: 'm',
                    billion: 'b',
                    trillion: 't'
                }
            });
            numeral.language('fr');
            var max = numeral(maxNumber).format('0a');
            var maxValue = "";
            array.forEach(max, function(single, i) {
                if (typeof single === "number") {
                    maxValue += single;
                } else {
                    switch (single) {
                        case "k":
                            single = i18n_common.thousand;
                            break;
                        case "m":
                            single = i18n_common.million;
                            break;
                        case "b":
                            single = i18n_common.billion;
                            break;
                        case "t":
                            single = i18n_common.trillion;
                            break;
                    }
                    maxValue += single;
                }
            });
            return maxValue;
        },
        getAmountValue: function(_setting) {
            var context = this;
            topic.publish("amounttextfield." + _setting.componentid, {
                fccd: context.valueObj.fccd,
                amount: domAttr.get(context.inputNode, "value")
            });
        },
        constructfccd: function(_setting) {
            var context = this;
            var domNode = this.domNode;

            var uniqueIdFormQueryDropdown = new GeneralUtils().createUUID();
            context.valueObj.fccdId = uniqueIdFormQueryDropdown;
            domAttr.set(context.amtTextfieldFccdNode, "id",
                uniqueIdFormQueryDropdown);
            var formQueryDropdown = new Dropdown().placeAt(context.amtTextfieldFccdNode);
            formQueryDropdown.p_construct({
                parentid: domAttr.get(domNode, "id"),
                dropdownid: uniqueIdFormQueryDropdown,
                data: _setting,
                searchplaceholder: "Search here...",
                _default: true,
                liveSearch: true
            });
            context.valueObj.fccd = _setting[0].value;
        },
        validateField: function(_setting) {
            var context = this;
            var domNode = this.domNode;
            var domNodeId = domAttr.get(domNode, "id");
            var hasPassLimitChecking = true;
            var hasPassRequiredChecking = true;
            var max = context.valueObj.maxAmount;
            var min = context.valueObj.minAmount

            var value = $(context.amtTextfieldNode).autoNumeric("get");

            if (value.length > 0) {
                if (undefined != max && undefined == min && value >= max) {
                    domClass.add(context.amtTextfieldNode, "input-error");
                    hasPassLimitChecking = false;

                } else if (undefined != min && undefined == max && value <= min) {
                    domClass.add(context.amtTextfieldNode, "input-error");
                    hasPassLimitChecking = false;

                } else if (undefined != min && undefined != max && (value <= min ||
                        value >= max)) {
                    domClass.add(context.amtTextfieldNode, "input-error");
                    hasPassLimitChecking = false;
                }
            }

            if (hasPassLimitChecking) {
                domClass.remove(context.amtTextfieldNode, "input-error");
            }


            if (_setting.isRequired) {
                var fccdPass = true;
                var amtPass = true;
                var decimalPass = true;

                if (isEmpty(context.valueObj.fccd)) {
                    domClass.add(context.amtTextfieldFccdNode, "input-error");
                    fccdPass = false;
                } else {
                    domClass.remove(context.amtTextfieldFccdNode, "input-error");
                    fccdPass = true;
                }

                if (isEmpty(domAttr.get(context.amtTextfieldDecimalNode,
                        "value"))) {
                    domClass.add(context.amtTextfieldDecimalNode, "input-error");
                    decimalPass = false;
                } else {
                    domClass.remove(context.amtTextfieldDecimalNode,
                        "input-error");
                    decimalPass = true;
                }

                if (isEmpty(value)) {
                    domClass.add(context.amtTextfieldNode, "input-error");
                    amtPass = false;
                } else {
                    amtPass = true;
                    if (hasPassLimitChecking) {
                        domClass.remove(context.amtTextfieldNode, "input-error");
                    }
                }
            }

            function isEmpty(_value) {
                if (undefined == _value || _value.length == 0) {
                    return true;
                } else {
                    return false;
                }
            }
        },
        setInputNodeValue: function(isValidate) {
            var context = this;
            var domNode = this.domNode;
            var amount = NaN;

            if ($(context.amtTextfieldNode).autoNumeric("get").length > 0 &&
                domAttr.get(context.amtTextfieldDecimalNode, "value").length >
                0) {
                amount = parseFloat($(context.amtTextfieldNode).autoNumeric(
                    "get") + "." + domAttr.get(context.amtTextfieldDecimalNode,
                    "value")).toFixed(2);
            }

            if (isNaN(amount)) {
                context.inputNode.value = "";
            } else {
                context.inputNode.value = amount;
            }

            if (isValidate) {
                context.p_checkingValidation(domNode, function(result) {});
            }
        }
    });

});
