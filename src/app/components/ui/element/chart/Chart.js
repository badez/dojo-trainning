define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/text!./html/Chart.html",
    "dojo/_base/array",
    "dojo/dom-style",
    "dojo/dom-construct",
    "dojo/_base/window",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "app/components/ui/element/BaseElement"
], function(
    declare,
    lang,
    template,
    array,
    domStyle,
    domConstruct,
    win,
    _Widget,
    TemplatedMixin,
    BaseElement
) {
    return declare("app.components.ui.element.chart.Chart", [BaseElement, TemplatedMixin], {
        templateString: template,
        p_construct: function(setting) {
            var domNode = this.domNode;
            var context = this;
            var duplicate = lang.clone(setting);
            var legend;

            context.startListening(duplicate);
            context.initInteraction(duplicate);
            context.setupContent(duplicate);
            context.i18n(duplicate);
        },
        startup: function() {
            this.inherited(arguments);

            var domNode = this.domNode;
            var context = this;
        },
        destroy: function() {
            this.inherited(arguments);
        },
        startListening: function(_setting) {
            var context = this;
            var domNode = this.domNode;

        },
        initInteraction: function(_setting) {
            var context = this;
        },
        setupContent: function(_setting) {
            var context = this;
            var domNode = this.domNode;
            var chart, colors = [];

            // var allBorderColors = ["rgba(220,220,220,1)", "rgba(26,179,148,1)", "rgba(189, 140, 191, 1)", "rgba(128, 255, 255,1)", "rgba(255, 186, 0,1)", "rgba(181, 184, 207,1)", "rgba(39, 196, 212,1)", "rgba(156, 156, 156,1)", "rgba(168, 234, 80,1)"];
            // var allFillColors = ["rgba(220,220,220,0.5)", "rgba(26,179,148,0.5)", "rgba(189, 140, 191, 0.5)", "rgba(128, 255, 255,0.5)", "rgba(255, 186, 0,0.5)", "rgba(181, 184, 207,0.5)", "rgba(39, 196, 212,0.5)", "rgba(156, 156, 156,0.5)", "rgba(168, 234, 80,0.5)"];
            //var barHoverFillColors = ["rgba(220,220,220,0.75)", "rgba(26,179,148,0.75)", "rgba(189, 140, 191, 0.75)", "rgba(128, 255, 255,0.75)", "rgba(255, 186, 0,0.75)", "rgba(181, 184, 207,0.75)", "rgba(39, 196, 212,0.75)", "rgba(156, 156, 156,0.75)", "rgba(168, 234, 80,0.75)"];
            //var barBorderColors = ["rgba(220,220,220,0.8)", "rgba(26,179,148,0.8)", "rgba(189, 140, 191, 0.8)", "rgba(128, 255, 255,0.8)", "rgba(255, 186, 0,0.8)", "rgba(181, 184, 207,0.8)", "rgba(39, 196, 212,0.8)", "rgba(156, 156, 156,0.8)", "rgba(168, 234, 80,0.8)"];
            //var solidFillColors = ["#a3e1d4", "#dedede", "#b5b8cf", "#e8f7ff", "#ffe399", "#e4d1e5", "#9ddff9", "#b5b5b5", "#d4f6a9"];

            var allBorderColors = ["rgba(220,220,220,1)", "rgba(9,169,136,1)", "rgba(220, 220, 220, 1)", "rgba(176, 228, 255,1)", "rgba(245, 208, 110,1)", "rgba(229, 180, 229,1)", "rgba(113, 199, 231,1)", "rgba(156, 156, 156,1)", "rgba(192, 233, 138,1)", "rgba(206, 173, 129,1)"];
            var allFillColors = ["rgba(220,220,220,0.5)", "rgba(9,169,136,0.5)", "rgba(181, 184, 207, 0.5)", "rgba(198, 236, 255,0.5)", "rgba(255, 186, 0,0.5)", "rgba(189, 140, 191,0.5)", "rgba(39, 165, 212,0.5)", "rgba(70, 70, 70,0.5)", "rgba(168, 234, 80,0.5)", "rgba(163, 98, 10,0.5)"];
            var barHoverFillColors = ["rgba(220,220,220,0.75)", "rgba(9,169,136,0.75)", "rgba(181, 184, 207, 0.75)", "rgba(198, 236, 255,0.75)", "rgba(255, 186, 0,0.75)", "rgba(189, 140, 191,0.75)", "rgba(39, 165, 212,0.75)", "rgba(70, 70, 70,0.75)", "rgba(168, 234, 80,0.75)", "rgba(163, 98, 10,0.75)"];
            var barBorderColors = ["rgba(220,220,220,0.8)", "rgba(9,169,136,0.8)", "rgba(220, 220, 220, 0.8)", "rgba(176, 228, 255,0.8)", "rgba(245, 208, 110,0.8)", "rgba(229, 180, 229,0.8)", "rgba(113, 199, 231,0.8)", "rgba(156, 156, 156,0.8)", "rgba(192, 233, 138,0.8)", "rgba(206, 173, 129,0.8)"];
            var solidFillColors = ["rgba(222,222,222,1)", "rgba(163,225,212,1)", "rgba(181, 184, 207,1)", "rgba(232, 247, 255,1)", "rgba(255, 227, 153,1)", "rgba(229, 209, 229,1)", "rgba(156, 223, 248,1)", "rgba(181, 181, 181,1)", "rgba(212, 246, 168,1)", "rgba(218, 192, 157,1)"];

            if (_setting == undefined || _setting.options == undefined)
                return;

            var intRandomizer = [];
            while (intRandomizer.length < _setting.options.data.length) {
                var randomNumber = Math.floor(Math.random() * 9);
                var found = false;

                found = array.some(intRandomizer, function(intRandom) {
                    return (intRandom == randomNumber);
                });

                if (!found)
                    intRandomizer[intRandomizer.length] = randomNumber;
            }

            var chart;

            if (_setting.size) {
                context.chartNode.width = _setting.size.width;
                context.chartNode.height = _setting.size.height;
            }

            var lineTension = 0.4;
            if(_setting.options.lineTension || _setting.options.lineTension === 0){
                lineTension = _setting.options.lineTension;
            }

            switch (_setting.type) {
                case "line":
                    array.forEach(_setting.options.data, function(data, i) {
                        data.backgroundColor = allFillColors[intRandomizer[i]];
                        data.borderColor = allBorderColors[intRandomizer[i]];
                        data.pointBackgroundColor = allBorderColors[intRandomizer[i]];
                        data.fill = true;
                        data.borderWidth = 2;
                        data.lineTension = lineTension;
                        data.pointBorderColor = "#fff";
                        data.pointHoverBorderColor = "#fff";
                        data.pointBorderWidth = 1;
                        data.pointRadius = 4;
                    });
                    var ctxLine = context.chartNode.getContext("2d");
                    chart = new Chart(ctxLine, {
                        type: "line",
                        data: {
                            labels: _setting.labels,
                            datasets: _setting.options.data
                        },
                        options: {
                            legend: {
                                display: (_setting.legend === false) ? _setting.legend : true,
                                position: "bottom",
                                labels: {
                                    boxWidth: 15
                                }
                            },
                            scales: {
                                yAxes: [{
                                    scaleLabel: {
                                        display: _setting.options.yLabel ? true : false,
                                        labelString: _setting.options.yLabel ? _setting.options.yLabel : "",
                                    },
                                    ticks: {
                                        beginAtZero: true
                                    }
                                }],
                                xAxes: [{
                                    scaleLabel: {
                                        display: _setting.options.xLabel ? true : false,
                                        labelString: _setting.options.xLabel ? _setting.options.xLabel : "",
                                    }
                                }]
                            }
                        }
                    });
                    break;
                case "pie":
                    var pieColor = [];
                    var pieHover = [];
                    array.forEach(_setting.options.data, function(data, i) {
                        pieColor.push(solidFillColors[intRandomizer[i]]);
                        pieHover.push("#1ab394");
                    });
                    var ctxPie = context.chartNode.getContext("2d");
                    chart = new Chart(ctxPie, {
                        type: "pie",
                        data: {
                            labels: _setting.labels,
                            datasets: [{
                                data: _setting.options.data,
                                backgroundColor: pieColor,
                                hoverBackgroundColor: pieHover
                            }]
                        },
                        options: {
                            legend: {
                                display: (_setting.legend === false) ? _setting.legend : true,
                                position: "bottom",
                                labels: {
                                    boxWidth: 15

                                }
                            }
                        }
                    });
                    break;
                case "donut":
                    var ctxDonut = context.chartNode.getContext("2d");
                    var donutColor = [];
                    var donutHover = [];
                    var tooltipLabels = {};
                    array.forEach(_setting.options.data, function(optionsData, i) {
                        donutColor.push(solidFillColors[intRandomizer[i]]);
                        donutHover.push("#1ab394");

                        array.forEach(_setting.labels, function(optionsLabels, j) {
                            if (i === j) {
                                tooltipLabels[optionsLabels] = optionsData;
                            }
                        });

                        if (optionsData < 0) {
                            _setting.options.data[i] = Math.abs(optionsData);
                        }
                    });
                    var customTooltips = function(tooltip) {

                        var tooltipEl = $('#chartjs-tooltip');


                        if (tooltipEl[0]) {
                            tooltipEl.remove();
                        }

                        // Hide if no tooltip
                        if (!tooltip.opacity) {
                            return;
                        }

                        var clientRect = $(context.chartNode)[0].getBoundingClientRect();
                        var offset = $(context.chartNode).offset();
                        domConstruct.create("div", {
                            id: "chartjs-tooltip",
                            style: "top: " + (offset.top + (clientRect.height / 2)) + "px; left: " + (offset.left + (clientRect.width / 2)) + "px;" + "opacity: 1; position: absolute; background: rgba(0, 0, 0, .7); color: white; border-radius: 3px; -webkit-transition: all .1s ease; transition: all .1s ease; pointer-events: none; -webkit-transform: translate(-50%, -50%); transform: translate(-50%, -50%); padding: 5px;"

                        }, win.body());

                        tooltipEl = $('#chartjs-tooltip');

                        $(this._chart.canvas).css('cursor', 'pointer');

                        tooltipEl.css({
                            opacity: 1
                        });
                        // Set Text
                        if (tooltip.body) {
                            var lines = "";
                            array.forEach(tooltip.body, function(tooltipBody) {
                                array.forEach(tooltipBody.lines, function(tooltipBodyLine) {
                                    var bodyLine = tooltipBodyLine.split(":");
                                    if (tooltipLabels[bodyLine[0]] < 0) {
                                        lines += tooltipBodyLine.replace(": ", ": -") + "<br>";
                                    } else {
                                        lines += tooltipBodyLine + "<br>";
                                    }
                                });
                            });
                            tooltipEl.html(lines);
                        }
                    };

                    chart = new Chart(ctxDonut, {
                        type: "doughnut",
                        data: {
                            labels: _setting.labels,
                            datasets: [{
                                data: _setting.options.data,
                                backgroundColor: donutColor,
                                hoverBackgroundColor: donutHover
                            }]
                        },
                        options: {
                            legend: {
                                display: (_setting.legend === false) ? _setting.legend : true,
                                position: "bottom",
                                labels: {
                                    boxWidth: 15

                                }
                            },
                            tooltips: {
                                enabled: false,
                                custom: (_setting.showTooltips === false) ? null : customTooltips,
                            },
                        }
                    });
                    break;
                case "gauge":
                    var ctxGauge = context.chartNode.getContext("2d");
                    var donutColor = [];
                    var donutHover = [];
                    if (_setting.showPercentage) {
                        donutColor = ["#1ab394", "#e0e0e0"];
                        donutHover = [];

                        console.log("_setting.percentage",_setting.percentage);
                    } else {
                        array.forEach(_setting.options.data, function(data, i) {
                            donutColor.push(solidFillColors[intRandomizer[i]]);
                            donutHover.push("#1ab394");
                        });
                    }
                    var data = {
                        labels: _setting.labels,
                        datasets: [{
                            data: _setting.options.data,
                            backgroundColor: donutColor,
                            hoverBackgroundColor: donutHover
                        }]
                    };
                    chart = new Chart(ctxGauge, {
                        type: "doughnut",
                        data: data,
                        options: {
                            legend: {
                                display: (_setting.legend === false) ? _setting.legend : true,
                                position: "bottom",
                                labels: {
                                    boxWidth: 15
                                }
                            },
                            tooltips: { enabled: (_setting.showTooltips === false) ? _setting.showTooltips : true },
                            showPercentage: (_setting.showPercentage === undefined) ? false : true,
                            rotation: 1 * Math.PI,
                            circumference: 1 * Math.PI,
                            animation: {
                                animateRotate : (_setting.animation === undefined) ? true : _setting.animation
                            }
                        }
                    });
                    break;
                case "bar":
                    array.forEach(_setting.options.data, function(data, i) {
                        data.borderColor = barBorderColors[intRandomizer[i]];
                        data.backgroundColor = allFillColors[intRandomizer[i]];
                        data.hoverBorderColor = allBorderColors[intRandomizer[i]];
                        data.hoverBackgroundColor = barHoverFillColors[intRandomizer[i]];
                        data.borderWidth = 2;
                    });
                    var ctxBar = context.chartNode.getContext("2d");
                    if (!_setting.stacked) {
                        chart = new Chart(ctxBar, {
                            type: "bar",
                            data: {
                                labels: _setting.labels,
                                datasets: _setting.options.data
                            },
                            options: {
                                legend: {
                                    display: (_setting.legend === false) ? _setting.legend : true,
                                    position: "bottom",
                                    labels: {
                                        boxWidth: 15

                                    }
                                },
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }]
                                }
                            }
                        });
                    } else {
                        chart = new Chart(ctxBar, {
                            type: "bar",
                            data: {
                                labels: _setting.labels,
                                datasets: _setting.options.data
                            },
                            options: {
                                legend: {
                                    display: (_setting.legend === false) ? _setting.legend : true,
                                    position: "bottom",
                                    labels: {
                                        boxWidth: 15
                                    }
                                },
                                scales: {
                                    xAxes: [{
                                        stacked: true
                                    }],
                                    yAxes: [{
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }]
                                }
                            }
                        });
                    }
                    break;
                default:
                    break;
            }

            Chart.pluginService.register({
                beforeDraw: function(chart) {
                    if (chart.config.options.showPercentage && _setting.percentage !== undefined) {

                        var width = chart.chart.width,
                            height = chart.chart.height,
                            ctx = chart.chart.ctx;
                        ctx.clearRect(0, 0, width, height);
                        ctx.restore();
                        var fontSize = (height / 60).toFixed(2);
                        ctx.font = fontSize + "em sans-serif";
                        var oldFillStyle = ctx.fillStyle;
                        if(_setting.percentageLabelColor){
                            ctx.fillStyle = _setting.percentageLabelColor;
                        }
                        ctx.textBaseline = "middle";
                        var text = _setting.percentage + "%",
                            textX = Math.round((width - ctx.measureText(text).width) / 2),
                            textY = height - (height / 8);

                        ctx.fillText(text, textX, textY);
                        if(_setting.percentageLabelColor){
                            ctx.fillStyle = oldFillStyle;
                        }
                        ctx.save();
                    }
                }
            });


            // legend = chart.generateLegend();

            // if (_setting.showLegend) {
            //     context.labelNode.innerHTML = legend;
            // }

        },
        // custom: function (_setting) {
        //     Chart.types.Bar.extend({
        //         name: "BarAlt",
        //         draw: function() {
        //             if (this.datasets.length == 1) {
        //                 this.options.barValueSpacing = this.chart.width / 16;
        //             } else if (this.datasets.length == 2) {
        //                 this.options.barValueSpacing = this.chart.width / 22;
        //             } else if (this.datasets.length == 3) {
        //                 this.options.barValueSpacing = this.chart.width / 32;
        //             } else if (this.datasets.length == 4) {
        //                 this.options.barValueSpacing = this.chart.width / 50;
        //             } else if (this.datasets.length == 5) {
        //                 this.options.barValueSpacing = this.chart.width / 90;
        //             }
        //             Chart.types.Bar.prototype.draw.apply(this, arguments);
        //         }
        //     });

        //     Chart.types.StackedBar.extend({
        //         name: "StackedBarAlt",
        //         draw: function() {
        //             this.options.barValueSpacing = this.chart.width / 16;
        //             Chart.types.StackedBar.prototype.draw.apply(this, arguments);
        //         }
        //     });
        // console.log("_setting", _setting);
        // Chart.pluginService.register({
        //     beforeDraw: function (chart) {
        //         var width = chart.chart.width,
        //             height = chart.chart.height,
        //             ctx = chart.chart.ctx;

        //         ctx.restore();
        //         var fontSize = (height / 60).toFixed(2);
        //         ctx.font = fontSize + "em sans-serif";
        //         ctx.textBaseline = "middle";

        //         var text = "75%",
        //             textX = Math.round((width - ctx.measureText(text).width) / 2),
        //             textY = height - (height / 10);

        //         ctx.fillText(text, textX, textY);
        //         ctx.save();
        //     }
        // });
        // },
        legend: function() {
            return legend;
        },
        i18n: function(_setting) {
            var context = this;
        }
    });
});
