define([
    "dojo/_base/declare",
    "dojo/text!./html/FilterCheckBoxCell.html",
    "dojo/html",
    "dojo/topic",
    "dojo/dom-attr",
    "dijit/_TemplatedMixin",
    "app/components/ui/cell/BaseCell"

], function(
    declare,
    template,
    html,
    topic,
    domAttr,
    TemplatedMixin,
    BaseCell
) {
    return declare("app.components.ui.element.checkboxfilter.FilterCheckBoxCell", [BaseCell, TemplatedMixin], {
        templateString: template,
        p_construct: function(setting) {
            var domNode = this.domNode;
            var context = this;

            context.startListening(setting);
            context.setupContent(setting);
            context.initInteraction(setting);
        },
        constructor: function(setting) {
            var context = this;

        },
        startup: function() {
            this.inherited(arguments);
        },
        destroy: function() {
            this.inherited(arguments);
        },
        startListening: function(_setting) {
            var context = this;
            var domNode = this.domNode;

        },
        initInteraction: function(_setting) {
            var context = this;
            var domNode = this.domNode;

            context.checkboxEventHandler(context.checkboxNode, "click", function(isChecked) {
                topic.publish(_setting.parentId + ".checkbox.checkforall");
            });

        },
        setupContent: function(_setting) {
            var context = this;
            var domNode = this.domNode;

            domAttr.set(context.checkboxNode, "name", _setting.name);
            html.set(context.checkboxLabelNode, _setting.keyname);
            domAttr.set(context.checkboxNode, "value", _setting.keyname);
        }
    });

});
