define([
    "dojo/_base/declare",
    "dojo/text!./html/FilterCheckBox.html",
    "dojo/_base/array",
    "dojo/html",
    "dojo/topic",
    "dijit/_TemplatedMixin",
    "app/components/ui/cell/BaseCell",
    "app/components/ui/element/checkboxfilter/FilterCheckBoxCell",
    "app/components/utils/General",

], function(
    declare,
    template,
    array,
    html,
    topic,
    TemplatedMixin,
    BaseCell,
    FilterCheckBoxCell,
    GeneralUtils
) {
    return declare("app.components.ui.element.checkboxfilter.FilterCheckBox", [BaseCell, TemplatedMixin], {
        templateString: template,
        childCheckBoxs: null,
        p_construct: function(setting) {
            var domNode = this.domNode;
            var context = this;

            context.startListening(setting);
            context.setupContent(setting);
            context.initInteraction(setting);
            context.i18n(setting);
        },
        constructor: function(setting) {
            var context = this;

        },
        startup: function() {
            this.inherited(arguments);

            var context = this;
            context.childCheckBoxs = [];
        },
        destroy: function() {
            this.inherited(arguments);
        },
        startListening: function(_setting) {
            var context = this;
            var domNode = this.domNode;
            var domNodeId = domAttr.get(domNode, "id");

            var checkboxHandler = topic.subscribe(domNodeId + ".checkbox.checkforall", function() {
                var falseCheckboxes = _.filter(context.childCheckBoxs, function(child) {
                    return !child.checkboxNode.checked;
                });

                if (falseCheckboxes.length === 0) {
                    context.checkboxToggle(context.checkboxAllNode, "check");
                } else {
                    context.checkboxToggle(context.checkboxAllNode, "uncheck");
                }
            });
            topic.publish("topic.handlers.add", checkboxHandler, domNodeId);

        },
        initInteraction: function(_setting) {
            var context = this;
            var domNode = this.domNode;
            var domNodeId = domAttr.get(domNode, "id");

            //parent onclick
            context.checkboxEventHandler(context.checkboxAllNode, "click", function(isChecked) {
                if (isChecked) {
                    context.checkboxToggleAll(_setting.name, "check");
                } else {
                    context.checkboxToggleAll(_setting.name, "uncheck");
                }
            });
        },
        setupContent: function(_setting) {
            var context = this;
            var domNode = this.domNode;
            var domNodeId = domAttr.get(domNode, "id");
            context.childCheckBoxs = [];

            html.set(context.titleLabelNode, _setting.labelTitle);
            if (_setting.data.length === 1) {
                domStyle.set(context.checkboxAllBoxNode, "display", "none");
                domClass.add(context.CheckboxContainerNode, "row");
            }
            array.forEach(_setting.data, function(single, i) {

                single.parentId = domNodeId;
                single.name = _setting.name;

                var checkBoxCell = new FilterCheckBoxCell().placeAt(context.CheckboxContainerNode);
                checkBoxCell.p_construct(single);
                context.childCheckBoxs.push(checkBoxCell);
            });
        },
        i18n: function(_setting) {
            var context = this;

            html.set(context.checkboxAllLabelNode, i18n_common.all);
        }
    });
});
