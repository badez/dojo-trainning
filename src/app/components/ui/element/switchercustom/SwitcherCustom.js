define([
    "dojo/_base/declare",
    "dojo/dom-attr",
    "dojo/dom-class",
    "dojo/text!./html/SwitcherCustom.html",
    "dojo/on",
    "dijit/_TemplatedMixin",
    "app/components/ui/element/BaseElement",
    "app/components/utils/General"
], function (
    declare,
    domAttr,
    domClass,
    template,
    on,
    TemplatedMixin,
    BaseElement,
    GeneralUtils
) {
    return declare("app.components.ui.element.switchercustom.SwitcherCustom", [BaseElement, TemplatedMixin], {
        templateString: template,
        p_construct: function (setting) {
            var domNode = this.domNode;
            var context = this;

            context.setupContent(setting);
            context.initInteraction(setting);
        },
        initInteraction: function (_setting) {
            var context = this;
            var domNode = this.domNode;

            on(context.swicherCustomNode, "change", function () {
                if (_setting.onChange) {
                    _setting.onChange(context.swicherCustomNode.checked);
                }
            });
        },
        setupContent: function (_setting) {
            var context = this;
            var domNode = this.domNode;
            var disable = false;

            var uniqueSwitcherCustomId = new GeneralUtils().createUUID();
            domAttr.set(context.swicherCustomNode, "id", uniqueSwitcherCustomId);
            domAttr.set(context.swicherCustomLabelNode, "for", uniqueSwitcherCustomId);

            if (_setting.disabled) {
                context.swicherCustomNode.disabled = true;
                domClass.add(context.swicherCustomLabelNode, "disabled");
            }
            if (_setting.checked) {
                context.swicherCustomNode.checked = true;
            }
        }
    });
});
