define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/topic",
    "dojo/dom-attr",
    "dojo/dom-geometry",
    "dojo/text!./html/Dropdown.html",
    "dojo/on",
    "dojo/_base/array",
    "dojo/dom-construct",
    "dojo/html",
    "dojo/i18n!app/locale/nls/common",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "app/components/ui/element/BaseElement",
    "app/components/ui/element/dropdown/DropdownCell",
    "app/components/utils/Rest"
], function(
    declare,
    lang,
    domClass,
    domStyle,
    topic,
    domAttr,
    domGeometry,
    template,
    on,
    array,
    domConstruct,
    html,
    i18n_common,
    _Widget,
    TemplatedMixin,
    BaseElement,
    DropdownCell,
    Rest
) {
    return declare("app.components.ui.element.dropdown.Dropdown", [BaseElement, TemplatedMixin], {
        templateString: template,
        //to store the 
        dropdownCellsMap: [],
        p_construct: function(setting) {
            this.inherited(arguments);

            var domNode = this.domNode;
            var context = this;

            var duplicate = lang.clone(setting);
            context.dropdownCellsMap = [];
            context.startListening(duplicate);
            context.initInteraction(duplicate);
            context.setupContent(duplicate);
            context.i18n(duplicate);
        },
        startup: function() {
            this.inherited(arguments);
            var context = this;
        },
        destroy: function() {
            var context = this;
            this.inherited(arguments);
        },
        startListening: function(_setting) {
            var context = this;
            var domNode = this.domNode;

            var dropdownHandler = topic.subscribe("dropdown.select." + _setting.dropdownid, function(selectedData) {
                if (_setting.required != undefined && _setting.required)
                    domAttr.set(context.inputNode, "value", "required");
                if (_setting.type != "download") {
                    html.set(context.dropdownLabelNode, selectedData.keyname);
                }
                array.forEach(context.dropdownCellsMap, function(single, i) {
                    single.unselectedValue(selectedData.cellid);
                });
                array.forEach(context.dropdownCellsMap, function(single, i) {
                    single.selectValue(selectedData.value);
                });

                topic.publish("dropdown.selected.retrieve." + _setting.parentid, selectedData);
            });
            topic.publish("topic.handlers.add", dropdownHandler, domAttr.get(domNode, "id"));

            //for use case set value to dropdown
            var dropdownSelectHandler = topic.subscribe("dropdown.select.value." + _setting.dropdownid, function(selectedValue) {

                if (_setting.api) {
                    if (context.apiData == undefined) {
                        var rest = new Rest().p_postapi({
                            p_api: _setting.api
                        }, _setting.prototypeUrl, _setting.dataPackage, "");

                        rest.then(function(response) {
                            _setting.data = response[_setting.dataSrc];
                            context.resetDropdownList(_setting);
                            context.apiData = _setting.data;
                            array.some(_setting.data, function(currentData) {
                                if (currentData.value == selectedValue) {
                                    if (_setting.required != undefined && _setting.required)
                                        domAttr.set(context.inputNode, "value", "required");
                                    if (_setting.type != "download")
                                        html.set(context.dropdownLabelNode, currentData.keyname);
                                    topic.publish("dropdown.selected.retrieve." + _setting.parentid, currentData);
                                }
                                return (currentData.value == selectedValue);
                            });
                        });
                    } else {
                        array.some(_setting.data, function(currentData) {
                            if (currentData.value == selectedValue) {
                                if (_setting.required != undefined && _setting.required)
                                    domAttr.set(context.inputNode, "value", "required");
                                if (_setting.type != "download")
                                    html.set(context.dropdownLabelNode, currentData.keyname);
                                topic.publish("dropdown.selected.retrieve." + _setting.parentid, currentData);
                            }
                            return (currentData.value == selectedValue);
                        });
                    }
                } else {
                    array.some(_setting.data, function(currentData) {
                        if (currentData.value == selectedValue) {
                            if (_setting.required != undefined && _setting.required)
                                domAttr.set(context.inputNode, "value", "required");
                            if (_setting.type != "download")
                                html.set(context.dropdownLabelNode, currentData.keyname);
                            topic.publish("dropdown.selected.retrieve." + _setting.parentid, currentData);
                        }
                        return (currentData.value == selectedValue);
                    });
                    array.some(_setting.data, function(currentData) {
                        if (currentData.keyname == selectedValue) {
                            if (_setting.required != undefined && _setting.required)
                                domAttr.set(context.inputNode, "value", "required");
                            if (_setting.type != "download")
                                html.set(context.dropdownLabelNode, currentData.keyname);
                            topic.publish("dropdown.selected.retrieve." + _setting.parentid, currentData);
                        }
                        return (currentData.keyname == selectedValue);
                    });
                }
                array.forEach(context.dropdownCellsMap, function(single, i) {
                    single.selectValue(selectedValue);
                });

            });
            topic.publish("topic.handlers.add", dropdownSelectHandler, domAttr.get(domNode, "id"));

            var resetHandler = topic.subscribe("reset.field", function(_id) {
                if (domAttr.get(context.inputNode, "id") == _id) {
                    if (_setting.label == undefined) {
                        html.set(context.dropdownLabelNode, i18n_common.pleaseselectone);
                    } else {
                        html.set(context.dropdownLabelNode, _setting.label);
                    }
                    topic.publish("dropdown.selected.retrieve." + _setting.parentid, "");
                }
            });
            topic.publish("topic.handlers.add", resetHandler, domAttr.get(domNode, "id"));

            var dropdownEnableHandler = topic.subscribe("dropdown.enable." + _setting.dropdownid, function(shouldEnable) {
                if (shouldEnable) {
                    domAttr.remove(context.dropdownBtnNode, "disabled");
                    domClass.remove(context.dropdownBtnNode, "dropdown-disabled");
                } else {
                    domAttr.set(context.dropdownBtnNode, "disabled", "true");
                    domClass.add(context.dropdownBtnNode, "dropdown-disabled");
                }
            });
            topic.publish("topic.handlers.add", dropdownEnableHandler, domAttr.get(domNode, "id"));

            // for use case to reset the dropdown list
            var resetDropdownListHandler = topic.subscribe("dropdown.resetDropdownList." + _setting.dropdownid, function(_list) {
                _setting.data = _list;
                context.resetDropdownList(_setting);
            });
            topic.publish("topic.handlers.add", resetDropdownListHandler, domAttr.get(domNode, "id"));

        },
        initInteraction: function(_setting) {
            var context = this;
            var domNode = this.domNode;

            $(domNode).on('hidden.bs.dropdown', function() {
                $(context.inputNode).trigger("change");
            })

            //to make sure the dropdown to stay expanded when user click on the search input
            on(context.searchInputBoxNode, "click", function(e) {
                //stop current javascript execution to prevent dropdown to collapse
                e.stopPropagation();
            });

            //to clear out filtering for the dropdown
            on(context.dropdownBtnNode, "click", function() {
                if (_setting.api) {
                    if (context.apiData == undefined) {
                        var rest = new Rest().p_postapi({
                            p_api: _setting.api
                        }, _setting.prototypeUrl, _setting.dataPackage, "");

                        rest.then(function(response) {
                            _setting.data = response[_setting.dataSrc];
                            context.resetDropdownList(_setting);
                            context.apiData = _setting.data;
                        });
                    } else {
                        _setting.data = context.apiData;
                        context.resetDropdownList(_setting);
                    }
                }
                array.forEach(context.dropdownCellsMap, function(single, i) {
                    context.searchInputNode.value = "";
                    single.showIfContain("");
                });


                // Commented out below section, as it disturbed bootstrap arrow key handling

                // setTimeout(function() {
                //     context.searchInputNode.focus();

                //     var dropdownlistItemHeight = 0;
                //     var dropdownListHeight = 0;

                //     topic.publish("getDropdownListHeight" + _setting.dropdownid, function(listHeightObj) {
                //         var dropdownlistHeight = listHeightObj.dropdownListHeight;
                //         var dropdownListPaddingTop = listHeightObj.dropdownListPaddingTop;
                //         var dropdownListPaddingBottom = listHeightObj.dropdownListPaddingBottom;
                //         dropdownlistItemHeight = dropdownlistHeight + dropdownListPaddingTop + dropdownListPaddingBottom;
                //     });

                //     var winHeight = window.innerHeight;
                //     var dropdownHeight = (domGeometry.position(context.dropdownNode)).y;
                //     var dropdownBtnHeight = domStyle.get(context.dropdownBtnNode, "height");

                //     if (_setting.data.length > 5) {
                //         var dropdownItemListMaxHeight = dropdownlistItemHeight * 5;
                //         dropdownListHeight = dropdownItemListMaxHeight;
                //         domStyle.set(context.dropdownMenuNode, "max-height", dropdownItemListMaxHeight + "px");
                //         domStyle.set(context.dropdownMenuNode, "overflow-y", "scroll");
                //     } else {
                //         dropdownListHeight = _setting.data.length * dropdownlistItemHeight;
                //     }

                //     var totalDropdownHeight = dropdownHeight + dropdownBtnHeight + dropdownListHeight;
                //     if (totalDropdownHeight >= winHeight) {
                //         domClass.add(context.dropdownNode, "dropup");
                //         domStyle.set(context.dropdownMenuNode, "border-top-width", "1px");
                //     } else {
                //         domClass.remove(context.dropdownNode, "dropup");
                //         domStyle.set(context.dropdownMenuNode, "border-top-width", "0px");
                //     }
                // }, 10);
            });

            //to handle key event when user press any key on search input
            on(context.searchInputNode, "keyup", function() {
                array.forEach(context.dropdownCellsMap, function(single, i) {
                    single.showIfContain(context.searchInputNode.value);
                });
            });

            //to handle auto select for first character when user press any key on dropdown 
            // on(context.dropdownBtnNode, "keyup", function(e) {

            //     var charCode = (e.which) ? e.which : e.keyCode
            //     var firstletter = String.fromCharCode(charCode);
            //     array.some(context.dropdownCellsMap, function(dropdownCell) {
            //         return (dropdownCell.isFirstLetter(firstletter, _setting.dropdownid));
            //     });
            // });
        },
        setupContent: function(_setting) {
            var context = this;
            var domNode = this.domNode;

            if (_setting.disabled) {
                domAttr.set(context.dropdownBtnNode, "disabled", "true");
                domClass.add(context.dropdownBtnNode, "dropdown-disabled");
            }

            if (_setting.listtitle != undefined)
                html.set(context.dropdownCellNode, _setting.listtitle);

            if (_setting.required != undefined && _setting.required) {
                domAttr.set(context.inputNode, "data-validation", "required");
                domAttr.set(context.inputNode, "data-target", "dropdown|" + context.id);
            } else {
                domAttr.set(context.inputNode, "data-validation", "optional");
                domAttr.set(context.inputNode, "data-target", "dropdown|" + context.id);
            }

            if (_setting.type != undefined && _setting.type == "download") {
                domClass.add(domNode, "custom-dropdown-download");
                domClass.add(context.dropdownCellNode, "dropdown-download");
                domClass.replace(context.dropdownIconNode, "fa-download", "arrow");
            }

            domAttr.set(context.dropdownCellNode, "id", "dropdownCellNode" + _setting.dropdownid);
            domAttr.set(context.inputNode, "id", "inputNode" + _setting.dropdownid);

            if (_setting.liveSearch == undefined || !_setting.liveSearch)
                domConstruct.destroy(context.searchInputBoxNode);

            context.resetDropdownList(_setting);
        },
        resetDropdownList: function(_setting) {
            var context = this;

            topic.publish("reset.field", domAttr.get(context.inputNode, "id"));

            context.dropdownCellsMap = [];
            domConstruct.empty(context.dropdownCellNode);
            array.forEach(_setting.data, function(single, i) {
                single.dropdownid = _setting.dropdownid;
                single.parentid = _setting.parentid;

                if (_setting.type == "download")
                    single.icon = "fa fa-file-o";

                var dropdownCell = new DropdownCell().placeAt(context.dropdownCellNode);
                dropdownCell.p_construct(single);
                context.dropdownCellsMap.push(dropdownCell);
                if (i == 0 && _setting.type == "download")
                    domClass.add(dropdownCell.domNode, "download-label");
            });
            switch (_setting._default) {
                case true:
                    if (typeof _setting.data[0].keyname == "number") {
                        _setting.data[0].keyname = _setting.data[0].keyname.toString();
                    }
                    _setting.data[0].dropdownid = _setting.dropdownid;
                    html.set(context.dropdownLabelNode, _setting.data[0].keyname);
                    topic.publish("dropdown.select." + _setting.dropdownid, _setting.data[0]);
                    break;
                default:
                    if (_setting.defaultDisplayValue) {
                        topic.publish("dropdown.select.value." + _setting.dropdownid, _setting.defaultDisplayValue);
                    }
                    break;
            }
        },
        i18n: function(_setting) {
            var context = this;

            if (!_setting._default && _setting.defaultDisplayValue === undefined) {
                if (_setting.label !== undefined) {
                    html.set(context.dropdownLabelNode, _setting.label);
                } else {
                    html.set(context.dropdownLabelNode, i18n_common.pleaseselectone);
                }
            }
        }
    });
});
