define([
    "dojo/_base/declare",
    "dojo/text!./html/DropdownCell.html",
    "dojo/html",
    "dojo/dom-attr",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/dom-construct",
    "dojo/topic",
    "dojo/on",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "app/components/ui/element/BaseElement",
    "app/components/utils/Rest"
], function(
    declare,
    template,
    html,
    domAttr,
    domClass,
    domStyle,
    domConstruct,
    topic,
    on,
    _Widget,
    TemplatedMixin,
    BaseElement,
    Rest
) {
    return declare("app.components.ui.element.dropdown.DropdownCell", [BaseElement, TemplatedMixin], {
        templateString: template,
        valueObj: null,
        p_construct: function(setting) {
            var context = this;

            context.startListening(setting);
            context.setupContent(setting);
            context.initInteraction(setting);
        },
        startup: function() {
            this.inherited(arguments);

            var domNode = this.domNode;
            var context = this;

            context.valueObj = "";
        },
        destroy: function() {
            this.inherited(arguments);
        },
        startListening: function(_setting) {
            var context = this;
            var domNode = this.domNode;
            var domNodeId = domAttr.get(domNode, "id");

            var dropdownListHeightHandler = topic.subscribe("getDropdownListHeight" + _setting.dropdownid, function(callback) {
                var dropdownCellObj = {};
                dropdownCellObj.dropdownListHeight = domStyle.get(context.listNode, "height");
                dropdownCellObj.dropdownListPaddingTop = domStyle.get(context.listNode, "padding-top");
                dropdownCellObj.dropdownListPaddingBottom = domStyle.get(context.listNode, "padding-bottom");
                callback(dropdownCellObj);
            });
            topic.publish("topic.handlers.add", dropdownListHeightHandler, domNodeId);
        },
        initInteraction: function(_setting) {
            var domNode = this.domNode;
            var context = this;
            _setting.cellid = domAttr.get(domNode, "id");

            on(domNode, "click", function() {
                topic.publish("dropdown.select." + _setting.dropdownid, _setting);
                domClass.add(context.listNode, "dropdown-cell-selected");
            });
        },
        setupContent: function(_setting) {
            var context = this;
            var domNode = this.domNode;

            context.valueObj = _setting.value;

            domAttr.set(context.cellRefNode, "href", "javaScript:void(0);");


            //need to assign some value to keyname just for display purpose
            if (typeof _setting.keyname == "number")
                _setting.keyname = _setting.keyname.toString();

            html.set(context.labelNode, _setting.keyname);

            if (_setting.icon != undefined)
                domClass.add(context.iconNode, _setting.icon);
            else
                domConstruct.destroy(context.iconNode);
        },
        //logic for query
        showIfContain: function(keyValue) {
            var context = this;
            var domNode = this.domNode;
            if (context.labelNode.innerHTML.toLowerCase().indexOf(keyValue.toLowerCase()) >= 0) {
                domClass.remove(context.listNode, "hide");
            } else {
                domClass.add(context.listNode, "hide");
            }
        },
        //logic for auto choose first letter value
        isFirstLetter: function(keyValue, dropdownid) {
            var context = this;
            var domNode = this.domNode;

            if (context.labelNode.innerHTML.toLowerCase().indexOf(keyValue.toLowerCase()) == 0) {
                var selectedValue = {};
                selectedValue.keyname = context.labelNode.innerHTML;
                selectedValue.value = context.labelNode.innerHTML;
                topic.publish("dropdown.select." + dropdownid, selectedValue);
                domClass.add(context.listNode, "dropdown-cell-selected");
                $(domNode).trigger("click");
                return true;
            } else {
                return false;
            }
        },
        unselectedValue: function(cellId) {
            var context = this;
            var domNode = this.domNode;

            if (cellId != domAttr.get(domNode, "id")) {
                domClass.remove(context.listNode, "dropdown-cell-selected");
            }
        },
        selectValue: function(value) {
            var context = this;
            var domNode = this.domNode;

            if (value == context.valueObj) {
                domClass.add(context.listNode, "dropdown-cell-selected");
            }
        }
    });
});
