define([
    "dojo/_base/declare",
    "dojo/text!./html/SearchInput.html",
    "dojo/dom-attr",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/keys",
    "dojo/topic",
    "dojo/on",
    "dojo/html",
    "dijit/_TemplatedMixin",
    "app/components/ui/element/BaseElement",
    "dojo/i18n!app/locale/nls/common"
], function(
    declare,
    template,
    domAttr,
    domClass,
    domStyle,
    keys,
    topic,
    on,
    html,
    TemplatedMixin,
    BaseElement,
    i18n
) {

    return declare("app.components.ui.element.searchinput.SearchInput", [BaseElement, TemplatedMixin], {
        templateString: template,
        state: true,
        p_construct: function(setting) {
            var domNode = this.domNode;
            var context = this;
            context.startListening(setting);
            context.initInteraction(setting);
            context.setupContent(setting);
        },
        startup: function() {
            this.inherited(arguments);
            var context = this;
            context.state = true;
        },
        startListening: function(_setting) {
            var context = this;
            var domNode = this.domNode;
            var domNodeId = domAttr.get(domNode, "id");
            var enableHandler = topic.subscribe("searchInput.enable." + _setting.searchInputId,
                function(_flag) {
                    context.enable(_flag);
                });
            topic.publish("topic.handlers.add", enableHandler, domNodeId);

            var setValueHandler = topic.subscribe("searchInput.setValue." + _setting.searchInputId,
                function(_value) {
                    domAttr.set(context.inputNode, "value", _value);
                });
            topic.publish("topic.handlers.add", setValueHandler, domNodeId);

            var clickSearchHandler = topic.subscribe("searchInput.clickSearch." + _setting.searchInputId,
                function() {
                    var inputValue = domAttr.get(context.inputNode, "value");
                    _setting.callback(inputValue);
                });
            topic.publish("topic.handlers.add", clickSearchHandler, domNodeId);

            on(context.inputNode, "keyup", function(event) {
                switch (event.keyCode) {
                    case keys.ENTER:
                        event.preventDefault();
                        var inputValue = domAttr.get(context.inputNode, "value");
                        _setting.callback(inputValue);
                        break;
                }
            });

            var placeholderHandler = topic.subscribe("searchInput.setPlaceholder." + _setting.searchInputId,
                function(_setting) {
                    if (_setting.placeHolder !== undefined) {
                        domAttr.set(context.inputNode, "placeholder", _setting.placeHolder);
                    } else {
                        domAttr.set(context.inputNode, "placeholder", i18n.searchinput);
                    }
                });
            topic.publish("topic.handlers.add", placeholderHandler, domNodeId);
        },
        initInteraction: function(_setting) {
            var context = this;
            var domNode = this.domNode;
            var inputValue;

            if (_setting.inputFieldClickable) {
                domClass.add(context.searchInputContainerNode, "search-input-group");
                on(context.searchInputContainerNode, "click", function() {
                    var inputValue = domAttr.get(context.inputNode, "value");
                    _setting.callback(inputValue);
                });
            } else {
                on(context.searchNode, "click", function() {
                    var inputValue = domAttr.get(context.inputNode, "value");
                    _setting.callback(inputValue);
                });
            }

            on(context.inputNode, "keyup", function() {
                inputValue = domAttr.get(context.inputNode, "value");
                domAttr.set(domNode, "value", inputValue);
            });

            on(context.inputNode, "blur", function() {
                inputValue = domAttr.get(context.inputNode, "value");
                if (0 == inputValue.length) {
                    _setting.callback(inputValue);
                }
            });
        },
        setupContent: function(_setting) {
            var context = this;

            switch (_setting.type) {
                case "icon":
                    domStyle.set(context.searchTextNode, "display", "none");
                    break;

                case "text":
                    domStyle.set(context.searchIconNode, "display", "none");
                    context.i18n();
                    break;

                default:
                    domStyle.set(context.searchIconNode, "padding-right", "5px");
                    context.i18n();
                    break;
            }
            if (_setting.iconCode != undefined && _setting.iconCode !== "") {
                domClass.remove(context.searchIconNode, "fa fa-search");
                domClass.add(context.searchIconNode, _setting.iconCode);
            }

            if (_setting.placeHolder !== undefined) {
                domAttr.set(context.inputNode, "placeholder", _setting.placeHolder);
            } else {
                domAttr.set(context.inputNode, "placeholder", i18n.searchinput);
            }

            if (_setting.isLovSearch !== undefined && _setting.isLovSearch) {
                domAttr.set(context.inputNode, "disabled", "");
                domAttr.set(context.inputNode, "style", "background-color: #ffffff");
            } else {
                context.enable(_setting.enable == undefined ? context.state : _setting.enable);
            }
        },
        enable: function(_flag) {
            var context = this;

            context.state = _flag;

            if (_flag) {
                domAttr.remove(context.inputNode, "disabled");
                domAttr.remove(context.searchNode, "disabled");
            } else {
                domAttr.set(context.inputNode, "disabled", "");
                domAttr.set(context.searchNode, "disabled", "");
            }
        },
        i18n: function(_setting) {
            var context = this;

            html.set(context.searchTextNode, i18n.search);
        }
    });
});
