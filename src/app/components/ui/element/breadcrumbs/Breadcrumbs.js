define([
    "dojo/_base/array",
    "dojo/_base/declare",
    "dojo/dom-attr",
    "dojo/dom-style",
    "dojo/topic",
    "dojo/html",
    "dojo/i18n!app/locale/nls/functionName-bundle",
    "dojo/on",
    "dojo/text!./html/Breadcrumbs.html",
    "../BaseElement",
    "../../cell/breadcrumbs/BreadcrumbsCell"
], function (
    v_array,
    v_declare,
    v_domAttr,
    v_domStyle,
    v_topic,
    v_html,
    v_i18n,
    v_on,
    v_template,
    v_BaseElement,
    v_BreadcrumbsCell
) {
    return v_declare([v_BaseElement], {
        templateString: v_template,
        v_blocks: null,
        p_construct: function (v_setting) {
            var v_context = this;
            v_context.p_startListening(v_setting);
        },
        startup: function () {
            this.inherited(arguments);
            var v_context = this;
            v_context.v_blocks = [];
        },
        p_startListening: function (v_setting) {
            var v_context = this;
            var v_domNodeId = v_domAttr.get(v_context.domNode, "id");

            var v_breadcrumbsConstruct = v_topic.subscribe("block.loaded", function (v_parentContext) {
                var v_baseFunction = v_parentContext.getSetting().p_baseFunction;

                if (v_baseFunction === v_setting.p_baseFunction) {
                    var v_functions = v_parentContext.getSetting()[v_baseFunction];

                    v_context.p_initInteraction(v_functions.length);

                    if (v_functions.length === 1) {
                        v_domStyle.set(v_context.backIconNode, "display", "none");
                        v_domStyle.set(v_context.breadcrumbsDetailsNode, "padding-left", "20px");
                    } else {
                        v_domStyle.set(v_context.backIconNode, "display", "inline-block");
                    }

                    var v_constructBlocks = [];
                    var v_totalCount = v_functions.length - 1;
                    v_array.forEach(v_functions, function (v_single, v_i) {
                        v_constructBlocks.push({
                            p_title: v_i18n[v_single.p_function] || v_single.p_functionName,
                            p_counter: v_totalCount,
                            p_total: v_functions.length
                        });
                        v_totalCount--;
                    });

                    v_array.forEach(v_constructBlocks, function (v_single, v_i) {
                        if (v_i === v_constructBlocks.length - 1) {
                            v_single.p_isLast = true;
                            v_html.set(v_context.breadcrumbsCellTitleNode, v_single.p_title);
                        }

                        var v_breadcrumbsCell = new v_BreadcrumbsCell();
                        v_breadcrumbsCell.placeAt(v_context.breadcrumbsCellContainerNode);
                        v_breadcrumbsCell.p_construct(v_single);
                    });
                }
                v_breadcrumbsConstruct.remove();
            });
        },
        p_initInteraction: function (v_setting) {
            var v_context = this;
            var v_i = v_setting;

            v_on(v_context.backIconNode, "click", function () {
                if (v_i > 1) {
                    v_topic.publish("navigation.block.previous");
                    v_i--;
                }
            });
        }
    });
});
