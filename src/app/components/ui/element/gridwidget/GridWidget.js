define([
    "dojo/_base/declare",
    "dojo/_base/array",
    "dojo/dom-construct",
    "dojo/html",
    "dojo/topic",
    "dojo/text!./html/GridWidget.html",
    "dojo/i18n!app/locale/nls/common",
    "dijit/_TemplatedMixin",
    "app/components/ui/element/BaseElement"
], function(
    declare,
    array,
    domConstruct,
    html,
    topic,
    template,
    i18n,
    TemplatedMixin,
    BaseElement
) {

    return declare("app.components.ui.element.gridwidget.GridWidget", [BaseElement, TemplatedMixin], {
        templateString: template,
        widgetKeyMap: null,
        cellHeight: null,
        width: null,
        verticalMargin: null,
        minWidth: null,
        p_construct: function(setting) {
            var domNode = this.domNode;
            var context = this;

            context.setupContent(setting);
        },
        setupContent: function(_setting) {
            var context = this;

            context.cellHeight = 49;
            context.width = 3;
            context.verticalMargin = 15;
            context.minWidth = 1200;

            var options = {
                cellHeight: context.cellHeight, //cell height
                width: context.width,
                disableResize: true, //disallows resizing of widgets
                verticalMargin: context.verticalMargin, //vertical gap size
                minWidth: context.minWidth, //min width before screen display grid in one column
                alwaysShowResizeHandle: /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
            };

            $(context.gridWidgetNode).gridstack(options);

            context.grid = $(context.gridWidgetNode).data('gridstack');
            // $(context.gridWidgetNode).draggable();

            array.forEach(_setting.serializedData, function(single) {
                single.widget.resizeWidgetHeight();
            });

            context.loadGrid(_setting.serializedData);

            // $(".grid-stack").draggable();
            // $(".grid-stack").addTouch();

            // $(".grid-widget-item").draggable();
            // $(".grid-widget-item").addTouch();
        },
        loadGrid: function(serializedData) {
            var context = this;
            context.grid.removeAll();
            context.widgetKeyMap = {};
            var items = GridStackUI.Utils.sort(serializedData);
            var viewportWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
            array.forEach(items, function(single, i) {
                // context.widgetKeyMap[single.key] = single.widget;
                single.widget.setupWidget({
                    grid: context.grid,
                });

                single.widget.verticalMargin = context.verticalMargin;
                context.grid.addWidget(
                    single.widget.outerDiv,
                    single.x,
                    single.y,
                    single.widget.widgetWidth,
                    // single.widget.renderHeight,
                    1,
                    null,
                    null,
                    null,
                    null,
                    null,
                    "widget_key_" + single.key
                );
            });


            topic.publish("manual.iboxinit", context.domNode);
        }
    });

});
