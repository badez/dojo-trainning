define([
    "dojo/_base/array",
    "dojo/_base/declare",
    "dojo/dom-attr",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/text!./html/UploadFile.html",
    "dojo/i18n!app/locale/nls/common",
    "dojo/i18n!app/locale/nls/uploadfile",
    "dojo/html",
    "dojo/on",
    "dijit/_TemplatedMixin",
    "app/components/utils/General",
    "app/components/ui/element/BaseElement",
    "dojo/i18n!app/locale/nls/common"
], function(
    array,
    declare,
    domAttr,
    domClass,
    domStyle,
    template,
    i18n_common,
    i18n,
    html,
    on,
    TemplatedMixin,
    GeneralUtils,
    BaseElement
) {

    return declare("app.components.ui.element.uploadfile.UploadFile", [BaseElement, TemplatedMixin], {
        templateString: template,
        processingFiles: null,
        p_construct: function(setting) {
            var domNode = this.domNode;
            var context = this;
            context.initInteraction(setting);
            context.setupContent(setting);
            context.i18n(setting);
        },
        initInteraction: function(_setting) {
            var context = this;
            on(context.clearBtnNode, "click", function() {
                context.resetComponent();
            });
            on(context.submitBtnNode, "click", function() {
                var fileDetails = [];
                array.forEach(context.processingFiles, function(single, i) {
                    fileDetails.push(single.fileDetail);
                });
                _setting.onSubmit(fileDetails);
            });
            // on(context.fileNode, "change", function(event) {
            //     context.fileNameNode.value = context.fileTypeNode.files[0].name;
            // });
        },
        setupContent: function(_setting) {
            var context = this;
            context.registerDropzone(_setting);
        },
        i18n: function(_setting) {
            var context = this;
            //Need to get from resources
            html.set(context.noteNode, i18n.uploadLimitTips);
            html.set(context.submitBtnNode, i18n_common.submit);
            html.set(context.clearBtnNode, i18n_common.clearAll);

            html.set(context.fileBtnNode, i18n.chooseFile);
            html.set(context.noteNode2, i18n.uploadLimitTips);
        },
        isIE: function() {
            var myNav = navigator.userAgent.toLowerCase();
            return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
        },
        registerDropzone: function(_setting) {
            var context = this;
            var dropzoneId = new GeneralUtils().createUUID();
            domAttr.set(context.dropzoneNode, "id", dropzoneId);

            if (!context.isIE() || (context.isIE() && context.isIE() > 9)) {
                // is not IE or IE 10+
                var previewTemplate =
                    "<div class='dz-preview'>" +
                    "   <div class='dz-details'>" +
                    "       <div class='dz-filename'><span data-dz-name></span></div>" +
                    "       <div class='dz-size' data-dz-size></div>" +
                    "       <img data-dz-thumbnail>" +
                    "   </div>" +
                    "   <div class='dz-progress'><span class='dz-upload' data-dz-uploadprogress></span></div>" +
                    "   <div class='ico-success'><span></span></div>" +
                    "   <div class='ico-failed'><span></span></div>" +
                    "   <div class='dz-error-message'><span data-dz-errormessage-custom></span></div>" +
                    "   <div class='remove-btn' data-dz-remove><i class='ico-delete' aria-hidden='true'></i></div>" +
                    "   <div class='refresh-btn'><i class='ico-refresh' aria-hidden='true'></i></div>" +
                    "</div>";

                $("#" + dropzoneId).dropzone({
                    previewTemplate: previewTemplate,
                    //upload file will be handle by framework...
                    autoProcessQueue: false,
                    maxFilesize: 5,
                    filesizeBase: 1024,
                    uploadMultiple: true,
                    init: function() {
                        context.myDropzone = this;
                        //to keep track uploaded file process...
                        context.processingFiles = [];
                        
                        this.on("addedfile", function(file) {

                            var uniqueId = new GeneralUtils().createUUID();
                            domAttr.set(file.previewElement, "id", uniqueId);

                            var dzUpload = $(".dz-upload", file.previewElement)[0];
                            var dzProgress = $(".dz-progress", file.previewElement)[0];
                            var dzSuccessMark = $(".ico-success", file.previewElement)[0];
                            var dzRemoveButton = $(".remove-btn", file.previewElement)[0];
                            var dzRefreshButton = $(".refresh-btn", file.previewElement)[0];
                            var dzErrorMark = $(".ico-failed", file.previewElement)[0];
                            var dzErrorMessageContainer = $("[data-dz-errormessage-custom]", file.previewElement)[0];

                            //check file size not exceed 5MB
                            if (file.size <= 5000000) {

                                context.processingFiles.push({ id: uniqueId, phase: "processing" });

                                var fileReader = new FileReader();
                                fileReader.readAsDataURL(file);
                                var fileName = file.name;

                                var fileType = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length).toLowerCase();
                                var fileDetail = {};

                                fileDetail.uploadedBy = $.jStorage.get("app.login.username");
                                fileDetail.imageFileName = fileName.substring(fileName.lastIndexOf("\\") + 1, fileName.length);
                                //default remarks to file name
                                fileDetail.remarks = fileDetail.imageFileName.substring(0, fileName.lastIndexOf("."));
                                fileReader.onloadend = function() {
                                    fileDetail.fileByteString = fileReader.result.substring(fileReader.result.indexOf(",") + 1, fileReader.result.length);
                                    array.forEach(context.processingFiles, function(single, i) {
                                        if (single.id == uniqueId) {
                                            single.fileDetail = fileDetail;
                                        }
                                    });
                                    if (_setting.fileAdd) {
                                        _setting.fileAdd(fileDetail, {
                                            updateProgress: function(progress) {

                                                domStyle.set(dzUpload, "width", progress + "%");
                                            },
                                            setAsCompleted: function() {
                                                domStyle.set(dzUpload, "width", "100%");
                                                array.forEach(context.processingFiles, function(single, i) {
                                                    if (single.id == uniqueId) {
                                                        single.phase = "finished";
                                                    }
                                                });
                                                setTimeout(function() {
                                                    domStyle.set(dzProgress, { "opacity": "0", });
                                                    domStyle.set(dzSuccessMark, { "opacity": "1", });
                                                    domStyle.set(dzErrorMark, { "opacity": "0", });
                                                    domStyle.set(dzRemoveButton, { "opacity": "1", });
                                                    domStyle.set(dzRefreshButton, { "opacity": "0", });
                                                    html.set(dzErrorMessageContainer, "");
                                                    context.checkAllUploadFileEnableSubmit();
                                                }, 500);
                                            },
                                            setAsError: function(errorMessage) {
                                                array.forEach(context.processingFiles, function(single, i) {
                                                    if (single.id == uniqueId) {
                                                        single.phase = "error";
                                                    }
                                                });
                                                domAttr.set(context.submitBtnNode, "disabled", null);
                                                setTimeout(function() {
                                                    domStyle.set(dzProgress, { "opacity": "0", });
                                                    domStyle.set(dzSuccessMark, { "opacity": "0", });
                                                    domStyle.set(dzErrorMark, { "opacity": "1", });
                                                    domStyle.set(dzRemoveButton, { "opacity": "1", });
                                                    domStyle.set(dzRefreshButton, { "opacity": "1", });
                                                    html.set(dzErrorMessageContainer, errorMessage);
                                                }, 500);
                                            },
                                            checkAllUploadFileEnableSubmit: function() {
                                                context.checkAllUploadFileEnableSubmit();
                                            }
                                        });
                                    }
                                };

                            } else {
                                context.processingFiles.push({ id: uniqueId, phase: "error" });
                                domAttr.set(context.submitBtnNode, "disabled", null);
                                setTimeout(function() {
                                    domStyle.set(dzProgress, { "opacity": "0", });
                                    domStyle.set(dzSuccessMark, { "opacity": "0", });
                                    domStyle.set(dzErrorMark, { "opacity": "1", });
                                    domStyle.set(dzRemoveButton, { "opacity": "1", });
                                    domStyle.set(dzRefreshButton, { "opacity": "0", });
                                    html.set(dzErrorMessageContainer, _setting.fileTooBigMessage);
                                }, 500);
                            }
                        });
                        this.on("removedfile", function(file) {
                            var id = domAttr.get(file.previewElement, "id");
                            for (var i = context.processingFiles.length - 1; i >= 0; i--) {
                                if (context.processingFiles[i].id === id) {
                                    context.processingFiles.splice(i, 1);
                                }
                            }
                            context.checkAllUploadFileEnableSubmit();
                        });
                    }
                });



            } else {
                // is IE 9 and below
                // alert("It is IE9");
                var uniqueIdFileUpload = new GeneralUtils().createUUID();
                domAttr.set(context.fileTypeNode, "id", uniqueIdFileUpload);
                domAttr.set(context.fileBtnNode, "for", uniqueIdFileUpload);
                domAttr.set(context.fileNameNode, "for", uniqueIdFileUpload);
                domClass.remove(context.altfileNode, "hidden");
                domClass.add(context.dropzoneNode, "hidden");
            }
        },
        checkAllUploadFileEnableSubmit: function() {
            var context = this;
            // domAttr.set(context.submitBtnNode, "disabled", null);
            setTimeout(function() {
                if (context.processingFiles != undefined && context.processingFiles.length > 0) {
                    var hasCompleted = true;
                    array.forEach(context.processingFiles, function(single, i) {
                        if (single.phase === "processing" || single.phase === "error") {
                            hasCompleted = false;
                        }
                    });
                    if (hasCompleted) {
                        domAttr.remove(context.submitBtnNode, "disabled");
                    } else {
                        domAttr.set(context.submitBtnNode, "disabled", null);
                    }
                } else {
                    domAttr.set(context.submitBtnNode, "disabled", null);
                }

            }, 1000);
        },
        resetComponent: function() {
            var context = this;
            context.myDropzone.removeAllFiles(true);
            domAttr.set(context.submitBtnNode, "disabled", null);
        }
    });

});
