define([
    "dojo/_base/declare",
    "dojo/dom-class",
    "dojo/html",
    "dojo/text!./html/Switcher.html",
    "dojo/on",
    "dijit/_TemplatedMixin",
    "app/components/ui/element/BaseElement"
], function (
    declare,
    domClass,
    html,
    template,
    on,
    TemplatedMixin,
    BaseElement
) {
    return declare("app.components.ui.element.switcher.Switcher", [BaseElement, TemplatedMixin], {
        templateString: template,
        p_construct: function (setting) {
            var domNode = this.domNode;
            var context = this;

            context.setupContent(setting);
            context.initInteraction(setting);
        },
        initInteraction: function (_setting) {
            var context = this;
            var domNode = this.domNode;

            on(context.swicherNode, "change", function () {
                if (_setting.onChange) {
                    _setting.onChange(context.swicherNode.checked);
                }
            });
        },
        setupContent: function (_setting) {
            var context = this;
            var domNode = this.domNode;
            var backgroundcolor = "#1ab394";
            var disable = false;

            if (_setting.backgroundcolor) {
                backgroundcolor = _setting.backgroundcolor;
            }
            if (_setting.disabled) {
                disable = _setting.disabled;
                domClass.add(context.swicherDivNode, "disabled");
            }
            if (_setting.checked) {
                context.swicherNode.checked = true;
            }
            if (_setting.text) {
                html.set(context.swicherTextNode, _setting.text);
                domClass.add(context.swicherTextNode, "pl10");
            }
            var init = new Switchery(context.swicherNode, { color: backgroundcolor, disabled: disable });
            
            if (_setting.switcherStatus) {
                _setting.switcherStatus(context.swicherNode.checked);
            }
        }
    });
});
