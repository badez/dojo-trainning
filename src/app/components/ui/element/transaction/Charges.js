define([
    "dojo/_base/declare",
    "dojo/_base/config",
    "dojo/_base/array",
    "dojo/i18n!app/locale/nls/common",
    "dojo/text!./html/Charges.html",
    "dojo/dom-construct",
    "dojo/dom-attr",
    "dojo/on",
    "dojo/dom-style",
    "dojo/topic",
    "dojo/html",
    "dijit/_TemplatedMixin",
    "dijit/focus",
    "app/components/ui/blocks/BaseBlock",
    "app/components/utils/Rest",
    "app/components/utils/General",
    "app/components/ui/element/customtable/CustomTable"
], function(
    declare,
    config,
    array,
    i18n_common,
    template,
    domConstruct,
    domAttr,
    on,
    domStyle,
    topic,
    html,
    TemplatedMixin,
    focusUtil,
    BaseBlock,
    Rest,
    GeneralUtils,
    CustomTable
) {
    return declare("app.components.ui.element.general.transaction.Charges", [BaseBlock, TemplatedMixin], {
        templateString: template,
        chargesTaxData: null,
        p_construct: function(setting) {
            var domNode = this.domNode;
            var context = this;

            context.i18n(setting);
            context.startListening(setting);
            context.setupContent(setting);
        },
        startup: function() {
            this.inherited(arguments);
            var context = this;

            context.chargesTaxData = {};
        },
        startListening: function(_setting) {
            var context = this;
            var domNode = this.domNode;
            var domNodeId = domAttr.get(context.domNode, "id");

            var updateChargesTaxDatapackage = topic.subscribe("charges.tax.data.obj." + _setting.tableId, function(callback) {
                topic.publish("customtable.getdata." + context.uniqueIdChargesTaxTable, function(data) {
                    var newFeeList = [];
                    array.forEach(data, function(single) {
                        single.chargeCurrency = single.feeCurrency;
                        single.chargeAmt = single.feeAmount;
                        single.type = single.feeType;
                        single.laterCollection = 'N';

                        if ("TAX" === single.feeType) {
                            single.taxCurrency = single.feeCurrency;
                            single.taxAmt = single.feeAmount;
                        } else {
                            single.taxCategory = null;
                        }
                        newFeeList.push(single);
                    });
                    context.chargesTaxData.feeList = newFeeList;
                });
                callback(context.chargesTaxData);
            });
            topic.publish("topic.handlers.add", updateChargesTaxDatapackage, domNodeId);

            var updateTableHandler = topic.subscribe("charges.table.update." + _setting.tableId, function(data) {
                if (data.dataPackage) {
                    _setting.dataPackage = data.dataPackage;
                    var prototypeUrl = "assets/data/charges_tax_data.json";
                    var dataPackage = _setting.dataPackage;
                    // same api
                    var rest = new Rest().p_postapi({ p_api: "fee/find" }, prototypeUrl, dataPackage);
                    rest.then(function(response) {
                        array.forEach(response.feeList, function(obj, index) {
                            var total = obj.feeAmount * obj.quantity;
                            obj.totalFeeAmount = total;

                            var feeAmount = obj.feeAmount;
                            var totalFeeAmount = obj.totalFeeAmount;

                            obj.feeAmount = feeAmount.toFixed(2);
                            obj.totalFeeAmount = totalFeeAmount.toFixed(2);

                            if (!obj.fee) {
                                obj.fee = i18n_common.notApplicable;
                            }

                            if (!obj.rateType) {
                                obj.rateType = i18n_common.notApplicable;
                            }

                            if (!obj.feeCurrency) {
                                obj.feeCurrency = i18n_common.notApplicable;
                            }

                            if (!obj.feeAmount) {
                                obj.feeAmount = i18n_common.notApplicable;
                            }

                            if (!obj.quantity) {
                                obj.quantity = 1;
                            }

                            if (!obj.totalFeeAmount) {
                                obj.totalFeeAmount = i18n_common.notApplicable;
                            }

                            //obj.waiveEditFlag = "Y";

                            array.forEach(context.previousData, function(previousData, indx) {
                                if ((obj.rateType === previousData.rateType) &&
                                    (obj.chargeCode === previousData.chargeCode) &&
                                    (obj.fee === previousData.fee) &&
                                    (obj.feeType === previousData.feeType) &&
                                    (obj.feeCurrency === previousData.feeCurrency) &&
                                    (obj.waiveEditFlag === previousData.waiveEditFlag) &&
                                    (obj.chargeTaxCategory === previousData.chargeTaxCategory) &&
                                    (obj.chargeRateType === previousData.chargeRateType) &&
                                    (obj.totalFeeAmount != previousData.totalFeeAmount)) {

                                    if (obj.feeAmount) {
                                        updateCell("feeAmount", obj.feeAmount);
                                    }

                                    if (previousData.quantity) {
                                        updateCell("quantity", previousData.quantity);
                                    }

                                    if (obj.totalFeeAmount) {
                                        var total = obj.feeAmount * previousData.quantity;
                                        updateCell("totalFeeAmount", total);
                                    }

                                    function updateCell(column, value) {
                                        topic.publish("customtable.data.update." + context.uniqueIdChargesTaxTable, {
                                            row: indx,
                                            columnKey: column,
                                            value: value
                                        });
                                    }

                                }
                            });
                        });
                    }, function(err) {
                        console.error(err);
                    });
                }
            });
            topic.publish("topic.handlers.add", updateTableHandler, domNodeId);

        },
        setupContent: function(_setting) {
            var context = this;
            var domNode = this.domNode;
            context.p_constructTable(_setting);
        },
        constructTable: function(_setting) {
            var context = this;
            var domNode = this.domNode;
            var domNodeId = domAttr.get(domNode, "id");

            var prototypeUrl = "assets/data/charges_tax_data.json";
            var dataPackage = _setting.dataPackage;
            var rest = new Rest().p_postapi({ p_api: "fee/find" }, prototypeUrl, dataPackage);
            rest.then(function(response) {
                array.forEach(response.feeList, function(obj, index) {
                    var total = obj.feeAmount * obj.quantity;
                    obj.totalFeeAmount = total;

                    var feeAmount = obj.feeAmount;
                    var totalFeeAmount = obj.totalFeeAmount;

                    obj.feeAmount = feeAmount.toFixed(2);
                    obj.totalFeeAmount = totalFeeAmount.toFixed(2);

                    if (!obj.fee) {
                        obj.fee = i18n_common.notApplicable;
                    }

                    if (!obj.rateType) {
                        obj.rateType = i18n_common.notApplicable;
                    }

                    if (!obj.feeCurrency) {
                        obj.feeCurrency = i18n_common.notApplicable;
                    }

                    if (!obj.feeAmount) {
                        obj.feeAmount = i18n_common.notApplicable;
                    }

                    if (!obj.quantity) {
                        obj.quantity = 1;
                    }

                    if (!obj.totalFeeAmount) {
                        obj.totalFeeAmount = i18n_common.notApplicable;
                    }

                    //obj.waiveEditFlag = "Y";
                });
                context.previousData = response.feeList;
                context.uniqueIdChargesTaxTable = new GeneralUtils().createUUID();
                domAttr.set(context.chargesTaxTableNode, "id", context.uniqueIdChargesTaxTable);
                var chargesTaxTable = new CustomTable();
                chargesTaxTable.placeAt(context.chargesTaxTableNode);
                chargesTaxTable.p_construct({
                    tableId: context.uniqueIdChargesTaxTable,
                    data: response.feeList,
                    paging: false,
                    searching: false,
                    ordering: false,
                    info: false,
                    columns: [{
                        key: "fee",
                        title: i18n_common.chargestaxcategory
                    }, {
                        key: "rateType",
                        title: i18n_common.ratetype,
                    }, {
                        key: "feeCurrency",
                        title: i18n_common.currency
                    }, {
                        key: "feeAmount",
                        title: i18n_common.amount,
                        customTitleClass: "text-right",
                        custom: {
                            displayCreateFunction: function(div, cellData, rowData, row, col) {
                                var amount = new GeneralUtils().formatCurrency(cellData, null);
                                domConstruct.create("div", { "class": "text-right", innerHTML: amount }, div);
                            }
                        }
                    }, {
                        key: "quantity",
                        title: i18n_common.quantity,
                        customTitleClass: "text-right",
                        custom: {
                            editCreateFunction: function(div, cellData, rowData, row, col) {
                                var columnKey = "quantity";
                                //create input component to be put in cell when on edit mode
                                if (rowData.waiveEditFlag == "Y") {
                                    var input = domConstruct.create("input", { value: cellData, style: "width: 100%;color:#1ab394;", type: "number", "class": "remove-spinner" }, div);
                                    //add action when input component lost focus
                                    on(input, "blur", function(e) {
                                        var rowValue = null;
                                        // update datatable data on this cell
                                        if (input.value.length == 0) {
                                            input.value = rowData.quantity;
                                        }
                                        topic.publish("customtable.data.update." + context.uniqueIdChargesTaxTable, {
                                            row: row,
                                            columnKey: columnKey,
                                            value: input.value,
                                            getReturnValue: function(value) {
                                                value.totalFeeAmount = value.feeAmount * value.quantity
                                                var totalFeeAmount = value.totalFeeAmount;
                                                value.totalFeeAmount = totalFeeAmount.toFixed(2);
                                                rowValue = value;
                                                topic.publish("customtable.data.update." + context.uniqueIdChargesTaxTable, {
                                                    row: row,
                                                    columnKey: "totalFeeAmount",
                                                    value: value.totalFeeAmount,
                                                    getReturnValue: function(value) {
                                                        rowValue = value;
                                                        array.forEach(response.feeList, function(obj) {
                                                            //using chargeCode to do checking
                                                            if (rowValue.quantity != obj.quantity) {
                                                                if ((obj.rateType === rowValue.rateType) &&
                                                                    (obj.chargeCode === rowValue.chargeCode) &&
                                                                    (obj.fee === rowValue.fee) &&
                                                                    (obj.feeType === rowValue.feeType) &&
                                                                    (obj.feeCurrency === rowValue.feeCurrency) &&
                                                                    (obj.waiveEditFlag === rowValue.waiveEditFlag) &&
                                                                    (obj.chargeTaxCategory === rowValue.chargeTaxCategory) &&
                                                                    (obj.chargeRateType === rowValue.chargeRateType)) {
                                                                    obj.totalFeeAmount = rowValue.totalFeeAmount;
                                                                    obj.quantity = rowValue.quantity;
                                                                }
                                                            }
                                                        });
                                                        topic.publish("chargestable.updated.rowdata." + _setting.parentId, {
                                                            rowData: rowValue
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    });
                                    focusUtil.focus(input);
                                } else {
                                    topic.publish("customtable.data.update." + context.uniqueIdChargesTaxTable, {
                                        row: row,
                                        columnKey: columnKey,
                                        value: rowData.quantity
                                    });
                                }
                            },
                            displayCreateFunction: function(div, cellData, rowData, row, col) {
                                var style = "";
                                if (rowData.waiveEditFlag == "Y") {
                                    style = "color:#1ab394;";
                                }
                                domConstruct.create("div", { innerHTML: cellData, style: style, "class": "text-right" }, div);
                            }
                        }
                    }, {
                        key: "totalFeeAmount",
                        title: i18n_common.totalamount,
                        customTitleClass: "text-right",
                        custom: {
                            displayCreateFunction: function(div, cellData, rowData, row, col) {
                                var amount = new GeneralUtils().formatCurrency(cellData, null);
                                domConstruct.create("div", { "class": "text-right", innerHTML: amount }, div);
                            }
                        }
                    }]
                });
            }, function(err) {
                console.error(err);
            });

            if (_setting.extraUi) {
                if (!_setting.extraUi.bottomSpace) {
                    domStyle.set(context.removeIboxContentBottomPaddingNode, "padding-bottom", "0");
                    domStyle.set(context.removeBottomMarginNode, "margin-bottom", "0");
                }
                if (!_setting.extraUi.iboxTitleTopBorder) {
                    domStyle.set(context.regularLineNode, "border", "none");
                }
            }
        },
        i18n: function(_setting) {
            var context = this;
            html.set(context.chargesTaxLabelNode, i18n_common.chargestax);
        }
    });
});
