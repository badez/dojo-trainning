define([
    "dojo/_base/declare",
    "dojo/dom-class",
    "dojo/text!./html/InputValidated.html",
    "app/components/utils/BaseUtils"
], function (
    declare,
    domClass,
    template,
    BaseElement
) {
    return declare("InputValidated", [BaseElement], {
        templateString: template,
        p_construct: function (setting) {
            var context = this;

            context.setupContent(setting);
        },
        startup: function () {
            this.inherited(arguments);

            var domNode = this.domNode;
            var context = this;
        },
        destroy: function () {
            this.inherited(arguments);
        },
        setupContent: function (_setting) {
            var context = this;
            var domNode = this.domNode;

            domClass.add(domNode, _setting.unique_identifier);
        }
    });
});
