define([
    "dojo/_base/array",
    "dojo/_base/declare",
    "dojo/dom",
    "dojo/dom-attr",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/html",
    "dojo/on",
    "dojo/query",
    "dojo/text!./html/MultitaskTab.html",
    "dojo/topic",
    "../BaseElement",
    "../../cell/multitasktab/MultitaskTabCell",
], function(
    v_array,
    v_declare,
    v_dom,
    v_domAttr,
    v_domClass,
    v_domStyle,
    v_html,
    v_on,
    v_query,
    v_template,
    v_topic,
    v_BaseElement,
    v_MultitaskTabCell
) {
    return v_declare([v_BaseElement], {
        templateString: v_template,
        p_construct: function(v_setting) {
            $.jStorage.set("keep.blocks", true);
            var v_context = this;

            v_context.p_startListening(v_setting);
            v_context.p_initInteraction(v_setting);
        },
        p_startListening: function(v_setting) {
            var v_context = this;
            var v_domNode = v_context.domNode;
            var v_domNodeId = v_domAttr.get(v_domNode, "id");
            var v_openedFunction = {};
            var v_openedTab = {};
            var v_activedFunction = "";
            var v_activedTab = {};

            var v_addTab = v_topic.subscribe("block.loaded", function(v_domContext) {
                var v_mainPage = v_domContext.getSetting().p_mainPage;
                if (!v_mainPage) {
                    return;
                }
                var v_function = v_domContext.getSetting().p_function;
                var v_functionName = v_domContext.getSetting().functionName;
                v_activedFunction = v_function;
                if (v_activedTab.domNode) {
                    v_domClass.toggle(v_activedTab.domNode, "active");
                }
                v_openedFunction[v_activedFunction] = v_domContext;
                var v_multitaskTabCell = new v_MultitaskTabCell().placeAt(v_context.tasktabListNode);
                v_multitaskTabCell.p_construct({ p_function: v_function, p_functionName: v_functionName });
                v_activedTab = v_multitaskTabCell;
                v_domClass.toggle(v_activedTab.domNode, "active");
                v_openedTab[v_activedFunction] = v_multitaskTabCell;

                v_on(v_multitaskTabCell.multitaskTabNode, "click", function() {
                    if (v_activedFunction !== v_function) {
                        v_topic.publish("navigation.block.open", v_function);
                    }
                });

                v_on(v_multitaskTabCell.multitaskItemCloseIconNode, "click", function() {
                    v_topic.publish("navigation.block.destroy", v_function);
                });
            });
            v_topic.publish("topic.handlers.add", v_addTab, v_domNodeId);

            var v_menuActive = v_topic.subscribe("block.active", function(v_function) {
                var v_baseFunction = v_function.getSetting().p_baseFunction;
                if (v_openedTab[v_baseFunction]) {
                    if (v_activedTab) {
                        v_domClass.toggle(v_activedTab.domNode, "active");
                    }
                    v_activedTab = v_openedTab[v_baseFunction];
                    v_activedFunction = v_baseFunction;
                    v_domClass.toggle(v_activedTab.domNode, "active");
                }
            });
            v_topic.publish("topic.handlers.add", v_menuActive, v_domNodeId);

            var v_destroyTab = v_topic.subscribe("block.destroyed", function(v_domContext) {
                var v_function = v_domContext.getSetting().p_function;

                //if a task tab does not exist or page is a not the parent block/main page
                if (!v_openedTab[v_function] || !v_domContext.getSetting().p_mainPage) {
                    return;
                }

                delete v_openedFunction[v_function];
                v_openedTab[v_function].destroy();
                delete v_openedTab[v_function];
                if (v_activedFunction === v_function) {
                    v_activedTab = undefined;
                    var first = Object.keys(v_openedFunction)[0];
                    v_topic.publish("navigation.block.open", first);
                }
            });
            v_topic.publish("topic.handlers.add", v_destroyTab, v_domNodeId);
        },
        p_initInteraction: function() {
            var v_context = this;
            var v_domNode = v_context.domNode;
            var v_showFlag = true;

            v_query("div#page-wrapper > *:not(#multitabContainer), nav.navbar-default").on("click, scroll", function() {
                if (v_domClass.contains(v_domNode, "show")) { v_domClass.toggle(v_domNode, "show"); }
            });

            v_on(v_context.showHideNode, "click", function() {
                v_domClass.toggle(v_domNode, "show");
            });
        }
    });
});
