define([
    "dojo/_base/declare",
    "dojo/dom-attr",
    "dojo/dom-class",
    "dojo/html",
    "dojo/text!./html/CarouselItem.html",
    "dijit/_TemplatedMixin",
    "app/components/ui/element/BaseElement"
], function(
    declare,
    domAttr,
    domClass,
    html,
    template,
    TemplatedMixin,
    BaseElement
) {
    return declare("app.components.ui.element.carousel.CarouselItem", [BaseElement, TemplatedMixin], {
        templateString: template,
        p_construct: function(setting) {
            var context = this;

            context.setupContent(setting);
            context.initInteraction(setting);
        },
        postCreate: function() {
            this.inherited(arguments);
        },
        startup: function() {
            var context = this;
            this.inherited(arguments);
        },
        destroy: function() {
            this.inherited(arguments);
        },
        initInteraction: function(_setting) {
            var context = this;

        },
        setupContent: function(_setting) {
            var domNode = this.domNode;
            var context = this;
            var domNodeId = domAttr.get(domNode, "id");

            switch (_setting.type) {
                case "dom":
                    _setting.content.placeAt(context.contentNode);
                    break;

                case "cell":
                    _setting.content.placeAt(context.contentNode);
                    _setting.content.p_construct(_setting.content);
                    break;
                default:
                    html.set(context.contentNode, _setting.content);
                    break;
            }
            if (_setting.active)
                domClass.add(domNode, "active");
        }
    });
});
