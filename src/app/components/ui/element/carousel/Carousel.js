define([
    "dojo/_base/array",
    "dojo/_base/declare",
    "dojo/dom-attr",
    "dojo/text!./html/Carousel.html",
    "dojo/topic",
    "dijit/_TemplatedMixin",
    "app/components/ui/element/BaseElement",
    "app/components/ui/element/carousel/CarouselItem",
    "app/components/utils/General"
], function(
    array,
    declare,
    domAttr,
    template,
    topic,
    TemplatedMixin,
    BaseElement,
    CarouselItem,
    GeneralUtils
) {
    return declare("app.components.ui.element.carousel.Carousel", [BaseElement, TemplatedMixin], {
        templateString: template,
        needArrow: false,
        owl: null,
        lastItem: null,
        p_construct: function(setting) {
            var context = this;

            context.setupContent(setting);
            context.startListening(setting);

        },
        constructor: function() {},
        postCreate: function() {
            this.inherited(arguments);
        },
        startup: function() {
            var context = this;
            this.inherited(arguments);


        },
        destroy: function() {
            this.inherited(arguments);
        },
        startListening: function(_setting) {
            var context = this;
            var domNode = this.domNode;
            var domNodeId = domAttr.get(domNode, "id");

            //Start condition for loan product dropdown 
            var addItemHandler = topic.subscribe("carousel.addItem." + _setting.carouselId, function(_itemData) {

                var itemObj = {};

                itemObj.content = _itemData;
                itemObj.type = _setting.type;
                itemObj.active = false;

                var carouselItem = new CarouselItem().placeAt(context.contentNode);
                carouselItem.p_construct(itemObj);

                context.owl.trigger('add.owl.carousel', [carouselItem.domNode, context.lastItem]).trigger('refresh.owl.carousel');
                context.lastItem++;
            });
            topic.publish("topic.handlers.add", addItemHandler, domNodeId);
            //End condition for loan product dropdown                 
        },
        setupContent: function(_setting) {
            var domNode = this.domNode;
            var context = this;

            context.owl = $(context.contentNode);

            var margin = (_setting.margin === undefined) ? 10 : _setting.margin;
            var fixedWidth = (_setting.fixedWidth === undefined) ? false : _setting.fixedWidth;
            var slidespeed = (_setting.slidespeed === undefined) ? 500 : _setting.slidespeed;
            var interval = (_setting.interval === undefined) ? 400 : _setting.interval;
            var autoPlay = (_setting.autoPlay === undefined) ? true : _setting.autoPlay;
            var navigation = (_setting.navigation === undefined) ? false : _setting.navigation;
            var stagePadding = (_setting.stagePadding === undefined) ? 0 : _setting.stagePadding;
            var pagination = (_setting.pagination === undefined) ? true : _setting.pagination;
            var responsive = {};
            var mouseDrag = (_setting.mouseDrag === undefined) ? true : _setting.mouseDrag;
            var navText = (_setting.navText === undefined) ? ["<i class='ico-carousel-prev ico-back'></i>", "<i class='ico-carousel-next ico-next'></i>"] : _setting.navText;

            if (_setting.responsive != undefined && stagePadding) {
                responsive = {
                    "0": {
                        items: 1,
                    },
                    "768": {
                        items: 1
                    },
                    "1100": {
                        items: 2,
                        stagePadding: stagePadding
                    }
                };
            }

            function customTitle() {
                if (_setting.title != undefined) {
                    $.each(this.owl.userItems, function(i) {
                        var paginationLinks = jQuery('.owl-controls .owl-pagination .owl-page');
                        $('.owl-controls .owl-pagination .owl-page span').remove();
                        $(paginationLinks[i]).prepend("<div class='text-pagination'>" + _setting.title[i] + "</div>");
                    });
                } else {
                    return false;
                }
            }

            if (_setting.data instanceof Array) {

                array.forEach(_setting.data, function(single, i) {
                    var itemObj = {};

                    itemObj.content = single;
                    itemObj.type = _setting.type;
                    itemObj.active = false;

                    if (i == 0) {
                        itemObj.active = true;
                    }

                    var carouselItem = new CarouselItem().placeAt(context.contentNode);
                    carouselItem.p_construct(itemObj);
                });
            }
            var owl = context.owl;
            var options = {
                margin: margin,
                nav: navigation,
                navText: navText,
                autoPlay: autoPlay,
                slideSpeed: interval,
                paginationSpeed: slidespeed,
                singleItem: !fixedWidth,
                pagination: pagination,
                autoHeight: true,
                responsive: responsive,
                mouseDrag: mouseDrag
            };

            owl.on('initialized.owl.carousel', function(event) {
                context.lastItem = event.item.count;
                context.lastItem -= 1;
            })
            owl.owlCarousel(options);

            var resizeHandler = topic.subscribe("carousel.resize", function() {
                setTimeout(function() {
                    owl.data('owlCarousel').destroy();
                    owl.owlCarousel(options);
                }, 500);
            });
            topic.publish("topic.handlers.add", resizeHandler, domAttr.get(domNode, "id"));
        }
    });
});
