define([
    "dojo/_base/array",
    "dojo/_base/declare",
    "dojo/dom-attr",
    "dojo/dom-construct",
    "dojo/dom-class",
    "dojo/html",
    "dojo/text!./html/DropdownStep.html",
    "dojo/on",
    "dojo/topic",
    "dijit/_TemplatedMixin",
    "app/components/ui/element/BaseElement",
    "app/components/ui/element/dropdown/Dropdown",
    "app/components/utils/General",
], function(
    array,
    declare,
    domAttr,
    domConstruct,
    domClass,
    html,
    template,
    on,
    topic,
    TemplatedMixin,
    BaseElement,
    Dropdown,
    GeneralUtils
) {
    return declare("app.components.ui.element.dropdownstep.DropdownStep", [BaseElement, TemplatedMixin], {
        templateString: template,
        functionMap: null,
        jsonValue: null,
        p_construct: function(setting) {
            var domNode = this.domNode;
            var context = this;

            context.setupContent(setting);
            context.initInteraction(setting);
        },
        initInteraction: function(_setting) {
            var context = this;
            var domNode = this.domNode;
        },
        setupContent: function(_setting) {
            var context = this;
            var domNode = this.domNode;

            context.functionMap = {};
            context.jsonValue = {};

            array.forEach(_setting.args, function(arg, i) {
                arg.isFirst = (i == 0);
                context.addNextDropdown(arg);
            });

        },
        updateValues: function(keyName, values) {
            var context = this;
            context.functionMap[keyName].updateValues(values);
        },
        enableDropdown: function(keyName, enable) {
            var context = this;
            context.functionMap[keyName].enable(enable);
        },
        selectValue: function(keyName, value){
            var context = this;
            context.functionMap[keyName].selectValue(value);
        },
        addNextDropdown: function(arg) {
            var context = this;
            var domNode = this.domNode;
            var domNodeId = domAttr.get(domNode, "id");

            var div = domConstruct.create("div", { "class": "dropdownstep-container" }, context.containerNode);

            var innerDiv = domConstruct.create("div", { "class": "dropdownstep-container-dropdown" }, div);
            if(arg.label){
                var label = domConstruct.create("label", { innerHTML: arg.label, style: "display: block"}, innerDiv);
            }

            var uniqueIdFormDropdown = new GeneralUtils().createUUID();
            formDropdown = new Dropdown().placeAt(innerDiv);
            formDropdown.p_construct({
                parentid: domAttr.get(domNode, "id"),
                dropdownid: uniqueIdFormDropdown,
                data: arg.dropdownValues,
                _default: (arg["default"]) ? true : false,
            });

            if (!arg.isFirst) {
                var div = domConstruct.create("i", {
                    "class": ("ico-next pointer" + ((arg.label)? " text-top":""))
                }, div, "first");
            }

            context.jsonValue[arg.keyName] = null;

            var dropdownOnChangeHandler = topic.subscribe("dropdown.selected.retrieve." + domNodeId,
                function(selectedData) {
                    if (selectedData.dropdownid == uniqueIdFormDropdown) {
                        context.jsonValue[arg.keyName] = selectedData.value;
                        if (arg.onChange) {
                            arg.onChange(selectedData.value);
                        }
                    }
                }
            );
            topic.publish("topic.handlers.add", dropdownOnChangeHandler, domNodeId);

            context.functionMap[arg.keyName] = {
                updateValues: function(values) {
                    topic.publish("dropdown.resetDropdownList." + uniqueIdFormDropdown, values);
                },
                enable: function(enable) {
                    topic.publish("dropdown.enable." + uniqueIdFormDropdown, enable);
                },
                selectValue: function(value){
                    topic.publish("dropdown.select.value." + uniqueIdFormDropdown, value);
                }
            }

        }
    });
});
