define([
    "app/components/ui/element/BaseElement",
    "dijit/_TemplatedMixin",
    "dojo/_base/declare",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/html",
    "dojo/text!./html/NestedTreeCell.html"
], function(
    BaseElement,
    TemplatedMixin,
    declare,
    domClass,
    domStyle,
    html,
    template
) {
    return declare("app.components.ui.element.nestedtree.NestedTreeCell", [BaseElement, TemplatedMixin], {
        templateString: template,
        p_construct: function(setting) {
            var domNode = this.domNode;
            var context = this;

            context.setupContent(setting);
        },
        setupContent: function(_setting) {
            var context = this;
            var domNode = this.domNode;

            if (_setting.icon) {
                var iconPx = (_setting.level - 2) * 18;
                if (_setting.level > 2) {
                    var descPx = (_setting.level * 10) + ((_setting.level - 1) * 5);
                } else {
                    var descPx = _setting.level * 10;
                }
                domStyle.set(context.iconNode, "display", "inline-block");
                domStyle.set(context.iconNode, "padding-left", iconPx + "px");
                domStyle.set(context.productCodeDescNode, "padding-left", descPx + "px");
            } else {
                domStyle.set(context.iconNode, "display", " none");
            }
            domStyle.set(context.productCodeDescNode, "width", "100%");

            if (_setting.noBorder) {
                domClass.remove(context.nestedTreeCellNode, "nestedtree-border-bottom");
            }

            if (_setting.marginTop) {
                domStyle.set(context.nestedTreeCellNode, "margin-top", _setting.marginTop + "px");
            }

            html.set(context.productCodeNode, _setting.title);
            html.set(context.productDecsNode, _setting.description);
        }
    });
});
