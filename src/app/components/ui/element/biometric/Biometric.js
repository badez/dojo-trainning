define([
    "dojo/_base/declare",
    "dojo/dom-attr",
    "dojo/dom-construct",
    "dojo/topic",
    "dojo/text!./html/Biometric.html",
    "dojo/on",
    "dojo/dom-class",
    "dojo/html",
    "dojo/i18n!app/locale/nls/common",
    "dijit/_TemplatedMixin",
    "app/components/ui/element/BaseElement",
    "app/components/ui/element/datepicker/Datepicker",
    "app/components/utils/General"
], function(
    declare,
    domAttr,
    domConstruct,
    topic,
    template,
    on,
    domClass,
    html,
    i18n_common,
    TemplatedMixin,
    BaseElement,
    Datepicker,
    GeneralUtils
) {
    return declare("app.components.ui.element.bimetric.Biometric", [BaseElement, TemplatedMixin], {
        templateString: template,
        chequescanner: null,
        myKadDetail: null,
        p_construct: function(setting) {
            this.inherited(arguments);
            var domNode = this.domNode;
            var context = this;

            context.setupContent(setting);
            context.initInteraction(setting);
            context.i18n(setting);
            context.init(setting);
        },
        initInteraction: function(_setting) {
            var context = this;
            var domNode = this.domNode;

            on(context.scanBtnNode, "click", function(event) {
                context.chequescanner.connect();
            });

            on(context.proceedBtnNode, "click", function() {
                if (_setting.successCallback != undefined) {
                    _setting.successCallback(myKadDetail);
                }
            });
        },
        setupContent: function(_setting) {
            var context = this;
            var domNode = this.domNode;
            var domNodeId = domAttr.get(domNode, "id");
            
            var uniqueIdDateOfBirthpicker = new GeneralUtils().createUUID();
            domAttr.set(context.dob, "id", uniqueIdDateOfBirthpicker);
            var dateOfBirth = new Datepicker().placeAt(context.dob);
            dateOfBirth.p_construct({
                parentid: domNodeId,
                datepickerid: uniqueIdDateOfBirthpicker
            });
            topic.publish("datepicker.enable." + uniqueIdDateOfBirthpicker, false);
            
            var uniqueIdCanvas = new GeneralUtils().createUUID();
            domAttr.set(context.photoNode, "id", uniqueIdCanvas);

            //if (_setting.onModel != undefined && _setting.onModel) {
                //domClass.remove(context.inputGroup, "col-lg-10");
                //domClass.remove(context.inputGroup, "col-lg-offset-1");
                //domClass.remove(context.inputGroup1, "col-lg-4");
                //domClass.remove(context.inputGroup2, "col-lg-4");
            //}
        },
        i18n: function(_setting) {
            var context = this;
            var domNode = this.domNode;

            html.set(context.customerIdNoLabel, i18n_common.customerIdNoLabel);
            domAttr.set(context.customerIdNo, "placeholder", i18n_common.customerIdNoLabel);
            html.set(context.alternativeIdNoLabel, i18n_common.alternativeIdNoLabel);
            domAttr.set(context.alternativeIdNo, "placeholder", i18n_common.alternativeIdNoLabel);
            html.set(context.fullNameLabel, i18n_common.fullNameLabel);
            domAttr.set(context.fullName, "placeholder", i18n_common.fullNameLabel);
            html.set(context.genderLabel, i18n_common.genderLabel);
            domAttr.set(context.gender, "placeholder", i18n_common.genderLabel);
            
            html.set(context.religionLabel, i18n_common.religionLabel);
            domAttr.set(context.religion, "placeholder", i18n_common.religionLabel);
            html.set(context.postCodeLabel, i18n_common.postCodeLabel);
            domAttr.set(context.postCode, "placeholder", i18n_common.postCodeLabel);
            html.set(context.cityLabel, i18n_common.cityLabel);
            domAttr.set(context.city, "placeholder", i18n_common.cityLabel);
            html.set(context.stateCodeLabel, i18n_common.stateCodeLabel);
            domAttr.set(context.stateCode, "placeholder", i18n_common.stateCodeLabel);
            html.set(context.countryLabel, i18n_common.countryLabel);
            domAttr.set(context.country, "placeholder", i18n_common.countryLabel);
            
            html.set(context.dobLabel, i18n_common.dobLabel);
            
            
            html.set(context.scanBtnNode, i18n_common.scan);
            html.set(context.proceedBtnNode, i18n_common.proceed);
        },
        init: function(_setting) {
            var context = this;
            var domNode = this.domNode;
            myKadDetail = {};
            context.chequescanner = {
                connect: function() {
                    topic.publish("transaction.process.started");
                    context.clearMyKadDetail();
                    var location = "ws://localhost:4040/mykadscanner/"
                    this._ws = new WebSocket(location);
                    this._ws.onopen = this._onopen;
                    this._ws.onmessage = this._onmessage;
                    this._ws.onclose = this._onclose;
                    this._ws.onerror = this._onerror;
                    this._ws.binaryType = "arraybuffer";
                },
                _onopen: function() {},
                _onmessage: function(message) {
                    try {
                        if (message.data) {
                            if (typeof message.data == "string") {
                                //after connect to JniServer
                                if (message.data == "Server received Web Socket upgrade and added it to Receiver List.") {
                                    context.chequescanner._send('verifyMykadAndFingerprint');
                                    context.chequescanner._send('getPicture');
                                    context.chequescanner._send('verifyFingerprint');
                                } else if (message.data.indexOf('{') == 0) {
                                    //handle json
                                    console.log("Got Json");
                                    console.log(message.data);
                                    var json = JSON.parse(message.data);
                                    if (json.authResult == undefined) {
                                        context.setMyKadDetail(json);
                                    } else if (98 == json.authResult) {
                                        context.chequescanner._close();
                                        //topic.publish("transaction.process.ended");
                                        //topic.publish("navigation.modal.open", "alert", {
                                        //    context: "error",
                                        //    title: i18n_common.error,
                                        //    desc: i18n_common.cardUnreadable
                                        //});
                                        context.clearMyKadDetail();
                                    }
                                } else if (message.data.indexOf('[99,') == 0) {
                                    context.chequescanner._close();
                                    topic.publish("transaction.process.ended");
                                    //topic.publish("navigation.modal.open", "alert", {
                                    //    context: "error",
                                    //    title: i18n_common.error,
                                    //    desc: i18n_common.thumbPrintNotVerify
                                    //});
                                    context.clearMyKadDetail();
                                } else if (message.data.indexOf('[0,') == 0) {
                                    context.chequescanner._close();
                                    topic.publish("transaction.process.ended");
                                    //if (_setting.onModel == undefined || !_setting.onModel) {
                                    //    topic.publish("navigation.modal.open", "alert", {
                                    //        context: "success",
                                    //        title: i18n_common.success,
                                    //        desc: i18n_common.thumbPrintVerify
                                    //    });
                                    //}
                                    context.populate();
                                    // if (_setting.successCallback != undefined) {
                                    //     _setting.successCallback(myKadDetail);
                                    // }
                                    // topic.publish("biometric.success." + _setting, myKadDetail);
                                    domAttr.remove(context.proceedBtnNode, "disabled"); //added by kamil
                                } else {
                                    console.log("Else message");
                                    console.log(message.data);
                                    context.chequescanner._close();
                                    topic.publish("transaction.process.ended");
                                }
                            } else {
                                var bytes = new Uint8Array(message.data);
                                var data = "";
                                var len = bytes.byteLength;
                                for (var i = 0; i < len; ++i) {
                                    data += String.fromCharCode(bytes[i]);
                                }
                                context.setPhoto(window.btoa(data));
                            }
                        }
                    } catch (e) {
                        console.log(e);
                    } finally {

                    }
                },
                _onclose: function() {
                    console.log("Web Socket Closed");
                },
                _onerror: function(e) {
                    console.log(e);
                    topic.publish("transaction.process.ended");
                    topic.publish("navigation.modal.open", "alert", {
                        context: "error",
                        title: i18n_common.error,
                        desc: i18n_common.jniServerNotUp
                    });
                },
                _send: function(message) {
                    if (this._ws)
                        this._ws.send(message);
                },
                _close: function() {
                    this._ws.close();
                }
            };
        },
        setMyKadDetail: function(theJson) {
            var context = this;

            //check the data -------By Kamil
            console.log(theJson);

            myKadDetail.customerIdNo = theJson.newIC.trim();
            myKadDetail.alternativeIdNo = theJson.oldIC.trim();
            myKadDetail.fullName = theJson.originalName.trim();
            myKadDetail.gender = theJson.sex.trim();
            myKadDetail.dob = theJson.birthDate.trim();
            myKadDetail.religion = theJson.religion.trim();
            myKadDetail.postCode = theJson.postCode.trim();
            myKadDetail.stateCode = theJson.state.trim();
            myKadDetail.city = theJson.city.trim();
            myKadDetail.country = theJson.citizenship.trim();
            myKadDetail.address1 = theJson.address1.trim();
            myKadDetail.address2 = theJson.address2.trim();
            myKadDetail.address3 = theJson.address3.trim();

        },
        populate: function() {
            var context = this;
            var domNode = this.domNode;
            var domNodeId = domAttr.get(domNode, "id");

            context.customerIdNo.value = myKadDetail.customerIdNo;
            context.alternativeIdNo.value = myKadDetail.alternativeIdNo;
            context.fullName.value = myKadDetail.fullName;
            context.gender.value = myKadDetail.gender;
            
            var birthdates = myKadDetail.dob.split("/");
            domConstruct.empty(context.dob);
            var uniqueIdDateOfBirthpicker = new GeneralUtils().createUUID();
            domAttr.set(context.dob, "id", uniqueIdDateOfBirthpicker);
            var dateOfBirth = new Datepicker().placeAt(context.dob);
            dateOfBirth.p_construct({
                parentid: domNodeId,
                datepickerid: uniqueIdDateOfBirthpicker,
                options: {
                    defaultDates: {
                        date1: new Date(birthdates[2],birthdates[1],birthdates[0])
                    }
                }
            });
            topic.publish("datepicker.enable." + uniqueIdDateOfBirthpicker, false);
            
            context.religion.value = myKadDetail.religion;
            context.postCode.value = myKadDetail.postCode;
            context.city.value = myKadDetail.city;
            context.stateCode.value = myKadDetail.stateCode;
            context.country.value = myKadDetail.country;
            //domAttr.set(context.photoNode, "src", "data:image/png;base64," + myKadDetail.photo);
            
            var canvas = document.getElementById(domAttr.get(context.photoNode, "id"));
            console.log(canvas);
            var ctx = canvas.getContext('2d');
            var img = new Image();

            img.onload = function() {
                var x = (canvas.width / 2) - (img.width / 2)
                ctx.drawImage(this, x, 0, img.width, img.height);
            }
            img.src = "data:image/png;base64," + myKadDetail.photo;
            
            domClass.remove(context.photoNode, "hidden");
            domClass.add(context.photoDefaultNode, "hidden");
            html.set(context.scanBtnNode, i18n_common.rescan);
        },
        clearMyKadDetail: function() {
            var context = this;
            var domNode = this.domNode;
            var domNodeId = domAttr.get(domNode, "id");
            
            context.customerIdNo.value = "";
            context.alternativeIdNo.value = "";
            context.fullName.value = "";
            context.gender.value = "";
            //context.dob.value = "";
            
            domConstruct.empty(context.dob);
            var uniqueIdDateOfBirthpicker = new GeneralUtils().createUUID();
            domAttr.set(context.dob, "id", uniqueIdDateOfBirthpicker);
            var dateOfBirth = new Datepicker().placeAt(context.dob);
            dateOfBirth.p_construct({
                parentid: domNodeId,
                datepickerid: uniqueIdDateOfBirthpicker
            });
            topic.publish("datepicker.enable." + uniqueIdDateOfBirthpicker, false);
            
            context.religion.value = "";
            context.postCode.value = "";
            context.city.value = "";
            context.stateCode.value = "";
            context.country.value = "";
            domAttr.set(context.photoNode, "src", "");
            domClass.add(context.photoNode, "hidden");
            domClass.remove(context.photoDefaultNode, "hidden");
        },
        setPhoto: function(byteString) {
            var context = this;
            myKadDetail.photo = byteString;
            // domAttr.set(context.photoNode, "src", "data:image/png;base64," + byteString);
        }
    });
});
