define([
    "dojo/_base/declare",
    "dojo/text!./html/TreeMenuCell.html",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/dom-style",
    "dojo/dom-attr",
    "dojo/on",
    "dojo/html",
    "dijit/_TemplatedMixin",
    "app/components/ui/element/BaseElement",
    "app/components/utils/General"
], function(
    declare,
    template,
    domClass,
    domConstruct,
    domStyle,
    domAttr,
    on,
    html,
    TemplatedMixin,
    BaseElement,
    GeneralUtils
) {

    return declare("app.components.ui.element.treemenu.TreeMenuCell", [BaseElement, TemplatedMixin], {
        templateString: template,
        p_construct: function(setting) {
            var domNode = this.domNode;
            var context = this;

            context.setupContent(setting);
            context.initInteraction(setting);
        },
        setupContent: function(_setting) {
            var context = this;
            var domNode = this.domNode;

            html.set(context.titleNode, _setting.title);

            if (_setting.description) {
                html.set(context.descriptionNode, _setting.description);

                if (_setting.onDescriptionClick) {
                    domClass.add(context.descriptionNode, "clickable");
                }
            } else {
                domConstruct.destroy(context.descriptionNode);
            }

            if (_setting.indent > 0) {
                domStyle.set(context.titleIndentNode, "padding-left", _setting.indent.toString() + "px");
            }

            if (_setting.children) {
                var uniqueHeadingId = new GeneralUtils().createUUID();
                domAttr.set(context.headingNode, "id", uniqueHeadingId);
                domAttr.set(context.collapseNode, "aria-labelledby", uniqueHeadingId);

                var uniqueCollapseId = new GeneralUtils().createUUID();
                domClass.add(context.titleNode, "clickable");
                domAttr.set(context.collapseNode, "id", uniqueCollapseId);
                domAttr.set(context.titleNode, "data-target", "#" + uniqueCollapseId);
                domAttr.set(context.titleNode, "aria-controls", uniqueCollapseId);
                _setting.uniqueCollapseId = uniqueCollapseId;
                $("#" + uniqueCollapseId).collapse({ toggle: false });
            } else {
                domConstruct.destroy(context.collapseNode);
                domConstruct.destroy(context.iconNode);
                domStyle.set(context.titleIndentNode, "margin-left", "20px");
            }
        },
        initInteraction: function(_setting) {
            var context = this;

            if (_setting.children) {
                on(context.titleNode, "click", function(event) {
                    domClass.toggle(context.iconNode, "fa-caret-right");
                    domClass.toggle(context.iconNode, "fa-caret-down");
                    $("#" + _setting.uniqueCollapseId).collapse("toggle");
                    event.stopPropagation();
                });
            }

            if (_setting.onDescriptionClick) {
                on(context.descriptionNode, "click", function(event) {
                    event.stopPropagation();
                    _setting.onDescriptionClick(_setting);
                });
            }
        },
        getBodyNode: function() {
            var context = this;
            return context.bodyNode;
        }
    });
});
