define([
    "dojo/_base/declare",
    "dojo/_base/array",
    "dojo/text!./html/TreeMenu.html",
    "dijit/_TemplatedMixin",
    "app/components/ui/element/BaseElement",
    "app/components/ui/element/treemenu/TreeMenuCell"
], function(
    declare,
    array,
    template,
    TemplatedMixin,
    BaseElement,
    TreeMenuCell
) {

    return declare("app.components.ui.element.treemenu.TreeMenu", [BaseElement, TemplatedMixin], {
        templateString: template,
        p_construct: function(setting) {
            var domNode = this.domNode;
            var context = this;

            context.setupContent(setting);
        },
        setupContent: function(_setting) {
            var context = this;
            var domNode = this.domNode;

            // Wrapping everything up with a topmost layer
            array.forEach(_setting.list, function(single) {
                var obj = {};
                obj.title = "MAIN";

                if (_setting.onDetailsClick) {
                    obj.onDescriptionClick = _setting.onDetailsClick;
                }

                obj.children = [];
                obj.children.push(single);

                context.p_constructTreeMenuCell(obj, domNode, 0);
            });
        },
        constructTreeMenuCell: function(setting, domNode, num) {
            var context = this;

            array.forEach(setting.children, function(single) {
                single.mainTitle = setting.title;
                single.indent = num * 20;
                single.onDescriptionClick = setting.onDescriptionClick;

                var treeMenuCell = new TreeMenuCell();
                treeMenuCell.placeAt(domNode);
                treeMenuCell.p_construct(single);
                var newDomNode = treeMenuCell.getBodyNode();

                if (single.children) {
                    context.p_constructTreeMenuCell(single, newDomNode, num + 1);
                }
            });
        }
    });
});
