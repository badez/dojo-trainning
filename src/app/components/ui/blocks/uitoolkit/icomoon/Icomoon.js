define([
    "dojo/_base/declare",
    "dojo/text!./html/Icomoon.html",
    "dojo/_base/array",
    "dojo/html",
    "../../BaseBlock",
    "../../../cell/uitoolkit/icomoon/IcomoonCell"
], function(
    v_declare,
    v_template,
    v_array,
    v_html,
    v_BaseBlock,
    v_IcomoonCell
) {

    return v_declare([v_BaseBlock], {
        templateString: v_template,
        p_construct: function(v_setting) {
            this.inherited(arguments);
            var v_context = this;

            v_context.p_setupContent(v_setting);
        },
        p_setupContent: function(v_setting) {
            var v_context = this;

            var v_data = [{
                "class": "ico-add"
            },{
                "class": "ico-search-2"
            }, {
                "class": "ico-legalcaseupdate"
            }, {
                "class": "ico-multitab"
            }, {
                "class": "ico-logout-2"
            }, {
                "class": "ico-language"
            }, {
                "class": "ico-logout-1"
            }, {
                "class": "ico-multitab-close"
            }, {
                "class": "ico-loansme"
            }, {
                "class": "ico-loaneducational"
            }, {
                "class": "ico-loandefault"
            }, {
                "class": "ico-star"
            }, {
                "class": "ico-loanpersonal"
            }, {
                "class": "ico-loanhousing"
            }, {
                "class": "ico-loanauto"
            }, {
                "class": "ico-verifylof"
            }, {
                "class": "ico-upload_failed"
            }, {
                "class": "ico-upload"
            }, {
                "class": "ico-timeline"
            }, {
                "class": "ico-refresh"
            }, {
                "class": "ico-quickca"
            }, {
                "class": "ico-printlof"
            }, {
                "class": "ico-paging-previousmax"
            }, {
                "class": "ico-paging-previous"
            }, {
                "class": "ico-paging-nextmax"
            }, {
                "class": "ico-paging-next"
            }, {
                "class": "ico-loanentitlement"
            }, {
                "class": "ico-loancalculator"
            }, {
                "class": "ico-legalcasereassignment"
            }, {
                "class": "ico-legalcaselisting"
            }, {
                "class": "ico-floating"
            }, {
                "class": "ico-filter"
            }, {
                "class": "ico-filetype-unknown"
            }, {
                "class": "ico-filetype-txt"
            }, {
                "class": "ico-filetype-png"
            }, {
                "class": "ico-filetype-pdf"
            }, {
                "class": "ico-filetype-jpg"
            }, {
                "class": "ico-filetype-doc"
            }, {
                "class": "ico-extra"
            }, {
                "class": "ico-edit"
            }, {
                "class": "ico-download"
            }, {
                "class": "ico-delete"
            }, {
                "class": "ico-customerexposureenquiry"
            }, {
                "class": "ico-caapproval"
            }, {
                "class": "ico-failed"
            }, {
                "class": "ico-success"
            }, {
                "class": "ico-warning"
            }, {
                "class": "ico-go-2"
            }, {
                "class": "ico-calendar"
            }, {
                "class": "ico-expand-active"
            }, {
                "class": "ico-next"
            }, {
                "class": "ico-relationship"
            }, {
                "class": "ico-sorting"
            }, {
                "class": "ico-sorting-active"
            }, {
                "class": "ico-journal"
            }, {
                "class": "ico-cheque-2"
            }, {
                "class": "ico-cash"
            }, {
                "class": "ico-go"
            }, {
                "class": "ico-sad"
            }, {
                "class": "ico-close"
            }, {
                "class": "ico-dashboard"
            }, {
                "class": "ico-expand"
            }, {
                "class": "ico-menu"
            }, {
                "class": "ico-qr"
            }, {
                "class": "ico-transfer"
            }, {
                "class": "ico-widget"
            }, {
                "class": "ico-back"
            }, {
                "class": "ico-biometric"
            }, {
                "class": "ico-caret-down"
            }, {
                "class": "ico-search-1"
            }, {
                "class": "ico-notification"
            }, {
                "class": "ico-account"
            }, {
                "class": "ico-address"
            }, {
                "class": "ico-certificate"
            }, {
                "class": "ico-deposit"
            }, {
                "class": "ico-email"
            }, {
                "class": "ico-signature"
            }, {
                "class": "ico-cheque-1"
            }, {
                "class": "ico-id"
            }, {
                "class": "ico-name"
            }, {
                "class": "ico-payment"
            }, {
                "class": "ico-phone"
            }, {
                "class": "ico-withdrawal"
            }];

            v_array.forEach(v_data, function(v_single) {
                var v_icomoonCell = new v_IcomoonCell();
                v_icomoonCell.placeAt(v_context.icomoonCellNode);
                v_icomoonCell.p_construct({
                    p_data: v_single["class"]
                });
            });
            v_html.set(v_context.totalNode, v_data.length);
        }
    });
});
