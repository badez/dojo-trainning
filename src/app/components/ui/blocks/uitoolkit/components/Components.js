define([
    "dojo/_base/array",
    "dojo/_base/declare",
    "dojo/text!./html/Components.html",
    "dojo/dom-attr",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/dom-construct",
    "dojo/html",
    "dojo/on",
    "dojo/topic",
    "dojo/NodeList-traverse",
    "dojo/query",
    "dijit/form/CheckBox",
    "dijit/form/RadioButton",
    "dijit/_TemplatedMixin",
    "app/components/ui/blocks/BaseBlock",
    "app/components/ui/element/dropdown/Dropdown",
    "app/components/ui/element/datepicker/Datepicker",
    "app/components/ui/element/daterangepicker/DateRangePicker",
    "app/components/ui/element/uploadfile/UploadFile",
    "app/components/ui/element/treemenu/TreeMenu",
    "app/components/ui/element/chart/Chart",
    "app/components/ui/element/alertbar/AlertBar",
    "app/components/ui/element/slider/Slider",
    "app/components/ui/element/noresult/NoResult",
    "app/components/ui/element/successpage/SuccessPage",
    "app/components/ui/element/timeframe/TimeFrame",
    "app/components/ui/element/timeframe/TimeFrameCellV2",
    "app/components/ui/element/biometric/Biometric",
    "app/components/ui/element/amounttextfield/AmountTextfield",
    "app/components/ui/element/searchinput/SearchInput",
    "app/components/ui/element/switchercustom/SwitcherCustom",
    "app/components/ui/element/togglebuttons/ToggleButtons",
    "app/components/ui/element/customtable/CustomTable",
    "app/components/ui/element/nestedtree/NestedTreeCell",
    "app/components/ui/element/dropdownstep/DropdownStep",
    "app/components/ui/element/quicknavigationmenu/QuickNavigationMenu",
    "app/components/utils/Rest",
    "app/components/utils/General",
    "dojo/i18n!app/locale/nls/components",
    "dojo/i18n!app/locale/nls/common"
], function(
    array,
    declare,
    template,
    domAttr,
    domClass,
    domStyle,
    domConstruct,
    html,
    on,
    topic,
    nodeList,
    query,
    CheckBox,
    RadioButton,
    TemplatedMixin,
    BaseBlock,
    Dropdown,
    Datepicker,
    DateRangePicker,
    UploadFile,
    TreeMenu,
    Chart,
    AlertBar,
    Slider,
    NoResult,
    SuccessPage,
    TimeFrame,
    TimeFrameCellV2,
    Biometric,
    AmountTextfield,
    SearchInput,
    SwitcherCustom,
    ToggleButtons,
    CustomTable,
    NestedTreeCell,
    DropdownStep,
    QuickNavigationMenu,
    Rest,
    GeneralUtils,
    i18n,
    i18n_common
) {
    return declare("app.components.ui.blocks.components.Components", [BaseBlock, TemplatedMixin], {
        templateString: template,
        p_construct: function(setting) {
            this.inherited(arguments);

            var domNode = this.domNode;
            var context = this;

            context.startListening(setting);
            context.setupContent(setting);
            context.initInteraction(setting);
            context.createQuickSearchList(setting);
        },
        startup: function() {
            this.inherited(arguments);
        },
        destroy: function() {
            this.inherited(arguments);
        },
        startListening: function(_setting) {
            var context = this;
            var domNode = this.domNode;
            var domNodeId = domAttr.get(domNode, "id");

            var dropdownDependencyHandler = topic.subscribe("dropdown.selected.retrieve." + domNodeId,
                function(selectedData) {
                    switch (selectedData.dropdownid) {
                        case domAttr.get(context.dropdown1Node, "id"):
                            var rest = new Rest().get("assets/data/sampledependency-" + selectedData.value + ".json");
                            rest.then(function(response) {
                                topic.publish("dropdown.resetDropdownList." + context.uniqueIdFormDependencyDropdown2,
                                    response.options);
                                topic.publish("dropdown.resetDropdownList." + context.uniqueIdFormDependencyDropdown3, []);
                            }, function(err) {
                                console.log(err, err);
                            }).then(function() {
                                topic.publish("dropdown.select.value." + context.uniqueIdFormDependencyDropdown2,
                                    context.dependencyDropdown2Value);

                                context.dependencyDropdown2Value = undefined;
                            }, function(err) {
                                console.log(err, err);
                            });
                            break;
                        case domAttr.get(context.dropdown2Node, "id"):
                            var rest = new Rest().get("assets/data/sampledependency-" + selectedData.value + ".json");
                            rest.then(function(response) {
                                topic.publish("dropdown.resetDropdownList." + context.uniqueIdFormDependencyDropdown3,
                                    response.options);
                            }, function(err) {
                                console.log(err, err);
                            }).then(function() {
                                topic.publish("dropdown.select.value." + context.uniqueIdFormDependencyDropdown3,
                                    context.dependencyDropdown3Value);

                                context.dependencyDropdown3Value = undefined;
                            }, function(err) {
                                console.log(err, err);
                            });
                            break;
                    }
                });
            topic.publish("topic.handlers.add", dropdownDependencyHandler, domNodeId);

            var datatableEditedDataHandler = topic.subscribe("datatable.edited.cell." + domNodeId, function(rowsData) {
                console.log("rowsData:: ", rowsData);
            });
            topic.publish("topic.handlers.add", datatableEditedDataHandler, domNodeId);
        },
        initInteraction: function(_setting) {
            var context = this;
            var domNode = this.domNode;
            var domNodeId = domAttr.get(domNode, "id");

            on(context.successBtnNode, "click", function() {
                topic.publish("navigation.modal.open", "SUCCESS_FAIL", {
                    p_state: "SUCCESS",
                    p_info: [{
                        p_label: "CA No.",
                        p_data: "00002/80/000015/14"
                    }, {
                        p_label: "Customer Name",
                        p_data: "Muhamad Kamil"
                    }, {
                        p_label: "Email",
                        p_data: "info@infopro.com.my"
                    }],
                    p_buttons: [{
                        p_label: "BACK",
                        p_type: "secondary",
                        p_action: function() {
                            alert("Button 2 is click!");
                        }
                    }, {
                        p_label: "FINISH",
                        p_type: "primary",
                        p_action: function() {
                            alert("Button 1 is click!");
                        }
                    }]
                });
            });

            on(context.failBtnNode, "click", function() {
                topic.publish("navigation.modal.open", "SUCCESS_FAIL", {
                    p_state: "FAIL",
                    p_title: "",
                    p_description: "",
                    p_buttons: [{
                        p_label: "TRY AGAIN!",
                        p_type: "secondary",
                        p_action: function() {
                            topic.publish("close.modal." + domNodeId);
                        }
                    }]
                });
            });

            on(context.warningBtnNode, "click", function() {
                topic.publish("navigation.modal.open", "SUCCESS_FAIL", {
                    p_state: "WARNING",
                    p_title: "",
                    p_description: "",
                    p_buttons: [{
                        p_label: "CANCEL",
                        p_type: "secondary",
                        p_action: function() {
                            alert("Button 2 is click!");
                        }
                    }, {
                        p_label: "PROCEED",
                        p_type: "primary",
                        p_action: function() {
                            alert("Button 1 is click!");
                        }
                    }]
                });
            });

            on(context.customTableAddBtnNode, "click", function() {
                topic.publish("customtable.add.row." + context.customTableId, {
                    logSequence: "24234324",
                    assignee: "002",
                    action: "2349585",
                    userId: "Pablo",
                    supervisor: "Voon Jin",
                    supervisor2: "BEN01",
                    "function": "645645",
                    deleteButton: null
                });
            })

            on(context.defaultBtnNode, "click", function() {
                context.changeTheme("default");
            })

            on(context.marineBtnNode, "click", function() {
                context.changeTheme("marine");
            })

            on(context.cocoaBtnNode, "click", function() {
                context.changeTheme("cocoa");
            })

            on(context.aquaBtnNode, "click", function() {
                context.changeTheme("aqua");
            })

            on(context.violetBtnNode, "click", function() {
                context.changeTheme("violet");
            })
        },
        setupNestedTree: function(productList, domNodeNestedTree, level, productListLength, productNum) {
            var context = this;
            var domNode = this.domNode;
            var domNodeId = domAttr.get(domNode, "id");

            var isLastProduct = productListLength == productNum;

            productList.level = level;

            if (isLastProduct && productList.children == null && level == 1) {
                productList.noBorder = true;
            }

            if (productNum == 1) {
                productList.marginTop = -5;
            }

            var nestedTree = new NestedTreeCell();
            nestedTree.placeAt(domNodeNestedTree);
            nestedTree.p_construct(productList);

            if (productList.children) {
                array.forEach(productList.children, function(singleProductChild, j) {
                    var productChildNum = j + 1;
                    var isLastProductChild = productList.children.length == productChildNum;
                    if (isLastProduct && isLastProductChild) {
                        singleProductChild.noBorder = true;
                    }
                    singleProductChild.icon = true;
                    context.setupNestedTree(singleProductChild, domNodeNestedTree, level + 1, productListLength, productNum);
                });
            }
        },
        setupContent: function(_setting) {
            var context = this;
            var domNode = this.domNode;
            var domNodeId = domAttr.get(domNode, "id");

            context.createMask(context.caNumberNode, {
                type: "caNumber"
            });
            context.createDecimalInput(context.percentageNode, "percentageDecimal");
            context.createDecimalInput(context.interestRateNode, "interestRateDecimal");
            context.createDecimalInput(context.decimalNode, 1);

            var prototypeUrl = "assets/data/nestedtreelist.json";
            var dataPackage = {};

            var rest = new Rest().p_postapi({ p_api: "/lof/details" }, prototypeUrl, dataPackage);
            rest.then(function(response) {
                array.forEach(response.productTreeResponse.list, function(singleProduct, i) {
                    context.setupNestedTree(singleProduct, context.treeListNode, 1, response.productTreeResponse.list.length, i + 1);
                });
            });

            var uniqueIdFormSearchInput = context.createUUID(context.searchInputNode);
            searchInput = new SearchInput().placeAt(context.searchInputNode);
            searchInput.p_construct({
                parentid: domAttr.get(domNode, "id"),
                searchInputId: uniqueIdFormSearchInput,
                placeHolder: "Search Input",
                callback: function(_inputValue) {
                    alert(_inputValue);
                }
            });

            uniqueIdFormSearchInput = context.createUUID(context.searchInputTextNode);
            searchInput = new SearchInput().placeAt(context.searchInputTextNode);
            searchInput.p_construct({
                parentid: domAttr.get(domNode, "id"),
                searchInputId: uniqueIdFormSearchInput,
                placeHolder: "Search Input",
                type: "text",
                callback: function(_inputValue) {
                    alert(_inputValue);
                }
            });

            uniqueIdFormSearchInput = context.createUUID(context.searchInputIconNode);
            searchInput = new SearchInput().placeAt(context.searchInputIconNode);
            searchInput.p_construct({
                parentid: domAttr.get(domNode, "id"),
                searchInputId: uniqueIdFormSearchInput,
                placeHolder: "Search Input",
                type: "icon",
                callback: function(_inputValue) {
                    alert(_inputValue);
                }
            });

            uniqueIdFormSearchInput = context.createUUID(context.searchInputNodeDisable);
            searchInput = new SearchInput().placeAt(context.searchInputNodeDisable);
            searchInput.p_construct({
                parentid: domAttr.get(domNode, "id"),
                searchInputId: uniqueIdFormSearchInput,
                placeHolder: "Search Input",
                enable: false,
            });

            /* //-- IMPORTANT --//
                Differences Between Search Modal With and Without API:

                With API ~  The API calling happened inside the Search Modal itself instead of caller usecase

                Without API ~   The API calling happened in the caller usecase, and pass the result to the Search Modal                
            */
            uniqueIdFormSearchInput = context.createUUID(context.searchInputLOVNode);
            searchInput = new SearchInput().placeAt(context.searchInputLOVNode);
            searchInput.p_construct({
                parentid: domAttr.get(domNode, "id"),
                searchInputId: uniqueIdFormSearchInput,
                placeHolder: "Search Input",
                isLovSearch: true,
                callback: function(_inputValue) {
                    topic.publish("navigation.modal.open", "SEARCH", {
                        tittle: "Search Modal With API Calling",
                        parentId: domAttr.get(domNode, "id"),
                        api: "modal/find",
                        serviceParameter: "P001",
                        dataSrc: "data",
                        isSearchRequired: true,
                        prototypeUrl: "assets/data/searchmodal.json",
                        columns: [
                            { title: "Currency Code", key: "value" },
                            { title: "Description", key: "keyname" }
                        ],
                        callback: function(trElement, rowData) {
                            if (rowData !== undefined) {
                                topic.publish("searchInput.setValue." + uniqueIdFormSearchInput, rowData.keyname);
                            }
                        }
                    });
                }
            });

            uniqueIdFormSearchInput2 = context.createUUID(context.searchInputLOV2Node);
            searchInput = new SearchInput().placeAt(context.searchInputLOV2Node);
            searchInput.p_construct({
                parentid: domAttr.get(domNode, "id"),
                searchInputId: uniqueIdFormSearchInput2,
                placeHolder: "Search Input",
                isLovSearch: true,
                callback: function(_inputValue) {
                    var dataPackage = {
                        serviceParameter: "P001",
                        searchKey: "MYR"
                    }
                    var rest = new Rest().p_postapi({ p_api: _setting.api }, "assets/data/searchmodal.json", dataPackage);
                    rest.then(function(response) {
                        topic.publish("navigation.modal.open", "SEARCH", {
                            tittle: "Search Modal Without API Calling",
                            parentId: domAttr.get(domNode, "id"),
                            isSearchRequired: false,
                            data: response.data,
                            columns: [
                                { title: "Currency Code", key: "value" },
                                { title: "Description", key: "keyname" }
                            ],
                            callback: function(trElement, rowData) {
                                if (rowData !== undefined) {
                                    topic.publish("searchInput.setValue." + uniqueIdFormSearchInput2, rowData.keyname);
                                }
                            }
                        });
                    });
                }
            });

            //Start Toggle Buttons
            var uniqueIdToggleButtons = context.createUUID(context.toggleButtonsNode);
            var toggleButtons = new ToggleButtons().placeAt(context.toggleButtonsNode);
            toggleButtons.p_construct({
                selectedPerTime: 1,
                args: [{
                    key: "corpLn",
                    text: "Corporate Loan",
                }, {
                    key: "corpLn2",
                    text: "Corporate Loan 2",
                }]
            });
            toggleButtons.select("corpLn");

            //ENd Toggle Buttons

            // Start create dropdown
            var formDropdownValues = [{
                keyname: "Value 1",
                value: "001"
            }, {
                keyname: "Value 2",
                value: "002"
            }];

            var uniqueIdFormDropdown = context.createUUID(context.dropdownNode);
            var formDropdown = new Dropdown().placeAt(context.dropdownNode);
            formDropdown.p_construct({
                parentid: domAttr.get(domNode, "id"),
                dropdownid: uniqueIdFormDropdown,
                data: formDropdownValues,
                _default: false
            });

            uniqueIdFormDropdown = context.createUUID(context.dropdownDisableNode);
            formDropdown = new Dropdown().placeAt(context.dropdownDisableNode);
            formDropdown.p_construct({
                parentid: domAttr.get(domNode, "id"),
                dropdownid: uniqueIdFormDropdown,
                data: formDropdownValues,
                _default: false,
                disabled: true,
            });

            var formQueryDropdownValues = [{
                keyname: "Value 1",
                value: "001"
            }, {
                keyname: "Value 2",
                value: "002"
            }, {
                keyname: "Value 3",
                value: "003"
            }, {
                keyname: "Value 4",
                value: "004"
            }, {
                keyname: "Value 5",
                value: "005"
            }, {
                keyname: "Value 6",
                value: "006"
            }, {
                keyname: "Value 7",
                value: "007"
            }, {
                keyname: "Value 8",
                value: "008"
            }, {
                keyname: "Value 9",
                value: "009"
            }, {
                keyname: "Value 10",
                value: "010"
            }, {
                keyname: "Value 11",
                value: "011"
            }];
  
            var uniqueIdFormQueryDropdown = context.createUUID(context.dropdownLiveFilterNode);
            var formQueryDropdown = new Dropdown().placeAt(context.dropdownLiveFilterNode);
            formQueryDropdown.p_construct({
                parentid: domAttr.get(domNode, "id"),
                dropdownid: uniqueIdFormQueryDropdown,
                data: formQueryDropdownValues,
                liveSearch: true,
                defaultDisplayValue: "010"
            });

            // Start create dependency dropdown
            var formDependencyDropdownValues = [{
                keyname: "Value 1",
                value: "001"
            }, {
                keyname: "Value 2",
                value: "002"
            }];

            var uniqueIdFormDependencyDropdown = context.createUUID(context.dropdown1Node);
            var formDependencyDropdown = new Dropdown().placeAt(context.dropdown1Node);
            formDependencyDropdown.p_construct({
                parentid: domAttr.get(domNode, "id"),
                dropdownid: uniqueIdFormDependencyDropdown,
                data: formDependencyDropdownValues
            });
            context.uniqueIdFormDependencyDropdown = uniqueIdFormDependencyDropdown;

            var uniqueIdFormDependencyDropdown2 = context.createUUID(context.dropdown2Node);
            var formDependency2Dropdown = new Dropdown().placeAt(context.dropdown2Node);
            formDependency2Dropdown.p_construct({
                parentid: domAttr.get(domNode, "id"),
                dropdownid: uniqueIdFormDependencyDropdown2,
                data: []
            });
            context.uniqueIdFormDependencyDropdown2 = uniqueIdFormDependencyDropdown2;

            var uniqueIdFormDependencyDropdown3 = context.createUUID(context.dropdown3Node);
            var formDependency3Dropdown = new Dropdown().placeAt(context.dropdown3Node);
            formDependency3Dropdown.p_construct({
                parentid: domAttr.get(domNode, "id"),
                dropdownid: uniqueIdFormDependencyDropdown3,
                data: []
            });
            context.uniqueIdFormDependencyDropdown3 = uniqueIdFormDependencyDropdown3;
            // End create dependency dropdown
            // End create dropdown

            // Start create datepicker
            var uniqueIdDatepicker = context.createUUID(context.datepickerNode);
            var datepicker = new Datepicker().placeAt(context.datepickerNode);
            datepicker.p_construct({
                parentid: domAttr.get(domNode, "id"),
                datepickerid: uniqueIdDatepicker,
                options: {}
            });
            context.datepickerNotRangeid = uniqueIdDatepicker;

            uniqueIdDatepicker = context.createUUID(context.datepickerDisabledNode);
            datepicker = new Datepicker().placeAt(context.datepickerDisabledNode);
            datepicker.p_construct({
                parentid: domAttr.get(domNode, "id"),
                datepickerid: uniqueIdDatepicker,
                options: {}
            });
            context.datepickerDisableNotRangeid = uniqueIdDatepicker;
            topic.publish("datepicker.enable." + uniqueIdDatepicker, false);

            // Start create datepicker
            var uniqueIdDatepickerRange = context.createUUID(context.datepickerRangeNode);
            var datepicker2 = new Datepicker("range").placeAt(context.datepickerRangeNode);
            datepicker2.p_construct({
                type: "range",
                parentid: domAttr.get(domNode, "id"),
                datepickerid: uniqueIdDatepickerRange,
                options: {}
            });
            context.datepickerid = uniqueIdDatepickerRange;
            // End create datepicker

            // Star create dateRangePicker
            var uniqueIdDateRangePicker = context.createUUID(context.datepickerRangeColumnNode);
            var daterangepicker = new DateRangePicker().placeAt(context.datepickerRangeColumnNode);
            daterangepicker.p_construct({
                parentid: domAttr.get(domNode, "id"),
                dateRangePickerId: uniqueIdDateRangePicker,
                fromDateValidation: "required",
                defaultFromDate: new Date(),
                defaultToDate: new Date(),
                fromDateLabel: "Offer Date",
                toDateLabel: "Expiry Date",
                fromDateVisible: true,
                toDateVisible: true,
                options: {}
            });

            var dateRangePickerHandler = topic.subscribe("daterangepicker.selected.retrieve." +
                uniqueIdDateRangePicker,
                function(setting) {
                    alert(JSON.stringify(setting));
                });
            topic.publish("topic.handlers.add", dateRangePickerHandler, domAttr.get(domNode, "id"));
            // End create datepicker

            // Star create dateRangePicker
            var uniqueIdDateRangePicker2 = context.createUUID(context.datepickerRangeColumnSideNode);
            var daterangepicker = new DateRangePicker("side").placeAt(context.datepickerRangeColumnSideNode);
            daterangepicker.p_construct({
                parentid: domAttr.get(domNode, "id"),
                dateRangePickerId: uniqueIdDateRangePicker2,
                fromDateValidation: "required",
                defaultFromDate: new Date(),
                defaultToDate: new Date(),
                fromDateLabel: "Offer Date",
                toDateLabel: "Expiry Date",
                fromDateVisible: true,
                toDateVisible: true,
                options: {}
            });

            var dateRangePickerHandler = topic.subscribe("daterangepicker.selected.retrieve." +
                uniqueIdDateRangePicker2,
                function(setting) {
                    alert(JSON.stringify(setting));
                });
            topic.publish("topic.handlers.add", dateRangePickerHandler, domAttr.get(domNode, "id"));
            // End create datepicker

            // Start create AmountTextfield
            var fccdDropdownValues = [{
                keyname: "MYR",
                value: "MYR"
            }, {
                keyname: "SGD",
                value: "SGD"
            }, {
                keyname: "USD",
                value: "USD"
            }, {
                keyname: "YEN",
                value: "YEN"
            }, {
                keyname: "AUD",
                value: "AUD"
            }, {
                keyname: "RUP",
                value: "RUP"
            }, {
                keyname: "DONG",
                value: "DONG"
            }, {
                keyname: "NUD",
                value: "NUD"
            }];

            var uniqueIdAmountTextfield = context.createUUID(context.amountTextfieldNode);
            context.amounttextfieldid = uniqueIdAmountTextfield;
            var amountTextfield = new AmountTextfield().placeAt(context.amountTextfieldNode);
            amountTextfield.p_construct({
                componentid: uniqueIdAmountTextfield,

                // //THESE ARE ALL THE OPTIONAL VARIABLE FOR AMOUNT TEXTFIELD COMPONENT
                // //BE ADVISE, ONLY PARSE TO THE COMPOENENT IF YOU ARE USING IT!!

                // //If you want to set the default value for amount textfield
                // defaultValue: 100,

                // //If you want to set the default value for decimal textfield
                // decimalDefaultValue: 22,

                //If you want to configure the currency dropdown LOV based on your list or default the currency value
                fccdList: fccdDropdownValues,

                //If you want to make currency combobox disabled, change it to true
                disableFccd: false,

                //If you want to make amount textfield disabled, change it to true
                disableAmount: false,

                //If you want to make decimal textfield disabled, change it to true
                disableDecimal: false,

                //If you want to make all component in amount textfield disabled, change it to true
                disableAll: false,

                //If you want to make the amountTextfield as a mandatory field
                isRequired: true,

                //If you want to set the minimumAmount
                minAmount: 10,

                //If you want to set the maximumAmount
                // maxAmount: 400
            });

            var masterTypeTopicHandler = topic.subscribe("amounttextfield." + uniqueIdAmountTextfield,
                function(_setting) {
                    console.log(_setting);
                });
            topic.publish("topic.handlers.add", masterTypeTopicHandler, domAttr.get(domNode, "id"));

            uniqueIdAmountTextfield = context.createUUID(context.amountTextfieldDisabledNode);
            context.amounttextfielddisableid = uniqueIdAmountTextfield;
            amountTextfield = new AmountTextfield().placeAt(context.amountTextfieldDisabledNode);
            amountTextfield.p_construct({
                componentid: uniqueIdAmountTextfield,

                // //THESE ARE ALL THE OPTIONAL VARIABLE FOR AMOUNT TEXTFIELD COMPONENT
                // //BE ADVISE, ONLY PARSE TO THE COMPOENENT IF YOU ARE USING IT!!

                // //If you want to set the default value for amount textfield
                // defaultValue: 100,

                // //If you want to set the default value for decimal textfield
                // decimalDefaultValue: 22,

                //If you want to configure the currency dropdown LOV based on your list or default the currency value
                fccdList: fccdDropdownValues,

                //If you want to make currency combobox disabled, change it to true
                disableFccd: false,

                //If you want to make amount textfield disabled, change it to true
                disableAmount: false,

                //If you want to make decimal textfield disabled, change it to true
                disableDecimal: false,

                //If you want to make all component in amount textfield disabled, change it to true
                disableAll: true,

                //If you want to make the amountTextfield as a mandatory field
                isRequired: true,

                //If you want to set the minimumAmount
                minAmount: 10,

                //If you want to set the maximumAmount
                // maxAmount: 400
            });
            // End create AmountTextfield

            //Start Swicher Custom
            var uniqueIdSwitcherCustom = context.createUUID(context.swicherCustomNode);
            var switcherCustom = new SwitcherCustom().placeAt(context.swicherCustomNode);
            switcherCustom.p_construct({
                parentid: domAttr.get(domNode, "id"),
                dropdownid: uniqueIdSwitcherCustom,
                disabled: true,
                onChange: function(checked) {
                    alert(checked);
                }
            });

            var uniqueIdSwitcherCustom2 = context.createUUID(context.swicherCustom2Node);
            var switcherCustom2 = new SwitcherCustom().placeAt(context.swicherCustom2Node);
            switcherCustom2.p_construct({
                parentid: domAttr.get(domNode, "id"),
                dropdownid: uniqueIdSwitcherCustom2,
                onChange: function(checked) {
                    alert(checked);
                }
            });

            var uniqueIdSwitcherCustom3 = context.createUUID(context.swicherCustom3Node);
            var switcherCustom3 = new SwitcherCustom().placeAt(context.swicherCustom3Node);
            switcherCustom3.p_construct({
                parentid: domAttr.get(domNode, "id"),
                dropdownid: uniqueIdSwitcherCustom3,
                checked: true,
                onChange: function(checked) {
                    alert(checked);
                }
            });
            //End Switcher Custom

            // Start create slider
            var slider = new Slider();
            slider.placeAt(context.sliderContainerNode);
            slider.p_construct({
                type: "single",
                grid: false,
                min: 0,
                max: 100,
                from: 0,
                step: 10
            });

            var sliderRange = new Slider();
            sliderRange.placeAt(context.sliderRangeContainerNode);
            sliderRange.p_construct({
                type: "double",
                grid: false,
                min: -1000,
                max: 1000,
                from: -500,
                to: 500,
                step: 1
            });
            // End create slider

            // Start create chart
            var lineChart = new Chart().placeAt(context.lineChartNode);
            lineChart.p_construct({
                type: "line",
                labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun"],
                options: {
                    data: [{
                        label: "My Line Chart",
                        data: [65, 29, 80, 81, 17, 55]
                    }, {
                        label: "2nd Chart",
                        data: [28, 48, 40, 19, 86, 27]
                    }],
                    yLabel: "Y Label"
                }
            });

            var pieChart = new Chart().placeAt(context.pieChartNode);
            pieChart.p_construct({
                type: "pie",
                labels: ["Cat", "Dog", "Hamster", "Unicorn"],
                legend: false,
                options: {
                    data: [100, 300, 25, 60]
                }
            });

            var donutChart = new Chart().placeAt(context.donutChartNode);
            donutChart.p_construct({
                type: "donut",
                labels: ["Cat", "Dog", "Hamster", "Unicorn"],
                options: {
                    data: [100, 20, 25, 60]
                }
            });

            var gaugeChart = new Chart().placeAt(context.gaugeChartNode);
            gaugeChart.p_construct({
                type: "gauge",
                labels: ["Cat", "Dog", "Hamster", "Unicorn"],
                options: {
                    data: [100, 20, 25, 60]
                }
            });

            var barChart = new Chart().placeAt(context.barChartNode);
            barChart.p_construct({
                type: "bar",
                labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun"],
                options: {
                    data: [{
                        label: "My Bar Chart",
                        data: [35, 29, 50, 51, 26, 25]
                    }, {
                        label: "My Bar Chart 2",
                        data: [65, 59, 80, 81, 56, 55]
                    }, {
                        label: "My Bar Chart 3",
                        data: [95, 89, 100, 15, 86, 85]
                    }]
                }
            });

            var stackedBarChart = new Chart().placeAt(context.stackChartNode);
            stackedBarChart.p_construct({
                type: "bar",
                stacked: true,
                labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun"],
                options: {
                    data: [{
                        label: "My Bar Chart",
                        data: [35, 29, 50, 10, 26, 25]
                    }, {
                        label: "My Bar Chart 2",
                        data: [65, 59, 80, 31, 56, 55]
                    }, {
                        label: "My Bar Chart 3",
                        data: [95, 89, 100, 49, 86, 85]
                    }]
                }
            });
            // End create chart

            //start alert bar
            var successAlert = new AlertBar().placeAt(context.successAlertBarNode);
            successAlert.p_construct({
                node: "successAlertBarNode",
                callerNodeId: domNodeId,
                data: [{
                    type: "success",
                    label: "Well done! You successfully read this important alert message."
                }]
            });
            topic.publish("alertbar.show.successAlertBarNode." + domNodeId, "success");

            var infoAlert = new AlertBar().placeAt(context.infoAlertBarNode);
            infoAlert.p_construct({
                node: "infoAlertBarNode",
                callerNodeId: domNodeId,
                data: [{
                    type: "info",
                    label: "Heads up! This alert need your attention, but it's not super important though."

                }]
            });
            topic.publish("alertbar.show.infoAlertBarNode." + domNodeId, "info");

            var warningAlert = new AlertBar().placeAt(context.warningAlertBarNode);
            warningAlert.p_construct({
                node: "warningAlertBarNode",
                callerNodeId: domNodeId,
                data: [{
                    type: "warning",
                    label: "Warning! Better check yourself, it's not looking too good."

                }]
            });
            topic.publish("alertbar.show.warningAlertBarNode." + domNodeId, "warning");

            var dangerAlert = new AlertBar().placeAt(context.dangerAlertBarNode);
            dangerAlert.p_construct({
                node: "dangerAlertBarNode",
                callerNodeId: domNodeId,
                data: [{
                    type: "danger",
                    label: "Aw snap! Change a few things up and try submitting again."
                }]
            });
            topic.publish("alertbar.show.dangerAlertBarNode." + domNodeId, "danger");

            var topAlert = new AlertBar().placeAt(context.topAlertBarNode);
            topAlert.p_construct({
                node: "topAlertBarNode",
                callerNodeId: domNodeId,
                data: [{
                    type: "success",
                    label: "Success Alert successfully generated :3"
                }, {
                    type: "danger",
                    label: "Danger Alert successfully generated :3"
                }]
            });
            //end alert bar

            // no result page start
            var uniqueIdNoResult = context.createUUID(context.noresultPageNode);
            var noResult = new NoResult().placeAt(context.noresultPageNode);
            noResult.p_construct({
                noResultTitle: "",
                noResultDesc: ""
            });
            // no result page end

            // success page
            var uniqueIdSuccessPage = context.createUUID(context.successPageNode);
            var successPage = new SuccessPage().placeAt(context.successPageNode);
            successPage.p_construct({
                description: "Application Has Been Created.",
                details: [{ title: "CA No.", data: "00000/0000/00/00000000/00" }, { title: "Application Date", data: "29-Apr-2016" },
                    { title: "Customer Name", data: "Muhamad Kamil Bin Muhamad Noor" }
                ],
                buttons: [{
                    text: "Proceed To Move Next CA",
                    type: "primary",
                    action: function() {
                        alert("Button 1 is click!");
                    }
                }, {
                    text: "Go Back To View Task",
                    type: "secondary",
                    action: function() {
                        alert("Button 2 is click!");
                    }
                }]
            });
            domClass.remove(successPage.domNode);

            var uniqueIdFailPage = context.createUUID(context.failPageNode);
            var failPage = new SuccessPage().placeAt(context.failPageNode);
            failPage.p_construct({
                isFail: true,
                description: "Failed",
                details: [{ title: "CA No.", data: "00000/0000/00/00000000/11" }, { title: "Application Date", data: "29-Apr-2016" },
                    { title: "Customer Name", data: "Muhamad Kalim Bin Muhamad Noor" }
                ]
            });
            domClass.remove(failPage.domNode);

            //start time frame
            var steps = new TimeFrame().placeAt(context.timeFrameContainerNode);
            steps.p_construct({
                data: [{
                    date: 1459823194950,
                    title: "1st frame",
                    label: "Here's my test for 1st frame"
                }, {
                    date: 1459823194950,
                    title: "2nd frame",
                    label: "Now time has come for second frame"
                }]
            });

            var cellArr = [];
            var step2Data = [{
                date: 1459823194950,
                title: "1st frame",
                label: "Here's my test for 1st frame"
            }, {
                date: 1459823194950,
                title: "2nd frame",
                label: "Now time has come for second frame"
            }];

            array.forEach(step2Data, function(single) {
                var timeFrameCell = new TimeFrameCellV2();
                timeFrameCell.p_construct(single);
                cellArr.push(timeFrameCell);
            });

            var steps2 = new TimeFrame().placeAt(context.timeFrameV2ContainerNode);
            steps2.p_construct({
                timeFrameClass: "vertical-timeline vertical-container dark-timeline",
                cellArr: cellArr
            });
            //end time frame

            var rest = new Rest().p_postapi({
                p_api: _setting.api
            }, "assets/data/sampledatatables.json", _setting.datapackage);
            rest.then(function(response) {
                var uniqueIdEditableTable = context.createUUID(context.customTable);
                context.customTableId = uniqueIdEditableTable;
                var customDatatable = new CustomTable().placeAt(context.customTable);
                array.forEach(response.dataSrc, function(data) {
                    data.deleteButton = null;
                })
                customDatatable.p_construct({
                    tableId: uniqueIdEditableTable,
                    data: response.dataSrc,
                    responsive: true,
                    // rowSelection: true,
                    // reaponsiveRowClick: true,
                    customSearchOuter: context.myInputTextFieldNode,
                    // paging: false,
                    // searching: false,
                    columns: [
                        { title: "Log Sequence", key: "logSequence" }, {
                            title: "Assignee",
                            key: "assignee",
                            allwaysShow: true,
                            custom: {
                                postTableDrawColumnCellFunction: function(div, cellData, rowData, row, col, columnKey) {
                                    var uniqueIdSingleAssigneeDropdown = context.createUUID(div);
                                    var singleAssigneeDropdown = new Dropdown();
                                    singleAssigneeDropdown.p_construct({
                                        parentid: domNodeId,
                                        dropdownid: uniqueIdSingleAssigneeDropdown,
                                        data: response.assineeLov,
                                        defaultDisplayValue: cellData
                                    });
                                    domConstruct.place(singleAssigneeDropdown.domNode, div);
                                    singleAssigneeDropdown.dropdownid = uniqueIdSingleAssigneeDropdown;

                                    var dropdownDependencyHandler = topic.subscribe("dropdown.selected.retrieve." + domNodeId,
                                        function(selectedData) {
                                            switch (selectedData.dropdownid) {
                                                case singleAssigneeDropdown.dropdownid:
                                                    topic.publish("customtable.data.update." + uniqueIdEditableTable, {
                                                        row: row,
                                                        columnKey: columnKey,
                                                        value: selectedData.value,
                                                        getReturnValue: function(value) {
                                                            rowValue = value;
                                                            // result value after user edit
                                                            console.log("Result value after user edit", rowValue);
                                                        }
                                                    }, true);
                                                    break;
                                            }
                                        });
                                    topic.publish("topic.handlers.add", dropdownDependencyHandler, domNodeId);
                                }
                            }
                        },
                        { title: "Function", key: "function", isDetail: true }, {
                            title: "Action",
                            key: "action",
                            custom: {
                                editCreateFunction: function(div, cellData, rowData, row, col, columnKey) {
                                    //create input component to be put in cell when on edit mode
                                    var input = domConstruct.create("input", { value: cellData, style: "width: 100%;" }, div);
                                    //add action when input component lost focus
                                    on(input, "blur", function(e) {
                                        var rowValue = null;
                                        // update datatable data on this cell
                                        topic.publish("customtable.data.update." + uniqueIdEditableTable, {
                                            row: row,
                                            columnKey: columnKey,
                                            value: input.value,
                                            getReturnValue: function(value) {
                                                rowValue = value;
                                                // result value after user edit
                                                console.log("Result value after user edit", rowValue);
                                            }
                                        });
                                    });
                                    input.select();
                                }
                            }
                        }, {
                            title: "User Id",
                            key: "userId",
                            custom: {
                                editCreateFunction: function(div, cellData, rowData, row, col, columnKey) {
                                    //create input component to be put in cell when on edit mode
                                    var input = domConstruct.create("input", { value: cellData, style: "width: 100%;" }, div);
                                    //add action when input component lost focus
                                    on(input, "blur", function(e) {
                                        var rowValue = null;
                                        // update datatable data on this cell
                                        topic.publish("customtable.data.update." + uniqueIdEditableTable, {
                                            row: row,
                                            columnKey: columnKey,
                                            value: input.value,
                                            getReturnValue: function(value) {
                                                rowValue = value;
                                                // result value after user edit
                                                console.log("Result value after user edit", rowValue);
                                                // update supervisor2 column for current row
                                                topic.publish("customtable.data.update." + uniqueIdEditableTable, {
                                                    row: row,
                                                    columnKey: "supervisor2",
                                                    value: rowValue[columnKey],
                                                    getReturnValue: function(value) {
                                                        rowValue = value;
                                                        // result value after use case process
                                                        console.log("Result value after use case process", rowValue);
                                                    }
                                                });
                                                // update supervisor column for current row so that the display renderer will update
                                                topic.publish("customtable.data.update." + uniqueIdEditableTable, {
                                                    row: row,
                                                    columnKey: "supervisor",
                                                    value: rowValue["supervisor"]
                                                });
                                            }
                                        });
                                    });
                                    input.select();
                                }
                            }
                        }, {
                            title: "Supervisor 1",
                            key: "supervisor",
                            custom: {
                                editCreateFunction: function(div, cellData, rowData, row, col, columnKey) {
                                    //create input component to be put in cell when on edit mode
                                    var input = domConstruct.create("input", { value: cellData, style: "width: 100%;" }, div);
                                    //add action when input component lost focus
                                    on(input, "blur", function(e) {
                                        var rowValue = null;
                                        // update datatable data on this cell
                                        topic.publish("customtable.data.update." + uniqueIdEditableTable, {
                                            row: row,
                                            columnKey: columnKey,
                                            value: input.value,
                                            getReturnValue: function(value) {
                                                rowValue = value;
                                                // result value after user edit
                                                console.log("Result value after user edit", rowValue);
                                            }
                                        });
                                    });
                                    input.select();
                                },
                                displayCreateFunction: function(div, cellData, rowData, row, col) {
                                    domConstruct.create("div", { innerHTML: cellData + " is supervisor for " + rowData["userId"] + " " + row + " " + col }, div);
                                }
                            }
                        },
                        { title: "Supervisor 2", key: "supervisor2" }, {
                            title: "Delete",
                            key: "deleteButton",
                            sortable: false,
                            allwaysShow: true,
                            custom: {
                                columnOnClickFunction: function(div, cellData, rowData, row, col, columnKey) {
                                    // get the row
                                    var rowElement = query(div).closest("tr");
                                    // trigger remove row
                                    topic.publish("customtable.remove.row." + uniqueIdEditableTable, {
                                        row: row,
                                        columnKey: columnKey,
                                        rowElement: rowElement
                                    });
                                },
                                displayCreateFunction: function(div, cellData, rowData, row, col) {
                                    var editIcon = "<i class='fa fa-trash-o pt15' aria-hidden='true'></i>";
                                    domConstruct.create("div", { innerHTML: editIcon }, div);
                                }
                            }
                        }
                    ],
                    onRowClick: function(trElement, rowData) {
                        console.log("trElement", trElement);
                        console.log("rowData", rowData);
                    }
                });

                //to get all data in table
                topic.publish("customtable.getdata." + uniqueIdEditableTable, function(tableData) {
                    console.log("tableData", tableData);
                });
            }, function(err) {
                console.log("Error", err);
            }, function(evt) {});

            //Start dropdown step component
            var dropdownStep = new DropdownStep();
            dropdownStep.placeAt(context.dropdownStepNode);
            var args = [{
                keyName: "currency",
                dropdownValues: [{
                    keyname: "MYR",
                    value: "MYR"
                }, {
                    keyname: "SGD",
                    value: "SGD"
                }, {
                    keyname: "USD",
                    value: "USD"
                }]
            }, {
                keyName: "currency2",
                dropdownValues: [{
                    keyname: "MYR",
                    value: "MYR"
                }, {
                    keyname: "SGD",
                    value: "SGD"
                }, {
                    keyname: "USD",
                    value: "USD"
                }],
                onChange: function(value) {
                    dropdownStep.enableDropdown("currency2", false);
                    dropdownStep.updateValues("currency3", [{
                        keyname: "A Very Very Very Very Very Very Very Very Long Sentence",
                        value: "MYR1"
                    }, {
                        keyname: "MYR2",
                        value: "MYR2"
                    }, {
                        keyname: "MYR3",
                        value: "MYR3"
                    }]);
                }
            }, {
                keyName: "currency3",
                dropdownValues: [{
                    keyname: "MYR",
                    value: "MYR"
                }, {
                    keyname: "SGD",
                    value: "SGD"
                }, {
                    keyname: "USD",
                    value: "USD"
                }],
                "default": true,
                onChange: function(value) {
                    alert(value);
                }
            }];
            dropdownStep.p_construct({ domNodeId: domNodeId, args: args });
            // End dropdown step component

            //Start Upload File component
            var uploadFile = new UploadFile();
            uploadFile.placeAt(context.fileUploadNode);
            uploadFile.p_construct({
                fileAdd: function(fileDetail, functions) {
                    console.log("File added for upload", fileDetail);
                    setTimeout(function() {
                        functions.updateProgress(25);
                        setTimeout(function() {
                            functions.updateProgress(50);
                            setTimeout(function() {
                                functions.updateProgress(75);
                                setTimeout(function() {
                                    functions.setAsError("Test Error");
                                    setTimeout(function() {
                                        functions.updateProgress(100);
                                        setTimeout(function() {
                                            functions.setAsCompleted();
                                        }, 250);
                                    }, 250);
                                }, 250);
                            }, 250);
                        }, 250);
                    }, 250);
                },
                fileTooBigMessage: "File too big Bro!!!",
                onSubmit: function(fileDetails) {
                    console.log("submiting fileDetails", fileDetails);
                }
            });
            //End Upload File Component
        },
        i18n: function(_setting) {
            var context = this;
        },
        amountValue: function(_setting) {
            console.log(_setting);
        },
        //For quick find
        createQuickSearchList: function(_setting) {
            var context = this;
             var domNode = this.domNode;

            var quickNavigationMenuData = [{
                label: "Input Texts",
                icon: "fa fa-list",
                domNodeItem: context.inputTextContainerNode
            }, {
                label: "Buttons & Modals",
                icon: "fa fa-list",
                domNodeItem: context.buttonContainerNode
            }, {
                label: "Tabs",
                icon: "fa fa-list",
                domNodeItem: context.tabContainerNode
            }, {
                label: "Dropdowns",
                icon: "fa fa-list",
                domNodeItem: context.dropdownContainerNode
            }, {
                label: "Datepickers",
                icon: "fa fa-list",
                domNodeItem: context.datepickerContainerNode
            }, {
                label: "Amount Textfield",
                icon: "fa fa-list",
                domNodeItem: context.amountTextFieldContainerNode
            }, {
                label: "Switchers",
                icon: "fa fa-list",
                domNodeItem: context.switcherContainerNode
            }, {
                label: "Sliders",
                icon: "fa fa-list",
                domNodeItem: context.slidersContainerNode
            }, {
                label: "Charts",
                icon: "fa fa-list",
                domNodeItem: context.chartsContainerNode
            }, {
                label: "Alert Bars",
                icon: "fa fa-list",
                domNodeItem: context.alertBarsContainerNode
            }, {
                label: "File Upload",
                icon: "fa fa-list",
                domNodeItem: context.fileUploadContainerNode
            }, {
                label: "No Result",
                icon: "fa fa-list",
                domNodeItem: context.noResultContainerNode
            }, {
                label: "Success And Fail Page",
                icon: "fa fa-list",
                domNodeItem: context.successFailContainerNode
            }, {
                label: "Time Frame",
                icon: "fa fa-list",
                domNodeItem: context.timeFrameContainerNode
            }, {
                label: "Tables",
                icon: "fa fa-list",
                domNodeItem: context.tablesContainerNode
            }, {
                label: "Dropdown Steps",
                icon: "fa fa-list",
                domNodeItem: context.dropdownStepNode
            }, {
                label: "Nested Tree List",
                icon: "fa fa-list",
                domNodeItem: context.treeListNode
            }];

            var uniqueIdQuickNavigationMenu = context.createUUID(context.quickNavigationMenuContainerNode);
            var quickNavigationMenu = new QuickNavigationMenu().placeAt(context.quickNavigationMenuContainerNode);
            quickNavigationMenu.construct({
                parentid: domAttr.get(domNode, "id"),
                quickNavigationMenuId: uniqueIdQuickNavigationMenu,
                quickNavigationMenuData: quickNavigationMenuData,
                quickNavigationMenuBtn: "fa fa-plus",
                quickNavigationMenuRemoveBtn: "fa fa-remove quicknavigationmenu-margintopbtn"
            });

            //Reconstruct the item list
            var quickNavigationMenuData2 = [{
                label: "Input Texts",
                icon: "fa fa-list",
                domNodeItem: context.inputTextContainerNode
            }, {
                label: "Buttons & Modals",
                icon: "fa fa-list",
                domNodeItem: context.buttonContainerNode
            }, {
                label: "Tabs",
                icon: "fa fa-list",
                domNodeItem: context.tabContainerNode
            }];

            topic.publish("reset.items.quicknavmenu." + uniqueIdQuickNavigationMenu, quickNavigationMenuData2);

        },
        changeTheme: function(themeName) {
            $("body").removeClass("skin-" + $.jStorage.get("app.data.theme"));
            switch (themeName) {
                case "marine":
                    $("body").addClass("skin-marine");
                    break;
                case "cocoa":
                    $("body").addClass("skin-cocoa");
                    break;
                case "aqua":
                    $("body").addClass("skin-aqua");
                    break;
                case "violet":
                    $("body").addClass("skin-violet");
                    break;
                default:
                    break;
            }
            $.jStorage.set("app.data.theme", themeName);
        }
    });
});
