define([
    "dojo/_base/declare",
    "dojo/dom-attr",
    "dojo/html",
    "dojo/on",
    "dojo/text!./html/FormExample2.html",
    "dojo/topic",
    "app/components/ui/blocks/BaseBlock",
    "app/components/utils/General"
], function (
    v_declare,
    v_domAttr,
    v_html,
    v_on,
    v_template,
    v_topic,
    v_BaseBlock,
    v_GeneralUtils
) {
    return v_declare([v_BaseBlock], {
        templateString: v_template,
        p_construct: function (v_setting) {
            this.inherited(arguments);

            var v_context = this;

            v_context.p_setupContent(v_setting);
            v_context.p_initInteraction(v_setting);
        },
        p_setupContent: function (v_setting) {
            var v_context = this;

            var v_transactionSetting = v_setting.p_transactionSetting;

            v_html.set(v_context.nameNode, v_transactionSetting.p_title + " " + v_transactionSetting.p_name);
            v_html.set(v_context.ageNode, v_transactionSetting.p_age);
            v_html.set(v_context.genderNode, v_transactionSetting.p_gender);
            v_html.set(v_context.favouriteFruitsNode, v_transactionSetting.p_favouriteFruits.toString());
            v_html.set(v_context.expectedSalaryNode, new v_GeneralUtils().formatCurrency(v_transactionSetting.p_salaryRange.from) + " to " + new GeneralUtils().formatCurrency(v_transactionSetting.salaryRange.to));
            v_html.set(v_context.emailNode, v_transactionSetting.p_email);
            v_html.set(v_context.alertsNode, v_transactionSetting.p_alerts);
        },
        p_initInteraction: function (v_setting) {
            var v_context = this;

            v_on(v_context.backNode, "click", function () {
                v_topic.publish("navigation.back");
            });
        }
    });
});
