define([
    "dojo/_base/declare",
    "dojo/dom-attr",
    "dojo/text!./html/FormExample.html",
    "dojo/on",
    "dojo/topic",
    "../../BaseBlock",
    "../../../element/dropdown/Dropdown",
    "../../../element/datepicker/Datepicker",
    "../../../element/slider/Slider"
], function (
    v_declare,
    v_domAttr,
    v_template,
    v_on,
    v_topic,
    v_BaseBlock,
    v_Dropdown,
    v_Datepicker,
    v_Slider
) {
        return v_declare([v_BaseBlock], {
            templateString: v_template,
            p_sliderValue: null,
            p_sliderRange: null,
            p_titleDropdownValue: null,
            p_alertStatus: null,
            p_construct: function (v_setting) {
                this.inherited(arguments);
                var v_context = this;
                v_context.p_startListening(v_setting);
                v_context.p_setupContent(v_setting);
                v_context.p_initInteraction(v_setting);
            },
            startup: function () {
                this.inherited(arguments);

                var v_context = this;
                v_context.p_sliderValue = {};
                v_context.p_sliderRange = null;
                v_context.p_titleDropdownValue = null;
                v_context.p_alertStatus = null;
            },
            p_startListening: function (v_setting) {
                var v_context = this;
                var v_domNode = v_context.domNode;
                var v_domNodeId = v_domAttr.get(v_domNode, "id");

                var v_dropdownHandler = v_topic.subscribe("dropdown.selected.retrieve." + v_domNodeId, function (v_selectedData) {
                    switch (v_selectedData.v_Dropdownid) {
                        case v_domAttr.get(v_context.titleDropdownNode, "id"):
                            v_context.p_titleDropdownValue = v_selectedData.value;
                            break;
                    }
                });
                v_topic.publish("topic.handlers.add", v_dropdownHandler, v_domNodeId);
            },
            p_setupContent: function (v_setting) {
                var v_context = this;
                var v_domNode = v_context.domNode;
                var v_domNodeId = v_domAttr.get(v_domNode, "id");

                // Start create v_Dropdown
                var v_titleDropdownValues = [{
                    keyname: "Mr.",
                    value: "Mr."
                }, {
                    keyname: "Ms.",
                    value: "Ms."
                }, {
                    keyname: "Mrs.",
                    value: "Mrs."
                }];

                v_context.autoInputDecimal(v_context.amountNode, "amountDecimal");

                var v_uniqueIdTitleDropdown = v_context.createUUID(v_context.titleDropdownNode);
                var v_titleDropdown = new v_Dropdown().placeAt(v_context.titleDropdownNode);
                v_titleDropdown.p_construct({
                    parentid: v_domNodeId,
                    dropdownid: v_uniqueIdTitleDropdown,
                    data: v_titleDropdownValues,
                    required: true
                });
                // End create v_Dropdown

                // Start create v_Slider
                v_context.p_sliderRange = new v_Slider();
                v_context.p_sliderRange.placeAt(v_context.salaryRangeContainerNode);
                v_context.p_sliderRange.p_construct({
                    type: "double",
                    grid: false,
                    min: 0,
                    max: 10000,
                    from: 1000,
                    to: 5000,
                    step: 50,
                    onStart: function (v_data) {
                        v_context.p_sliderValue = {
                            from: v_data.from,
                            to: v_data.to
                        };
                    },
                    onFinish: function (v_data) {
                        v_context.p_sliderValue = {
                            from: v_data.from,
                            to: v_data.to
                        };
                    }
                });
                // End create v_Slider

                // Start create v_Datepicker
                var v_uniqueIdDatepickerRange = v_context.createUUID(v_context.datepickerRangeNode);
                var v_Datepicker2 = new v_Datepicker("range").placeAt(v_context.datepickerRangeNode);
                v_Datepicker2.p_construct({
                    type: "range",
                    required: true,
                    parentid: v_domNodeId,
                    datepickerid: v_uniqueIdDatepickerRange,
                    options: {}
                });
                // End create v_Datepicker
            },
            p_initInteraction: function (v_setting) {
                var v_context = this;
                var v_domNode = this.domNode;

                v_on(v_context.submitNode, "click", function () {
                    // v_context.p_checkingValidation(v_domNode, function (v_isValid) {
                    //     if (v_isValid) {
                    //         // Get all input values
                    //         var v_transactionSetting = {
                    //             p_title: v_context.p_titleDropdownValue,
                    //             p_name: v_domAttr.get(v_context.nameNode, "value"),
                    //             p_age: v_domAttr.get(v_context.ageNode, "value"),
                    //             p_gender: v_context.getRadioValue("gender"),
                    //             p_favouriteFruits: v_context.getCheckboxValue("fruits"),
                    //             p_salaryRange: v_context.p_sliderValue,
                    //             p_email: v_domAttr.get(v_context.emailNode, "value"),
                    //             p_alerts: v_context.p_alertStatus
                    //         };

                            v_topic.publish("navigation.block.open", "FORM_EXAMPLE_2", {
                                // p_transactionSetting: v_transactionSetting
                            // });
                        // }
                    });
                });

                v_on(v_context.resetNode, "click", function () {
                    v_context.p_removeAllValidation(v_domNode, function () {
                        // Reset v_Slider
                        v_context.p_sliderRange.reset();
                    });
                });
            }
        });
    });
