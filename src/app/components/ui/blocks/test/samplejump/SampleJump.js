define([
    "dojo/_base/declare",
    "dojo/on",
    "dojo/text!./html/SampleJump.html",
    "dojo/topic",
    "app/components/ui/blocks/BaseBlock"
], function (
    v_declare,
    v_on,
    v_template,
    v_topic,
    v_BaseBlock
) {
    return v_declare([v_BaseBlock], {
        templateString: v_template,
        p_opennedFunction: "",
        p_construct: function (v_setting) {
            this.inherited(arguments);
            var v_context = this;

            v_context.p_initInteraction(v_setting);
            v_context.p_setupContent(v_setting);
        },
        startup: function() {
            this.inherited(arguments);
            var v_context = this;
            v_context.p_opennedFunction = "";
        },
        p_initInteraction: function (v_setting) {
            var v_context = this;

            v_on(v_context.page1Node, "click", function () {
                v_topic.publish("navigation.block.jump", v_context.p_opennedFunction, "JUMP_PAGE_1", {}, v_context.containerNode);
                v_context.p_opennedFunction = "JUMP_PAGE_1";
            });
            v_on(v_context.page2Node, "click", function () {
                v_topic.publish("navigation.block.jump", v_context.p_opennedFunction, "JUMP_PAGE_2", {}, v_context.containerNode);
                v_context.p_opennedFunction = "JUMP_PAGE_2";
            });
            v_on(v_context.page3Node, "click", function () {
                v_topic.publish("navigation.block.jump", v_context.p_opennedFunction, "JUMP_PAGE_3", {}, v_context.containerNode);
                v_context.p_opennedFunction = "JUMP_PAGE_3";
            });
        },
        p_setupContent: function (v_setting) {
            var v_context = this;
            v_topic.publish("navigation.block.jump", v_context.p_opennedFunction, "JUMP_PAGE_1", {}, v_context.containerNode);
            v_context.p_opennedFunction = "JUMP_PAGE_1";
        }
    });
});
