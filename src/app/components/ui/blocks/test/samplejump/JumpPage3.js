define([
    "dojo/_base/declare",
    "dojo/text!./html/JumpPage3.html",
    "app/components/ui/blocks/BaseBlock"
], function(
    v_declare,
    v_template,
    v_BaseBlock
) {
    return v_declare([v_BaseBlock], {
        templateString: v_template
    });
});
