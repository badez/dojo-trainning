define([
    "dojo/_base/declare",
    "dojo/dom-attr",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/dom",
    "dojo/html",
    "dojo/on",
    "dojo/text!./html/CloudStorage.html",
    "dojo/topic",
    "dojo/dom-form",
    "app/components/ui/blocks/BaseBlock",
    "dijit/_TemplatedMixin"
], function(
    v_declare,
    v_domAttr,
    v_domClass,
    v_domConstruct,
    v_dom,
    v_html,
    v_on,
    v_template,
    v_topic,
    v_domForm,
    v_BaseBlock,
    v_TemplatedMixin
) {
    return v_declare("app.components.ui.blocks.test.cloudstorage.CloudStorage", [v_BaseBlock, v_TemplatedMixin], {
        templateString: v_template,
        p_construct: function(v_setting) {
            this.inherited(arguments);
            var v_context = this;

            v_context.p_initInteraction(v_setting);

        },
        startup: function() {
            this.inherited(arguments);
        },

        p_initInteraction: function(v_setting) {
            var v_context = this;

            //---------start On event 1-------------
            v_on(v_context.submitBtnNode, "click", function() {

                console.log("i was clicked!");
                alert('You pressed the button');



                function start() {
                    // 2. Initialize the JavaScript client library.

                    gapi.client.init({
                        'apiKey': 'AIzaSyD4Pk9ACRgKw1uQNfb3qf1fonzlpwyxPII',
                        // clientId and scope are optional if auth is not required.
                        'clientId': '624668296876-797b0f2am655d7610buevf521b5j46ce.apps.googleusercontent.com',
                        'scope': 'profile',
                    }).then(function() {
                        // 3. Initialize and make the API request.
                        return gapi.client.request({
                            'path': 'https://www.googleapis.com/discovery/v1/apis/drive/v3/rest',

                        })
                    }).then(function(response) {
                        console.log(response.result);
                        alert('respond');

                        APIcall();
                        alert('uploaded!');


                    }, function(reason) {
                        console.log('Error ');
                    });


                    function APIcall(callback) {

                        var reader = new FileReader();
                        var fileData = new File([""], "assets/img/1.png");
                        reader.readAsBinaryString(fileData);
                        reader.onload = function(e) {
                            var contentType = fileData.type || 'application/octet-stream';
                            var metadata = {
                                'title': '1.png',
                                'mimeType': contentType
                            };


                            const boundary = '-------314159265358979323846';
                            const delimiter = "\r\n--" + boundary + "\r\n";
                            const close_delim = "\r\n--" + boundary + "--";

                            //var delimiter, close_delim = ";";

                            var base64Data = btoa(reader.result);
                            console.log(base64Data);
                            var multipartRequestBody =
                                delimiter +
                                'Content-Type: application/json\r\n\r\n' +
                                JSON.stringify(metadata) +
                                delimiter +
                                'Content-Type: ' + contentType + '\r\n' +
                                'Content-Transfer-Encoding: base64\r\n' +
                                '\r\n' +
                                base64Data +
                                close_delim;

                            require(['https://apis.google.com/js/api.js?'])

                            var request = gapi.client.request({

                                'path': '/upload/drive/v3/files',
                                'method': 'POST',
                                'params': { 'uploadType': 'multipart' },
                                'headers': { 'Content-Type': 'multipart/related; boundary="' + boundary + '"' },
                                'body': multipartRequestBody,

                            });
                            if (!callback) {
                                callback = function(file) {
                                    console.log(file)
                                };
                            }
                            request.execute(callback);
                        }

                    }
                };

                //1. Load the JavaScript client library.
                require(['https://apis.google.com/js/api.js?']);
                gapi.load('client', start);



            });
            //------------end On event 1------------

            //-----------start On event 2-----------
            v_on(v_context.submitBtnNode2, "click", function() {

                console.log("i was clicked!");

                function initClient() {

                    gapi.client.init({
                        apiKey: 'AIzaSyD4Pk9ACRgKw1uQNfb3qf1fonzlpwyxPII',
                        discoveryDocs: ["https://www.googleapis.com/discovery/v1/apis/drive/v3/rest"],
                        clientId: '624668296876-797b0f2am655d7610buevf521b5j46ce.apps.googleusercontent.com',
                        scope: 'https://www.googleapis.com/auth/drive'
                    }).then(function() {
                        // Listen for sign-in state changes.
                        gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

                        // Handle the initial sign-in state.
                        updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());

                        // Sign In
                        gapi.auth2.getAuthInstance().signIn();
                    });
                }

                function updateSigninStatus(isSignedIn) {

                    if (isSignedIn) {
                        makeApiCall();
                    }
                }

                // function handleSignOutClick(event) {
                //   gapi.auth2.getAuthInstance().signOut();
                // }

                function makeApiCall() {

                    console.log("api call!");

                    var blob, reader;

                    var meta = {
                        "title": "1.png",
                        "mimeType": "image/png",
                        "description": "Gambar"
                    };


                    var user = gapi.auth2.getAuthInstance().currentUser.get();
                    var oauthToken = user.getAuthResponse(true).access_token;

                    console.log(oauthToken);

                    var xhr = new XMLHttpRequest();
                    xhr.open('GET', 'assets/img/1.png', true);
                    xhr.responseType = 'arraybuffer';

                    xhr.onload = function(e) {
                        if (this.status == 200) {

                            blob = new Blob([this.response], { type: 'image/png' });
                            console.log(blob);

                            //v_dom.byId("img").src = window.URL.createObjectURL(blob);

                            reader = new window.FileReader();
                            reader.readAsDataURL(blob);

                            reader.onloadend = function(e) {
                                console.log('Reader OK');

                                var bound = 287032396531387;

                                var parts = [];
                                parts.push('--' + bound);
                                parts.push('Content-Type: application/json');
                                parts.push('');
                                parts.push(JSON.stringify(meta));
                                parts.push('--' + bound);
                                parts.push('Content-Type: image/png');
                                parts.push('Content-Transfer-Encoding: base64');
                                parts.push('');
                                parts.push(reader.result.replace(/^data:image\/(png|jpg);base64,/, ""));
                                //parts.push(reader.result);
                                parts.push('--' + bound + '--');
                                parts.push('');

                                var xhr = new XMLHttpRequest();
                                xhr.open("POST", "https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart", true);
                                xhr.setRequestHeader("Authorization", "Bearer " + oauthToken);
                                xhr.setRequestHeader("Content-Type", "multipart/related; boundary=" + bound);


                                xhr.onload = function(e) {
                                    console.log("DRIVE OK", this, e);
                                };

                                xhr.send(parts.join("\r\n"));
                            }
                        }
                    };

                    xhr.send();
                }

                gapi.load('client:auth2', initClient);
                require(['https://apis.google.com/js/api.js?'])

            });
            //----------end On event 2--------------

            //------------------CORS---------------- 
            var src = "//localhost:50/dojoBase/src/app/components/ui/blocks/test/cloudstorage/html/";
            var filename = "1.png";
            var sitename = "My Company Name";

            // v_html.set(v_context.fileUploadNode3, "<div data-src='" + src + "' data-filename='" + filename + "' data-sitename='" + sitename + "'>jkkjikj</div>");
            v_domAttr.set(v_context.fileUploadNode3, { class: "g-savetodrive", "data-src": src, "data-filename": filename, "data-sitename": sitename });
            require(['https://apis.google.com/js/platform.js?']);
            //------------------CORS---------------- 
        }


    });
});