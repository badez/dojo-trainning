define([
    "dojo/_base/declare",
    "dojo/dom-class",
    "dojo/html",
    "dojo/on",
    "dojo/text!./html/ExtraPage1.html",
    "dojo/topic",
    "app/components/ui/blocks/BaseBlock"
], function(
    v_declare,
    v_domClass,
    v_html,
    v_on,
    v_template,
    v_topic,
    v_BaseBlock
) {
    return v_declare("app.components.ui.blocks.test.extrapage.ExtraPage1", [v_BaseBlock], {
        templateString: v_template,
        p_construct: function(v_setting) {
            this.inherited(arguments);
            var v_context = this;

            v_context.p_i18n(v_setting);
            v_context.p_initInteraction(v_setting);
            v_context.p_setupContent(v_setting);
        },
        p_initInteraction: function(v_setting) {
            var v_context = this;

            v_on(v_context.previousNode, "click", function() {
                v_topic.publish("navigation.block.previous");
            });
            v_on(v_context.nextNode, "click", function() {
                v_topic.publish("navigation.block.next", "EXTRA_PAGE_1", {
                    p_count: v_setting.p_count ? v_setting.p_count + 1 : 2
                });
            });
        },
        p_setupContent: function(v_setting) {
            var v_context = this;
            var v_domNode = this.domNode;

            if (v_setting.p_count) {
                v_domClass.remove(v_context.previousNode, "hide");
            }
        },
        p_i18n: function(v_setting) {
            var v_context = this;
            v_html.set(v_context.titleNumber1Node, v_setting.p_count ? v_setting.p_count : 1);
            v_html.set(v_context.titleNumber2Node, v_setting.p_count ? v_setting.p_count : 1);
        }
    });
});
