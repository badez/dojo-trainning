define([
    "dojo/_base/declare",
    "dojo/on",
    "dojo/text!./html/SamplePage1.html",
    "dojo/topic",
    "app/components/ui/blocks/BaseBlock"
], function(
    v_declare,
    v_on,
    v_template,
    v_topic,
    v_BaseBlock
) {
    return v_declare("app.components.ui.blocks.test.samplepage.SamplePage1", [v_BaseBlock], {
        templateString: v_template,
        p_construct: function(v_setting) {
            this.inherited(arguments);
            var v_context = this;

            v_context.p_initInteraction(v_setting);
        },
        p_initInteraction: function(v_setting) {
            var v_context = this;

            v_on(v_context.openModalNode, "click", function() {
                v_topic.publish("navigation.modal.open", "SAMPLE_MODAL_1", { p_test: "test" });
            });
            v_on(v_context.nextNode, "click", function() {
                v_topic.publish("navigation.block.next", "SAMPLE_PAGE_2", {});
            });
        }
    });
});
