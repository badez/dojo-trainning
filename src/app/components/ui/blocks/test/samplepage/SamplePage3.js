define([
    "dojo/_base/declare",
    "dojo/dom-attr",
    "dojo/on",
    "dojo/text!./html/SamplePage3.html",
    "dijit/_TemplatedMixin",
    "app/components/ui/blocks/BaseBlock",
    "app/components/ui/element/breadcrumbs/Breadcrumbs",
    "app/components/utils/Rest",
    "app/components/utils/General"
], function (
    declare,
    domAttr,
    on,
    template,
    TemplatedMixin,
    BaseBlock,
    Breadcrumbs,
    Rest,
    GeneralUtils
) {
    return declare("app.components.ui.blocks.test.samplepage.SamplePage3", [BaseBlock, TemplatedMixin], {
        templateString: template,
        p_construct: function (setting) {
            this.inherited(arguments);

            var domNode = this.domNode;
            var context = this;

            context.setupContent(setting);
            context.initInteraction(setting);
        },
        startup: function () {
            this.inherited(arguments);
        },
        destroy: function () {
            this.inherited(arguments);
        },
        initInteraction: function (_setting) {
            var context = this;
            var domNode = this.domNode;

            on(context.backNode, "click", function () {
                topic.publish("navigation.back");
            });
        },
        setupContent: function (_setting) {
            var context = this;
            var domNode = this.domNode;
            var domNodeId = domAttr.get(domNode, "id");

            // Start create breadcrumbs
            var breadcrumbs = new Breadcrumbs();
            breadcrumbs.placeAt(context.breadcrumbsContainerNode);
            breadcrumbs.p_construct(domNodeId);
            // End create breadcrumbs
        },
        i18n: function (_setting) {
            var context = this;
        }
    });
});
