define([
    "dojo/_base/declare",
    "dojo/dom-attr",
    "dojo/on",
    "dojo/dom",
    "dojo/topic",
    "app/components/ui/blocks/BaseBlock",
    "dojo/text!./html/ExercisePage2.html"
], function(
    v_declare,
    v_domAttr,
    v_on,
    v_dom,
    v_topic,
    v_BaseBlock,
    v_template
) {
    return v_declare("app.components.ui.blocks.test.exercisepage.ExercisePage2",[v_BaseBlock], {
        templateString: v_template,
        p_alertStatus: null,
        p_construct: function (v_setting) {
            this.inherited(arguments);
            var v_context = this;
            v_context.p_startListening(v_setting);
            v_context.p_setupContent(v_setting);
            v_context.p_initInteraction(v_setting);
        },
        startup: function () {
            this.inherited(arguments);

            var v_context = this;
            v_context.p_alertStatus = null;
        },
        p_startListening: function () {
            var v_context = this;
            var v_domNode = v_context.domNode;
            var v_domNodeId = v_domAttr.get(v_domNode, 'id');

        },
        p_setupContent: function () {
            var v_context = this;
            var v_domNode = v_context.domNode;
            var v_domNodeId = v_domAttr.get(v_domNode, 'id');

        },
        p_initInteraction: function () {
            var v_context = this;
            var v_domNode = v_context.domNode;
            var v_domNodeId = v_domAttr.get(v_domNode, 'id');

        }    
            
        
    });
    
});