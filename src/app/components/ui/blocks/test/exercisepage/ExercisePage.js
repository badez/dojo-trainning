define([
    "dojo/_base/declare",
    "dojo/dom-attr",
    "dojo/on",
    "dojo/dom",
    "dojo/topic",
    "dojo/query",
    "app/components/ui/blocks/BaseBlock",
    "../../../element/dropdown/Dropdown",
    "../../../element/datepicker/Datepicker",
    "dojo/text!./html/ExercisePage.html",
    "app/components/utils/Rest",
    "app/components/ui/element/togglebuttons/ToggleButtons",
    "app/components/ui/element/customtable/CustomTable",
    "app/components/ui/element/nestedtree/NestedTreeCell",
    "app/components/ui/element/dropdownstep/DropdownStep",
    "app/components/ui/element/quicknavigationmenu/QuickNavigationMenu",
    "app/components/utils/Rest",
    "app/components/utils/General",
    "dojo/i18n!app/locale/nls/components",
    "dojo/i18n!app/locale/nls/common"
], function (
    v_declare,
    v_domAttr,
    v_on,
    v_dom,
    v_topic,
    v_query,
    v_BaseBlock,
    v_Dropdown,
    v_Datepicker,
    v_template,
    Rest,
    ToggleButtons,
    CustomTable,
    NestedTreeCell,
    DropdownStep,
    QuickNavigationMenu,
    Rest,
    v_GeneralUtils,
    i18n,
    i18n_common
) {
        return v_declare("app.components.ui.blocks.test.exercisepage.ExercisePage", [v_BaseBlock], {
            templateString: v_template,
            carriedData: null,
            p_alertStatus: null,
            p_construct: function (v_setting) {
                this.inherited(arguments);
                var v_context = this;
                v_context.p_startListening(v_setting);
                v_context.p_setupContent(v_setting);
                v_context.p_initInteraction(v_setting);

            },
            startup: function () {
                this.inherited(arguments);

                var v_context = this;
                v_context.p_alertStatus = null;
                var rest = new Rest().p_get("assets/data/demo.json");
                v_context.toggleButtons;
            },
            p_startListening: function (v_setting) {
                var v_context = this;
                var v_domNode = v_context.domNode;
                var v_domNodeId = v_domAttr.get(v_domNode, 'id');

                //
                var v_dropdownHandler = v_topic.subscribe("dropdown.selected.retrieve." + v_domNodeId, function (v_selectedData) {
                    console.log(v_selectedData);
                    switch (v_selectedData.dropdownid) {
                        case v_domAttr.get(v_context.salutationDropdownNode, "id"):

                            v_context.p_salutationDropdownValue = v_selectedData.value;
                            break;
                    }
                });
                v_topic.publish("topic.handlers.add", v_dropdownHandler, v_domNodeId);

                var today = new Date();
                var matureDate = new Date(568036800000);
                var longestDate = new Date(3155760000000);
                var defaultDates = today - matureDate;
                var defaultDates2 = today - longestDate;
                var v_uniqueIdDatepickerDOB = new v_GeneralUtils().createUUID();
                v_domAttr.set(v_context.datepickerDOBNode, "id", v_uniqueIdDatepickerDOB);
                var v_datepickerDOB = new v_Datepicker().placeAt(v_context.datepickerDOBNode);
                v_datepickerDOB.p_construct({
                    parentid: v_domNodeId,
                    datepickerid: v_uniqueIdDatepickerDOB,
                    dateFormat: "mm/dd/yyyy",
                    // setStartDate: new Date(),
                    // setEndDate: new Date(),
                    options: {
                        endDate: new Date(defaultDates),
                        startDate: new Date(defaultDates2)
                    }
                });
                v_topic.publish("datepicker.enable." + v_context.v_uniqueIdDatepickerDOB, true);
                //end create datepicker

                //

                // var v_datepickerRetrievalHandler = v_topic.subscribe("datepicker.selected.retrieve." + v_domNodeId, function (v_selectedData) {
                //     switch (v_selectedData.datepickerid) {
                //         case v_domAttr.get(v_context.datepickerDOBNode, "id"):
                //             // v_context.upadatePageData("birthdate", v_selectedData.timestamp);
                //             v_context.storedData = v_selectedData.timestamp;

                //         if (v_selectedData.timestamp) {
                //             // var today = moment();
                //             var dateEstablished = v_selectedData.timestamp;

                //             var diffInYears = today.diff(dateEstablished, 'year');

                //             v_domAttr.set(v_context.yearsOfCommencementNode, "value", diffInYears);
                //             v_context.upadatePageData("yearsOfCommencement", diffInYears);
                //         }
                //         default:
                //             break;
                //     }
                // });
                // v_topic.publish("topic.handlers.add", v_datepickerRetrievalHandler, v_domNodeId);

                //old datepicker start
                var v_DOB = new Date("01/27/1991");
                v_DOB = Date.parse(v_DOB);
                console.log(v_DOB);
                var today = new Date();
                var v_Age = Date.parse(today);
                var v_Agecalc = v_Age - v_DOB;
                v_Age = v_Agecalc / 31557600000;
                v_Age = Math.trunc(v_Age);
                console.log(v_Age);
                v_context.carriedData = v_Age;
                console.log(v_context.carriedData);

                var v_datepickerRetrievalHandler = v_topic.subscribe("datepicker.selected.retrieve." + v_domNodeId, function (v_selectedData) {
                    // v_context.datepickerDOB.from = v_selectedData.fromDate;
                    // v_context.datepickerDOB.to = v_selectedData.toDate;
                    console.log(v_selectedData);
                    v_domAttr.get(v_context.datepickerDOBNode, "id");
                    v_context.datepickerDOB = v_selectedData.timestamp;
                    v_context.datepickerDOB = new Date(v_context.datepickerDOB);
                    // $fn.datepickerDOB.defaults.format = "mm/dd/yyyy";


                });
                v_topic.publish("topic.handlers.add", v_datepickerRetrievalHandler, v_domNodeId);
                // old datepicker end



            },
            p_setupContent: function (v_setting) {
                var v_context = this;
                var v_domNode = v_context.domNode;
                var v_domNodeId = v_domAttr.get(v_domNode, "id");

                var v_prototypeUrl = "assets/data/demo.json";
                var rest = new Rest().p_get(v_prototypeUrl);
                rest.then(function (response) {
                    var v_uniqueIdSalutationDropdown = v_context.createUUID(v_context.salutationDropdownNode);
                    var v_salutationDropdown = new v_Dropdown().placeAt(v_context.salutationDropdownNode);
                    v_salutationDropdown.p_construct({
                        parentid: v_domNodeId,
                        dropdownid: v_uniqueIdSalutationDropdown,
                        data: response.salutation,
                        required: true
                    });
                    v_context.p_initValidation(v_salutationDropdown.domNode);
                });

                // v_context.createMask(v_context.NRICNode, {
                //     type: "NRIC",

                // });
                v_context.createMask(v_context.NRICNode, {
                    type: "NRIC",
                });

                //datepicker code old start
                // var toDate = new Date();
                // toDate.setDate(toDate.getDate() - 7);

                // var v_uniqueIdDatepickerDOB = v_context.createUUID(v_context.datepickerDOBNode);
                // var v_datepickerDOB = new v_Datepicker().placeAt(v_context.datepickerDOBNode);
                // v_datepickerDOB.p_construct({
                //     parentid: v_domAttr.get(v_domNode, "id"),
                //     datepickerid: v_uniqueIdDatepickerDOB,
                //     // disabled: false,
                //     required: true,
                //     // startDate: toDate,
                //     // endDate: new Date(),

                //     options: {
                //         // defaultDates: toDate,
                //         maxdate: new Date()
                //     }
                // });
                // v_context.v_datepickerDOBid = v_uniqueIdDatepickerDOB;
                // v_topic.publish("datepicker.enable." + v_uniqueIdDatepickerDOB, true);
                // console.log(v_context.v_datepickerDOB);
                // datepicker old end


                v_context.createMask(v_context.mobileNoNode, {
                    type: "mobileNo"
                });

                //Start Toggle Buttons
                var uniqueIdToggleButtons = v_context.createUUID(v_context.toggleButtonsNode);
                var toggleButtons = new ToggleButtons().placeAt(v_context.toggleButtonsNode);
                toggleButtons.p_construct({
                    selectedPerTime: 1,
                    args: [{
                        key: "genderM",
                        text: "Male",
                    }, {
                        key: "genderF",
                        text: "Female",
                    }]
                });
                toggleButtons.select("genderM");
                v_context.toggleButtons = toggleButtons;

                //ENd Toggle Buttons
            },
            p_initInteraction: function (v_setting) {
                var v_context = this;
                var v_domNode = this.domNode;

                v_on(v_context.nextNode, "click", function () {

                    var x = v_domAttr.get(v_context.NRICNode, "value");
                    var y = x.slice(13);
                    var v_genderIdentifier;
                    y = parseInt(y);

                    if ((y % 2) == 1) {
                        v_genderIdentifier = "Male";
                    }
                    else if ((y % 2) == 0) {
                        v_genderIdentifier = "female";
                    }

                    // z = x.slice(0,6);
                    var v_yearLast = new String(x.slice(0, 2));
                    var v_month = new String(x.slice(2, 4));
                    var v_day = new String(x.slice(4, 6));
                    var v_seperate = "/";
                    var v_yearfirst = "19";
                    var birthdatestr = v_month.concat(v_seperate, v_day, v_seperate, v_yearfirst, v_yearLast);
                    var birthdate = new Date(birthdatestr);
                    var birthdatecalc = Date.parse(birthdate);
                    var todaydate = new Date();
                    var todaydatescalc = Date.parse(todaydate);

                    var agecalc = todaydatescalc - birthdatecalc;
                    agecalc = agecalc / 31557600000;
                    agecalc = Math.trunc(agecalc);



                    v_context.p_checkingValidation(v_domNode, function (v_isValid) {
                        // if (v_isValid) {
                        // Get all input values

                        var v_transactionSetting = {
                            p_salutation: v_context.p_salutationDropdownValue,
                            p_NRIC: v_domAttr.get(v_context.NRICNode, "value"),
                            p_DOB: v_context.datepickerDOB,//v_domAttr.get(v_context.datepickerDOBNode,"value"),//
                            p_mobileNo: v_domAttr.get(v_context.mobileNoNode, "value"),
                            p_name: v_domAttr.get(v_context.nameNode, "value"),
                            p_gender: v_context.toggleButtons.selectedKey,//v_genderIdentifier,//v_domAttr.get(v_context.toggleButtons.selectedKey),//v_genderIdentifier,//v_context.getRadioValue("gender"),
                            p_age: v_context.carriedData,//agecalc//v_domAttr.get(v_context.ageNode, "value"),
                        };
                        console.log(v_context.p_salutationDropdownValue);
                        console.log(v_context.storedData);
                        console.log(v_context.carriedData);
                        console.log(v_genderIdentifier);
                        console.log(v_context.toggleButtons.selectedKey);


                        v_topic.publish("navigation.block.open", "EXERCISE_CONFIRMATION_PAGE", {
                            p_transactionSetting: v_transactionSetting
                        });
                        // }
                    });
                });

                v_on(v_context.resetNode, "click", function () {
                    v_context.p_removeAllValidation(v_domNode, function () {
                        // Reset v_Slider
                        // v_context.p_sliderRange.reset();
                        v_topic.publish("navigation.modal.open", "EXERCISE_PAGE_MODAL_1", {});
                        // });
                    });

                });

                // v_on(v_context.nextNode, "click", function () {
                //     v_topic.publish("navigation.block.next", "EXERCISE_CONFIRMATION_PAGE", {});
                // });
            }


        });
    });