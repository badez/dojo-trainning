define([
    "dojo/_base/declare",
    "dojo/dom-attr",
    "dojo/html",
    "dojo/on",
    "dojo/text!./html/ExerciseConfirmationPage.html",
    "dojo/topic",
    "app/components/ui/blocks/BaseBlock",
    "app/components/utils/General",
    "dijit/_TemplatedMixin",
    "app/components/ui/element/breadcrumbs/Breadcrumbs"
], function (
    v_declare,
    v_domAttr,
    v_html,
    v_on,
    v_template,
    v_topic,
    v_BaseBlock,
    v_GeneralUtils,
    TemplatedMixin,
    Breadcrumbs,
    ) {
        // return v_declare([v_BaseBlock], {
        //     templateString: v_template,
        //     p_construct: function (v_setting) {
        //         this.inherited(arguments);

        //         var v_context = this;

        //         v_context.p_setupContent(v_setting);
        //         v_context.p_initInteraction(v_setting);

        //     },
        //     p_setupContent: function (v_setting) {
        //         var v_context = this;

        //         var v_transactionSetting = v_setting.p_transactionSetting;
        //         console.log(v_transactionSetting);
        //         v_html.set(v_context.salutationDropdownNode, v_transactionSetting.p_salutation);
        //         v_html.set(v_context.NRICNode, v_transactionSetting.p_NRIC);
        //         v_html.set(v_context.datepickerDOBNode, v_transactionSetting.p_DOB);
        //         v_html.set(v_context.mobileNoNode, v_transactionSetting.p_mobileNo);
        //         v_html.set(v_context.nameNode, v_transactionSetting.p_name);
        //         // v_html.set(v_context.genderNode, v_transactionSetting.p_gender);
        //         v_html.set(v_context.ageNode, v_transactionSetting.p_age);

        //     },
        //     p_initInteraction: function (v_setting) {
        //         var v_context = this;
        //         var v_transactionSetting = v_setting.p_transactionSetting;

        //         v_on(v_context.previousNode, "click", function () {
        //             v_topic.publish("navigation.back", "EXERCISE_PAGE");
        //         });

        //         v_on(v_context.submitNode, "click", function () {

        //             v_topic.publish("navigation.modal.open", "EXERCISE_PAGE_MODAL_2", {
        //                 p_transactionSetting: v_transactionSetting
        //             });
        //         });
        //     }
        // });
        return v_declare([v_BaseBlock], {
            templateString: v_template,
            p_construct: function (v_setting) {
                this.inherited(arguments);

                var v_context = this;

                v_context.p_setupContent(v_setting);
                v_context.p_initInteraction(v_setting);
            },
            p_setupContent: function (v_setting) {
                var v_context = this;
                var v_domNode = this.domNode;
                var v_domNodeId = v_domAttr.get(v_domNode, "id");

                var v_transactionSetting = v_setting.p_transactionSetting;
                console.log(v_transactionSetting);
                v_html.set(v_context.salutationDropdownNode, v_transactionSetting.p_salutation);
                v_html.set(v_context.NRICNode, v_transactionSetting.p_NRIC);
                v_html.set(v_context.datepickerDOBNode, v_transactionSetting.p_DOB);
                v_html.set(v_context.mobileNoNode, v_transactionSetting.p_mobileNo);
                v_html.set(v_context.nameNode, v_transactionSetting.p_name);
                // v_html.set(v_context.genderNode, v_transactionSetting.p_gender);
                v_html.set(v_context.ageNode, v_transactionSetting.p_age);

                // var breadcrumbs = new Breadcrumbs();
                // breadcrumbs.placeAt(v_context.breadcrumbsContainerNode);
                // breadcrumbs.p_construct(v_domNodeId);
            },
            p_initInteraction: function (v_setting) {
                var v_context = this;
                var v_transactionSetting = v_setting.p_transactionSetting;
                v_on(v_context.previousNode, "click", function () {
                    v_topic.publish("navigation.block.previous");
                });

                v_on(v_context.submitNode, "click", function () {

                    v_topic.publish("navigation.modal.open", "EXERCISE_PAGE_MODAL_2", {
                        p_transactionSetting: v_transactionSetting
                    });
                });
            }
        });
    });
