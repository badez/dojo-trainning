define([
    "dojo/_base/declare",
    "dojo/dom-attr",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/on",
    "dojo/query",
    "dojo/text!./html/DynamicExample.html",
    "dojo/topic",
    "dijit/_TemplatedMixin",
    "app/components/ui/blocks/BaseBlock",
    "app/components/ui/element/breadcrumbs/Breadcrumbs",
    "app/components/ui/element/dynamicpanel/DynamicPanel",
    "app/components/utils/Rest"
], function(
    declare,
    domAttr,
    domClass,
    domStyle,
    on,
    query,
    template,
    topic,
    TemplatedMixin,
    BaseBlock,
    Breadcrumbs,
    DynamicPanel,
    Rest
) {
    return declare("app.components.ui.blocks.dynamicpanelexample.DynamicExample", [
        BaseBlock,
        TemplatedMixin
    ], {
        templateString: template,
        isPanel1Valid: false,
        isPanel2Valid: false,
        savedData: null,
        savedData2: null,
        p_construct: function(setting) {
            this.inherited(arguments);

            var context = this;

            context.listenTaskTab(setting);
            context.initInteraction(setting);
            context.setupContent(setting);
        },
        startup: function() {
            this.inherited(arguments);

            var context = this;

            context.isPanel1Valid = false;
            context.isPanel2Valid = false;
            context.savedData = null;
            context.savedData2 = null;
        },
        initInteraction: function(_setting) {
            var context = this;

            on(context.submitNode, "click", function() {
                context.isPanel1Valid = true;
                query("input", context.dynamicPanelNode).forEach(function(single) {
                    on.emit(single, "blur", {
                        bubbles: true,
                        cancelable: true
                    });

                    if (domClass.contains(single, "input-error")) {
                        context.isPanel1Valid = false;
                    }
                });

                if (context.isPanel1Valid) {
                    topic.publish("navigation.modal.open", "SUCCESS_FAIL", {
                        state: "success",
                        title: "Submit Success!",
                        description: JSON.stringify(context.savedData, null, 2)
                    });
                }
            });
            on(context.submit2Node, "click", function() {
                context.isPanel2Valid = true;
                query("input", context.dynamicPanel2Node).forEach(function(single) {
                    on.emit(single, "blur", {
                        bubbles: true,
                        cancelable: true
                    });

                    if (domClass.contains(single, "input-error")) {
                        context.isPanel2Valid = false;
                    }
                });

                if (context.isPanel2Valid) {
                    topic.publish("navigation.modal.open", "SUCCESS_FAIL", {
                        state: "success",
                        title: "Submit Success!",
                        description: JSON.stringify(context.savedData2, null, 2)
                    });
                }
            });
        },
        setupDynamicPanel1: function(_setting) {
            var context = this;

            var rest = new Rest().get(
                "assets/data/sampledynamicpanel_data.json");

            rest.then(function(response) {
                var dynamicPanel = new DynamicPanel().placeAt(context.dynamicPanelNode);
                dynamicPanel.p_construct({
                    data: response,
                    idAccessor: "fldCd",
                    typeAccessor: "fldTyp",
                    descriptionAccessor: "fldDesc",
                    valueAccessor: "value",
                    mandatoryAccessor: "mandatoryFlag",
                    lengthAccessor: "fldLength",
                    column: 2,
                    savedData: function(savedData) {
                        context.savedData = savedData;
                    }
                });

                // to trigger only once when user lost focus in each input.
                query("div.form-group", context.dynamicPanelNode).forEach(function(single) {
                    on.once($("input", single)[0], "blur", function() {
                        context.checkingValidation(single);
                    });
                })
            });

        },
        setupDynamicPanel2: function(_setting) {
            var context = this;

            var rest2 = new Rest().get(
                "assets/data/sampledynamicpanel2_data.json");

            rest2.then(function(response) {
                var dynamicPanel = new DynamicPanel().placeAt(context.dynamicPanel2Node);
                dynamicPanel.p_construct({
                    data: response,
                    idAccessor: "fldCd",
                    typeAccessor: "fldTyp",
                    descriptionAccessor: "fldDesc",
                    valueAccessor: "value",
                    mandatoryAccessor: "mandatoryFlag",
                    lengthAccessor: "fldLength",
                    column: 2,
                    savedData: function(savedData) {
                        context.savedData2 = savedData;
                    }
                });

                // to trigger only once when user lost focus in each input.
                query("div.form-group", context.dynamicPanel2Node).forEach(function(single) {
                    on.once($("input", single)[0], "blur", function() {
                        context.checkingValidation(single);
                    });
                })
            });
        },
        setupContent: function(_setting) {
            var context = this;
            var domNode = this.domNode;
            var domNodeId = domAttr.get(domNode, "id");

            context.setupDynamicPanel1(_setting);
            context.setupDynamicPanel2(_setting);

            // Start create breadcrumbs
            var breadcrumbs = new Breadcrumbs();
            breadcrumbs.placeAt(context.breadcrumbsContainerNode);
            breadcrumbs.p_construct(domNodeId);
            // End create breadcrumbs
        }
    });
});
