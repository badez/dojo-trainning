define([
    "dojo/_base/array",
    "dojo/_base/declare",
    "dojo/_base/window",
    "dojo/dom",
    "dojo/dom-attr",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/dom-style",
    "dojo/html",
    "dojo/i18n!app/locale/nls/common",
    "dojo/on",
    "dojo/query",
    "dojo/text!./html/InboxNotificationAll.html",
    "dojo/topic",
    "dijit/_TemplatedMixin",
    "app/components/ui/blocks/BaseBlock",
    "app/components/ui/cell/inboxnotification/InboxNotificationAllCell",
    "app/components/ui/element/breadcrumbs/Breadcrumbs",
    "app/components/ui/element/searchinput/SearchInput",
    "app/components/utils/General",
    "app/components/utils/Rest"
], function(
    array,
    declare,
    win,
    dom,
    domAttr,
    domClass,
    domConstruct,
    domStyle,
    html,
    i18n_common,
    on,
    query,
    template,
    topic,
    _TemplatedMixin,
    BaseBlock,
    InboxNotificationAllCell,
    Breadcrumbs,
    SearchInput,
    GeneralUtils,
    Rest
) {
    return declare("app.components.ui.blocks.inboxnotification.InboxNotificationAll", [BaseBlock, _TemplatedMixin], {
        templateString: template,
        tempArray: null,
        p_construct: function(setting) {
            this.inherited(arguments);
            var context = this;

            context.startListening(setting);
            context.listenTaskTab(setting);
            context.setupContent(setting);
            context.initInteraction(setting);
        },
        startup: function() {
            this.inherited(arguments);
            var context = this;

            context.tempArray = [];
        },
        startListening: function(_setting) {
            var context = this;
            var domNode = this.domNode;
            var domNodeId = domAttr.get(domNode, "id");

            var disabledEnableHandler = topic.subscribe("enable.disabled." + domNodeId, function(data) {

                if (data.status) {
                    context.tempArray.push(data);
                } else {
                    array.forEach(context.tempArray, function(single) {
                        if (single.cellId == data.cellId) {
                            context.tempArray = _.reject(context.tempArray, function(el) {
                                return el.cellId === data.cellId;
                            });
                        }
                    });
                }

                var deleteButton = dom.byId(context.buttonMap["delete"]);
                var refreshButton = dom.byId(context.buttonMap["refresh"]);
                if (context.tempArray.length == 0) {
                    domAttr.set(deleteButton, "disabled", "disabled");
                    domAttr.remove(refreshButton, "disabled");
                } else {
                    domAttr.remove(deleteButton, "disabled");
                    domAttr.set(refreshButton, "disabled", "disabled");
                }

            });


        },
        setupContent: function(_setting) {
            var context = this;
            var domNode = this.domNode;
            var domNodeId = domAttr.get(domNode, "id");

            domStyle.set(context.clearBtnNode, "display", "none");

            // Start create breadcrumbs
            var breadcrumbs = new Breadcrumbs();
            breadcrumbs.placeAt(context.breadcrumbsContainerNode);
            breadcrumbs.p_construct(domNodeId);
            // End create breadcrumbs

            var buttons = [{
                className: "btn btn-default",
                label: "",
                icon: "ico-refresh",
                key: "refresh",
                action: function() {
                    domAttr.set(context.searchSubjectNode, "value", "");
                    domStyle.set(context.clearBtnNode, "display", "none");
                    context.callRest("assets/data/inboxnotification_data.json", {}, {}, function(response) {
                        domConstruct.empty(context.emailTbodyTableNode);
                        array.forEach(response.webInboxNotification, function(single) {
                            single.parentid = domAttr.get(domNode, "id");
                            var inboxNotificationAllCell = new InboxNotificationAllCell();
                            inboxNotificationAllCell.placeAt(context.emailTbodyTableNode);
                            inboxNotificationAllCell.p_construct(single);
                        });
                        html.set(context.inboxLabelNode, i18n_common.inbox + "(" + response.webInboxNotification.length + ")");
                    });
                }
            }, {
                className: "btn btn-default",
                label: false,
                icon: "ico-delete",
                key: "delete",
                action: function() {
                    var tempArray = [];
                    var state;
                    var buttons;
                    var description = "";
                    topic.publish("delete.checked.messages." + domNodeId, function(data) {
                        tempArray.push(data);
                    });

                    if (tempArray.length > 0) {
                        state = "warning";
                        buttons = [{
                            label: "NO",
                            type: "secondary",
                            action: function() {
                                topic.publish("close.modal." + domNodeId);
                            }
                        }, {
                            label: "YES",
                            type: "primary",
                            action: function() {
                                ///call api to pass deleted messages

                                context.callRest("assets/data/inboxnotification_data.json", {}, {}, function(response) {
                                    domConstruct.empty(context.emailTbodyTableNode);
                                    array.forEach(response.webInboxNotification, function(single) {
                                        single.parentid = domAttr.get(domNode, "id");
                                        var inboxNotificationAllCell = new InboxNotificationAllCell();
                                        inboxNotificationAllCell.placeAt(context.emailTbodyTableNode);
                                        inboxNotificationAllCell.p_construct(single);
                                    });
                                    topic.publish("close.modal." + domNodeId);
                                });
                            }
                        }];
                    } else {
                        state = "fail";
                        description = i18n_common.selectToRemove
                        buttons = [{
                            label: "BACK",
                            type: "secondary",
                            action: function() {
                                topic.publish("close.modal." + domNodeId);
                            }
                        }]
                    }

                    topic.publish("navigation.modal.open", "SUCCESS_FAIL", {
                        state: state,
                        title: "",
                        description: description,
                        parentId: domNodeId,
                        buttons: buttons
                    });
                }
            }];

            // create button
            var tempButtons = [];
            context.buttonMap = {};

            array.forEach(buttons, function(btn) {
                var uniqueButtonId = new GeneralUtils().createUUID();
                context.buttonMap[btn.key] = uniqueButtonId;
                var domButton = domConstruct.create("button", {
                    id: uniqueButtonId,
                    className: btn.className,
                    style: (btn.style ? btn.style : {}),
                    type: "button",
                }, context.buttonsNode);

                if (btn.icon == "ico-delete") {
                    domAttr.set(domButton, "disabled", "disabled");
                }

                var itag = domConstruct.create("i", {
                    className: (btn.icon ? btn.icon : ""),
                    style: "font-size:20px",
                }, domButton);

                domStyle.set(context.buttonsNode, "padding-left", "35px");
                domStyle.set(context.buttonsNode, "padding-top", "15px");
                domStyle.set(domButton, "margin-right", "5px");
                on(domButton, "click", function(event) { btn.action() });

                tempButtons.push(domButton);
            });

            domStyle.set(context.emailTbodyTableNode, "border-bottom", "1px solid #D1D3D5");

            context.callRest("assets/data/inboxnotification_data.json", {}, {}, function(response) {
                array.forEach(response.webInboxNotification, function(single) {
                    single.parentid = domAttr.get(domNode, "id");
                    var inboxNotificationAllCell = new InboxNotificationAllCell();
                    inboxNotificationAllCell.placeAt(context.emailTbodyTableNode);
                    inboxNotificationAllCell.p_construct(single);
                });
                html.set(context.inboxLabelNode, i18n_common.inbox + "(" + response.webInboxNotification.length + ")");
            });

        },
        initInteraction: function(_setting) {
            var context = this;
            var domNode = this.domNode;

            on(context.searchSubjectNode, "keyup", function() {
                var userInput = context.searchSubjectNode.value.toLowerCase();

                var tempArray = [];

                context.callRest("assets/data/inboxnotification_data.json", {}, {}, function(response) {
                    array.forEach(response.webInboxNotification, function(single) {
                        var subject = single.subject.toLowerCase();
                        var result = subject.search(userInput);
                        if (result >= 0) {
                            tempArray.push(single);
                        }
                    });

                    domConstruct.empty(context.emailTbodyTableNode);
                    if (tempArray.length > 0) {
                        array.forEach(tempArray, function(single) {
                            single.parentid = domAttr.get(domNode, "id");
                            var inboxNotificationAllCell = new InboxNotificationAllCell();
                            inboxNotificationAllCell.placeAt(context.emailTbodyTableNode);
                            inboxNotificationAllCell.p_construct(single);
                        });
                        html.set(context.inboxLabelNode, i18n_common.inbox + "(" + tempArray.length + ")");
                    } else {
                        var inboxNotificationAllCell = new InboxNotificationAllCell();
                        inboxNotificationAllCell.placeAt(context.emailTbodyTableNode);
                        inboxNotificationAllCell.p_construct({
                            status: 0,
                            subject: "No matching records found"
                        });
                    }

                    if (context.searchSubjectNode.value) {
                        domStyle.set(context.clearBtnNode, "display", "block");
                    } else {
                        domStyle.set(context.clearBtnNode, "display", "none");
                    }
                });


            });

            on(context.clearBtnNode, "click", function() {
                domAttr.set(context.searchSubjectNode, "value", "");
                domStyle.set(context.clearBtnNode, "display", "none");

                context.callRest("assets/data/inboxnotification_data.json", {}, {}, function(response) {
                    domConstruct.empty(context.emailTbodyTableNode);
                    array.forEach(response.webInboxNotification, function(single) {
                        single.parentid = domAttr.get(domNode, "id");
                        var inboxNotificationAllCell = new InboxNotificationAllCell();
                        inboxNotificationAllCell.placeAt(context.emailTbodyTableNode);
                        inboxNotificationAllCell.p_construct(single);
                    });
                    html.set(context.inboxLabelNode, i18n_common.inbox + "(" + response.webInboxNotification.length + ")");
                });
            });
        },
        callRest: function(prototypeUrl, datapackage, apiObj, handler) {
            var rest = new Rest().get(prototypeUrl);
            rest.then(function(response) {
                handler(response);
            }, function(err) {
                console.error(err);
            });
        }
    });
});
