define([
    "dojo/_base/declare",
    "dojo/dom-attr",
    "dojo/dom-construct",
    "dojo/query",
    "dojo/topic",
    "../../utils/BaseUtils",
    "../element/breadcrumbs/Breadcrumbs"
], function (
    v_declare,
    v_domAttr,
    v_domConstruct,
    v_query,
    v_topic,
    v_BaseUtils,
    v_Breadcrumbs
) {
    return v_declare("app.components.ui.blocks.BaseBlock", [v_BaseUtils], {
        p_construct: function (v_setting) {
            var v_context = this;

            v_context.getSetting = function () {
                return v_setting ? v_setting : {};
            };

            v_query("[breadcrumbs]", v_context.domNode).forEach(function (v_single) {
                // Start create breadcrumbs
                var v_breadcrumbs = new v_Breadcrumbs();
                v_breadcrumbs.placeAt(v_context.domNode, 0);
                v_breadcrumbs.p_construct(v_context.getSetting());
                // End create breadcrumbs

                v_domConstruct.destroy(v_single);
            });

            v_context.p_initValidation(v_context.domNode);

            v_topic.publish("block.loaded", v_context);
            v_topic.publish("block.active", v_context);
        },
        destroy: function () {
            var v_context = this;
            v_topic.publish("block.destroyed", v_context);
            this.inherited(arguments);
        }
    });
});
