define([
    "dojo/_base/declare",
    "dojo/on",
    "dojo/topic",
    "dojo/text!./html/SampleModal1.html",
    "app/components/ui/modal/BaseModal"
], function (
    v_declare,
    v_on,
    v_topic,
    v_template,
    v_BaseModal
) {
    return v_declare("app.components.ui.modal.test.samplemodal.SampleModal1", [v_BaseModal], {
        templateString: v_template,
        p_construct: function (v_setting) {
            this.inherited(arguments);
            var v_context = this;

            v_context.p_initInteraction(v_setting);

            console.log(v_setting.p_test);
        },
        p_initInteraction: function(v_setting) {
            var v_context = this;
            var v_domNode = v_context.domNode;

            v_on(v_context.nextStackedNode, "click", function() {
                v_topic.publish("navigation.modal.open", "SAMPLE_MODAL_2", {});
            });
            v_on(v_context.nextNode, "click", function() {
                $(v_domNode).modal("hide");
                v_topic.publish("navigation.modal.open", "SAMPLE_MODAL_2", {});
            });
        }
    });

});
