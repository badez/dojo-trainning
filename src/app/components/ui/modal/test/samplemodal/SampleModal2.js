define([
    "dojo/_base/declare",
    "dojo/text!./html/SampleModal2.html",
    "app/components/ui/modal/BaseModal"
], function (
    v_declare,
    v_template,
    v_BaseModal
) {
    return v_declare("app.components.ui.modal.test.samplemodal.SampleModal2", [v_BaseModal], {
        templateString: v_template
    });
});
