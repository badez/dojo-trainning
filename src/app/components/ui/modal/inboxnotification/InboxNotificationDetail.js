define([
    "dojo/_base/array",
    "dojo/_base/declare",
    "dojo/dom",
    "dojo/dom-attr",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/dom-style",
    "dojo/html",
    "dojo/i18n!app/locale/nls/common",
    "dojo/on",
    "dojo/topic",
    "dojo/text!./html/InboxNotificationDetail.html",
    "app/components/ui/element/searchinput/SearchInput",
    "app/components/ui/modal/BaseModal",
    "app/components/utils/General"
], function(
    array,
    declare,
    dom,
    domAttr,
    domClass,
    domConstruct,
    domStyle,
    html,
    i18n_common,
    on,
    topic,
    template,
    SearchInput,
    BaseModal,
    GeneralUtils
) {
    return declare("InboxNotificationDetail", [BaseModal], {
        templateString: template,
        p_construct: function(setting) {
            this.inherited(arguments);
            var context = this;
            
            context.i18n(setting);
            context.setupContent(setting);
            context.initInteraction(setting);
        },
        initInteraction: function(_setting) {
            var context = this;
            var domNode = context.domNode;

            on(context.closeLinkNode, "click", function() {
                context.modalAnimation();
            });
        },
        setupContent: function(_setting) {
            var context = this;

            html.set(context.subjectLabelNode, i18n_common.subject);
            html.set(context.senderLabelNode, i18n_common.from);

            var subject = _setting.subject;
            if (_setting.subject.length > 37) {
                subject = _setting.subject.substring(0, 37) + " . . .";
            }
            html.set(context.subjectNode, subject);

            var sender = _setting.sender;
            if (_setting.sender.length > 30) {
                sender = _setting.sender.substring(0, 30) + " . . .";
            }
            html.set(context.senderNode, sender);
            html.set(context.messageContentNode, _setting.messageContent);

            domStyle.set(context.subjectNode, "font-weight", "bold");
            domStyle.set(context.subjectNode, "font-size", "15px");

            domStyle.set(context.senderNode, "font-weight", "bold");
            domStyle.set(context.senderNode, "font-size", "12px");
        },
        i18n: function(_setting) {
            var context = this;
            var domNode = context.domNode;

            html.set(context.inboxLabelNode, i18n_common.viewMessage);
            var date = new GeneralUtils().formatDate(_setting.date, { display: "datetime" });

            var newDate = date.slice(0, 16) + date.slice(19);
            var index = newDate.charAt(11);
            if(index == 0){
                newDate = newDate.slice(0, 11) + newDate.slice(12);
            }
            html.set(context.dateNode, newDate);
        },
        modalAnimation: function(_setting) {
            var context = this;
            var domNode = context.domNode;
            var domNodeId = domAttr.get(domNode, "id");

            //changing animation
            domClass.remove(context.modalDialogNode, "bounceInDown");
            domClass.add(context.modalDialogNode, "bounceOutUp");
            //timer to play animation first before closing modal

            var appTimeout;
            clearTimeout(appTimeout);
            appTimeout = setTimeout(function() {
                $(dom.byId(domNodeId)).modal('hide');
            }, 500);

        }
    });

});
