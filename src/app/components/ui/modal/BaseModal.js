define([
    "dojo/_base/declare",
    "dojo/topic",
    "app/components/utils/BaseUtils"
], function (
    v_declare,
    v_topic,
    v_BaseUtils
) {
    return v_declare([v_BaseUtils], {
        p_construct: function (v_setting) {
            var v_context = this;

            v_context.getSetting = function () {
                return v_setting ? v_setting : {};
            };

            v_context.p_initValidation(v_context.domNode);

            v_topic.publish("modal.loaded", v_context);
        }
    });
});
