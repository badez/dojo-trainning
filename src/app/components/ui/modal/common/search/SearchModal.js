define([
    "dojo/on",
    "dojo/dom",
    "dojo/html",
    "dojo/dom-attr",
    "dojo/dom-class",
    "dojo/_base/array",
    "dojo/dom-construct",
    "dojo/_base/declare",
    "dojo/i18n!app/locale/nls/common",
    "dojo/text!./html/SearchModal.html",
    "app/components/utils/Rest",
    "app/components/ui/modal/BaseModal",
    "app/components/utils/General",
    "app/components/ui/element/searchinput/SearchInput",
    "app/components/ui/element/customtable/CustomTable",
], function(
    v_on,
    v_dom,
    v_html,
    v_domAttr,
    v_domClass,
    v_array,
    v_domConstruct,
    v_declare,
    v_i18n_common,
    v_template,
    v_Rest,
    v_BaseModal,
    v_GeneralUtils,
    v_SearchInput,
    v_CustomTable
) {
    return v_declare("SearchModal", [v_BaseModal], {
        templateString: v_template,
        tableId: null,
        p_construct: function(v_setting) {
            this.inherited(arguments);
            var v_context = this;
            v_context.tableId = "";

            v_setting.p_backdrop = "static";

            v_context.p_i18n(v_setting);
            v_context.p_setupContent(v_setting);
            v_context.p_initInteraction(v_setting);

        },
        p_i18n: function(v_setting) {
            var v_context = this;

            if (v_setting.tittle !== undefined && v_setting.tittle !== null) {
                v_html.set(v_context.titleNode, v_setting.tittle);
            } else {
                v_html.set(v_context.titleNode, v_i18n_common.search);
            }
        },
        p_setupContent: function(v_setting) {
            var v_context = this;
            var v_domNode = v_context.domNode;

            if (v_setting.isSearchRequired !== undefined && v_setting.isSearchRequired) {
                var v_uniqueIdFormSearchInput = v_context.p_createUUID(v_context.searchNode);
                var v_searchInput = new v_SearchInput().placeAt(v_context.searchNode);
                v_searchInput.p_construct({
                    parentid: v_domAttr.get(v_domNode, "id"),
                    searchInputId: v_uniqueIdFormSearchInput,
                    type: "text",
                    placeHolder: v_i18n_common.search,
                    callback: function(v_inputValue) {
                        v_context.p_constructSearchResult(v_setting, v_inputValue, false);
                    }
                });
            }

            setTimeout(function() {
                $(v_context.closeLinkNode).trigger("focus");
            }, 300)

            v_context.p_constructSearchResult(v_setting, "", true);
        },
        p_initInteraction: function(v_setting) {
            var v_context = this;

            v_on(v_context.closeLinkNode, "click", function() {
                v_context.p_closeModal();
            });
        },
        p_constructSearchResult: function(v_setting, v_keyword, v_firstTime) {
            var v_context = this;

            v_domConstruct.empty(v_context.resultNode);

            var v_dataPackage = new Object();
            v_dataPackage.serviceParameter = v_setting.serviceParameter;
            v_dataPackage[v_setting.searchKey] = v_keyword;

            if (v_setting.additionalSearchKey !== undefined && v_setting.additionalSearchKey.length > 0) {
                v_array.forEach(v_setting.additionalSearchKey, function(v_single) {
                    v_dataPackage[v_single.searchKey] = v_single.searchValue;
                });
            }
            if (v_setting.data === undefined) {
                var v_rest = new v_Rest().p_postapi({ "api": v_setting.api }, v_setting.prototypeUrl, v_dataPackage);
                v_rest.then(function(v_response) {
                    v_context.tableId = v_context.p_createUUID(v_context.resultNode);
                    var v_customDatatable = new v_CustomTable().placeAt(v_context.resultNode);
                    v_customDatatable.p_construct({
                        parentId: v_setting.parentId,
                        tableId: v_context.tableId,
                        data: v_firstTime ? [] : v_response[v_setting.dataSrc],
                        searching: false,
                        columns: v_setting.columns,
                        ordering: (v_setting.ordering === undefined) ? [] : v_setting.ordering,
                        noVerticalBorder: true,
                        paging: v_setting.paging,
                        pagingType: v_setting.pagingType,
                        dom: v_setting.dom,
                        rowSelection: v_firstTime || !v_response[v_setting.dataSrc] ? false : true,
                        customSearchClass: v_setting.customSearchClass,
                        onRowClick: function(v_trElement, v_rowData) {
                            v_context.p_closeModal();
                            v_customDatatable.destroy();
                            v_setting.callback(v_trElement, v_rowData);
                        }
                    });
                });
            } else {
                v_context.tableId = v_context.p_createUUID(v_context.resultNode);
                var v_customDatatable = new v_CustomTable().placeAt(v_context.resultNode);
                v_customDatatable.p_construct({
                    parentId: v_setting.parentId,
                    tableId: v_context.tableId,
                    data: v_setting.data,
                    searching: true,
                    ordering: (v_setting.ordering === undefined) ? [] : v_setting.ordering,
                    paging: v_setting.paging,
                    pagingType: v_setting.pagingType,
                    dom: v_setting.dom,
                    pageLength: v_setting.pageLength,
                    columns: v_setting.columns,
                    noVerticalBorder: true,
                    rowSelection: true,
                    customSearchClass: v_setting.customSearchClass,
                    onRowClick: function(v_trElement, v_rowData) {
                        v_context.p_closeModal();
                        v_setting.callback(v_trElement, v_rowData);
                    }
                });
            }
        },
        p_createUUID: function(v_node) {
            var v_context = this;
            var uuid = new v_GeneralUtils().createUUID();
            v_domAttr.set(v_node, "id", uuid);
            return uuid;
        },
        p_closeModal: function() {
            var v_context = this;
            var v_domNode = v_context.domNode;
            var v_domNodeId = v_domAttr.get(v_domNode, "id");

            v_domClass.remove(v_context.modalDialogNode, "animated bounceInDown");
            v_domClass.add(v_context.modalDialogNode, "animated bounceOutUp");

            var v_appTimeout;
            clearTimeout(v_appTimeout);
            v_appTimeout = setTimeout(function() {
                $(v_dom.byId(v_domNodeId)).modal('hide');
            }, 500);
        },
    });
});
