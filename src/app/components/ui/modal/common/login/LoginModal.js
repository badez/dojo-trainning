define([
    "dojo/_base/config",
    "dojo/_base/declare",
    "dojo/dom-attr",
    "dojo/keys",
    "dojo/on",
    "dojo/html",
    "dojo/text!./html/LoginModal.html",
    "dojo/topic",
    "dojo/i18n!app/locale/nls/login-bundle",
    "app/components/ui/modal/BaseModal",
    "app/components/utils/Rest"
], function (
    v_config,
    v_declare,
    v_domAttr,
    v_keys,
    v_on,
    v_html,
    v_template,
    v_topic,
    v_i18n,
    v_BaseModal,
    v_Rest

) {
    return v_declare([v_BaseModal], {
        templateString: v_template,
        p_construct: function (v_setting) {
            this.inherited(arguments);
            var v_context = this;

            v_setting.p_backdrop = "static";

            v_context.p_initInteraction(v_setting);
            v_context.p_setupContent(v_setting);
            v_context.i18n(v_setting);
        },
        p_initInteraction: function (v_setting) {
            var v_context = this;

            v_on(v_context.loginNode, "click", function () {
                v_context.p_login(v_setting);
            });
            v_on(v_context.passwordNode, "keyup", function (v_event) {
                switch (v_event.keyCode) {
                    case v_keys.ENTER:
                        v_context.p_login();
                        break;
                }
            });
        },
        i18n: function (v_setting) {
            var v_context = this;
            v_html.set(v_context.loginNode, v_i18n.loginbtn);
            v_html.set(v_context.forgotPwdNode, v_i18n.forgotpasswordlabel);
            v_html.set(v_context.copyRightNode, v_i18n.copyrightlabel);
            v_domAttr.set(v_context.usernameNode, "placeholder", v_i18n.username);
            v_domAttr.set(v_context.passwordNode, "placeholder", v_i18n.password);
        },
        p_setupContent: function () {
            $.jStorage.set("app.login", false);
        },
        p_login: function (v_setting) {
            var v_context = this;
            var v_domNode = this.domNode;

            var v_restKey = new v_Rest().p_getapi({ p_api: "auth/key" }, null);

            v_restKey.response.then(function (v_responseKey) {
                $.jStorage.set("app.login.publickey", v_responseKey.getHeader('public-key'));

                v_context.usernameNode.value = v_context.usernameNode.value.toUpperCase();

                var v_unamePwd = v_domAttr.get(v_context.usernameNode, "value") + ":" + v_domAttr.get(v_context.passwordNode, "value");

                var v_encrypt = new JSEncrypt();
                v_encrypt.setPublicKey($.jStorage.get("app.login.publickey"));
                var v_encryptedUnamePwd = v_encrypt.encrypt(v_unamePwd);

                var v_dataPackage = {};
                v_dataPackage.data = v_encryptedUnamePwd;
                var v_restLogin;
                switch (v_config.projRoleType) {
                    case "AGENT":
                        v_restLogin = new v_Rest().p_postapi({ p_api: "auth/agentlogin", p_delegate: "login" }, null, v_dataPackage);
                        break;
                    default:
                        v_restLogin = new v_Rest().p_postapi({ p_api: "auth/login", p_delegate: "login" }, null, v_dataPackage);
                }
                v_restLogin.then(function (v_response) {
                    if (parseInt(v_response.status / 100) == 2 || v_config.isExperimental) {
                        if ($.jStorage.get("app.login.username") == undefined && v_domAttr.get(v_context.usernameNode, "value") != undefined) {
                            $.jStorage.set("app.login.username", v_domAttr.get(v_context.usernameNode, "value"));
                        }

                        v_response.data = v_response.data ? v_response.data : {};

                        if ($.jStorage.get("app.login.userid") == undefined && v_response.data.userName != undefined) {
                            $.jStorage.set("app.login.userid", v_response.data.userName);
                        }

                        $.jStorage.set("app.login", true);
                        // f_getUserBranch();

                        $.jStorage.set("app.date.long", v_response.data.applicationDate);
                        v_topic.publish("navigation.login.success");

                        $(v_domNode).modal("hide");
                    } else {
                        v_topic.publish("navigation.modal.open", "ALERT", {
                            p_type: "ERROR",
                            p_title: "",
                            p_desc: ""
                        });
                    }
                });

                function f_getUserBranch() {
                    //START: Get User Branch Code
                    var v_dataPackage = {
                        serviceParameter: "P001",
                        userId: v_domAttr.get(v_context.usernameNode, "value"),
                        baseFunctionCode: 'KERN_SYSTEM_FUNCTION',
                        baseModuleCode: 'KERN'
                    };
                    var v_rest = new v_Rest().p_postapi({ p_api: "user/find" }, "assets/data/postprototype.json", v_dataPackage);
                    v_rest.then(function (v_response) {
                        if (v_response !== undefined && v_response.user !== undefined && v_response.user.branchCode !== undefined) {
                            $.jStorage.set("app.login.userBranchCode", v_response.user.branchCode);
                        }
                    });
                    //END: Get User Branch Code
                }
            });
        }
    });
});
