define([
    "dojo/_base/declare",
    "dojo/dom-class",
    "dojo/html",
    "dojo/on",
    "dojo/topic",
    "dojo/text!./html/AlertModal.html",
    "app/components/ui/modal/BaseModal"
], function(
    v_declare,
    v_domClass,
    v_html,
    v_on,
    v_topic,
    v_template,
    v_BaseModal
) {
    return v_declare([v_BaseModal], {
        templateString: v_template,
        p_construct: function(v_setting) {
            this.inherited(arguments);
            var v_context = this;

            v_setting.p_backdrop = "static";

            v_context.p_i18n(v_setting);
            v_context.p_initInteraction(v_setting);
            v_context.p_setupContent(v_setting);
        },
        p_i18n: function(v_setting) {
            var v_context = this;

            v_html.set(v_context.titleNode, v_setting.p_title);
            v_html.set(v_context.descriptionNode, v_setting.p_desc);
        },
        p_initInteraction: function(v_setting) {
            var v_context = this;

            v_on(v_context.closeNode, "click", function() {
                if (v_setting.p_callback) {
                    v_setting.p_callback();
                }
            });
        },
        p_setupContent: function(v_setting) {
            var v_context = this;
            switch (v_setting.p_type) {
                case "WARNING":
                    v_domClass.add(v_context.iconNode, "glyphicon glyphicon-warning-sign");
                    break;
                case "ERROR":
                    v_domClass.add(v_context.iconNode, "glyphicon glyphicon-remove-sign");
                    break;
                case "SUCCESS":
                    v_domClass.add(v_context.iconNode, "glyphicon glyphicon-ok-sign");
                    break;
                case "INFO":
                    v_domClass.add(v_context.iconNode, "glyphicon glyphicon-info-sign");
                    break;
            }
        }
    });

});
