define([
    "dojo/_base/array",
    "dojo/_base/declare",
    "dojo/dom-attr",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/dom-style",
    "dojo/html",
    "dojo/i18n!app/locale/nls/common",
    "dojo/on",
    "dojo/text!./html/SuccessFailModal.html",
    "app/components/ui/modal/BaseModal"
], function (
    v_array,
    v_declare,
    v_domAttr,
    v_domClass,
    v_domConstruct,
    v_domStyle,
    v_html,
    v_i18n_common,
    v_on,
    v_template,
    v_BaseModal
) {
    return v_declare([v_BaseModal], {
        templateString: v_template,
        p_construct: function (v_setting) {
            this.inherited(arguments);
            var v_context = this;

            v_setting.p_backdrop = "static";
            v_context.p_setupContent(v_setting);
            v_context.p_initInteraction(v_setting);
        },
        p_initInteraction: function (v_setting) {
            var v_context = this;
            var v_domNode = v_context.domNode;

            v_on(v_context.closeLinkNode, "click", function () {
                $(v_domNode).modal("hide");
            });
        },
        p_setupContent: function (v_setting) {
            var v_context = this;
            var v_domNode = v_context.domNode;

            //if disabledCrossIcon true
            if (v_setting.p_hideCloseLink) {
                v_domStyle.set(v_context.closeLinkNode, "display", "none");
            }

            v_domStyle.set(v_context.iconNode, "width", "120px");
            switch (v_setting.p_state) {
                case "SUCCESS":
                    if (!v_setting.p_title) {
                        v_setting.p_title = v_i18n_common.modalSuccess;
                    }
                    if (!v_setting.p_description) {
                        v_setting.p_description = v_i18n_common.successfullyProcessed;
                    }
                    v_domClass.replace(v_context.headerNode, "widget-head-color-box header navy-bg");
                    v_domClass.remove(v_context.modalDialogNode, "modal-md");
                    v_domClass.replace(v_context.titleNode, "title-success");
                    v_domAttr.set(v_context.iconNode, "src", "assets/img/icon_success.png");
                    break;
                case "FAIL":
                    if (!v_setting.p_title) {
                        v_setting.p_title = v_i18n_common.modalFail;
                    }
                    if (!v_setting.p_description) {
                        v_setting.p_description = v_i18n_common.somethingWentWrong;
                    }
                    v_domClass.replace(v_context.headerNode, "widget-head-color-box header red-bg");
                    v_domClass.replace(v_context.titleNode, "title-fail");
                    v_domAttr.set(v_context.iconNode, "src", "assets/img/icon_error.png");
                    break;
                case "WARNING":
                    if (!v_setting.p_title) {
                        v_setting.p_title = v_i18n_common.modalWarning;
                    }
                    if (!v_setting.p_description) {
                        v_setting.p_description = v_i18n_common.proceedConfirmation;
                    }
                    v_domClass.replace(v_context.headerNode, "widget-head-color-box header orange-bg");
                    v_domClass.replace(v_context.titleNode, "title-warning");
                    v_domAttr.set(v_context.iconNode, "src", "assets/img/icon_warning.png");
                    break;
            }
            if (v_setting.p_info) {
                var v_domTable = v_domConstruct.create("table", { style: "width: 100%;" });
                v_array.forEach(v_setting.p_info, function (v_single) {
                    var v_domRow = v_domConstruct.create("tr");
                    v_domConstruct.place(v_domRow, v_domTable);

                    var v_domCellLabel = v_domConstruct.create("td", { "class": "text-right", style: "width: 50%;" });
                    v_domCellLabel.innerHTML = v_single.p_label;
                    v_domConstruct.place(v_domCellLabel, v_domRow);

                    var v_domCellData = v_domConstruct.create("td", { "class": "text-left font-bold", style: "width: 50%;" });
                    v_domConstruct.place(v_domConstruct.create("span", { "class": "m-l-sm", innerHTML: v_single.p_data}), v_domCellData);
                    v_domConstruct.place(v_domCellData, v_domRow);

                });
                v_domConstruct.place(v_domTable, v_context.extraNode);
            } else {
                v_domConstruct.empty(v_context.descriptionNode);
            }

            if (v_setting.p_buttons) {
                var v_domDiv = v_domConstruct.create("div", { "class": "row" });
                var v_classCol = "";
                switch (v_setting.p_buttons.length) {
                    case 1:
                        v_classCol = "col-xs-12 col-sm-12 col-md-12 col-lg-12";
                        break;
                    case 2:
                    case 3:
                        v_classCol = "col-xs-6 col-sm-6 col-md-6 col-lg-6";
                        break;
                }

                var v_btnType = "";
                v_array.forEach(v_setting.p_buttons, function (v_button, v_i) {
                    if (v_button.p_type === "primary") {
                        v_btnType = "btn-primary";
                    } else {
                        v_btnType = "btn-default";
                    }

                    var v_domInnerDiv;
                    if (v_setting.p_buttons.length === 3 && v_i === 0) {
                        v_domStyle.set(v_context.contentNode, "padding-bottom", "50px");
                        v_domInnerDiv = v_domConstruct.create("div", { "class": "col-xs-12 col-sm-12 col-md-12 col-lg-12 pb10 pr5 pl5" });
                    } else {
                        v_domInnerDiv = v_domConstruct.create("div", { "class": v_classCol + " pr5 pl5" });
                    }

                    var v_buttonTag = v_domConstruct.create("button", { "class": "btn custom-btn full-width " + v_btnType, innerHTML: (v_button.p_label ? v_button.p_label : "") });
                    v_on(v_buttonTag, "click", function () {
                        v_button.p_action(v_domNode);
                    });
                    v_domConstruct.place(v_buttonTag, v_domInnerDiv);
                    v_domConstruct.place(v_domInnerDiv, v_domDiv);
                });
                v_domConstruct.place(v_domDiv, v_context.buttonNode);
            } else {
                v_domConstruct.empty(v_context.buttonNode);
            }

            v_html.set(v_context.titleNode, v_setting.p_title);
            v_html.set(v_context.descriptionNode, v_setting.p_description);
        }
    });

});
