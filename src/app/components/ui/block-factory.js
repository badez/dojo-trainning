define([
    "dojo/dom-construct",
    "app/components/ui/block-keypairs",
    "./element/loadingblock/LoadingBlock"
], function(
    v_domConstruct,
    v_blockKeypairs,
    v_LoadingBlock
) {
    var v_blockFactory = {};

    v_blockFactory.create = function(v_function, v_container, v_setting, v_callback) {
        var v_block = null;

        if (v_setting == undefined) {
            v_setting = {};
        }

        v_setting.p_function = v_function;

        v_blockKeypairs.getBlock(v_function, function(v_Block) {
            v_block = new v_Block();
            var v_loadingBlock = new v_LoadingBlock().placeAt(v_block.domNode);
            v_loadingBlock.p_construct();
            v_block.placeAt(v_container);
            v_block.startup();

            if (v_callback !== undefined && typeof v_callback === 'function') {
                v_callback(v_block);
            }

            v_block.p_construct(v_setting);
        });
    };

    return v_blockFactory;
});
