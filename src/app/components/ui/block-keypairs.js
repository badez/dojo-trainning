define([], function() {
    var v_blockKeypairs = {};

    v_blockKeypairs.getBlock = function(v_key, v_callback) {
        function f_requireWidget(v_path) {
            require([v_path], function(v_keypairs) {
                v_keypairs.get(v_key, function(v_block, v_options) {
                    v_callback(v_block, v_options);
                });
            });
        }

        switch (v_key) {
            case "COMPONENTS":
            case "ICOMOON":
            case "LAYOUT_1":
            case "LAYOUT_2":
            case "LAYOUT_3":
            case "LAYOUT_4":
            case "LAYOUT_5":
            case "FORM_EXAMPLE":
            case "FORM_EXAMPLE_2":
                f_requireWidget("app/keypairs/uitoolkitKeypairs");
                break;
            case "SAMPLE_PAGE_1":
            case "SAMPLE_PAGE_2":
                f_requireWidget("app/keypairs/test/samplePageKeypairs");
                break;
            case "EXTRA_PAGE_1":
                f_requireWidget("app/keypairs/test/extraPageKeypairs");
                break;
            case "SAMPLE_JUMP":
            case "JUMP_PAGE_1":
            case "JUMP_PAGE_2":
            case "JUMP_PAGE_3":
                f_requireWidget("app/keypairs/test/sampleJumpKeypairs");
                break;
            case "CLOUD_STORAGE": 
                f_requireWidget("app/keypairs/test/cloudStorageKeypairs");
                break;
            case "EXERCISE_PAGE":
            case "EXERCISE_CONFIRMATION_PAGE":
                f_requireWidget("app/keypairs/test/exercisePageKeypairs");
                break;
            case "EXERCISE_PAGE_2":
                f_requireWidget("app/keypairs/test/exercisePageKeypairs2");
                break;

        }
    };

    return v_blockKeypairs;
});
