define([
    "app/components/ui/modal-keypairs"
], function(
    v_modalKeypairs
) {
    var v_modalFactory = {};

    v_modalFactory.create = function(v_modalKey, v_container, v_setting, v_callback) {
        var v_modal;

        if (v_setting === undefined) {
            v_setting = {};
        }

        v_setting.p_modalKey = v_modalKey;

        v_modalKeypairs.getModal(v_modalKey, function(v_Modal) {
            v_modal = new v_Modal();
            v_modal.placeAt(v_container);
            v_modal.startup();
            v_modal.p_construct(v_setting);

            if (v_callback !== undefined && typeof v_callback === 'function') {
                v_callback(v_modal);
            }
        });
    };

    return v_modalFactory;
});
