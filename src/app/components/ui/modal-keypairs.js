define([], function() {
    var v_modalKeypairs = {};

    v_modalKeypairs.getModal = function(v_key, v_callback) {
        function f_requireWidget(v_path) {
            require([v_path], function(v_keypairs) {
                v_keypairs.get(v_key, function(v_modal, v_options) {
                    v_callback(v_modal, v_options);
                });
            });
        }

        switch (v_key) {
            case "SAMPLE_MODAL_1":
            case "SAMPLE_MODAL_2":
                f_requireWidget("app/keypairs/test/sampleModalKeypairs");
                break;
            case "EXERCISE_PAGE_MODAL_1":
            case "EXERCISE_PAGE_MODAL_2":
                f_requireWidget("app/keypairs/test/exercisePageModalKeypairs");
                break;
            case "ALERT":
            case "LOGIN":
            case "SUCCESS_FAIL":
            case "SEARCH":
            case "INBOX_NOTIFICATION_DETAIL":
                f_requireWidget("app/keypairs/common/commonModalKeypairs");
                break;
        }
    };

    return v_modalKeypairs;
});
