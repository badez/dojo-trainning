define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/dom",
    "dojo/dom-attr",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/_base/array",
    "dojo/_base/config",
    "app/components/ui/element/alert/ErrorWarning",
    "app/components/ui/element/input/InputValidated",
    "app/components/utils/General",
    "dojo/i18n!app/locale/nls/common",
], function(
    declare,
    lang,
    dom,
    domAttr,
    domClass,
    domStyle,
    array,
    config,
    ErrorWarning,
    InputValidated,
    GeneralUtils,
    i18n_common
) {
    return declare("FormValidation", null, {
        validate: function(inputfield) {
            var inputId = domAttr.get(inputfield, "id");
            var unique_identifier;

            if (inputId == undefined || inputId == null) {
                unique_identifier = new GeneralUtils().createUUID();
                domAttr.set(inputfield, "id", unique_identifier);
            } else
                unique_identifier = inputId;

            if (config.isIE8) {
                $("#" + unique_identifier).placeholder();

                if (domAttr.get(inputfield, "type") == "password") {
                    //placholder library replaced value by placeholder and changed all type to text
                    //need to validate for both input type text and password
                    var newInputField = lang.clone(inputfield);
                    $(newInputField).on("input change blur", function() {
                        validate();
                    });
                }
            }

            $("#" + unique_identifier).on("input change blur", function() {
                validate();
            });

            // function insertValidDom() {
            //     if (domClass.contains(inputfield, "input-valid")) {
            //         if (domAttr.get(inputfield, "data-isdropdown") === null) {
            //             var inputValidated = new InputValidated();
            //             var inputSetting = {};
            //             inputSetting.unique_identifier = unique_identifier;
            //             inputValidated.construct(inputSetting);
            //             $("#" + unique_identifier).after(inputValidated.domNode);
            //             domClass.add($("#" + unique_identifier).parent()[0], "has-feedback");
            //         }
            //     }
            // }

            function validate() {
                var error = "";
                var optional = false;
                var value = domAttr.get(inputfield, "value");
                var placeholder = domAttr.get(inputfield, "placeholder");
                var validationRules = [];
                var target = domAttr.get(inputfield, "data-target"),
                    targetData = null;

                if (domAttr.get(inputfield, "data-validation") != null)
                    validationRules = domAttr.get(inputfield, "data-validation").split(" ");

                if (target != null) {
                    if (target.indexOf("|") > -1) {
                        targetData = target.split("|");

                        target = targetData[0];
                        targetData = targetData[1];
                    }
                }

                var continueLoop = true;
                array.forEach(validationRules, function(single, i) {
                    var data = "";
                    if (single.indexOf("|") > -1) {
                        data = single.split("|");
                        single = data[0];
                        data = data[1];
                    }

                    if (continueLoop) {
                        switch (single) {
                            case "optional":
                                if (isEmpty(value)) {
                                    optional = true;
                                } else {
                                    if (config.isIE8 && value == placeholder)
                                        optional = true;
                                }

                                break;

                            case "password":
                                if (!validatePassword(value))
                                    error = i18n_common.passwordcriteria;
                                break;

                            case "accountnumber":
                                if (value == data)
                                    error = i18n_common.inputinvalidaccountnumber;

                                break;

                            case "username":
                                if (!validateAlphaNumberic(value))
                                    error = i18n_common.inputalphanumeric;
                                break;

                            case "required":
                                if (isEmpty(value)) {
                                    continueLoop = false;
                                    if (target != null) {
                                        switch (target) {
                                            case "dropdown":
                                                error = i18n_common.pleaseselectone;
                                                break;

                                            case "datepicker":
                                                error = i18n_common.pleaseselectdate;
                                                break;

                                            default:
                                                break;
                                        }
                                    } else {
                                        error = i18n_common.inputrequired;
                                    }
                                } else {
                                    if (config.isIE8 && value == placeholder) {
                                        continueLoop = false;

                                        if (target != null) {
                                            switch (target) {
                                                case "dropdown":
                                                    error = i18n_common.pleaseselectone;
                                                    break;

                                                case "datepicker":
                                                    error = i18n_common.pleaseselectdate;
                                                    break;

                                                default:
                                                    break;
                                            }
                                        } else
                                            error = i18n_common.inputrequired;
                                    }
                                }
                                break;

                            case "contact":
                                if (!validatePhone(value))
                                    error = i18n_common.inputtel;
                                break;

                            case "name":
                                if (!validateName(value))
                                    error = i18n_common.inputname;
                                break;

                            case "numberonly":
                                // value = value.replace(/[ ]/g, "");
                                if (!validateNumber(value) || isNaN(value) || value.length == 0)
                                    error = i18n_common.inputnumber;
                                else if (parseInt(value) < 0)
                                    error = i18n_common.inputpositivenumber;
                                break;

                            case "limit":
                                if (parseFloat(value) > parseFloat(data))
                                    error = i18n_common.inputlimit;
                                break;

                            case "balance":
                                if (validateCurrency(value)) {
                                    if (parseFloat(value) > parseFloat(data))
                                        error = i18n_common.inputinsufficientbalance;
                                    if (parseFloat(data) < 0)
                                        error = i18n_common.inputselectaccount;
                                }
                                break;

                            case "minamount":
                                if (validateCurrency(value)) {
                                    if (parseFloat(value) < parseFloat(data))
                                        error = i18n_common.inputminimumamount + data;
                                }
                                break;

                            case "length":
                                if (data instanceof Array) {
                                    var haveError = true;
                                    for (var i = 0; i < data.length; i++) {
                                        $.trim(value).length = data[i];
                                        if (value.length == data[i]) {
                                            haveError = false;
                                            break;
                                        }
                                    }

                                    if (haveError)
                                        error = i18n_common.inputlength + data;
                                } else {
                                    if (value.length != data)
                                        error = i18n_common.inputlength + data;
                                }
                                break;

                            case "minlength":
                                if (data instanceof Array) {
                                    var haveError = true;
                                    for (var i = 0; i < data.length; i++) {
                                        $.trim(value).length = data[i];
                                        if (value.length >= data[i]) {
                                            haveError = false;
                                            break;
                                        }
                                    }

                                    if (haveError)
                                        error = i18n_common.inputlengthmin + (parseInt(data) - 1);
                                } else {
                                    var datalenth = value.toString().split(".")[0];
                                    if (datalenth.length < data)
                                        error = i18n_common.inputlengthmin + (parseInt(data) - 1);
                                }
                                break;

                            case "minimumequal":
                                var minValue = 0;

                                if (isNaN(data))
                                    minValue = dom.byId(data).value;
                                else
                                    minValue = data;
                                //                                    console.log(minValue);
                                if (parseFloat(value) < parseFloat(minValue))
                                    error = i18n_common.inputminimumequal + minValue;
                                break;

                            case "maximumequal":
                                var maxValue = 999999999999999;

                                if (isNaN(data))
                                    maxValue = dom.byId(data).value;
                                else
                                    maxValue = data;
                                // console.log("max", maxValue);
                                if (parseFloat(value) > parseFloat(maxValue))
                                    error = i18n_common.inputmaximumequal + maxValue;
                                break;

                            case "minimum":
                                var minValue = 0;

                                if (isNaN(data))
                                    minValue = dom.byId(data).value;
                                else
                                    minValue = data;
                                // console.log(minValue);
                                if (parseFloat(value) <= parseFloat(minValue))
                                    error = i18n_common.inputminimum + minValue;
                                break;

                            case "maximum":
                                var maxValue = 999999999999999;

                                if (isNaN(data))
                                    maxValue = dom.byId(data).value;
                                else
                                    maxValue = data;
                                // console.log("max", maxValue);
                                if (parseFloat(value) >= parseFloat(maxValue))
                                    error = i18n_common.inputmaximum + maxValue;
                                break;

                            case "matching":
                                var matchInput = dom.byId(data);
                                var matchValue = matchInput.value;
                                if (value != matchValue)
                                    error = i18n_common.inputpassword;
                                else if ($.trim(value).length > 0) {
                                    if (!domClass.contains(matchInput, "input-error")) {
                                        //                                                domClass.remove(matchInput, "input-error");
                                        //                                                $("." + data).remove();
                                        domClass.add(matchInput, "input-valid");
                                    }
                                }
                                break;

                            case "different":
                                var matchInput = dom.byId(data);
                                var matchValue = matchInput.value;
                                if (value == matchValue)
                                    error = i18n_common.inputsamepassword;
                                break;

                            case "maxlength":
                                if (data instanceof Array) {
                                    var haveError = true;
                                    for (var i = 0; i < data.length; i++) {
                                        $.trim(value).length = data[i];
                                        if (value.length <= data[i]) {
                                            haveError = false;
                                            break;
                                        }
                                    }

                                    if (haveError)
                                        error = i18n_common.inputlengthmax + (parseInt(data));
                                } else {
                                    if (value.length > data)
                                        error = i18n_common.inputlengthmax + (parseInt(data));
                                }
                                break;

                            case "minlength":
                                break;

                            case "email":
                                if (!validateEmail(value))
                                    error = i18n_common.inputemail;
                                break;

                            case "decimal":
                                var decimalPoint = 0;
                                if (parseFloat(data) !== "NaN") {
                                    decimalPoint = data;
                                } else {
                                    decimalPoint = config[data];
                                }

                                if (!validateDecimalV2(value, decimalPoint)) {
                                    error = i18n_common.inputnumber;
                                }
                                break;

                            case "amount":
                                var decimalPoint = config.amountDecimal;

                                if (!validateDecimal(value, decimalPoint)) {
                                    error = i18n_common.inputamount;
                                }
                                break;

                            case "interestrate":
                                var decimalPoint = config.interestRateDecimal;

                                if (!validateDecimal(value, decimalPoint)) {
                                    error = i18n_common.inputnumber;
                                }
                                break;

                            case "percentage":
                                var decimalPoint = config.percentageDecimal;

                                if (!validateDecimal(value, decimalPoint)) {
                                    error = i18n_common.inputnumber;
                                }
                                break;

                            case "exchangerate":
                                var decimalPoint = config.exchangeRateDecimal;

                                if (!validateDecimal(value, decimalPoint)) {
                                    error = i18n_common.inputnumber;
                                }
                                break;

                            case "revolvingmax":
                                error = i18n_common.inputmaximum + data;
                                break;

                            default:
                                break;
                        }
                    }
                });

                //check emoji
                // var noEmoji = /[\u1F60-\u1F64]|[\u2702-\u27B0]|[\u1F68-\u1F6C]|[\u1F30-\u1F70]{\u2600-\u26ff]/g;
                // var noEmoji = /([\uE000-\uF8FF]|\uD83C[\uDF00-\uDFFF]|\uD83D[\uDC00-\uDDFF])/g;
                var china = new RegExp("[\u4E00-\u9FFF|\u2FF0-\u2FFF|\u31C0-\u31EF|\u3200-\u9FBF|\uF900-\uFAFF]");

                if (value.replace(/[\x00-\x7F]/g, "").length != 0) {
                    if (!china.test(value))
                        error = i18n_common.inputemoticon;
                }

                function isEmpty(_value) {
                    if (_value.length == 0) {
                        return true;
                    } else {
                        return false;
                    }
                }

                function validateNumber(number) {
                    //number = number.replace(/[+ -]/g, "");
                    var re = /^\d+$/;
                    return re.test(number);
                }

                function validateDecimal(number, decimalPoint) {
                    var re = new RegExp("^[0-9]*(?:\\.\\d{" + decimalPoint + "," + decimalPoint + "})?$");
                    return re.test(number);
                }

                function validateDecimalV2(number, decimalPoint) {
                    // no point continue checking since input is empty
                    if (number === "") {
                        return true;
                    }

                    var re;
                    if (decimalPoint === 0) {
                        re = /^\-?\d+$/;
                    } else if (decimalPoint < 0) {
                        re = /^\-?\d+(\.\d\d*)?$/;
                    } else {
                        re = /^\-?\d+(\.\d{ + decimalPoint + })?$/;
                    }

                    return re.test(number);
                }

                function validateCurrency(number) {
                    var re = new RegExp("^[0-9]*(?:\\.\\d{" + config.amountDecimal + "," + config.amountDecimal + "})?$");
                    return re.test(number);
                }

                function validatePhone(phone) {
                    phone = phone.replace(/[+ -]/g, "");
                    var re = /^\+?(?:[0-9] ?){6,14}[0-9]$/;
                    return re.test(phone);
                }

                function validateAlphaNumberic(data) {
                    var re = /^(?:[0-9]+[a-z]|[a-z]+[0-9])[a-z0-9]*$/i;
                    return re.test(data);
                }

                function validatePassword(data) {
                    var re = /(?=.*\d)(?=.*[!@#$%^&*]+)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;
                    return re.test(data);
                }

                function validateEmail(email) {
                    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    return re.test(email);
                }

                function validateName(name) {
                    var re = /^[A-Za-z\s]{1,}[\.]{0,1}[\']{0,1}[\@]{0,1}[A-Za-z\s]{0,}$/;
                    return re.test(name);
                }


                //placeholder plugin for IE set the temporary value as placeholder
                //in IE need to do some checking if value is same with placeholder
                var isValuePlaceholderSame = true;

                if ((config.isIE8 && value != placeholder) || !config.isIE8) {
                    isValuePlaceholderSame = false;
                }

                if (!optional || (optional && $.trim(value).length > 0 && !isValuePlaceholderSame)) {
                    function checkError() {
                        domClass.remove(inputfield, "input-error");
                        domClass.remove(inputfield, "input-valid");

                        if (domAttr.get(inputfield, "data-isdropdown") !== null) {
                            domClass.remove($(inputfield).prev()[0], "input-error");
                        }

                        if (targetData != null) {
                            domClass.remove(dom.byId(targetData), "input-error");
                            domClass.remove(dom.byId(targetData), "input-valid");
                        }


                        if (error != "") {
                            $("." + unique_identifier).remove();

                            if (domAttr.get($("#" + unique_identifier)[0], "data-isdropdown") === null) {
                                if ($("#" + unique_identifier).next().length > 0) {
                                    domStyle.set($("#" + unique_identifier).next()[0], "display", "none");
                                }
                            }

                            var errorWarning = new ErrorWarning();
                            var errorSetting = {};
                            errorSetting.unique_identifier = unique_identifier;
                            errorSetting.errorArray = error;
                            errorWarning.p_construct(errorSetting);
                            $("#" + unique_identifier).after(errorWarning.domNode);

                            if (!domClass.contains(inputfield, "input-error")) {
                                if (domAttr.get(inputfield, "data-isdropdown") !== null) {
                                    domClass.add($(inputfield).prev()[0], "input-error");
                                }

                                domClass.add(inputfield, "input-error");

                                if (targetData != null) {
                                    domClass.add(dom.byId(targetData), "input-error");
                                }
                            }
                        } else {
                            $("." + unique_identifier).remove();

                            if (domAttr.get($("#" + unique_identifier)[0], "data-isdropdown") === null) {
                                if ($("#" + unique_identifier).next().length > 0) {
                                    domStyle.set($("#" + unique_identifier).next()[0], "display", "none");
                                }
                            }

                            if (!domClass.contains(inputfield, "input-valid")) {
                                domClass.add(inputfield, "input-valid");

                                if (targetData != null) {
                                    domClass.add(dom.byId(targetData), "input-valid");
                                }

                                // insertValidDom();
                            }
                        }
                    }

                    if ($("#" + unique_identifier).is(":enabled")) {
                        if (domAttr.get(inputfield, "type") == "hidden") {
                            domAttr.set(inputfield, "type", "text");

                            if ($("#" + unique_identifier).is(":visible")) {
                                checkError();
                            }
                            domAttr.set(inputfield, "type", "hidden");
                        } else {
                            if ($("#" + unique_identifier).is(":visible")) {
                                checkError();
                            }
                        }
                    }
                } else if (optional && $.trim(value).length == 0) {
                    $("." + unique_identifier).remove();
                    if (!domClass.contains(inputfield, "input-valid")) {
                        domClass.add(inputfield, "input-valid");
                    }
                } else {
                    if (!domClass.contains(inputfield, "input-valid")) {
                        domClass.add(inputfield, "input-valid");
                    }
                }

            }
        },
        triggerError: function(inputNode, errorMessage) {
            if (domClass.contains(inputNode, "input-valid")) {
                domClass.remove(inputNode, "input-valid");
                domClass.add(inputNode, "input-error");
            }
            unique_identifier = new GeneralUtils().createUUID();
            domAttr.set(inputNode, "id", unique_identifier);

            var errorWarning = new ErrorWarning();
            var errorSetting = {};
            errorSetting.unique_identifier = unique_identifier;
            errorSetting.errorArray = errorMessage;
            errorWarning.p_construct(errorSetting);
            $("#" + unique_identifier).after(errorWarning.domNode);
        },
        removeError: function(inputNode) {
            if (domClass.contains(inputNode, "input-error")) {
                domClass.remove(inputNode, "input-error");
                domClass.add(inputNode, "input-valid");
            }
        }
    });
});
