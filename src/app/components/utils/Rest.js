define([
    "dojo/_base/array",
    "dojo/_base/config",
    "dojo/_base/declare",
    "dojo/dom-style",
    "dojo/Deferred",
    "dojo/i18n!app/locale/nls/common",
    "dojo/i18n!app/locale/nls/error",
    "dojo/request/xhr",
    "dojo/request",
    "dojo/store/JsonRest",
    "dojo/topic"
], function(
    v_array,
    v_config,
    v_declare,
    v_domStyle,
    v_Deferred,
    v_i18n_common,
    v_i18n_error,
    v_xhr,
    v_Request,
    v_JsonRest,
    v_topic
) {
    return v_declare("app.components.utils.Rest", null, {
        activeLoadingBlocks: {},
        p_setupPackage: function(v_package) {
            v_package.userid = $.jStorage.get("app.login.username");
            v_package.baseFunctionCode = $.jStorage.get("current.functionCode");
            v_package.baseModuleCode = $.jStorage.get("current.moduleCode");

            return v_package;
        },
        p_getMenu: function(v_api, v_url, v_package, v_loading) {
            var v_context = this;
            var v_menu = v_context.p_get(v_url);
            var v_returnObj = new v_Deferred();
            v_returnObj.promise;

            v_menu.then(function(v_response) {
                if (v_config.isExperimental) {
                    v_returnObj.resolve(v_response);
                } else if (v_response.dynamic) {
                    v_context.p_postapi(v_api, v_url, v_package, v_loading).then(
                        function(v_response) {
                            v_returnObj.resolve(v_response);
                        });
                } else {
                    v_returnObj.resolve(v_response);
                }
            });

            return v_returnObj;
        },
        p_get: function(v_dataUrl) {
            var v_RequestParam = v_dataUrl;

            var v_deferred = new v_JsonRest({
                target: v_dataUrl
            });
            return v_deferred.get();
        },
        p_getapi: function(v_apiobj, v_prototypeurl, v_package) {
            var v_baseURL = v_config.cibaseurl;
            var v_RequestURL = v_baseURL + v_apiobj.p_api;
            var v_deferred;

            if (v_package != undefined) {
                v_package.userid = $.jStorage.get("app.login.username");
                v_package = JSON.stringify(v_package);

                if (!v_config.isExperimental) {
                    v_deferred = new v_Deferred();
                    v_deferred.resolve({ errorcode: "0" });
                    window.open(v_RequestURL + "?param=" + v_package, '_blank');
                    return v_deferred;
                }
            } else {
                v_package = "";
            }

            if (v_config.isExperimental) {
                v_RequestURL = v_prototypeurl;
                if (v_prototypeurl == null) {
                    v_RequestURL = "assets/data/postprototype.json";
                }
            }

            v_deferred = v_xhr(v_RequestURL, {
                handleAs: "json",
                headers: {
                    "Content-Type": "application/json"
                }
            });
            return v_deferred;
        },
        p_downloadapi: function(v_apiobj, v_prototypeurl, v_package) {
            var v_baseURL = v_config.cibaseurl;
            var v_RequestURL = v_baseURL + v_apiobj.p_api;
            var v_deferred;

            if (v_package != undefined) {
                v_package.userid = $.jStorage.get("app.login.username");

                if (!v_config.isExperimental) {
                    var v_params = Object.keys(v_package).map(function(v_key) {
                        return encodeURIComponent(v_key) + '=' + encodeURIComponent(v_package[v_key]);
                    }).join('&');

                    v_deferred = new v_Deferred();
                    v_deferred.resolve({ errorCode: "0" });
                    window.open(v_RequestURL + "?" + v_params, '_blank');
                    return v_deferred;
                }
            } else {
                v_package = "";
            }

            if (v_config.isExperimental) {
                v_RequestURL = v_prototypeurl;
                if (v_prototypeurl == null) {
                    v_RequestURL = "assets/data/postprototype.json";
                }
            }

            v_deferred = v_xhr(v_RequestURL, {
                handleAs: "json",
                headers: {
                    "Content-Type": "application/json"
                }
            });
            return v_deferred;
        },
        p_postapi: function(v_apiobj, v_prototypeurl, v_package, v_loading) {
            var v_context = this;
            var p_type = this;
            var v_activeBlock;

            v_topic.publish("navigation.block.active", function(v_returnObj) {
                v_activeBlock = v_returnObj;
            });
            var v_id;
            var v_loadingBlocks;
            if (v_activeBlock) {
                v_id = v_activeBlock.domNode.id;
                v_loadingBlocks = v_query('[id*=\"loadingblock\"]', v_activeBlock.domNode);
                if (!p_type.activeLoadingBlocks[v_id]) {
                    v_domStyle.set(v_loadingBlocks[v_loadingBlocks.length - 1], "display", "block");
                    p_type.activeLoadingBlocks[v_id] = 1;
                } else {
                    p_type.activeLoadingBlocks[v_id] = p_type.activeLoadingBlocks[v_id] + 1;
                }
            }
            var v_baseURL = v_config.cibaseurl;

            var v_RequestURL = v_baseURL + v_apiobj.p_api;
            var v_deferred;

            if (v_apiobj.p_delegate == undefined || v_apiobj.p_delegate == null) {
                v_apiobj.p_delegate = "";
            }

            if (v_package != null) {
                v_package.userid = $.jStorage.get("app.login.username");
                v_package = p_type.p_cleanUp(v_package);
            } else if (v_package == undefined || v_package == null) {
                v_package = {};
                v_package.data = "";
            }

            if (v_config.isExperimental) {
                //Offline version
                if (v_prototypeurl == null) {

                    v_RequestURL = "assets/data/postprototype.json";
                    v_deferred = v_xhr(v_RequestURL, {
                        handleAs: "json",
                        headers: {
                            "Content-Type": "application/json"
                        }
                    });
                    //return deferred;
                } else {
                    v_RequestURL = v_prototypeurl;
                    v_deferred = v_xhr(v_RequestURL, {
                        handleAs: "json",
                        headers: {
                            "Content-Type": "application/json"
                        }
                    });
                }
            } else {
                //Online version
                switch (v_apiobj.p_delegate) {
                    case "login":
                    case "forgotpassword":
                    case "resetpassword":
                    case "firsttime":
                    case "register":
                    case "ftltac":
                        v_deferred = v_xhr(v_RequestURL, {
                            handleAs: "json",
                            method: "POST",
                            headers: {
                                "Authorization": "auth " + v_package.data
                            }
                        });
                        break;
                    case "logout":
                    case "timeout":
                    case "changepassword":
                        v_deferred = v_xhr(v_RequestURL, {
                            handleAs: "json",
                            method: "POST",
                            headers: {
                                "Authorization": "token " + $.jStorage.get("app.login.token") + ":" + v_package.data
                            }
                        });
                        break;
                    default:
                        v_deferred = v_xhr(v_RequestURL, {
                            handleAs: "json",
                            method: "POST",
                            headers: {
                                "Authorization": "token " + $.jStorage.get("app.login.token"),
                                "Content-Type": "application/json"
                            },
                            data: JSON.stringify(p_type.p_setupPackage(v_package))
                        });
                        break;
                }
            }

            var v_filteredDeferred = new v_Deferred();
            v_filteredDeferred.promise;
            v_deferred.response.then(function(v_response) {
                if (v_response.data) {
                    if (v_response.data.errorCode === "1") {
                        v_topic.publish("navigation.modal.open", "ALERT", {
                            p_type: "ERROR",
                            p_title: v_i18n_common.error,
                            p_desc: v_i18n_error.somethingwentwrong
                        });
                    }
                }
                //renew token
                $.jStorage.set("app.login.token", v_response.getHeader('token'));

                var v_filteredData;

                switch (v_apiobj.p_delegate) {
                    case "login":
                    case "logout":
                    case "timeout":
                    case "forgotpassword":
                    case "resetpassword":
                    case "firsttime":
                    case "ftltac":
                    case "register":
                    case "changepassword":
                        var v_tempData = v_response.data;
                        v_filteredData = v_response;
                        v_filteredData.data = v_tempData;
                        break;
                    default:
                        v_filteredData = p_type.p_cleanUp(v_response.data);
                        break;
                }
                v_filteredDeferred.resolve(v_filteredData);
            }, function(v_err) {
                switch (v_err.response.status) {
                    case 401:
                        switch (v_apiobj.p_delegate) {
                            case "resetpassword":
                            case "firsttime":
                            case "ftltac":
                            case "register":
                                v_topic.publish("navigation.modal.open", "ALERT", {
                                    p_type: "ERROR",
                                    p_title: v_i18n_common.error,
                                    p_desc: v_i18n_error.somethingwentwrong
                                });
                                break;
                            case "forgotpassword":
                                v_topic.publish("navigation.modal.open", "ALERT", {
                                    p_type: "ERROR",
                                    p_title: v_i18n_common.error,
                                    p_desc: v_i18n_error.ie8desc
                                });
                                break;
                            case "changepassword":
                                v_topic.publish("navigation.modal.open", "ALERT", {
                                    p_type: "ERROR",
                                    p_title: v_i18n_common.error,
                                    p_desc: v_i18n_error.ie8desc
                                });
                                break;
                            case "login":
                                var v_errors = v_err.response.text.split(":");
                                var v_errmessage = v_i18n_error["err" + v_errors[0]];
                                if (!v_errmessage) {
                                    v_errmessage = v_i18n_error.somethingwentwrong;
                                }

                                v_topic.publish("navigation.modal.open", "ALERT", {
                                    p_type: "ERROR",
                                    p_title: v_i18n_common.error,
                                    p_desc: v_errmessage
                                });
                                break;
                            case "timeout":
                                v_topic.publish("analytics.event", "logout");
                                v_topic.publish("navigation.modal.open", "LOGIN");

                                $.jStorage.flush();
                                $.jStorage.set("app.login", false);
                                sessionStorage.clear();
                                break;
                            case "logout":
                                v_topic.publish("navigation.modal.open", "LOGIN");
                                break;
                            default:
                                v_topic.publish("navigation.modal.open", "ALERT", {
                                    p_type: "ERROR",
                                    p_title: v_i18n_common.error,
                                    p_desc: v_i18n_error.needtorelogin
                                });
                                break;
                        }
                        break;
                    case 404:
                        if (v_apiobj.p_delegate != undefined) {
                            switch (v_apiobj.p_delegate) {
                                case "login":
                                    v_topic.publish("navigation.modal.open", "ALERT", {
                                        p_type: "ERROR",
                                        p_title: v_i18n_common.error,
                                        p_desc: v_i18n_error.wrongusernamepassword
                                    });
                                    break;
                                default:
                                    v_topic.publish("navigation.modal.open", "ALERT", {
                                        p_type: "transaction.error",
                                        p_title: v_i18n_common.error,
                                        p_desc: v_i18n_error.somethingwentwrong
                                    });
                                    break;
                            }
                        }
                        break;
                    case 422:
                        $.jStorage.set("err-code", v_err.response.getHeader('err-code'));
                        v_topic.publish("navigation.modal.open", "ALERT");
                        break;
                    default:
                        switch (v_apiobj.p_delegate) {
                            case "timeout":
                                v_topic.publish("analytics.event", "logout");
                                v_topic.publish("navigation.modal.open", "LOGIN");

                                $.jStorage.flush();
                                $.jStorage.set("app.login", false);
                                sessionStorage.clear();
                                break;
                            case "logout":
                                topic.publish("navigation.page", "login");
                                break;
                            default:
                                v_topic.publish("navigation.modal.open", "ALERT", {
                                    p_type: "transaction.error",
                                    p_title: v_i18n_common.error,
                                    p_desc: v_i18n_error.somethingwentwrong
                                });
                                break;
                        }
                        break;
                }
            });

            v_deferred.always(function() {
                if (v_id) {
                    if (p_type.activeLoadingBlocks[v_id]) {
                        setTimeout(function() {
                            p_type.activeLoadingBlocks[v_id] = p_type.activeLoadingBlocks[v_id] - 1;
                            if (p_type.activeLoadingBlocks[v_id] < 1) {
                                if (v_loadingBlocks) {
                                    v_domStyle.set(v_loadingBlocks[v_loadingBlocks.length - 1], "display", "none");
                                }
                                delete p_type.activeLoadingBlocks[v_id];
                            }
                        }, 1000);
                    }
                }
            });

            return v_filteredDeferred;
        },
        p_postapiprogress: function(v_apiobj, v_prototypeurl, v_package, v_progressFunction) {

            var v_type = this;

            var v_deferred = new v_Deferred();
            v_deferred.promise;
            var v_baseURL = v_config.cibaseurl;
            var v_RequestURL = "";
            //clean up v_package
            v_package = v_type.p_cleanUp(v_package);

            if (v_config.isExperimental) {
                if (v_prototypeurl == null) {
                    v_RequestURL = "assets/data/postprototype.json";
                } else {
                    v_RequestURL = v_prototypeurl;
                }
            } else {
                v_RequestURL = v_baseURL + v_apiobj.p_api;
            }

            var v_xhttp = new XMLHttpRequest();

            v_xhttp.onreadystatechange = function() {
                if (v_xhttp.readyState == 4 && v_xhttp.status == 200) {
                    var v_response = JSON.parse(v_xhttp.response);
                    if (v_response.errorCode == "0") {
                        v_deferred.resolve(v_response);
                    } else {
                        v_deferred.reject(new Error('xhttp.status ' + v_xhttp.status + ', xhttp.response ' + JSON.parse(v_xhttp.response)));
                    }
                } else if (v_xhttp.readyState == 4 && v_xhttp.response.errorCode != "0") {
                    v_deferred.reject(new Error('xhttp.status ' + v_xhttp.status + ', xhttp.response ' + JSON.parse(v_xhttp.response)));
                }
            };
            v_xhttp.open("POST", v_RequestURL, true);
            v_xhttp.setRequestHeader("Authorization", "token " + $.jStorage.get("app.login.token"));
            v_xhttp.setRequestHeader("Content-Type", "application/json");
            v_xhttp.upload.addEventListener("progress", function(v_evt) {
                if (v_evt.lengthComputable) {
                    var _percentComplete = v_evt.loaded / v_evt.total;
                    _percentComplete = parseInt(_percentComplete * 100);
                    v_progressFunction(_percentComplete);
                }
            }, true);
            v_xhttp.send(JSON.stringify(v_type.p_setupPackage(v_package)));
            return v_deferred
        },
        p_sanitize: function(v_txt) {
            var v_validHTMLTags = /^(?:javascript|a|abbr|acronym|address|applet|area|article|aside|audio|b|base|basefont|bdi|bdo|bgsound|big|blink|blockquote|body|br|button|canvas|caption|center|cite|code|col|colgroup|data|datalist|dd|del|details|dfn|dir|div|dl|dt|em|embed|fieldset|figcaption|figure|font|footer|form|frame|frameset|h1|h2|h3|h4|h5|h6|head|header|hgroup|hr|html|i|iframe|img|input|ins|isindex|kbd|keygen|label|legend|li|link|listing|main|map|mark|marquee|menu|menuitem|meta|meter|nav|nobr|noframes|noscript|object|ol|optgroup|option|output|p|param|plaintext|pre|progress|q|rp|rt|ruby|s|samp|script|section|select|small|source|spacer|span|strike|strong|style|sub|summary|sup|table|tbody|td|textarea|tfoot|th|thead|time|title|tr|track|tt|u|ul|var|video|wbr|xmp)$/i;
            var v_protos = document.body.constructor === window.HTMLBodyElement;
            if (v_txt != null) {
                var // This regex normalises anything between quotes
                    v_normaliseQuotes = /=(["'])(?=[^\1]*[<>])[^\1]*\1/g,
                    v_normaliseFn = function($0, v_q, v_sym) {
                        return $0.replace(/</g, '&lt;').replace(/>/g, '&gt;');
                    },
                    v_replaceInvalid = function($0, v_tag, v_off, v_txt) {
                        // Is it a valid tag?
                        var v_invalidTag = v_protos && document.createElement(v_tag) instanceof HTMLUnknownElement || !v_validHTMLTags.test(v_tag);
                        // Is the tag complete?
                        var v_isComplete = v_txt.slice(v_off + 1).search(/^[^<]+>/) > -1;
                        var v_rn = !v_invalidTag && v_isComplete ? '&lt;' + v_tag : $0;
                        return v_rn;
                    };
                var v_temp = v_txt;
                v_txt = v_txt.toString();
                v_txt = v_txt.replace(v_normaliseQuotes, v_normaliseFn).replace(/<(\w+)/g, v_replaceInvalid);
                if (v_txt === v_temp.toString()) {
                    v_txt = v_temp;
                    return v_txt;
                }

                var v_tmp = document.createElement("DIV");
                v_tmp.innerHTML = v_txt;
                return v_tmp.textContent || v_tmp.innerText;
            } else {
                return v_txt;
            }
        },
        p_cleanUp: function(v_data) {
            var p_type = this;
            for (var v_key in v_data) {
                if (v_data.hasOwnProperty(v_key)) {
                    if (v_data[v_key] instanceof Array) {
                        v_array.forEach(v_data[v_key], function(v_single) {
                            if (v_single instanceof Array || v_single instanceof Object)
                                p_type.p_cleanUp(v_single);
                            else {
                                v_single = p_type.p_sanitize(v_single);
                            }
                        });
                    } else if (v_data[v_key] instanceof Object) {
                        for (var v_insideKey in v_data[v_key]) {
                            if (v_data[v_key].hasOwnProperty(v_insideKey)) {
                                if (v_data[v_key][v_insideKey] instanceof Array || v_data[v_key][v_insideKey] instanceof Object)
                                    p_type.p_cleanUp(v_data[v_key][v_insideKey]);
                                else {
                                    v_data[v_key][v_insideKey] = p_type.p_sanitize(
                                        v_data[v_key][v_insideKey]);
                                }
                            }
                        }
                    } else {
                        v_data[v_key] = p_type.p_sanitize(v_data[v_key]);
                    }
                }
            }
            return v_data;
        }
    });
});
