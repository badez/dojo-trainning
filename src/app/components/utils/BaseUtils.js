define([
    "dojo/_base/array",
    "dojo/_base/config",
    "dojo/_base/declare",
    "dojo/dom-attr",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/html",
    "dojo/on",
    "dojo/query",
    "dojo/topic",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "./General"
], function (
    v_array,
    v_config,
    v_declare,
    v_domAttr,
    v_domClass,
    v_domConstruct,
    v_html,
    v_on,
    v_query,
    v_topic,
    v_WidgetBase,
    v_TemplatedMixin,
    v_GeneralUtils
) {
    return v_declare("app.components.utils.BaseUtils", [v_WidgetBase, v_TemplatedMixin], {
        destroy: function () {
            var v_context = this;
            var v_domNode = v_context.domNode;
            if (v_domNode !== null) {
                v_topic.publish("topic.handlers.destroy", v_domAttr.get(v_domNode, "id"));
            }
            this.inherited(arguments);
        },
        createUUID: function (v_node) {
            var v_context = this;
            var v_uuid = new v_GeneralUtils().createUUID();
            if (v_node) {
                v_domAttr.set(v_node, "id", v_uuid);
            }
            return v_uuid;
        },
        destroyCheckbox: function (node) {
            var v_context = this;

            v_context._destroyCheckboxRadio(node);
        },
        destroyRadio: function (node) {
            var v_context = this;

            v_context._destroyCheckboxRadio(node);
        },
        _destroyCheckboxRadio: function (node) {
            $(node).iCheck("destroy");
            v_domConstruct.destroy(node);
        },
        _resetRadioCheckbox: function (name) {
            var v_context = this;
            var domNode = this.domNode;

            v_query("input[name='" + name + "']", domNode).forEach(function (node) {
                $(node).iCheck("uncheck");
            });

            var theSingleNode = $("input[name='" + name + "']")[0];
            $(theSingleNode).iCheck("check");
        },
        getRadioValue: function (name) {
            var v_context = this;
            var domNode = this.domNode;

            return v_query('input[name="' + name + '"]:checked', domNode)[0].value;
        },
        getCheckboxValue: function (name, containerNode) {
            var v_context = this;
            var domNode = this.domNode;

            if (!containerNode) {
                containerNode = domNode;
            }

            var arr = [];

            v_query('input[name="' + name + '"]:checked', containerNode).forEach(function (node) {
                arr.push(node.value);
            });

            return arr;
        },
        _checkboxRadioEventHandler: function (node, eventName, v_callback) {
            // Wrapper for both checkbox and radio events
            var v_context = this;
            var domNode = this.domNode;

            switch (eventName) {
                case "check":
                    $(node).v_on("ifChecked", function () {
                        v_callback();
                    });
                    break;

                case "uncheck":
                    $(node).v_on("ifUnchecked", function () {
                        v_callback();
                    });
                    break;

                case "click":
                    $(node).v_on("ifClicked", function () {
                        $(node).iCheck("toggle");
                        v_callback(node.checked);
                    });
                    break;
                case "toggle":
                    $(node).iCheck("toggle");
                    break;
                case "enable":
                    $(node).iCheck("enable");
                    break;
                case "disable":
                    $(node).iCheck("disable");
                    break;
            }
        },
        radioEventHandler: function (radioNode, eventName, v_callback) {
            var v_context = this;
            v_context._checkboxRadioEventHandler(radioNode, eventName, v_callback);
        },
        checkboxEventHandler: function (checkboxNode, eventName, v_callback) {
            var v_context = this;

            v_context._checkboxRadioEventHandler(checkboxNode, eventName, v_callback);
        },
        _checkboxRadioToggle: function (node, state) {
            var v_context = this;

            switch (state) {
                case "check":
                case "uncheck":
                case "disable":
                case "enable":
                    $(node).iCheck(state);
                    break;
            }
        },
        checkboxToggle: function (node, state) {
            var v_context = this;

            v_context._checkboxRadioToggle(node, state);
        },
        checkboxToggleAll: function (name, state) {
            var v_context = this;
            var domNode = this.domNode;

            var checkboxList = v_query('input[name="' + name + '"]', domNode);

            switch (state) {
                case "check":
                    checkboxList.forEach(function (node) {
                        $(node).iCheck("check");
                    });
                    break;

                case "uncheck":
                    checkboxList.forEach(function (node) {
                        $(node).iCheck("uncheck");
                    });
                    break;
            }
        },
        radioToggle: function (node, state) {
            var v_context = this;

            v_context._checkboxRadioToggle(node, state);
        },
        createMask: function (v_inputNode, maskObject) {
            var maskType = maskObject.type;
            $(v_inputNode).mask(v_config[maskType], {
                reverse: true
            });
            v_domAttr.set(v_inputNode, "placeholder", v_config[maskType]);
        },
        createNumberInput: function (v_inputNode, isDecimal) {
            var oldValue = v_inputNode.value;
            v_on(v_inputNode, "keyup", function (e) {
                var newValue = v_inputNode.value;
                var re = /^\d*$/;
                if (isDecimal) {
                    re = /^\d*(\.\d*)?$/;
                }
                if (re.test(newValue)) {
                    oldValue = newValue;
                } else {
                    v_inputNode.value = oldValue;
                    v_on.emit(v_inputNode, "change", {
                        bubbles: true,
                        cancelable: true
                    });
                }
            });
        },
        createDecimalInput: function (v_inputNode, _decimalPoint) {
            var decimalPoint = 0;
            if (typeof _decimalPoint == 'number') {
                decimalPoint = _decimalPoint;
            } else {
                decimalPoint = v_config[_decimalPoint];
            }
            v_on(v_inputNode, "keyup", function (e) {
                //check if input value has '.' and if input key in is number, e(exponent) or -(negative) character
                if (v_inputNode.value.indexOf('.') != -1 && (v_inputNode.value.length - v_inputNode.value.indexOf('.') > (decimalPoint + 1)) && ((e.which >= 48 && e.which <= 57) || (e.which >= 97 && e.which <= 105) || e.which == 69 || e.which == 189 || e.which == 109)) {
                    v_inputNode.value = v_inputNode.value.substr(0, v_inputNode.value.indexOf('.') + (decimalPoint + 1));
                }
            });
            v_on(v_inputNode, "blur", function (e) {
                if (v_inputNode.value.indexOf('.') == -1) {
                    if (0 == v_inputNode.value.length) {
                        v_inputNode.value = 0 + "." + Array(decimalPoint + 1).join("0");
                    } else {
                        v_inputNode.value = v_inputNode.value + "." + Array(decimalPoint + 1).join("0");
                    }

                }
                v_inputNode.value = v_inputNode.value.substr(0, v_inputNode.value.indexOf('.') + (decimalPoint + 1));
            });
        },
        autoInputDecimal: function (v_inputNode, _decimalPoint) {
            var decimalPoint = 0;
            if (typeof _decimalPoint == 'number') {
                decimalPoint = _decimalPoint;
            } else {
                decimalPoint = v_config[_decimalPoint];
            }

            v_on(v_inputNode, "blur", function (e) {
                if (v_inputNode.value.indexOf('.') == -1) {
                    if (0 == v_inputNode.value.length) {
                        v_inputNode.value = 0 + "." + Array(decimalPoint + 1).join("0");
                    } else {
                        v_inputNode.value = v_inputNode.value + "." + Array(decimalPoint + 1).join("0");
                    }

                } else {
                    v_inputNode.value = v_inputNode.value + Array((decimalPoint + 1) - (v_inputNode.value.length - v_inputNode.value.indexOf('.') - 1)).join("0");
                }
            });
        },
        p_initValidation: function (v_containerNode) {
            v_query("input[data-validation],textarea[data-validation]:not([readonly])", v_containerNode).forEach(function (v_node) {
                require(["app/components/utils/FormValidation"], function (v_FormValidation) {
                    new v_FormValidation().validate(v_node);
                });
            });
        },
        p_checkingValidation: function (v_containerNode, v_callback) {
            var v_context = this;

            var v_valid = true;
            v_query("input[data-validation],textarea[data-validation][id]:not([readonly])", v_containerNode).forEach(function (v_node) {
                if ($(v_node).is(":enabled")) {
                    if (v_domAttr.get(v_node, "type") == "hidden") {
                        var v_nodeValue = v_domAttr.get(v_node, "value");
                        var v_nodeId = v_context.createUUID();

                        if (v_domAttr.get(v_node, "name") != "tac" && v_domAttr.get(v_node, "name") != "declare") {
                            v_domAttr.set(v_node, "data-isdropdown", "");
                        }

                        $(v_node).after('<input id="' + v_nodeId + '" value="' + v_nodeValue + '">');

                        var v_tempNode = $('#' + v_nodeId);

                        if (v_tempNode.is(":visible")) {
                            $(v_node).trigger("change");

                            if (!v_domClass.contains(v_node, "input-valid")) {
                                v_valid = false;
                            }
                        }

                        v_tempNode.remove();
                    } else {
                        if ($(v_node).is(":visible")) {
                            $(v_node).trigger("change");

                            if (!v_domClass.contains(v_node, "input-valid")) {
                                v_valid = false;
                            }
                        }
                    }
                }
            });

            if (v_callback) {
                v_callback(v_valid);
            }
        },
        p_removeAllValidation: function (v_containerNode, v_callback) {
            var v_context = this;
            var v_radioBtn = [];

            v_query("input[data-validation],textarea[data-validation]:not([readonly])", v_containerNode).forEach(function (v_node) {
                var v_inputId = v_domAttr.get(v_node, "id");
                v_topic.publish("reset.field", v_inputId);

                if (v_inputId != undefined || v_inputId != null) {
                    v_domAttr.set(v_node, "value", "");
                    $("." + v_inputId).remove();

                    if (v_domClass.contains(v_node, "input-error")) {
                        v_domClass.remove(v_node, "input-error");
                    }

                    if (v_domClass.contains(v_node, "input-valid")) {
                        v_domClass.remove(v_node, "input-valid");
                    }

                    if (v_domAttr.has(v_node, "data-date-format")) {
                        v_domAttr.set(v_node, "value", "");
                    }

                    if (v_domAttr.get($("#" + v_inputId)[0], "data-isdropdown") === null) {
                        if ($("#" + v_inputId).next().length > 0) {
                            domStyle.set($("#" + v_inputId).next()[0], "display", "block");
                        }
                    } else {
                        v_domClass.remove($(v_node).prev()[0], "input-error");
                    }
                }
            });

            v_query("input", v_containerNode).forEach(function (v_node) {
                if (v_domAttr.has(v_node, "readonly")) {
                    if (v_domAttr.has(v_node, "data-date-format")) {
                        v_domAttr.set(v_node, "value", "");
                    }
                } else {
                    if (v_domAttr.get(v_node, "type") == "radio") {
                        var radioName = v_domAttr.get(v_node, "name");
                        var found = false;
                        for (var i = 0; i < v_radioBtn.length; i++) {
                            if (v_radioBtn[i] == radioName) {
                                found = true;
                            }
                        }

                        if (!found) {
                            v_radioBtn.push(radioName);
                        }
                    }
                }
            });

            for (var i = 0; i < v_radioBtn.length; i++) {
                var radioDom = v_query("input[name=" + v_radioBtn[i] + "]", v_containerNode)
                if (radioDom.length > 0) {
                    $(radioDom[0]).trigger("click");
                }
            }

            v_query("input[type='checkbox']", v_containerNode).forEach(function (v_node) {
                v_context._resetRadioCheckbox(v_domAttr.get(v_node, "name"));
            });

            v_query("input[type='radio']", v_containerNode).forEach(function (v_node) {
                v_context._resetRadioCheckbox(v_domAttr.get(v_node, "name"));
            });

            if (v_callback) {
                v_callback();
            }
        },
        p_removeValidation: function (v_inputfield) {
            var v_inputId = v_domAttr.get(v_inputfield, "id");

            if (v_inputId != undefined || v_inputId != null) {
                v_domAttr.set(v_inputfield, "value", "");
                $("." + v_inputId).remove();

                if (v_domClass.contains(v_inputfield, "input-error")) {
                    v_domClass.remove(v_inputfield, "input-error");
                }

                if (v_domClass.contains(v_inputfield, "input-valid")) {
                    v_domClass.remove(v_inputfield, "input-valid");
                }
            }
        },
        startup: function () {
            this.inherited(arguments);
            var v_context = this;
            var domNode = this.domNode;

            // iCheck radio input initialisation
            var allRadioInputs = v_query('input[type=radio]', domNode);

            $(allRadioInputs).iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
                increaseArea: '20%' // optional
            });

            // iCheck checkbox input initialisation
            var allCheckboxInputs = v_query('input[type=checkbox]:not(.onoffswitch-checkbox)', domNode);

            $(allCheckboxInputs).iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
                increaseArea: '20%' // optional
            });

            //find all labels with radio button
            v_query("label").forEach(function (node, index, arr) {
                if (v_domAttr.get(node, "data-checkbox-ie") == null) {
                    v_on(node, "click", function () {
                        //find checkbox or radio button inside label
                        v_query("input[type=radio]", node).forEach(function (node2, index2, arr2) {
                            //reset all radio buttons
                            v_query("input[name=" + node2.name + "]").forEach(function (node3, index3, arr3) {
                                v_domAttr.remove(node3, "checked");
                            });

                            v_domAttr.set(node2, "checked", "checked");
                        });
                    });
                    v_domAttr.set(node, "data-checkbox-ie", "true");
                }
            });

            // Find all inputs that are shortcoded with the custom-input attribute
            v_query("input[custom-input]", domNode).forEach(function (node) {
                v_context.p_structureCustomInputs(node);
            });
        },
        p_structureCustomInputs: function (v_theNode) {
            var v_context = this;

            var v_node = v_theNode;
            v_domAttr.remove(v_node, "custom-input");

            var formGroupWrapper = v_domConstruct.create("div");
            v_domClass.add(formGroupWrapper, "form-group");

            v_domClass.add(v_node, "form-control");

            if (v_domAttr.get(v_node, "with-icon") !== null) {
                v_domClass.add(formGroupWrapper, "has-feedback");
                $(v_node).wrap(formGroupWrapper);

                var spanNode = v_domConstruct.create("span");
                v_domClass.add(spanNode, v_domAttr.get(v_node, "with-icon"));
                v_domClass.add(spanNode, "form-control-feedback");
                $(v_node).after(spanNode);

                v_domAttr.remove(v_node, "with-icon");
            } else {
                $(v_node).wrap(formGroupWrapper);
            }

            // if (domAttr.get(node, "label") !== null) {
            var labelNode = v_domConstruct.create("label");

            if (v_domAttr.get(v_node, "label") !== null) {
                labelNode.innerHTML = v_domAttr.get(v_node, "label");
            }

            if (v_domAttr.get(v_node, "data-validation") !== null) {
                var validationString = v_domAttr.get(v_node, "data-validation");
                var validationList = validationString.split(' ');
                var found = false;
                v_array.forEach(validationList, function (single) {
                    if (single === "required") {
                        found = true;
                    }
                });

                if (found) {
                    v_domClass.add(labelNode, "required");
                }
            }

            $(v_node).before(labelNode);

            v_domAttr.remove(v_node, "label");
            // }   
        },
        p_customInputHTMLSet: function (v_inputNode, v_resourceString) {
            var v_context = this;

            v_html.set($(v_inputNode).prev()[0], v_resourceString);
        }
    });
});
