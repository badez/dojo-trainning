define([
    "dojo/_base/config",
    "dojo/_base/declare"
], function (
    v_config,
    v_declare
) {
    return v_declare("app.components.utils.General", null, {
        createUUID: function () {
            var v_s = [];
            var v_hexDigits = "0123456789abcdef";
            for (var v_i = 0; v_i < 36; v_i++) {
                v_s[v_i] = v_hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
            }
            v_s[14] = "4"; // bits 12-15 of the time_hi_and_version field to 0010
            v_s[19] = v_hexDigits.substr((v_s[19] & 0x3) | 0x8, 1); // bits 6-7 of the clock_seq_hi_and_reserved to 01
            v_s[8] = v_s[13] = v_s[18] = v_s[23] = "-";

            var v_uuid = v_s.join("");
            return v_uuid;
        },
         checkNullAndSetFormat: function(format, data) {
            var context = this;
            var dataFormat = data;

            if (dataFormat !== undefined && dataFormat !== null && dataFormat !== "") {
                switch (format) {
                    case "date":
                        dataFormat = context.formatDate(data, { display: "date" });
                        break;
                    case "amount":
                        dataFormat = context.formatCurrency(data, null);
                        break;
                    case "datetime":
                        dataFormat = context.formatDate(data, { display: "datetime" });
                        break;
                }
            } else {
                switch (format) {
                    case "amount":
                        dataFormat = context.formatCurrency(0, null);
                        break;
                    default:
                        dataFormat = v_config.nullDefaultValue !== undefined && v_config.nullDefaultValue !== null? v_config.nullDefaultValue: "N/A";
                        break;
                }
            }
            return dataFormat;
        },
        formatCurrency: function(number, el) {
            // Defaults
            var settings = {
                delay: 10,
                time: 500
            };

            if (el != undefined && !config.isIE8) {
                var thisEl = $(el);

                function counter() {
                    // Store the object
                    var runningNumber;
                    var counterUpper = function() {
                        var nums = [];
                        var num = parseFloat(number).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                        var divisions = settings.time / settings.delay;
                        var isComma = /[0-9]+,[0-9]+/.test(num);
                        num = num.replace(/,/g, '');
                        var isInt = /^[0-9]+$/.test(num);
                        var isFloat = /^[0-9]+\.[0-9]+$/.test(num);
                        var decimalPlaces = isFloat ? (num.split('.')[1] || []).length : 0;

                        // Generate list of incremental numbers to display
                        for (var i = divisions; i >= 1; i--) {

                            // Preserve as int if input was int
                            var newNum = parseInt(num / divisions * i);

                            // Preserve float if input was float
                            if (isFloat) {
                                newNum = parseFloat(num / divisions * i).toFixed(decimalPlaces);
                            }

                            // Preserve commas if input had commas
                            if (isComma) {
                                while (/(\d+)(\d{3})/.test(newNum.toString())) {
                                    newNum = newNum.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
                                }
                            }

                            nums.unshift(newNum);
                        }

                        thisEl.data('counterup-nums', nums);
                        thisEl.text('0');

                        // Updates the number until we're done
                        var f = function() {
                            var shiftedData = thisEl.data('counterup-nums').shift();
                            if (shiftedData != null) {
                                thisEl.text(shiftedData);
                                if (thisEl.data('counterup-nums').length) {
                                    setTimeout(thisEl.data('counterup-func'), settings.delay);
                                } else {
                                    thisEl.data('counterup-nums', null);
                                    thisEl.data('counterup-func', null);
                                }
                            }
                        };
                        thisEl.data('counterup-func', f);

                        // Start the count up
                        setTimeout(thisEl.data('counterup-func'), settings.delay);
                    };
                    // Perform counts when the element gets into view
                    thisEl.waypoint(function() {
                        counterUpper();
                        this.destroy()
                    }, { triggerOnce: true, offset: '100%' });
                }

                return counter();
            } else {
                if (isNaN(number))
                    number = 0;
                var num = number.toString().replace(" ", "");
                return parseFloat(num).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }
        },
        formatDate: function(dateTimestamp, passedOptions) {
            // options = {
            //     separator: "/", // Any type of string as a separator
            //     display: "datetime", // "date", "time", "datetime"
            //     monthType: "number" // "number", "stringshort", "stringfull"
            // };

            var monthType = "number";

            if (v_config.monthFormat === "MMMMM") {
                monthType = "stringfull";
            } else if (v_config.monthFormat === "MMM") {
                monthType = "stringshort";
            } else if (v_config.monthFormat === "MM") {
                monthType = "numberWithZero";
            }

            var options = {
                separator: v_config.dateSeparator,
                display: "datetime",
                monthType: monthType
            };

            if (passedOptions !== undefined) {
                if (passedOptions instanceof Object) {
                    if (passedOptions.separator) {
                        options.separator = passedOptions.separator;
                    }

                    if (passedOptions.display) {
                        options.display = passedOptions.display;
                    }

                    // TODO: monthType handler (if needed)
                    // if (passedOptions.monthType) {
                    //     options.monthType = passedOptions.monthType;
                    // }
                }
            }

            var date = new Date(dateTimestamp);

            var day = v_config.dayFormat === "dd" ? wrapTwoDigits(date.getDate()) : date.getDate();
            var month;
            var year = date.getFullYear();

            if (v_config.yearFormat === "yy") {
                year = year.toString().substr(2, 2);
            }

            var monthNames = ["January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"
            ];
            var monthShortNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
            ];

            switch (options.monthType) {
                case "numberWithZero":
                    month = wrapTwoDigits(date.getMonth() + 1);
                    break;

                case "number":
                    month = date.getMonth() + 1;
                    break;

                case "stringshort":
                    month = monthShortNames[date.getMonth()];
                    break;

                case "stringfull":
                    month = monthNames[date.getMonth()];
                    break;
            }

            function getTimeString() {
                var hours = date.getHours();
                var hourString;
                var amPm;

                if (hours === 0) {
                    hourString = "12";
                    amPm = "AM";
                } else if (hours < 12) {
                    hourString = wrapTwoDigits(hours);
                    amPm = "AM";
                } else if (hours === 12) {
                    hourString = hours.toString();
                    amPm = "PM";
                } else {
                    hourString = wrapTwoDigits(hours - 12);
                    amPm = "PM";
                }

                return hourString + ":" + wrapTwoDigits(date.getMinutes()) + ":" + wrapTwoDigits(date.getSeconds()) + " " + amPm;
            }

            function wrapTwoDigits(num) {
                if (num < 10) {
                    return "0" + num.toString();
                } else {
                    return num.toString();
                }
            }

            var returnString;

            switch (options.display) {
                case "datetime":
                    returnString = day + options.separator + month + options.separator + year + " " + getTimeString();
                    break;

                case "date":
                    returnString = day + options.separator + month + options.separator + year;
                    break;

                case "time":
                    returnString = getTimeString();
                    break;
            }

            return returnString;
        }
    })
});