define([
    "dojo/_base/array",
    "dojo/_base/config",
    "dojo/_base/lang",
    "dojo/dom",
    "dojo/dom-attr",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/i18n!app/locale/nls/common",
    "dojo/on",
    "dojo/topic",
    "app/components/ui/block-factory",
    "app/components/ui/modal-factory",
    "app/components/utils/Rest"
], function (
    v_array,
    v_config,
    v_lang,
    v_dom,
    v_domAttr,
    v_domClass,
    v_domStyle,
    v_i18nCommon,
    v_on,
    v_topic,
    v_blockFactory,
    v_modalFactory,
    v_Rest
) {
    return {
        p_init: function () {
            var v_openedFunction = "";
            var v_openedFunctionsBlocks = {};
            var v_openedFunctions = {};

            // open new function block or existing function block
            v_topic.subscribe("navigation.block.open", function (v_function, v_setting, v_domNode) {
                if (!v_domNode) {
                    v_domNode = v_dom.byId("contentContainer");
                }
                var v_found = false;
                if ($.jStorage.get("keep.blocks")) {
                    for (var v_openedFunc in v_openedFunctionsBlocks) {
                        // hide all block first
                        v_array.forEach(v_openedFunctionsBlocks[v_openedFunc], function (v_single) {
                            v_domStyle.set(v_single.p_block.domNode, "display", "none");
                        });
                        // if found block to be opened, mark it
                        if (v_openedFunc === v_function) {
                            v_found = true;
                        }
                    }
                } else {
                    v_topic.publish("navigation.block.destroy");
                }

                $(window).scrollTop(0);

                // if found, display the block. if not found, open new block.
                v_openedFunction = v_function;
                if (v_found) {
                    var v_lastElement = v_openedFunctionsBlocks[v_function].pop();
                    v_openedFunctionsBlocks[v_function].push(v_lastElement);
                    v_topic.publish("block.active", v_lastElement.p_block);
                    v_domStyle.set(v_lastElement.p_block.domNode, "display", "block");
                } else {

                    if (v_setting === undefined) {
                        v_setting = {};
                    }
                    v_setting.p_baseFunction = v_function;
                    v_openedFunctions[v_function] = [];
                    v_openedFunctions[v_function].push({ p_function: v_function, p_functionName: v_setting.functionName });
                    v_setting[v_function] = v_openedFunctions[v_function];
                    v_setting.p_mainPage = true;
                    v_blockFactory.create(v_function, v_domNode, v_setting, function (v_newBlock) {
                        if (v_newBlock !== null && v_newBlock !== undefined) {
                            v_openedFunctionsBlocks[v_function] = [];
                            v_openedFunctionsBlocks[v_function].push({ p_block: v_newBlock });
                        }
                    });
                }
            });

            // use for breadcrumb
            // go to previous block in same container
            v_topic.subscribe("navigation.block.previous", function () {
                if (v_openedFunctionsBlocks[v_openedFunction].length > 0) {
                    v_openedFunctions[v_openedFunction].pop();
                    var v_lastElement = v_openedFunctionsBlocks[v_openedFunction].pop();
                    v_lastElement.p_block.destroy();
                    if (v_lastElement.p_innerBlocks) {
                        v_array.forEach(v_lastElement.p_innerBlocks, function (v_single) {
                            v_single.destroy();
                        });
                    }

                    v_lastElement = v_openedFunctionsBlocks[v_openedFunction].pop();
                    v_openedFunctionsBlocks[v_openedFunction].push(v_lastElement);

                    v_domStyle.set(v_lastElement.p_block.domNode, "display", "block");
                    v_topic.publish("block.active", v_lastElement.p_block);
                }
            });

            // use for breadcrumb
            // go to next block in same container
            v_topic.subscribe("navigation.block.next", function (v_function, v_setting, v_domNode) {
                if (!v_domNode) {
                    v_domNode = v_dom.byId("contentContainer");
                }
                var v_lastElement = v_openedFunctionsBlocks[v_openedFunction].pop();
                v_openedFunctionsBlocks[v_openedFunction].push(v_lastElement);
                v_domStyle.set(v_lastElement.p_block.domNode, "display", "none");

                if (v_setting === undefined) {
                    v_setting = {};
                }
                v_setting.p_baseFunction = v_openedFunction;
                v_openedFunctions[v_openedFunction].push({ p_function: v_function, p_functionName: v_setting.functionName });
                v_setting[v_openedFunction] = v_openedFunctions[v_openedFunction];
                v_blockFactory.create(v_function, v_domNode, v_setting, function (v_newBlock) {
                    if (v_newBlock !== null && v_newBlock !== undefined) {
                        if (!v_openedFunctionsBlocks[v_openedFunction]) {
                            v_openedFunctionsBlocks[v_openedFunction] = [];
                        }
                        v_openedFunctionsBlocks[v_openedFunction].push({ p_block: v_newBlock });
                    }
                });
            });

            // use for jumping around in same container
            v_topic.subscribe("navigation.block.jump", function (v_currentFunction, v_jumpFunction, v_setting, v_domNode, v_reset) {
                if (v_currentFunction === v_jumpFunction) {
                    return;
                }

                if (!v_domNode) {
                    v_domNode = v_dom.byId("contentContainer");
                }
                var v_lastElement = v_openedFunctionsBlocks[v_openedFunction].pop();
                v_openedFunctionsBlocks[v_openedFunction].push(v_lastElement);
                if (!v_lastElement.p_innerBlocks) {
                    v_lastElement.p_innerBlocks = [];
                }
                var v_openedInnerPages = v_lastElement.p_innerBlocks;
                var v_found = false;
                var v_pageToBeOpened = {};
                var v_index;

                v_array.forEach(v_openedInnerPages, function (v_single, v_i) {
                    var v_function = v_single.getSetting().p_function;
                    if (v_jumpFunction === v_function) {
                        v_found = true;
                        v_pageToBeOpened = v_single;
                        v_index = v_i;
                    } else if (v_currentFunction === v_function) {
                        v_domStyle.set(v_single.domNode, "display", "none");
                    }
                });

                if (v_reset) {
                    v_found = false;
                    v_pageToBeOpened.destroy();
                    v_openedInnerPages.splice(v_index, 1);
                }

                if (v_found) {
                    v_domStyle.set(v_pageToBeOpened.domNode, "display", "block");
                } else {
                    v_blockFactory.create(v_jumpFunction, v_domNode, v_setting, function (v_newBlock) {
                        if (v_newBlock !== null && v_newBlock !== undefined) {
                            v_openedInnerPages.push(v_newBlock);
                        }
                    });
                }
            });

            v_topic.subscribe("navigation.block.destroy", function (v_function) {
                if (v_function) {
                    var v_deleteBlock = v_openedFunctionsBlocks[v_function];
                    if (v_deleteBlock !== undefined) {
                        v_array.forEach(v_openedFunctionsBlocks[v_function], function (v_single) {
                            if (v_single.p_innerBlocks) {
                                v_array.forEach(v_single.p_innerBlocks, function (v_single) {
                                    v_single.destroy();
                                });
                            }
                            v_single.p_block.destroy();
                        });

                        v_openedFunctionsBlocks[v_function] = [];
                        delete v_openedFunctionsBlocks[v_function];
                        v_openedFunctions[v_function] = [];
                        delete v_openedFunctions[v_function];
                    }
                } else {
                    for (var v_block in v_openedFunctionsBlocks) {
                        v_array.forEach(v_openedFunctionsBlocks[v_block], function (v_single) {
                            if (v_single.p_innerBlocks) {
                                v_array.forEach(v_single.p_innerBlocks, function (v_single) {
                                    v_single.destroy();
                                });
                            }
                            setTimeout(function () {
                                v_single.p_block.destroy();
                            }, 200);
                        });
                    }
                    v_openedFunctionsBlocks = {};
                    v_openedFunctions = {};
                }
            });

            // modal
            var v_openedModals = {};
            v_topic.subscribe("navigation.modal.open", function (v_modalKey, v_setting) {
                v_modalFactory.create(v_modalKey, v_dom.byId("modalContainer"), v_setting, function (v_newModal) {
                    v_openedModals[v_newModal.id] = v_newModal;
                    var v_backdrop = v_newModal.getSetting().p_backdrop;
                    $(v_newModal.domNode).modal({
                        backdrop: v_backdrop ? v_backdrop : true,
                        show: true,
                        keyboard: false
                    });
                    $(v_newModal.domNode).one("hidden.bs.modal", function () {
                        delete v_openedModals[v_newModal.id];
                        v_newModal.destroy();
                    })
                });
            });

            v_topic.subscribe("navigation.modal.destroy", function () {
                for (var v_property in v_openedModals) {
                    $(v_openedModals[v_property].domNode).modal("hide");
                }
            });

            // etc.
            v_topic.subscribe("navigation.logout", function (v_locale) {
                if ($.jStorage.get("app.login") == true) {
                    var v_rest = new v_Rest().p_postapi({
                        p_api: "auth/logout",
                        p_delegate: "logout"
                    }, null, null);
                    v_rest.then(function () {
                        var v_currentLocale = $.jStorage.get("app.data.locale");
                        var v_currentTheme = $.jStorage.get("app.data.theme");

                        $.jStorage.flush();
                        $.jStorage.set("app.login", false);

                        //06/12/16 Faiz: Added this line to set the userLocale.
                        if (v_locale !== undefined) {
                            $.jStorage.set("app.data.locale", v_locale);
                            location.reload(false);
                        } else {
                            $.jStorage.set("app.data.locale", v_currentLocale);
                            $.jStorage.set("app.data.theme", v_currentTheme);
                        }

                        sessionStorage.clear();
                    }).then(function () {
                        v_topic.publish("navigation.logout.success");
                    });
                } else {
                    v_topic.publish("navigation.logout.success");
                }
            });

            var v_appTimeout;
            v_topic.subscribe("navigation.reset.timeout", function () {
                clearTimeout(v_appTimeout);
                v_appTimeout = setTimeout(function () {
                    var v_isLogin = $.jStorage.get("app.login");
                    if (v_isLogin) {
                        v_topic.publish("navigation.modal.open", "ALERT", {
                            p_type: "WARNING",
                            p_title: v_i18nCommon.p_sessionTimeout,
                            p_desc: v_i18nCommon.p_sessionTimeoutDesc,
                            p_callback: function () {
                                v_topic.publish("navigation.logout");
                            }
                        });
                    }
                }, parseInt(v_config.sessionTimeout) * 60 * 1000);
            });

            v_on(window, "scroll, resize, load, click", function () {
                v_topic.publish("navigation.reset.timeout");
            });
        }
    }
});
