require([
    "dojo/_base/config",
    "dojo/_base/window",
    "dojo/dom",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/on",
    "dojo/topic",
    "dojo/query",
    "app/inspinia",
    "app/navigation-center",
    "app/components/ui/element/pageHeader/PageHeader",
    "app/components/ui/element/sideMenu/SideMenu",
    "app/components/ui/element/pageFooter/PageFooter",
    "dojo/domReady!"
], function (
    v_config,
    v_win,
    v_dom,
    v_domClass,
    v_domStyle,
    v_on,
    v_topic,
    v_query,
    v_inspinia,
    v_navigation,
    v_PageHeader,
    v_SideMenu,
    v_PageFooter
) {
    console.log("[EXPERIMENTAL] (╯°Д°）╯︵ /(.□ . \)", v_config.isExperimental);

    // window.onbeforeunload = function() {
    //     return "You will be logged out upon reloading.";
    // };

    v_inspinia.p_init();
    v_navigation.p_init();

    f_init();

    function f_init() {
        f_initContent();
        f_initEvent();
        f_initListener();
    }

    function f_initContent() {
        v_topic.publish("navigation.modal.open", "LOGIN", {});
    }

    function f_initEvent() {

    }

    function f_initListener() {
        var v_pageHeader = {};
        var v_multiTaskTab = {};
        var v_sideMenu = {};
        var v_pageFooter = {};
        v_topic.subscribe("navigation.login.success", function () {
            // what happen after login success
            v_domClass.toggle(v_dom.byId("page-wrapper"), "gray-bg");
            v_pageHeader = new v_PageHeader().placeAt(v_dom.byId("headerContainer"), 0);
            v_pageHeader.p_construct({});
            v_sideMenu = new v_SideMenu().placeAt(v_dom.byId("wrapper"), 0);
            v_sideMenu.p_construct({});
            v_inspinia.p_sideMenuInit();
            v_pageFooter = new v_PageFooter().placeAt(v_dom.byId("footerContainer"), 0);
        });

        v_topic.subscribe("navigation.logout.success", function () {
            // what happen after logout success
            v_topic.publish("navigation.modal.destroy");
            v_topic.publish("navigation.block.destroy");
            v_pageHeader.destroy();
            v_sideMenu.destroy();
            v_domClass.toggle(v_dom.byId("page-wrapper"), "gray-bg");
            v_topic.publish("navigation.modal.open", "LOGIN");
        });

        v_topic.subscribe("block.active", function (v_block) {
            // set current function code and module code
            $.jStorage.set("current.functionCode", v_block.getSetting().functionCode);
            $.jStorage.set("current.moduleCode", v_block.getSetting().moduleCode);
        });

        v_topic.subscribe("block.loaded", function (v_block) {
            // what happen after block is loaded
            v_inspinia.p_iboxInit(v_block.domNode);
            v_query("input, textarea", v_block.domNode).forEach(function (v_single) {
                $(v_single).placeholder();
            });
        });

        v_topic.subscribe("modal.loaded", function (v_modal) {
            // what happen after modal is loaded
            v_inspinia.p_iboxInit(v_modal.domNode);
            v_query("input, textarea", v_modal.domNode).forEach(function (v_single) {
                $(v_single).placeholder();
            });
        });
    }
});
