require([
    "dojo/_base/array",
    "dojo/topic",
    "dojo/domReady!"
], function (
    v_array,
    v_topic
) {
    var v_masterTopicHandlers = {};

    f_startListening();
    console.log("[Topic Center Running]");

    function f_startListening() {
        v_topic.subscribe("topic.handlers.add", function (v_topichandler, v_identifier) {
            if (typeof v_masterTopicHandlers[v_identifier] === "undefined") {
                var v_topicHandlerArray = [];
                v_topicHandlerArray.push(v_topichandler);

                v_masterTopicHandlers[v_identifier] = v_topicHandlerArray;
                return;
            }

            var v_existingTopicHandlerArray = v_masterTopicHandlers[v_identifier];
            v_existingTopicHandlerArray.push(v_topichandler);

            v_masterTopicHandlers[v_identifier] = v_existingTopicHandlerArray;
        });

        v_topic.subscribe("topic.handlers.destroy", function (v_identifier) {
            if (typeof v_masterTopicHandlers[v_identifier] !== "undefined") {
                var v_topicHandlerArray = v_masterTopicHandlers[v_identifier];
                v_array.forEach(v_topicHandlerArray, function (v_singleTopic) {
                    try {
                        if (v_singleTopic != null) {
                            v_singleTopic.remove();
                        }
                    } catch (v_err) {
                        console.warn("[Topic Center GB]", v_err);
                    }
                });

                delete v_masterTopicHandlers[v_identifier];
            }
        });
    }
});
