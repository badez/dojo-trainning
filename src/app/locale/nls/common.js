define({
    root: ({
        //Generic Label and Button 
        from: "From",
        to: "To",
        save: "Save",
        search: "Search",
        find: "Find",
        cancel: "Cancel",
        reset: "Reset",

        //start home label
        navbarwelcomemsg: "Hello, welcome to DBranch Admin System",
        welcome: "Welcome, ",
        loancalculator: "Loan Calculator",
        branchlocator: "Branch Locator",
        changeusergroup: "Change User Group",
        changepassword: "Change Password",
        logout: "Logout",
        upload: "Upload",
        quicksearch: "Quick Search",
        notification: "See All",
        gotnewmessage: "You got new message",
        taskempty: "Task empty...",
        nonotification: "No Notification",
        copyrightlabel: "Copyright @",
        copyrightdesc: "2016 Infopro Sdn Bhd. All rights reserved",
        //end home label

        //start button label
        done: "Done",
        ok: "Ok",
        yes: "Yes",
        no: "No",
        proceed: "Proceed",
        filter: "Filter",
        download: "Download",
        back: "Back",
        reprint: "Reprint",
        preview: "Preview",
        print: "Print",
        searchinput: "Search Input",
        scan: "Scan",
        apply: "Apply",
        _continue: "Continue",
        //end button label

        //start General Label
        remarks: "Remarks",
        description: "Description",
        branchcode: "Branch Code",
        branchname: "Branch Name",
        createdby: "Created By",
        createdon: "Created On",
        updatedby: "Updated By",
        updatedon: "Updated On",
        lastupdatedby: "Last Updated By",
        lastupdatedon: "Last Updated On",
        //end General Label

        //start modal label
        success: "Success",
        successdesc: "Transaction Completed",
        warning: "Warning",
        surewanttoproceed: "Are You Sure You Want To Proceed?",
        error: "Error",
        errordesc: "Transaction Cannot Be Processed",
        sessiontimeout: "Session Expired",
        sessiontimeoutdesc: "You Have Been Idle For a Long Time, Please Re Login To Continue",
        ie8message: "Internet Explorer 8",
        ie8desc: "For Better Experience, Please Use The Latest Version Of Internet Explorer Or Other Modern Browser",
        confirmation: "Confirmation",
        //end modal label

        //start input error label
        pleaseselectdate: "Please Select Date",
        notavailable: "Not Available",
        incorrectinputcolon: "Incorrect Input",
        inputrequired: "Field Is Required",
        inputtel: "Please Enter Valid Phone Number",
        inputname: "This Name Is Invalid",
        inputnumber: "Please Enter Valid Number Only",
        inputpositivenumber: "Please Enter Positive Number Only",
        inputlimit: "Exceeding Max Limit",
        inputinsufficientbalance: "Insufficient Balance",
        inputselectaccount: "Please Select Your Account",
        inputlength: "The Input Length Should Be ",
        inputlengthmin: "The Input Length Should Be more than ",
        inputlengthmax: "The Input Length Should Be less than ",
        inputalphanumeric: "The Input Should Be Alphanumeric",
        passwordcriteria: "Must Contain Uppercase, Lowercase, Numeric And Special Characters",
        inputminimum: "Please Enter Number Greater Than ",
        inputmaximum: "Please Enter Number Lesser Than ",
        inputminimumequal: "Please Enter Number Equal or Greater Than ",
        inputmaximumequal: "Please Enter Number Equal or Lesser Than ",
        inputpassword: "The Password Does Not Match",
        inputsamepassword: "The Current And New Password Are Same",
        inputemail: "This Email Is Invalid",
        inputemoticon: "Emoticon Is not allowed",
        inputamount: "Please Enter Valid Amount Only",
        inputinvalidaccountnumber: "Invalid Account Number",
        currentandnewpasswordsame: "Current Password And New Password Should Not Be Equal",
        inputminimumamount: "Amount Should Be More Than ",
        //end input error label

        //start input label
        pleaseselectone: "Please Select One",
        //end input label

        //start no result page
        noResultTitle: "Select An Option To Get Started",
        noResultDesc: "Select an option from the above panel to proceed further",
        //end no result page

        //start biometric
        customerIdNoLabel: "Customer ID No",
        alternativeIdNoLabel: "Alternative ID No",
        fullNameLabel: "Full Name",
        genderLabel: "Gender",
        dobLabel: "Date Of Birth",
        religionLabel: "Religion",
        postCodeLabel: "Post Code",
        cityLabel: "City",
        stateCodeLabel: "State Code",
        countryLabel: "Country",
        cardUnreadable: "Card Unreadable Or Device Unavailable.",
        thumbPrintNotVerify: "Can Not Verify The Thumb Print.",
        thumbPrintVerify: "Thumb Print Verified.",
        jniServerNotUp: "Can Not Connect To JniServer Please Make Sure The JniServer Was Up And Running.",
        //end biometric

        //Transaction Related Bundle
        quantity: "Quantity",
        cash: "Cash",
        cheque: "Cheque",
        transfer: "Transfer",
        journal: "Journal",
        receipt: "Receipt",
        payment: "Payment",
        product: "Product",
        currency: "Currency",
        amount: "Amount",
        chequeno: "Cheque No.",
        chequetype: "Cheque Type",
        floatingday: "Floating Day(s)",
        accountno: "Account No.",
        customerid: "Customer Id",
        customername: "Customer Name",
        glcode: "GL Code",
        balance: "Available Balance",
        chargestax: "Charges And Taxes",
        chargestaxcategory: "Charges / Tax Category",
        ratetype: "Rate Type",
        quantity: "Quantity",
        totalamount: "Total Amount",
        notapplicable: "N/A",
        //End Transaction Related Bundle

        //start filter
        all: "All",
        //end filter

        //product related bundle
        caTitleLabel: "Current Account",
        saTitleLabel: "Savings Account",
        fdTitleLabel: "Fixed Deposit",
        loanTitleLabel: "Loans",
        odTitleLabel: "Overdraft",
        //end product realated bundle

        successfullyProcessed: "Successfully processed",
        somethingWentWrong: "Something went wrong! Please try again",
        proceedConfirmation: "Are you sure you want to proceed?",
        modalFail: "FAILED",
        modalSuccess: "SUCCESS",
        modalWarning: "WARNING",

        submit: "Submit",
        clearAll: "Clear All",
        filterBy: "Filter By",

        readAllMessages: "Read All Messages",
        searchToGetStarted: "Start Search To Get Started",
        searchToProceedFurther: "Start search by subject to proceed further",
        subject: "Subject",
        viewMessage: "View Message",
        inbox: "Inbox",
        selectToRemove: "Please select inbox message(s) you want to remove!",

        //Numeral
        thousand: " Thousand",
        million: " Million",
        billion: " Billion",
        trillion: " Trillion"

    }),
    "zh_CN": true
});
