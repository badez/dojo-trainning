define({
    root: ({
        somethingwentwrong: "Oops... Something went wrong. Please try again later.",
        // Unauthorized Error
        needtorelogin: "Your current session has ended, please re-login to continue",
        // Login Error
        wrongusernamepassword: "Your username and password seems not right. Please enter valid username and password",

        //new login error messages
        err0101012: "User ID And Password Do Not Match",
        err0101007: "The number of System Login Attempts Has Been Exceeded. Account Has Been Suspended",
        err0101115: "User ID Expired. Contact administrator to reset the status.",
        err0101003: "User ID Suspended. Contact administrator to reset status.",
        err0101006: "Inactive User ID. Contact administrator to reset status.",
        err0101061: "User ID appears as off-duty. Contact administrator to reset the leave plan.",
        err0101163: "Login denied! Check your login details.",
        err0101173: "Login denied. Pending for Overriding."
    })
});