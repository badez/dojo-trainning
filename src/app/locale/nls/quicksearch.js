define({
    root: ({
        searchbtn: "Search",
        allnoresult: "There Is No Result Matching The Criteria",
		detailnoresult: "There Is No Application Under This Customer",
		namelabel: "Customer Name",
		idnolabel: "Customer ID",
		birthdatelabel: "Birthdate / Established",
		phonelabel: "Phone No.",
		emaillabel: "Email Address",
		addresslabel: "Address",
		canolabel: "CA No.",
		statuslabel: "Status",
		entitynamelabel: "Entity Name",
		accofficerlabel: "Account Officer",
		parkbranchlabel: "Park Branch",
		applicationdatelabel: "Application Date",
		detailtabletitle: "Application Listing",
		noresultdesc: "Please Enter Customer Name Or Customer ID."
    })
});