define({
	root: ({
		testTitleLabel: "Test Campaign",
		testExpiryLabel: "Expired in 30days",
		testDescLabel: "This is the description for this campaign",
		readMoreLabel: "Read More",
		viewMoreLabel: "View More",
		applyLabel: "Apply"
	})
});