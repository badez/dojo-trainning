define({
    root: ({
        loginbtn: "Login",
        forgotpasswordlabel: "Forgot password?",
        copyrightlabel: "Infopro Sdn. Bhd. &copy; 2016" ,
        username:"Username",
        password:"Password",
        logout:"Logout"
    }),
    "zh_CN": true
});
