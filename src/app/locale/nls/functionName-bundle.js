 define({
     root: ({
         //Menu Code
         UITOOLKIT: "UI Toolkit",
         TEST: "Test Pages",
         LAYOUT_GROUP: "Layouts",

         //Function Code
         COMPONENTS: "Components",
         ICOMOON: "Icomoon",
         SAMPLE_PAGE_1: "Sample Page 1",
         SAMPLE_PAGE_2: "Sample Page 2",
         EXTRA_PAGE_1: "Extra Page 1",
         SAMPLE_JUMP: "Sample Jump",
         JUMP_PAGE_1: "Page 1",
         JUMP_PAGE_2: "Page 2",
         JUMP_PAGE_3: "Page 3",
         LAYOUT_1: "Layout 1 - Three Columns",
         LAYOUT_2: "Layout 2 - Two Columns",
         LAYOUT_3: "Layout 3 - Two Columns",
         LAYOUT_4: "Layout 4 - Four Columns",
         LAYOUT_5: "Layout 5 - One Small Column",
         FORM_EXAMPLE: "Form Example",
         FORM_EXAMPLE_2: "Form Example 2",
         CLOUD_STORAGE: "Cloud Storage",
         EXERCISE_PAGE:"Exercise Page"
     }),
     "zh_CN": false
 });
