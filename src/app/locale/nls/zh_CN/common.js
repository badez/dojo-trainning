define({
    //Generic Label and Button 
    from: "从",
    to: "至",
    save: "保存",
    search: "搜索",
    find: "拾物",
    cancel: "撤消",
    reset: "重启",

    //Numeral
    thousand: "千",
    million: "百万",
    billion: "十亿",
    trillion: "兆"
});
