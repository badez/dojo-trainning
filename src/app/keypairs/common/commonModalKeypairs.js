define([
    "app/components/ui/modal/common/alert/AlertModal",
    "app/components/ui/modal/common/login/LoginModal",
    "app/components/ui/modal/common/successfail/SuccessFailModal",
    "app/components/ui/modal/common/search/SearchModal",
    "app/components/ui/modal/inboxnotification/InboxNotificationDetail"
], function(
    v_Alert,
    v_Login,
    v_SuccessFail,
    v_Search,
    v_InboxNotificationDetail
) {
    var v_keypairs = {};

    v_keypairs.get = function(v_key, v_callback) {
        var v_Modal = null;
        var v_options = null;

        switch (v_key) {
            case "ALERT":
                v_Modal = v_Alert;
                break;
            case "LOGIN":
                v_Modal = v_Login;
                break;
            case "SUCCESS_FAIL":
                v_Modal = v_SuccessFail;
                break;
            case "SEARCH":
                v_Modal = v_Search;
                break;
            case "INBOX_NOTIFICATION_DETAIL":
                v_Modal = v_InboxNotificationDetail;
                break;
        }

        v_callback(v_Modal, v_options);
    };

    return v_keypairs;
});
