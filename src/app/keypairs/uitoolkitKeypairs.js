define([
    "app/components/ui/blocks/uitoolkit/components/Components",
    "app/components/ui/blocks/uitoolkit/icomoon/Icomoon",
    "app/components/ui/blocks/uitoolkit/layouts/layout1/Layout1",
    "app/components/ui/blocks/uitoolkit/layouts/layout2/Layout2",
    "app/components/ui/blocks/uitoolkit/layouts/layout3/Layout3",
    "app/components/ui/blocks/uitoolkit/layouts/layout4/Layout4",
    "app/components/ui/blocks/uitoolkit/layouts/layout5/Layout5",
    "app/components/ui/blocks/uitoolkit/formexample/FormExample",
    "app/components/ui/blocks/uitoolkit/formexample/FormExample2"
], function(
    v_Components,
    v_Icomoon,
    v_Layout1,
    v_Layout2,
    v_Layout3,
    v_Layout4,
    v_Layout5,
    v_FormExample,
    v_FormExample2
) {
    var v_keypairs = {};

    v_keypairs.get = function(v_key, v_callback) {
        var v_Block = null;
        var v_options = null;

        switch (v_key) {
            case "COMPONENTS":
                v_Block = v_Components;
                break;
            case "ICOMOON":
                v_Block = v_Icomoon;
                break;
            case "LAYOUT_1":
                v_Block = v_Layout1;
                break;
            case "LAYOUT_2":
                v_Block = v_Layout2;
                break;
            case "LAYOUT_3":
                v_Block = v_Layout3;
                break;
            case "LAYOUT_4":
                v_Block = v_Layout4;
                break;
            case "LAYOUT_5":
                v_Block = v_Layout5;
                break;
            case "FORM_EXAMPLE":
                v_Block = v_FormExample;
                break;
            case "FORM_EXAMPLE_2":
                v_Block = v_FormExample2;
                break;
        }

        v_callback(v_Block, v_options);
    };

    return v_keypairs;
});
