define([
    "app/components/ui/blocks/inboxnotification/InboxNotificationAll"
], function(
    InboxNotificationAll
) {
    var keypairs = {};

    keypairs.get = function(key, callback) {
        var Block = null;
        var options = null;

        switch (key) {
            case "INBOX_NOTIFICATION_ALL":
                Block = InboxNotificationAll;
                break;
        }

        callback(Block, options);
    };

    return keypairs;
});
