define([
    "app/components/ui/modal/test/exercisepagemodal/ExercisePageModal1",
    "app/components/ui/modal/test/exercisepagemodal/ExercisePageModal2"
], function(
    v_ExercisePageModal1,
    v_ExercisePageModal2
) {
    var v_keypairs = {};

    v_keypairs.get = function(v_key, v_callback) {
        var v_Block = null;
        var v_options = null;

        switch (v_key) {
            case "EXERCISE_PAGE_MODAL_1":
                v_Block = v_ExercisePageModal1;
                break;
            case "EXERCISE_PAGE_MODAL_2":
                v_Block = v_ExercisePageModal2;
                break;
        }

        v_callback(v_Block, v_options);
    };

    return v_keypairs;
});
