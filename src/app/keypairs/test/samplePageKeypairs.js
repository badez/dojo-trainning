define([
    "app/components/ui/blocks/test/samplepage/SamplePage1",
    "app/components/ui/blocks/test/samplepage/SamplePage2"
], function(
    v_SamplePage1,
    v_SamplePage2
) {
    var v_keypairs = {};

    v_keypairs.get = function(v_key, v_callback) {
        var v_Block = null;
        var v_options = null;

        switch (v_key) {
            case "SAMPLE_PAGE_1":
                v_Block = v_SamplePage1;
                break;
            case "SAMPLE_PAGE_2":
                v_Block = v_SamplePage2;
                break;
        }

        v_callback(v_Block, v_options);
    };

    return v_keypairs;
});
