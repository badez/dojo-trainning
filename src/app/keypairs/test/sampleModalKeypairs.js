define([
    "app/components/ui/modal/test/samplemodal/SampleModal1",
    "app/components/ui/modal/test/samplemodal/SampleModal2"
], function(
    v_SampleModal1,
    v_SampleModal2
) {
    var v_keypairs = {};

    v_keypairs.get = function(v_key, v_callback) {
        var v_Modal = null;
        var v_options = null;

        switch (v_key) {
            case "SAMPLE_MODAL_1":
                v_Modal = v_SampleModal1;
                break;
            case "SAMPLE_MODAL_2":
                v_Modal = v_SampleModal2;
                break;
        }

        v_callback(v_Modal, v_options);
    };

    return v_keypairs;
});
