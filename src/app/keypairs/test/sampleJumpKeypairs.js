define([
    "app/components/ui/blocks/test/samplejump/SampleJump",
    "app/components/ui/blocks/test/samplejump/JumpPage1",
    "app/components/ui/blocks/test/samplejump/JumpPage2",
    "app/components/ui/blocks/test/samplejump/JumpPage3"
], function(
    v_SampleJump,
    v_JumpPage1,
    v_JumpPage2,
    v_JumpPage3
) {
    var v_keypairs = {};

    v_keypairs.get = function(v_key, v_callback) {
        var v_Block = null;
        var v_options = null;

        switch (v_key) {
            case "SAMPLE_JUMP":
                v_Block = v_SampleJump;
                break;
            case "JUMP_PAGE_1":
                v_Block = v_JumpPage1;
                break;
            case "JUMP_PAGE_2":
                v_Block = v_JumpPage2;
                break;
            case "JUMP_PAGE_3":
                v_Block = v_JumpPage3;
                break;
        }

        v_callback(v_Block, v_options);
    };

    return v_keypairs;
});
