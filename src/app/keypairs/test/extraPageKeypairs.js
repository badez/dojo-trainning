define([
    "app/components/ui/blocks/test/extrapage/ExtraPage1"
], function(
    v_ExtraPage1
) {
    var v_keypairs = {};

    v_keypairs.get = function(v_key, v_callback) {
        var v_Block = null;
        var v_options = null;

        switch (v_key) {
            case "EXTRA_PAGE_1":
                v_Block = v_ExtraPage1;
                break;
        }

        v_callback(v_Block, v_options);
    };

    return v_keypairs;
});
