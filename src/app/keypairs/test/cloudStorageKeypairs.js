define([
    "app/components/ui/blocks/test/cloudstorage/CloudStorage"
], function(
    v_CloudStorage
) {
    var v_keypairs = {};

    v_keypairs.get = function(v_key, v_callback) {
        var v_Block = null;
        var v_options = null;

        switch (v_key) {
            case "CLOUD_STORAGE":
                v_Block = v_CloudStorage;
                break;
        }

        v_callback(v_Block, v_options);
    };

    return v_keypairs;
});
