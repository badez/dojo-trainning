define([
    "app/components/ui/blocks/test/exercisepage/ExercisePage",
    "app/components/ui/blocks/test/exercisepage/ExerciseConfirmationPage"
], function(
    v_ExercisePage,
    v_ExerciseConfirmationPage
) {
    var v_keypairs = {};

    v_keypairs.get = function(v_key, v_callback) {
        var v_Block = null;
        var v_options = null;

        switch (v_key) {
            case "EXERCISE_PAGE":
                v_Block = v_ExercisePage;
                break;
            case "EXERCISE_CONFIRMATION_PAGE":
                v_Block = v_ExerciseConfirmationPage;
                break;
        }

        v_callback(v_Block, v_options);
    };

    return v_keypairs;
});
