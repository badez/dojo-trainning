# Getting Start with Web/Mobile Framework

## Environment
### Mobile and Web
* [Git](http://git-scm.com/downloads/)
* [NodeJS](https://nodejs.org/)
* Bower
* [Local server (wamp)](http://www.wampserver.com/en/)
* IDE
*  [Ruby for Sass](http://sass-lang.com/install)
### Mobile
* Cordova
* Android/iOS/BB/Windows SDK
* Apache ANT 

### Notes
For details please read this file 
"M:\EBANK-DOC\Internal Training\HTML5\HTML5 Installation v0.1.pptx"


## Clone Repository
```
TBD
```

## Install Packages
```
$ git config --global url."https://".insteadOf git://
$ npm install
```

## Gulp Usage
```
$ gulp web-install // Copy necessary file from npm module folder into src folder
$ gulp web-release // Copy necessary file from src folder to release folder. Then, auto.ly execute dojo build
```
